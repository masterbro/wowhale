(function($){

	$(function(){

		$('#show-term').click(function(){
			$('#modal-term').modal('show');
		});

		$('#send-captcha').click(sendSms);
		$('#step1-submit').click(step1Sub);
		$('#step2-submit').click(step2Sub);
		$('#step3-submit').click(step3Sub);


		var uploadBtn = $('#ww-add-img'), previewContainer = $('#ww-preview');
		uploadBtn.click(function(){
			$("#fileupload_input").trigger('click')
		});


		$("#fileupload_input").fileupload({
			url: BASE_URL + "/upload",//文件上传地址，当然也可以直接写在input的data-url属性内
			send: function() {
				uploadBtn.button('loading');
			},
			done:function(e,data){
				var result = JSON.parse(data.result);
				if(result.error) {
					alert(result.error);
				} else {
					var html = '';
					html += '<img src="'+ BASE_URL + '/assets/upload/' +  result.img  +'" />';
					previewContainer.append(html);
					uploadBtn.next('input').val(result.img);
				}
			},
			always: function (e, data) {
				uploadBtn.button('reset');
			}
		});
	});


	//step1
	var step1Sub = function() {
		var btn = $('#step1-submit');

		if(btn.attr('data-sending') == 'true')
			return false;

		var mobile = $('input[name="mobile"]').val();

		if(!mobile) {
			alert('请输入你的手机号');
			return false;
		}

		if(!/^1\d{10}$/.test(mobile)) {
			alert('请输入正确的手机号');
			return false;
		}

		var captcha = $('input[name="captcha"]').val();

		if(!captcha) {
			alert('请输入验证码');
			return false;
		}

		var password = $('input[name="password"]').val();

		if(!password) {
			alert('请输入密码');
			return false;
		}

		if(password.length > 30 || password < 6) {
			alert('密码必须为6-30位');
			return false;
		}

		if(password != $('input[name="password_confirm"]').val()) {
			alert('两次密码输入不一至');
			return false;
		}

		sending = true;
		btn.html('发送中...').attr('data-sending', 'true');
		$.ajax({
			url:'/joinUs/step1',
			type:'post',
			dataType:'json',
			data:{mobile:mobile, captcha:captcha, password:password},
			success: function(data) {
				if(data.error) {
					alert(data.msg);
					btn.html('提交注册').attr('data-sending', 'false');
				} else {
					$('#join-step-content-1').addClass('hidden');
					$('#join-step-content-2').removeClass('hidden');
					$('.join-step .active').removeClass('active');
					$('.join-step .col-md-4').eq(1).addClass('active');
					alert(data.msg);
				}
			},
			error: function(){
				btn.html('提交注册').attr('data-sending', 'false');
			}
		})
	};

	//step2
	var step2Sub = function() {
		var btn = $('#step2-submit');
		btn.button('loading');

		$.ajax({
			url:'/joinUs/step2',
			type:'post',
			dataType:'json',
			data:{agree:1},
			success: function(data) {
				btn.button('reset');


				if(!data.error) {
					$('#join-step-content-2').addClass('hidden');
					$('#join-step-content-3').removeClass('hidden');

					$('.join-step .active').removeClass('active');
					$('.join-step .col-md-4').eq(2).addClass('active');
				} else {
					alert(data.msg);
				}
			},
			error: function(){
				btn.button('reset');
			}
		})
	};

	//step3
	var step3Sub = function() {

		var name = $('input[name="name"]').val();

		if(!name) {
			alert('请输入商户名称');
			return false;
		}

		var email = $('input[name="email"]').val();
		if(!email) {
			alert('请输入邮箱');
			return false;
		}
		var emailReg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
		if(!emailReg.test(email)) {
			alert('请输入有效的邮箱');
			return false;
		}

		var contacts = $('input[name="contacts"]').val();
		if(!contacts) {
			alert('请输入联系人');
			return false;
		}

		var license = $('input[name="license"]').val();
		if(!license) {
			alert('请上传营业执照');
			return false;
		}

		var description = $('textarea[name="description"]').val();
		if(!description) {
			alert('请填写你的业务描述');
			return false;
		}

		var btn = $('#step3-submit');
		btn.button('loading');

		$.ajax({
			url:'/joinUs/step3',
			type:'post',
			dataType:'json',
			data:{
				name:name, email:email, contacts:contacts, license:license,description:description,
				website:$('input[name="website"]').val(),
				wechat_id:$('input[name="wechat_id"]').val()
			},
			success: function(data) {
				btn.button('reset');
				alert(data.msg);

				if(!data.error) {
					window.location.href = BASE_URL + '/joinUs/status';
				}
			},
			error: function(){
				btn.button('reset');
			}
		})
	};


	var bindSmsCountDownInterval = null, sending = false;

	var sendSms = function() {
		var btn = $('#send-captcha');

		if(btn.attr('data-sending') == 'true')
			return false;

		var mobile = $('input[name="mobile"]').val();

		if(!mobile) {
			alert('请输入你的手机号');
			return false;
		}

		if(!/^1\d{10}$/.test(mobile)) {
			alert('请输入正确的手机号');
			return false;
		}

		sending = true;
		btn.html('发送中...').attr('data-sending', 'true');
		$.ajax({
			url:'/joinUs/captcha',
			type:'post',
			dataType:'json',
			data:{mobile:mobile},
			success: function(data) {
				if(data.error) {
					alert(data.msg);
					btn.html('发送验证码').attr('data-sending', 'false');
				} else {
					bindSmsCountDown();
				}
			},
			error: function(){
				sending = false;
				btn.html('发送验证码').attr('data-sending', 'false');
			}
		})
	}

	var bindSmsCountDown = function() {
		$('#send-captcha').html('<em>60</em> 秒后重新发送');

		bindSmsCountDownInterval = setInterval(function(){
			var btn = $('#send-captcha');
			var i = parseInt(btn.find('em').html());
			i--;
			if(i>0) {
				btn.html('<em>'+ i +'</em> 秒后重新发送');
			} else {
				sending = false;
				btn.html('发送验证码').attr('data-sending', 'false');
				clearInterval(bindSmsCountDownInterval);
			}
		}, 1000);
	};

}(jQuery));