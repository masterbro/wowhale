(function($){
	$.fn.datepicker.dates['en'] = {
		days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
		daysShort: ["日", "一", "二", "三", "四", "五", "六"],
		daysMin: ["日", "一", "二", "三", "四", "五", "六"],
		months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		today: "今天",
		clear: "取消"
	};

	$(function(){
		$('#hr-search-faq').click(function(){
			var form = $('#hr-search-faq-form');
			if(!form.find('input').val()) {
				return false;
			} else {
				form[0].submit();
			}
		})

		$('.show-tooltip').tooltip()

		$('.hr-show-login').click(function(){
			$('#login-tab-content').hide();
			$('#login-content').show();
		});

		$('#hr-show-reg').click(function(){
			$('#login-content').hide();
			$('#login-tab-content').show();
		});


		var loginFormWrap = $('#login-from');

		loginFormWrap.find('input[name="captcha"]').blur(function(){
			if($(this).parent().parent().find('img').attr('src') != '')
				return true;

			loginFormWrap.find('img').attr('src', '');
			$(this).parent().parent().find('img').attr('src', BASE_URL+'/captcha?r='+Math.random())
		});

		loginFormWrap.find('.hr-home-submit').click(function(){
			var btn = $(this);
			var form = $(this).parent();

			var mobile = form.find('input[name="mobile"]');
			if(!mobile.val()) {
				alert(mobile.attr('placeholder') + '不能为空！');
				return false;
			}

			if(!/^1\d{10}$/.test(mobile.val())){
				alert(mobile.attr('placeholder') + '格式不正确。');
				return false;
			}

			var password = form.find('input[name="password"]');
			if(password.val().length < 6) {
				alert('密码不能小于6位！');
				return false;
			}

			var captcha = form.find('input[name="captcha"]');
			if(captcha.val().length != 4) {
				alert('验证码错误！');
				return false;
			}

			if(btn.attr('data-type') == 'reg-customer' || btn.attr('data-type') == 'reg-hr') {
				if(!form.find('input[name="term"]').prop('checked')) {
					alert('必须接受用户服务协议');
					return false;
				}

				reg(btn, form.serialize());
			} else {
				login(btn, form.serialize());
			}
		});


		$('form.validate').each(function(){
			$(this).validate({
				errorElement : 'span',
				errorClass : 'help-block',
				highlight : function(element) {
					$(element).closest('.form-group').addClass('has-error');
				},

				success : function(label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement : function(error, element) {
					if (element.is(':radio') || element.is(':checkbox')) { //如果是radio或checkbox
						var eid = element.attr('name'); //获取元素的name属性
						element.parent().parent('div').append(error);
					} else {
						element.parent('div').append(error);
					}

				}
//			errorPlacement:function(error,element) {
//				element.next('p').html(error)
//			}
			});
		});

		$('.hrm-province-select').each(function(){
			showCities($(this));
		});

		$('.hrm-province-select').change(function(){
			showCities($(this));
		});


		$('.input-datepicker').datepicker({
			format: 'yyyy-mm-dd',
			autoclose:true
		});


		$('select[name="trade_fid"]').change(function(){
			var arr = $.parseJSON($(this).find("option:selected").attr('data-son'));

			var html = '';
			$.each(arr, function(k,v){
				html += '<option value="'+ v.id +'">'+ v.name +'</option>';
			});

			$('select[name="trade_id"]').html(html);
		});

		$('.hrm-job-request').click(function(){
			var btn = $(this);

			if(btn.hasClass('btn-success'))
				return false;

			btn.button('loading');
			$.ajax({
				url: BASE_URL + '/user/jobRequest',
				type:'post',
				data: {job_id:btn.attr('data-job-id')},
				dataType: 'json',
				success: function(data) {
					btn.button('reset');
					alert(data.msg);

					if(!data.error) {
						btn.addClass('btn-success');
						setTimeout(function(){
							btn.html('申请成功')
						}, 300);
					}
				},
				complete: function() {

				}
			});
		})
	});


	var login = function(btn, postData) {
		btn.button('loading');
		$.ajax({
			url: BASE_URL + '/session/login',
			type:'post',
			data: postData,
			dataType: 'json',
			success: function(data) {
				if(data.error) {
					alert(data.msg);
				} else {
					alert('登录成功，欢迎回来');
					window.location.href = BASE_URL + '/user';
					//window.location.href = '<?php echo $this->createUrl('/api/mail') ?>';
				}
			},
			complete: function() {
				btn.button('reset');
			}
		});
	};

	var reg = function(btn, postData) {
		btn.button('loading');
		$.ajax({
			url: BASE_URL + '/session/reg',
			type:'post',
			data: postData,
			dataType: 'json',
			success: function(data) {
				if(data.error) {
					alert(data.msg);
				} else {
					alert('注册成功。');
					window.location.href = BASE_URL + '/user';
				}
			},
			complete: function() {
				btn.button('reset');
			}
		})
	};


	var showCities = function(select) {
		var cities = $.parseJSON(select.find('option:selected').attr('data-cities'));
		var html = '';
		$.each(cities, function(k, v){
			html += '<option value="'+ v.id +'">'+ v.name +'</option>';
		});

		$('#'+select.attr('data-target')).html(html);
	};
}(jQuery));