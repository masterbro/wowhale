﻿(function($){

	$(function(){
		$('.ww-login-sub').click(function(){
			var btn = $(this);
			var _formObj = btn.parent().parent();

			var mobile = $('input[name="mobile"]',_formObj).val();

			if(!mobile) {
				alert('请输入你的手机号');
				return false;
			}

//			if(!/^1\d{10}$/.test(mobile)) {
//				alert('请输入正确的手机号');
//				return false;
//			}

			var password = $('input[name="password"]',_formObj).val();

			if(!password) {
				alert('请输入密码');
				return false;
			}

			btn.button('loading');

			$.ajax({
				url:'/session/login',
				type:'post',
				dataType:'json',
				data:{username:mobile, password:password},
				success: function(data) {
					btn.button('reset');
					alert(data.msg);

					if(!data.error) {
						window.location.href = data.redirect;
					}
				},
				error: function(){
					btn.button('reset');
				}
			})
		})
	});

}(jQuery));


