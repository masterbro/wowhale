(function($){

	$(function(){
		$("#owl-merchant").owlCarousel({
			autoPlay : 5000,
			lazyLoad:true,
			lazyFollow:false,
			pagination:true,
			afterInit: function(){
				$("#owl-merchant").find('.owl-item.loading').css('min-height', window.owlH);
			}
		});

		$('#navbar-toggle').click(function(){
			$(this).toggleClass('in');
			$('#header-nav').toggleClass('in');
		});


		$('#product-list .item h4').click(function(){
			var parent = $(this).parent();
			if(parent.hasClass('on')) {
				parent.removeClass('on');
				parent.find('.active').trigger('click');
				if(parent.parent().find('.on').size() == 0) {
					$('#ww-cash').addClass('hidden');
					$('#ww-pay').removeClass('hidden');
				}
			} else {
				parent.addClass('on');
				total();
				$('#ww-pay').addClass('hidden');
				$('#ww-cash').removeClass('hidden');

				parent.find('.group i').eq(0).trigger('click');
			}
		});

		$('#product-list .item .group i').click(function(){
			var parent = $(this).parent();
			if($(this).hasClass('active')) {
				$(this).removeClass('active');

				//if(parent.find(''))
			} else {
				parent.find('i.active').removeClass('active');
				$(this).addClass('active');
			}

			pdPrice(parent.attr('data-pid'));
			total();
		});

		$('#product-detail-attr .item .group i').click(function(){
			var parent = $(this).parent();
			if($(this).hasClass('active')) {
				$(this).removeClass('active');

				//if(parent.find(''))
			} else {
				parent.find('i.active').removeClass('active');
				$(this).addClass('active');
			}

			pdPrice(parent.attr('data-pid'));
		});

		$('#product-detail-attr  .item .group i').eq(0).trigger('click');

		if(window.page == 'page-product') {
//			$('#product-detail-attr .item .group').each(function(){
//				$(this).find('i').eq(0).trigger('click');
//			});
		}


		$('#ww-cash').click(function(){
			if($(this).attr('data-paying') == 'true')
				return true;

			var postData = {
				pid:[],
				attr_id:[],
				remark:$('textarea').val(),
				post_from: window.page
			};

			if(window.page == 'page-product') {
				if(!postData.info[0].student_name) {
					alert('请输入学生姓名');
					$('input[name="student_name"]').focus();
					return false;
				}

				if(!postData.info[0].parent_name) {
					alert('请输入家长姓名');
					$('input[name="parent_name"]').focus();
					return false;
				}

				if(!postData.info[0].tel) {
					alert('请输入联系电话');
					$('input[name="tel"]').focus();
					return false;
				}

				if(!postData.info[0].birth) {
					alert('请输入学生生日');
					$('input[name="birth"]').focus();
					return false;
				}

				//商家页面
				var total_money = parseInt(parseFloat($('#ww-cash .ww-total').html()) * 100);
				if(total_money <= 0)
					return false;

				postData.info = [{
					student_name:$('input[name="student_name"]').val(),
					parent_name:$('input[name="parent_name"]').val(),
					tel:$('input[name="tel"]').val(),
					birth:$('input[name="birth"]').val()
				}];

				var action = '/order/create';
			} else {
				//学校页面
				var total_money = total();
				//console.log(total_money);
				if(total_money <= 0)
					return false;

				postData.student_id = $('select[name="student_id"]').val();

				var action = '/schoolOrder/create?partner_id=' + window.partner_id ;
			}




//			if(window.page == 'page-merchant') {
//				//home page
//				if(!$('input[name="student_grade"]').val()) {
//					alert('请输入学生班级');
//					$('input[name="student_grade"]').focus();
//					return false;
//				}
//				postData.info[0].student_grade = $('input[name="student_grade"]').val();
//			}

			var btn = $(this);
			var html = btn.html();
			btn.html('付款中...').attr('data-paying', 'true');

			if(window.page == 'page-product') {
				var pid = window.pid;
				postData.pid.push(pid);

				$('#product-detail-attr .item .group i.active').each(function(){
					if(typeof postData.attr_id[pid] == 'undefined')
						postData.attr_id[pid] = [];

					postData.attr_id[pid].push($(this).attr('data-attr-id'))
				});
			} else {
				$('#product-list .item.on').each(function(){
					var pid = parseInt($(this).attr('data-pid'));
					postData.pid.push(pid);

					$(this).find('.group i.active').each(function(){
						if(typeof postData.attr_id[pid] == 'undefined')
							postData.attr_id[pid] = [];

						postData.attr_id[pid].push($(this).attr('data-attr-id'))
					});
				});
			}


			$.ajax({
				url:action,
				data:postData,
				dataType:'json',
				type:'post',
				success: function(data) {
					if(data.error) {
						alert(data.msg);
						btn.html(html).attr('data-paying', 'false');
					} else {
						wxPayment(data.package, data.order);
					}
				},
				error: function() {
					alert('创建订单失败，请重试');
					btn.html(html).attr('data-paying', 'false');
				}
			});
		});



		$('#shipping-info-sub').click(shippingInfo);

		//商品详情页面跳转
		$('#ww-product-sub').click(goProductConfirm);

		//商品确认页面
		$('#pd-minus').click(pdMinus);
		$('#pd-plus').click(pdPlus);

		$('#ww-cash-single').click(function(){
			if($(this).attr('data-paying') == 'true')
				return true;

			var list = $('#shipping-info-list');
			var error = false;
			list.find('.ipt').each(function(){
				if(!$(this).val()) {
					alert($(this).attr('data-name') + '不能为空');
					$(this).focus();
					error = true;
					return false;
				}
			});

			if(error)
				return true;

			var postData = {
				pid:[window.pid],
				attr_id:[],
				info:[],
				qty:[],
				remark:$('textarea').val(),
				post_from: window.page
			};

			var qtyObj = $('#pd-qty');
			var qty = parseInt(qtyObj.html());
			if(!qty) {
				alert('数量不能为0');
				return false;
			}

			postData.attr_id[window.pid] = window.pdAttr;
			postData.qty[window.pid] = qty;

			list.find('.order-item').each(function(){
				postData.info.push({
					student_name:$(this).find('.student_name').val(),
					parent_name:$(this).find('.parent_name').val(),
					tel:$(this).find('.tel').val(),
					birth:$(this).find('.birth').val()
				})
			});

			var btn = $(this);
			var html = btn.html();
			btn.html('付款中...').attr('data-paying', 'true');


			$.ajax({
				url:'/order/create',
				data:postData,
				dataType:'json',
				type:'post',
				success: function(data) {
					if(data.error) {
						alert(data.msg);
						btn.html(html).attr('data-paying', 'false');
					} else {
						wxPayment(data.package, data.order);
					}
				},
				error: function() {
					alert('创建订单失败，请重试');
					btn.html(html).attr('data-paying', 'false');
				}
			});
		});
	});

	var pdPrice = function(pid) {
		var pd = $('#pd-'+pid);
		var price = parseInt(pd.attr('data-price'));

		pd.find('.group i.active').each(function(){
			var tmp = $(this).attr('data-attr-price');
			if(!tmp) tmp = 0;
			price += parseInt(tmp);
		});

		$('.pd-price-'+pid).html(displayMoney(price))
	};

	var total = function() {
		var total_money = 0;
		$('#product-list .item.on').each(function(){
			var tmp = parseInt($(this).attr('data-price'));
			$(this).find('.group i.active').each(function(){
				if($(this).attr('data-attr-price') != '')
					tmp += parseInt($(this).attr('data-attr-price'));
			});

			total_money += tmp;
		});

		$('.ww-total').html(displayMoney(total_money));

		return total_money;
	};

	var displayMoney = function(money) {
		return parseFloat(money/100);
	};


	var wxPayment = function wxPayment(biz_package, order) {
		WeixinJSBridge.invoke('getBrandWCPayRequest', biz_package, function(res){
			//WeixinJSBridge.log(res.err_msg);
			switch (res.err_msg) {
                case 'get_brand_wcpay_request:cancel' :
                    resetPd();
                    break;

                case 'get_brand_wcpay_request:ok' :
                    $('#ww-cash').html('确认中...');
                    checkPaymentStatus(order);
                    break;

                case 'get_brand_wcpay_request:fail' :
                    alert(res.err_code+res.err_desc+res.err_msg);
                    alert('支付错误，请重试');
                    resetPd();
                    break;

                default:
                    alert('支付错误，请重试');
                    resetPd();
                    break;
			}
		});
	};

	var checkPaymentStatus = function(order) {
		$.ajax({
			url:'/payment/status',
			data: order,
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if(data.status) {
					alert('支付成功!');
					resetPd();
				} else {
					setTimeout(function(){
						checkPaymentStatus()
					}, 2000);
				}
			},
			error: function() {
//				alert('ajax call success');
			}
		})
	};

	var resetPd = function() {

		$('#product-list .on h4').trigger('click');

		var cash = $('#ww-cash');
		cash.attr('data-paying', 'false');


		$('#ww-cash-single').attr('data-paying', 'false');

		if(window.page == 'page-product') {
			cash.html('付款<span>￥<em class="ww-total pd-price-'+ window.pid +'">' + $('.product-title .pd-price-'+ window.pid).html() + '</em></span>')
		} else {
			cash.html('付款<span>￥<em class="ww-total">0.00</em></span>');
		}
	};


	var shippingInfo = function() {
		if(!$('input[name="student_name"]').val()) {
			alert('请输入学生姓名');
			$('input[name="student_name"]').focus();
			return false;
		}

		if(!$('input[name="parent_name"]').val()) {
			alert('请输入家长姓名');
			$('input[name="parent_name"]').focus();
			return false;
		}

		if(!$('input[name="tel"]').val()) {
			alert('请输入联系电话');
			$('input[name="tel"]').focus();
			return false;
		}

		if(!$('input[name="birth"]').val()) {
			alert('请输入学生生日');
			$('input[name="birth"]').focus();
			return false;
		}

		$('#shipping-info').submit();
	};


	var goProductConfirm = function() {
		var t = parseFloat($('.price').val());
		if(t <= 0) {
			alert('请至少选择一个属性');
			return false;
		}

		var attr = '';
		$('.group i.active').each(function(){
			attr += "&attr[]="+$(this).attr('data-attr-id');
		});

		window.location.href = '/product/confirm?pid='+window.pid+attr;
	};


	var pdMinus = function() {
		var qtyObj = $('#pd-qty');
		var qty = parseInt(qtyObj.html());

		if(qty <= 1) {
			return false;
		}

		qty--;

		$('#shipping-info-list .order-item').last().remove();
		qtyObj.html(qty);
		$('.ww-total').html(displayMoney(qty * window.price));
	};

	var pdPlus = function() {
		var qtyObj = $('#pd-qty');
		var qty = parseInt(qtyObj.html());

		qty++;

		var html = $('#confirm-field-tpl').html();

		$('#shipping-info-list').append(html);
		$('#shipping-info-list .order-item').last().find('.num').html(qty);
		qtyObj.html(qty);
		$('.ww-total').html(displayMoney(qty * window.price));
	};
}(jQuery));


var fillShippingInfo = function(obj) {
	console.log(obj.val());
	if(!obj.val())
		return false;

	var parent = obj.parent().parent();
	var data = jQuery.parseJSON(obj.find('option:selected').attr('data-info'));


	parent.find('.student_name').val(data.student_name);
	parent.find('.parent_name').val(data.parent_name);
	parent.find('.tel').val(data.tel);
	parent.find('.birth').val(data.birth);
};