//第一次加载时设置侧边栏的高度
var h = $(document).height();
h = h + 'px';
$(".sidebar").css("min-height",h);

//修改侧边栏高度的函数
function change_size(){
	var h = $("#center-right").height()+80;
	h = h + 'px';
	$(".sidebar").height(h);
}

/*监听浏览器窗口大小改变的事件*/
window.onresize = function(){
	change_size();//改变侧边栏高度方法
}


/*为页面加载完成后注册事件*/
window.onload = function(){
	change_size();//改变侧边栏高度方法
}

/*为鼠标滚动注册事件*/
window.onscroll = function(){
	change_size();
}

