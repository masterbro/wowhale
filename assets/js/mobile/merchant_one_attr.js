(function($){

	$(function(){
		$("#owl-merchant").owlCarousel({
			autoPlay : 5000,
			lazyLoad:true,
			lazyFollow:false,
			pagination:true,
			afterInit: function(){
				$("#owl-merchant").find('.owl-item.loading').css('min-height', window.owlH);
			}
		});

		$('#navbar-toggle').click(function(){
			$('#header-nav').toggleClass('in');
		});

		$('#partner-apply').click(partnerApply);

		//补全信息
		$('#profile-sub').click(profileSub);
		$('#send-profile-sms').click(sendSms);


		$('#product-list .item h4').click(function(){
			var parent = $(this).parent();
			if(parent.hasClass('on')) {
				parent.removeClass('on');
				parent.find('.active').trigger('click');
				if(parent.parent().find('.on').size() == 0) {
					$('#ww-cash').addClass('hidden');
					$('#ww-pay').removeClass('hidden');
				}
			} else {
				parent.addClass('on');
				total();
				$('#ww-pay').addClass('hidden');
				$('#ww-cash').removeClass('hidden');

				parent.find('.group i').eq(0).trigger('click');
			}
		});

		$('#product-list .item .group i').click(function(){
			var parent = $(this).parent();
			if($(this).hasClass('active')) {
				$(this).removeClass('active');

				//if(parent.find(''))
			} else {
				parent.find('i.active').removeClass('active');
				$(this).addClass('active');
			}

			pdPrice(parent.attr('data-pid'));
			total();
		});

		$('#product-detail-attr .item .group i').click(function(){
			var parent = $(this).parent();
			if($(this).hasClass('active')) {
				$(this).removeClass('active');

				//if(parent.find(''))
			} else {
				parent.find('i.active').removeClass('active');
				$(this).addClass('active');
			}

			pdPrice(parent.attr('data-pid'));
		});

		$('#product-detail-attr  .item .group i').eq(0).trigger('click');

		if(window.page == 'page-product') {
//			$('#product-detail-attr .item .group').each(function(){
//				$(this).find('i').eq(0).trigger('click');
//			});
		}


		$('#ww-cash').click(function(){
			if($(this).attr('data-paying') == 'true')
				return true;

			if(window.page == 'page-product') {
				var total_money = parseInt(parseFloat($('#ww-cash .ww-total').html()) * 100);
				if(total_money <= 0)
					return false;
			} else {
				var total_money = total();
				//console.log(total_money);
				if(total_money <= 0)
					return false;
			}
			var postData = {
				pid:[],
				attr_id:[],
				info:{
					student_name:$('input[name="student_name"]').val(),
					parent_name:$('input[name="parent_name"]').val(),
					tel:$('input[name="tel"]').val(),
					birth:$('input[name="birth"]').val(),
					remark:$('textarea').val()
				},
				post_from: window.page
			};

			if(!postData.info.student_name) {
				alert('请输入学生姓名');
				$('input[name="student_name"]').focus();
				return false;
			}

			if(!postData.info.parent_name) {
				alert('请输入家长姓名');
				$('input[name="parent_name"]').focus();
				return false;
			}

			if(!postData.info.tel) {
				alert('请输入联系电话');
				$('input[name="tel"]').focus();
				return false;
			}

			if(!postData.info.birth) {
				alert('请输入学生生日');
				$('input[name="birth"]').focus();
				return false;
			}

			if(window.page == 'page-merchant') {
				//home page
				if(!$('input[name="student_grade"]').val()) {
					alert('请输入学生班级');
					$('input[name="student_grade"]').focus();
					return false;
				}
				postData.info.student_grade = $('input[name="student_grade"]').val();
			}

			var btn = $(this);
			var html = btn.html();
			btn.html('付款中...').attr('data-paying', 'true');

			if(window.page == 'page-product') {
				var pid = window.pid;
				postData.pid.push(pid);

				$('#product-detail-attr .item .group i.active').each(function(){
					if(typeof postData.attr_id[pid] == 'undefined')
						postData.attr_id[pid] = [];

					postData.attr_id[pid].push($(this).attr('data-attr-id'))
				});
			} else {
				$('#product-list .item.on').each(function(){
					var pid = parseInt($(this).attr('data-pid'));
					postData.pid.push(pid);

					$(this).find('.group i.active').each(function(){
						if(typeof postData.attr_id[pid] == 'undefined')
							postData.attr_id[pid] = [];

						postData.attr_id[pid].push($(this).attr('data-attr-id'))
					});
				});
			}


			$.ajax({
				url:'/order/create',
				data:postData,
				dataType:'json',
				type:'post',
				success: function(data) {
					if(data.error) {
						alert(data.msg);
						btn.html(html).attr('data-paying', 'false');
					} else {
						wxPayment(data.package, data.order);
					}
				},
				error: function() {
					alert('创建订单失败，请重试');
					btn.html(html).attr('data-paying', 'false');
				}
			});
		});
	});

	var pdPrice = function(pid) {
		var pd = $('#pd-'+pid);
		//var price = parseInt($(this).attr('data-attr-price'));

//		pd.find('.group i.active').each(function(){
//			var tmp = $(this).attr('data-attr-price');
//			if(!tmp) tmp = 0;
//			price += parseInt(tmp);
//		});

		var price = pd.find('.group i.active').attr('data-attr-price');

		$('.pd-price-'+pid).html(displayMoney(price))
	};

	var total = function() {
		var total_money = 0;
		$('#product-list .item.on').each(function(){
			var tmp = parseInt($(this).attr('data-price'));
			$(this).find('.group i.active').each(function(){
				if($(this).attr('data-attr-price') != '')
					tmp += parseInt($(this).attr('data-attr-price'));
			});

			total_money += tmp;
		});

		$('.ww-total').html(displayMoney(total_money));

		return total_money;
	};

	var displayMoney = function(money) {
		return parseFloat(money/100);
	};


	var wxPayment = function wxPayment(biz_package, order) {
		WeixinJSBridge.invoke('getBrandWCPayRequest', biz_package, function(res){
			//WeixinJSBridge.log(res.err_msg);
			switch (res.err_msg) {
			case 'get_brand_wcpay_request:cancel' :
				resetPd();
			break;

			case 'get_brand_wcpay_request:ok' :
				$('#ww-cash').html('确认中...');
				checkPaymentStatus(order);
			break;

			case 'get_brand_wcpay_request:fail' :
				alert(res.err_code+res.err_desc+res.err_msg);
				alert('支付错误，请重试');
				resetPd();
				break;

				default:
					alert('支付错误，请重试');
					resetPd();
					break;
			}
		});
	};

	var checkPaymentStatus = function(order) {
		$.ajax({
			url:'/payment/status',
			data: order,
			type: 'post',
			dataType: 'json',
			success: function(data) {
				if(data.status) {
					alert('支付成功!');
					resetPd();
				} else {
					setTimeout(function(){
						checkPaymentStatus()
					}, 2000);
				}
			},
			error: function() {
//				alert('ajax call success');
			}
		})
	};

	var resetPd = function() {

		$('#product-list .on h4').trigger('click');

		var cash = $('#ww-cash');
		cash.attr('data-paying', 'false');

		if(window.page == 'page-product') {
			cash.html('付款<span>￥<em class="ww-total pd-price-'+ window.pid +'">' + $('.product-title .pd-price-'+ window.pid).html() + '</em></span>')
		} else {
			cash.html('付款<span>￥<em class="ww-total">0.00</em></span>');
		}
	};

	var partnerApply = function() {
		var btn = $('#partner-apply');
		if(btn.attr('data-loading') == 'true')
			return false;

		var form = $('#partner-apply-form');

		if(!form.find('input[name="name"]').val()) {
			alert('商户名称不能为空');
			return false;
		}
		if(!form.find('input[name="email"]').val()) {
			alert('邮箱不能为空');
			return false;
		}

		if(!form.find('input[name="contacts"]').val()) {
			alert('联系人不能为空');
			return false;
		}

		if(!form.find('input[name="tel"]').val()) {
			alert('联系电话不能为空');
			return false;
		}

		if(!form.find('input[name="term"]').prop('checked')) {
			alert('请先同意服务协议');
			return false;
		}

		btn.attr('data-loading', 'true').html('提交中...');
		form.submit();
	};


	//补全信息
	var profileSub = function() {
		var btn = $('#profile-sub');

		if(btn.attr('data-sending') == 'true')
			return false;

		var mobile = $('input[name="mobile"]').val();

		if(!mobile) {
			alert('请输入你的手机号');
			return false;
		}

		if(!/^1\d{10}$/.test(mobile)) {
			alert('请输入正确的手机号');
			return false;
		}

		var captcha = $('input[name="captcha"]').val();

		if(!captcha) {
			alert('请输入验证码');
			return false;
		}

		sending = true;
		btn.html('发送中...').attr('data-sending', 'true');
		$.ajax({
			url:'/account',
			type:'post',
			dataType:'json',
			data:{mobile:mobile, captcha:captcha},
			success: function(data) {
				alert(data.msg);
				if(data.error) {
					btn.html('补全信息').attr('data-sending', 'false');
				} else {
					window.location.reload();
				}
			},
			error: function(){
				btn.html('补全信息').attr('data-sending', 'false');
			}
		})
	};


	var bindSmsCountDownInterval = null, sending = false;

	var sendSms = function() {
		var btn = $('#send-profile-sms');

		if(btn.attr('data-sending') == 'true')
			return false;

		var mobile = $('input[name="mobile"]').val();

		if(!mobile) {
			alert('请输入你的手机号');
			return false;
		}

		if(!/^1\d{10}$/.test(mobile)) {
			alert('*请输入正确的手机号');
			return false;
		}

		sending = true;
		btn.html('发送中...').attr('data-sending', 'true');
		$.ajax({
			url:'/account/captcha',
			type:'post',
			dataType:'json',
			data:{mobile:mobile},
			success: function(data) {
				if(data.error) {
					alert(data.msg);
					btn.html('发送验证码').attr('data-sending', 'false');
				} else {
					bindSmsCountDown();
				}
			},
			error: function(){
				sending = false;
				btn.html('发送验证码').attr('data-sending', 'false');
			}
		})
	}

	var bindSmsCountDown = function() {
		$('#send-profile-sms').html('<em>60</em> 秒后重新发送');

		bindSmsCountDownInterval = setInterval(function(){
			var i = parseInt($('#send-profile-sms').find('em').html());
			i--;
			if(i>0) {
				$('#send-profile-sms').html('<em>'+ i +'</em> 秒后重新发送');
			} else {
				sending = false;
				$('#send-profile-sms').html('发送验证码').attr('data-sending', 'false');
				clearInterval(bindSmsCountDownInterval);
			}
		}, 1000);
	};

}(jQuery));