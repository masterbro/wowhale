/*修改侧边栏高度的函数*/
function change_size(){
	var h = $("#center-right").height()+80;
	h = h + 'px';
	$(".sidebar").height(h);/*padding-bottom有20px*/
}


$(function(){
	$('#show-qrcode').click(function(){
		$('#modal-qrcode').modal();
	})

	var uploadBtn = $('#ww-add-img');
	var container = $('#img-container');
	var sliderID = $('.ww-school-slider').size();
	var form = $('#ww-school-page-form');

	$('body').on('click', '.ww-del-slider', function() {
		if(!confirm('你确定要删除此图片吗?'))
			return false;

		form.find('#ww-school-slider-'+$(this).attr('data-id')).remove();
		$(this).parent().remove();	
	})

	uploadBtn.click(function(){
		$('#fileupload_input').trigger('click');
	});

	form.find('button').click(function(){
		var sliderSize = $('.ww-school-slider').size();

		if(sliderSize < 1) {
			alert('请至少上传一张图片');
			return false;
		}

		if(sliderSize > 10) {
			alert('图片不能大于10张');
			return false;
		}

		if(form.valid()) {
			editor.sync();
			form.submit();
		}
		
	});

	$("#fileupload_input").fileupload({
		url: BASE_URL + "/manage/upload?type=school",//文件上传地址，当然也可以直接写在input的data-url属性内
		send: function() {
			uploadBtn.button('loading');
		},
		done:function(e,data){
			var result = JSON.parse(data.result);
			if(result.error) {
				alert(result.error);
			} else {
				container.find('.alert').remove();

				sliderID++;
				var html = '';
				html += '<div class="school-slider-item">';
				html += '<img src="'+ BASE_URL + '/assets/upload/' +  result.img  +'" />';
				html += '<a href="#" class="ww-del-slider" data-id="' + sliderID + '"><i class="fa fa-trash"></i></a>';
				html += '</div>';
				var _html_obj = $(html);
				container.append(_html_obj);
								
				
				var input = '<input type="hidden" name="slider[]" value="' + result.img + '" class="ww-school-slider" id="ww-school-slider-' + sliderID +'" />';
				form.append(input);			
				
				change_size();/*添加图片时，修改侧边栏的高度*/
			}
		},
		always: function (e, data) {
			uploadBtn.button('reset');
		}
	})
})