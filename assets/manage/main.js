$(function(){

	$.fn.datepicker.dates['en'] = {
		days: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],
		daysShort: ["日", "一", "二", "三", "四", "五", "六"],
		daysMin: ["日", "一", "二", "三", "四", "五", "六"],
		months: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		monthsShort: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
		today: "今天",
		clear: "取消"
	};


	$('.input-datepicker').datepicker({
		format: 'yyyy-mm-dd',
		autoclose:true
	});

	$('form.validate').each(function(){
		$(this).validate({
			errorElement : 'span',
			errorClass : 'help-block',
			highlight : function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},

			success : function(label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},

			errorPlacement : function(error, element) {
				if (element.is(':radio') || element.is(':checkbox')) { //如果是radio或checkbox
					var eid = element.attr('name'); //获取元素的name属性
					element.parent().parent('div').append(error);
				} else {
					element.parent('div').append(error);
				}

			}
//			errorPlacement:function(error,element) {
//				element.next('p').html(error)
//			}
		});
	});


	$('.hr-save').click(function(){
		var btn = $(this);

		var form = btn.parent();
		if(form.hasClass('validate') && !form.valid()) {
			return false;
		}

		btn.button('loading');
		$.ajax({
			url: form.attr('action'),
			type:'post',
			data: form.serialize(),
			dataType: 'json',
			success: function(data) {
				alert(data.msg);
				if(!data.error) {
					if(btn.attr('data-callback')) {
						eval(btn.attr('data-callback'));
					} else {
						form[0].reset();
					}
				}
			},
			complete: function() {
				btn.button('reset');
			}
		});
	})

	$('.hrm-province-select').each(function(){
		showCities($(this));
	});

	$('.hrm-province-select').change(function(){
		showCities($(this));
	});
})

var showCities = function(select) {
	var cities = $.parseJSON(select.find('option:selected').attr('data-cities'));
	var html = '';
	$.each(cities, function(k, v){
		html += '<option value="'+ v.id +'">'+ v.name +'</option>';
	});

	$('#'+select.attr('data-target')).html(html);
};