$(function(){
	$('#add-pd-attr').click(addAttr);

	$('#product-attr')
		.on('click', '.add', function() {
			addSubAttr($(this));
		})
		.on('click', '.remove', function() {

			if(confirm('你确定要删除此属性吗？此属性下的属性值将会被全部删除')) {
				if($(this).attr('data-id')) {
					$('input[name="remove_attr"]').val($('input[name="remove_attr"]').val()+','+$(this).attr('data-id'));
				}

				$(this).parent().parent().parent().find('.remove-sub').each(function(){
					if($(this).attr('data-id')) {
						$('input[name="remove_attr"]').val($('input[name="remove_attr"]').val()+','+$(this).attr('data-id'));
					}
				});
				$(this).parent().parent().parent().parent().remove();

				$('#add-pd-attr').show();
			}
		})
		.on('click', '.remove-sub', function() {
			if(confirm('你确定要删除此属性值吗？')) {
				if($(this).attr('data-id')) {
					$('input[name="remove_attr"]').val($('input[name="remove_attr"]').val()+','+$(this).attr('data-id'));
				}
				var _this_prent =  $(this).parent().parent().parent();
				_this_prent.prev('input[type="hidden"]').remove();
				_this_prent.remove();
			}
		})
		.on('click', 'input[type="checkbox"]', function() {
			var row = $(this).parent().parent().parent().parent();
			if($(this).prop('checked')) {
				row.find('.attr_price').removeClass('hidden');
				$(this).next('input').val(1);
			} else {
				$(this).next('input').val(0);
				row.find('.attr_price').addClass('hidden');
			}
		});
})

var addAttr = function() {
	$('#add-pd-attr').hide();

	var html = '';
	var eq = $('.pd-attr-group').size();
	html += '<div class="pd-attr-group" id="pd-attr-group-'+eq+'">'
	html += '<div class="row">';
	html += '<div class="col-md-4"><div class="form-group">';
	html += '<label for="">属性类型：<span class="text-danger">*</span></label>';
	html += '<input type="text" class="form-control" placeholder="属性类型" name="attr_parent['+ eq +']" value="" /><p class="help-block"></p>';
	html += '</div></div>';//end form-group & col-md-4
	html += '<div class="col-md-4"><div class="form-group pt10">';
	html += '<label for=""> </label><br />';
	html += '<a href="javascript:;" class="btn btn-xs btn-info add" data-eq="'+eq+'"><i class="fa fa-plus"></i></a>';
	html += '<a href="javascript:;" class="btn btn-xs btn-danger remove"><i class="fa fa-trash"></i></a>';
	html += '</div></div>';//end form-group & col-md-4
	html += '</div>';//end row
	html += '</div>';//end pd-attr-group

	$('#product-attr').append(html);
}

var addSubAttr = function(obj) {
	var eq = obj.attr('data-eq');
	var wrap = $('#pd-attr-group-'+eq);

	var html = '';

	html += '<div class="row">';
	html += '<div class="col-md-3"><div class="form-group">';
	html += '<label for="">名称：<span class="text-danger">*</span></label>';
	html += '<input type="text" name="attr_name['+eq+'][]" value="" class="form-control required" placeholder="属性名称" />';
	html += '<p class="help-block"></p></div></div>';//end form-group & col-md-3
	html += '<div class="col-md-1"><div class="checkbox pt20"><label>';
	html += '<input type="checkbox" name="attr_change_price['+eq+'][]" value="1" class="fx-checkbox" /><input type="hidden" name="attr_change_price_val['+eq+'][]" value="0" />';
	html += ' 价格</label></div></div>';//end checkbox & col-md-1
	html += '<div class="col-md-2 attr_price hidden"><div class="form-group">';
	html += '<label for="">&nbsp;</label>';
	html += '<input type="text" name="attr_price['+eq+'][]" class="form-control" placeholder="价格" />';
	html += '<p class="help-block"></p></div></div>';//end checkbox & col-md-2
	html += '<div class="col-md-1"><div class="form-group pt5"><label for="">&nbsp;</label><br />';
	html += '<a href="javascript:;" class="btn btn-xs btn-danger remove-sub"><i class="fa fa-trash"></i></a>';
	html += '</label></div></div>';//end form-group & col-md-1
	html += '</div>';//row

	wrap.append(html);
}