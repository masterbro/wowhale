<?php
class WxApp {

    private $access_token, $appid, $secret;

    public function __construct($params) {
        $this->access_token = $params['access_token'];
        $this->appid = $params['appid'];
        $this->secret = $params['secret'];
    }


    public function getOpenId($code) {
        return $this->getResult("https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->appid}&secret={$this->secret}&code={$code}&grant_type=authorization_code");
    }

    public function getUserInfo($open_id) {
        return $this->getResult("https://api.weixin.qq.com/sns/userinfo?access_token={$this->access_token}&openid={$open_id}&lang=zh_CN");
    }

	public function getAccessToken() {
		return $this->getResult("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->appid}&secret={$this->secret}");
	}

	public function getQrScence($desk_id) {
		return $this->postResult(
			"https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={$this->access_token}",
			array(
				'action_name' => 'QR_LIMIT_SCENE',
				'action_info' => array(
					'scene' => array(
						'scene_id' => $desk_id
					)
				),
			),
			true
		);
	}

    private function getResult($url) {

        $ch	= curl_init();
        curl_setopt ($ch, CURLOPT_URL, trim($url));
        curl_setopt ($ch, CURLOPT_CAINFO, "./protected/data/cacert.pem");
	    curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $contents = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
        if($errno !== 0) throw new WxAppException('系统错误，请稍候再试');

        return $this->handleResult($contents);
    }

    private function postResult($url, $post, $json = false) {
        $ch	= curl_init();
        curl_setopt ($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CAINFO, "./protected/data/cacert.pem");
	    curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
	    if($json) {
		    $post = json_encode($post);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    'Content-Type: application/json; charset=utf-8',
				    'Content-Length: ' . strlen($post))
		    );
	    }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);


        $contents = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);

        if($errno !== 0) throw new WxAppException('系统错误，请稍候再试1');

        return $this->handleResult($contents);
    }

    private function handleResult($data) {
        $data = json_decode($data);
        if ($data->errcode && $data->errcode !== 0)
            throw new WxAppException(
                $data->errmsg
                //$this->errors[$data->errcode] ? $this->errors[$data->errcode] : '系统错误，请稍候再试'
            );

        return $data;
    }

    public function setAccessToken($access_token) {
        $this->access_token = $access_token;
    }

	public function sendTxtMsg($open_id, $text) {
		if(!$text) throw new WxAppException('消息内容不能为空');
		$msg = array(
			'touser' => $open_id,
			'msgtype' => 'text',
			'text' => array('content' => '_sxt_0_sxt_'),
		);
		return $this->postMsg($msg, array(
			'search' => array('_sxt_0_sxt_'),
			'replace' => array($text),
		));
	}

	private function postMsg($msg, $replacement) {
		if(is_array($msg)) $msg = (Object) $msg;
		$msg = json_encode($msg);

		foreach($replacement['replace'] as $k=>$v)
			$replacement['replace'][$k] = $this->slashJsonTxt($v);

		$msg = str_replace($replacement['search'], $replacement['replace'], $msg);
		$access_token = $this->getCachedAccessToken();
		return $this->postResult(
			"https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={$access_token}",
			$msg
		);
	}

	private function slashJsonTxt($str) {
		return str_replace(array('"'), array('\\"'), $str);
	}

	public function getCachedAccessToken() {
		$cache_key = md5('wx_access_token_'.$this->appid);

		$cache = Yii::app()->cache;
		$data = $cache->get($cache_key);

		if ( $data === FALSE ) {
			$access = $this->getAccessToken();
			$data = $access->access_token;
			$cache->set($cache_key , $data, $access->expires_in - 300);
		}

		return $data;
	}


	public function getWxUserInfo($open_id) {
		$access_token = $this->getCachedAccessToken();
		return $this->getResult("https://api.weixin.qq.com/cgi-bin/user/info?access_token={$access_token}&openid={$open_id}&lang=zh_CN");
	}

	public function sendNotify($open_id, $template_id, $access_token, $data) {
		$place_holder = '_sxt_0_sxt_';
		$msg = array(
			'touser' => $open_id,
			'template_id' => $template_id,
			'url' => $data['url'],
			'topcolor' => $data['topcolor'] ? $data['topcolor'] : '#FF0000',
			'data' => array()
		);

		foreach($data['msg'] as $k=>$v) {

			$msg['data'][$k] = array(
				'value' => $place_holder,
				'color' => $v['color'] ? $v['color'] : '',
			);

			$search[] = '_sxt_0_sxt_';
			$replace[] = $this->slashNotifyJson($v['value']);
		}
		$msg = str_replace($search, $replace, json_encode($msg));

		return $this->postResult(
			"https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$access_token}",
			$msg
		);
	}

	private function slashNotifyJson($txt) {
		$txt = $this->slashJsonTxt($txt);
		return str_replace(array('{{', '}}'), array('', ''), $txt);
	}

	public function cardStock($card_id, $num) {
		$access_token = $this->getCachedAccessToken();
		return $this->postResult(
			"https://api.weixin.qq.com/card/modifystock?access_token={$access_token}",
			array(
				'card_id' => $card_id,
				'increase_stock_value' => $num > 0 ? $num : 0,
				'reduce_stock_value' => $num < 0 ? abs($num) : 0,
			),
			true
		);
	}

	public function cardCreate($card) {
		$access_token = $this->getCachedAccessToken();
		return $this->postResult(
			"https://api.weixin.qq.com/card/create?access_token={$access_token}",
			$card,
			true
		);
	}
}