<?php
class WxQySuite extends WxQyApp {

	public function get_suite_token($data) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token",
			$data, true
		);
	}

	public function get_pre_auth_code($suite_id, $suite_access_token) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token={$suite_access_token}",
			array('suite_id' => $suite_id), true
		);
	}

	public function set_session_info($pre_auth_code, $suite_access_token, $appid) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token={$suite_access_token}",
			array('pre_auth_code' => $pre_auth_code, 'session_info' => array('appid' => $appid)), true
		);
	}

	public function get_permanent_code($suite_id, $suite_access_token, $auth_code) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token={$suite_access_token}",
			array('suite_id' => $suite_id, 'auth_code' => $auth_code), true
		);
	}

	public static function getSuite($id = 1) {
		$suite = WxSuite::model()->findByPk($id);


		if(!$suite->suite_access_token || $suite->suite_access_token_expired < time()) {
			$app = new WxQySuite();
			$app->throw_exception = true;
			$token = $app->get_suite_token(array(
				'suite_id' => $suite->suite_id,
				'suite_secret' => $suite->suite_secret,
				'suite_ticket' => $suite->suite_ticket,
			));
			if($token) {
				$suite->suite_access_token = $token->suite_access_token;
				$suite->suite_access_token_expired = $token->expires_in + time();
				$suite->save();
			}
		}

		return $suite;
	}
}