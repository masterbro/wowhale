<?php
class WxQyApp {

	public $throw_exception = false;
    protected $access_token, $corpid, $corpsecret;

    public function __construct($params = false) {
	    if($params) {
		    $this->corpid = $params->corpid;
            $this->js_ticket = $params->js_ticket;
		    $this->corpsecret = $params->corpsecret;
		    $this->access_token = $params->access_token;
	    }
    }


	public static function getAccessToken($params) {
		$wx_app = new WxQyApp($params);

		try {
			$result = $wx_app->getResult("https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={$params->corpid}&corpsecret={$params->corpsecret}");
		} catch(WxAppException $e) {
			return false;
		}

		return $result;
	}

	public static function getThirdPartAccessToken($params, $suite_id = 1) {
		$suite = $suite = WxQySuite::getSuite($suite_id);
		$wx_app = new WxQyApp();
        $wx_app->throw_exception = true;
		$result = $wx_app->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token={$suite->suite_access_token}",
			array(
				'suite_id' => $suite->suite_id,
				'auth_corpid' => $params->corpid,
				'permanent_code' => $params->permanent_code,
			), true
		);

		return $result;
	}
    public static function getThirdPartJsTicket($params) {
        //$suite = $suite = WxQySuite::getSuite();
        $wx_app = new WxQyApp();
        $result = $wx_app->getResult(
            "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token={$params->access_token}"
        );

        return $result;
    }
    public function getSignPackage() {
        $jsapiTicket = $this->js_ticket;
        // 注意 URL 一定要动态获取，不能 hardcode.
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $timestamp = time();
        $nonceStr = $this->createNonceStr();

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket={$jsapiTicket}&noncestr={$nonceStr}&timestamp={$timestamp}&url={$url}";

        $signature = sha1($string);

        $signPackage = array(
            "appId"     => $this->corpid,
            "nonceStr"  => $nonceStr,
            "timestamp" => $timestamp,
            "url"       => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }
    private function createNonceStr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }


	public function upload($type, $file_path) {
		return $this->postResult(
				"https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={$this->access_token}&type={$type}",
				array(
					'media' => '@'.realpath($file_path),
				)
			);
	}

	public function menu($menu, $agentid) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token={$this->access_token}&agentid={$agentid}",
			$menu, true
		);
	}

	public function appSetting($data) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/agent/set?access_token={$this->access_token}",
			$data, true
		);
	}

    protected function getResult($url) {

        $ch	= curl_init();
        curl_setopt ($ch, CURLOPT_URL, trim($url));
        curl_setopt ($ch, CURLOPT_CAINFO, "./protected/data/cacert.pem");
	    curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $contents = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);
	    if($errno !== 0) {
		    if($this->throw_exception) {
			    throw new WxAppException('系统错误，请稍候再试');
		    } else {
                Yii::log($errno.' '.$contents,'error');
			    return false;
		    }
	    }

        return $this->handleResult($contents);
    }

	protected function postResult($url, $post, $json = false) {
        $ch	= curl_init();
        curl_setopt ($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CAINFO, "./protected/data/cacert.pem");
	    curl_setopt($ch, CURLOPT_SSLVERSION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
	    if($json) {
		    $post = json_encode($post, JSON_UNESCAPED_UNICODE);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				    'Content-Type: application/json; charset=utf-8',
				    'Content-Length: ' . strlen($post))
		    );
	    }


		if (isset($post['media'] ) && function_exists('curl_file_create')) {
			$file = ltrim($post['media'], '@');
			$post['media'] = curl_file_create($file);
			curl_setopt($ch, CURLOPT_INFILESIZE, filesize($file));
		}

        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);


        $contents = curl_exec($ch);
        $errno = curl_errno($ch);
        curl_close($ch);

        if($errno !== 0) {
            Yii::log('post error '  . $errno.' '.$contents,'error');
	        if($this->throw_exception) {
		        throw new WxAppException('系统错误，请稍候再试');
	        } else {
		        return false;
	        }
        }

        return $this->handleResult($contents);
    }

    private function handleResult($data) {
        $data = json_decode($data);
        if ($data->errcode && ($data->errcode !== 0 && $data->errcode !== 60008) ) {
            Yii::log($data->errcode.' '.$data->errmsg,'error');
	        if($this->throw_exception) {
		        throw new WxAppException(
			        $data->errmsg
		        //$this->errors[$data->errcode] ? $this->errors[$data->errcode] : '系统错误，请稍候再试'
		        );
	        } else {
		        return false;
	        }
        }

        return $data;
    }

}