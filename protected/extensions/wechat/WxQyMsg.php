<?php
class WxQyMsg extends WxQyApp {

	public function text($to, $content, $agentid) {
		return $this->send(array(
				'touser' => $to,
				'toparty' => "",
				'totag' => "",
				'msgtype' => 'text',
				'agentid' => $agentid,
				'text' => array('content' => $content),
//				'safe' => 1,
			));
	}

	public function news($to, $news, $agentid) {
		return $this->send(array(
			'touser' => $to,
			'msgtype' => 'news',
			'agentid' => $agentid,
			'news' => array('articles' => $news),
		));
	}
    public function image($to, $media_id, $agentid) {
        return $this->send(array(
            'touser' => $to,
            'msgtype' => 'image',
            'agentid' => $agentid,
            'image' => array('media_id' => $media_id),
        ));
    }


	private function send($data) {
		return $this->postResult(
				"https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={$this->access_token}",
				$data, true
			);
	}
    public function getMediaCounts($agent_id) {
        return $this->getResult( "https://qyapi.weixin.qq.com/cgi-bin/material/get_count?access_token={$this->access_token}&agentid={$agent_id}" );
    }
	public function chat($data) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/chat/create?access_token={$this->access_token}",
			$data, true
		);
	}
    public function updateChat($data) {
        return $this->postResult(
            "https://qyapi.weixin.qq.com/cgi-bin/chat/update?access_token={$this->access_token}",
            $data, true
        );
    }
    public function getChat($chat_id) {
        return $this->getResult( "https://qyapi.weixin.qq.com/cgi-bin/chat/get?access_token={$this->access_token}&chatid={$chat_id}" );
    }
    public function quitChat($chat_id) {
        return $this->getResult( "https://qyapi.weixin.qq.com/cgi-bin/chat/quit?access_token={$this->access_token}&chatid={$chat_id}" );
    }

	public function chatSend($data) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/chat/send?access_token={$this->access_token}",
			$data, true
		);
	}
}