<?php
class WxQyContacts extends WxQyApp {


	public function departmentList() {
		return $this->getResult("https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token={$this->access_token}");
	}
    public function createDepart($base_department,$name){
        $post_data = array(
            'parentid' => $base_department,
            'name' => $name
        );
        try{
            $student_department_result = $this->postResult(
                "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$this->access_token}",
                $post_data, true
            );
            $student_department = $student_department_result->id;
        }
        catch(Exception $e){
            $student_department = 0;
        }
        return $student_department;
    }
    public function updateDepart($department_id,$name,$parentid=''){
        $post_data = array(
            'name' => $name,
            'id'=>$department_id
        );
        if($parentid){
            $post_data['parentid']  = $base_department;
        }

        try{
            $result = $this->postResult(
                "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token={$this->access_token}",
                $post_data, true
            );
            //$student_department = $student_department_result->id;
        }
        catch(Exception $e){
            return false;
        }
        return $result;
    }
    public function createDepartment($departments=array('student_department'=>1,'teacher_department'=>1)) {
        $base_departments = $this->departmentList();
        if($base_departments && $base_departments->department){
            $base_department = $base_departments->department[0];
            if($base_department && $base_department->id){
                $base_department = $base_department->id;
            }
            else{
                return false;
            }

        }
        if(!$base_department){
            return false;
        }
        if($departments['student_department']){
            /*$post_data = array(
                'parentid' => $base_department,
                'name' => '学生家长'
            );
            try{
                $student_department_result = $this->postResult(
                    "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$this->access_token}",
                    $post_data, true
                );
                $student_department = $student_department_result->id;
            }
            catch(Exception $e){
                $student_department = 0;
            }*/
            $student_department = $this->createDepart($base_department,'学生家长');
        }

        if($departments['teacher_department']) {
            $post_data1 = array(
                'parentid' => $base_department,
                'name' => '教师'
            );
            /*try {
                $teacher_department_result = $this->postResult(
                    "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$this->access_token}",
                    $post_data1, true
                );
                $teacher_department = $teacher_department_result->id;
            } catch (Exception $e) {
                $teacher_department = 0;
            }*/
            $teacher_department = $this->createDepart($base_department,'教师');
        }
        if($departments['leader_department']) {
            /*$post_data1 = array(
                'parentid' => $base_department,
                'name' => '园长'
            );
            try {
                $leader_department_result = $this->postResult(
                    "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={$this->access_token}",
                    $post_data1, true
                );
                $leader_department = $leader_department_result->id;
            } catch (Exception $e) {
                $leader_department = 0;
            }*/

            $leader_department = $this->createDepart($base_department,'园长');
        }
        return compact('teacher_department','student_department','leader_department');
    }

	public function create($ww_contacts, $department) {

		$post_data = array(
			'userid' => 'sh'.$ww_contacts->id,
			'name' => $this->name($ww_contacts),
			'department' => $department,
			'position' =>  $this->positionName($ww_contacts),
			'mobile' =>  $ww_contacts->parent_phone,
		);

		return $this->postResult(
				"https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={$this->access_token}",
				$post_data, true
			);
	}

	public function update($ww_contacts, $department) {

		$post_data = array(
			'userid' => 'sh'.$ww_contacts->id,
			'name' => $this->name($ww_contacts),
			'department' => $department,
			'position' =>  $this->positionName($ww_contacts),
			'mobile' =>  $ww_contacts->parent_phone,
		);

		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={$this->access_token}",
			$post_data, true
		);
	}

	private function positionName($ww_contacts) {
		if( $ww_contacts->type == Contacts::TY_STUDENT ) {
			$position = '家长';
		} else {
			$position = $ww_contacts->leader ? '园长' : '老师';
		}
		return $position;
	}

	private function name($ww_contacts) {
		if($ww_contacts->type == Contacts::TY_STUDENT) {
			$students = '';
			foreach($ww_contacts->students as $s) {
				$students .= '|'.$s->name;
			}

			return ltrim(ltrim($students, ','),'|');
		} else {
			return $ww_contacts->parent_name;
		}

		//return $ww_contacts->type == Contacts::TY_STUDENT ? $ww_contacts->student_name : $ww_contacts->parent_name;
	}

	public function delete($ww_contacts) {
		return $this->getResult( "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={$this->access_token}&userid=sh{$ww_contacts->id}" );
	}

	public function invite($ww_contacts) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/invite/send?access_token={$this->access_token}",
			array('useridlist' => 'sh'.$ww_contacts->id), true
		);
	}
    public function batchDelete($useridlist) {
        return $this->postResult(
            "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token={$this->access_token}",
            array('useridlist' => $useridlist), true
        );
    }
	public function batchCreate($ww_contacts_array, $department) {
		if(!$ww_contacts_array)
			return false;

		$csv = "姓名,帐号,微信号,手机号,邮箱,所在部门,职位\r\n";
		foreach($ww_contacts_array as $ww) {
			$tmp = array(
				'userid' => 'sh'.$ww->id,
				'name' => $this->name($ww),
				'department' => is_array($department) ? implode(';',$department) : $department,
				'position' =>  $this->positionName($ww),
				'mobile' =>  $ww->parent_phone,
			);

			$csv .= implode(',', $tmp)."\r\n";
		}

		$file = './assets/upload/tmp/batch_wx.csv';
		if(file_exists($file))
			unlink($file);

		file_put_contents($file, $csv);

		$media = $this->upload('file', $file);

		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={$this->access_token}",
			array('media_id' => $media->media_id), true
		);
	}
    public function batchUpdate($partner_id,$ww_contacts_array, $department) {
        if(!$ww_contacts_array)
            return false;

        $csv = "姓名,帐号,微信号,手机号,邮箱,所在部门,职位\r\n";
        foreach($ww_contacts_array as $ww) {
            $tmp = array(
                'name' => $this->name($ww),
                'userid' => 'sh'.$ww->id,
                'wx' => '',
                'mobile' =>  $ww->parent_phone,
                'email' => '',
                'department' => is_array($department[$ww->id]) ? implode(';',$department[$ww->id]) : $department[$ww->id],
                'position' =>  $this->positionName($ww),
            );

            $csv .= implode(',', $tmp)."\r\n";
        }
        $file = TMP_PATH.'/batch_wx_update_'.$partner_id.'.csv';
        $file = './assets/upload/tmp/batch_wx.csv';
        if(file_exists($file))
            unlink($file);

        file_put_contents($file, $csv);

        $media = $this->upload('file',$file);
        //var_dump($media);exit;
        if(!$media ||!$media->media_id){
            return false;
        }
        return $this->postResult(
            "https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token={$this->access_token}",
            array('media_id' => $media->media_id), true
        );
    }

	public function chat($params) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={$this->access_token}",
			$params, true
		);
	}

//	public function auth() {
//
//	}
//	public function send($params) {
//		//'crazyhorse', 'darkhorse_pu'
//		return $this->postResult(
//			"https://qyapi.weixin.qq.com/cgi-bin/chat/send?access_token={$this->access_token}",
//			array(
//				'receiver' => array('type' => 'single', 'id' => 'tester2'),
//				'sender' => 'crazyhorse',
//				'msgtype' => 'text',
//				'text' => array('content' => '测试'),
//			), true
//		);
//	}
}