<?php
class WxQyUser extends WxQyApp {

	public function getUserId($code) {
		return $this->getResult("https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token={$this->access_token}&code=".$code);
	}


	public function getOpenId($user_id) {
		return $this->postResult(
			"https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token={$this->access_token}",
			array('userid' => $user_id), true
		);
	}

    public  function getUserInfo($user_id){
        return $this->getResult(
            "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={$this->access_token}&userid={$user_id}"
        );
    }
}