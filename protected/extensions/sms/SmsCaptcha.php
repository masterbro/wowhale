﻿<?php
class SmsCaptcha {
    private $key;
    private $ttl;//两次发送间隔

    public function __construct($key, $ttl = 60) {
        $this->key = 'yl_sms_'.$key;
        $this->ttl = $ttl;
    }

    public function valid($code) {
        $cache = $this->getCache();
        if($cache === FALSE) throw new SmsCaptchaException('验证码失效，请重新发送');

        if($cache['error_times'] >= 4) {
            throw new SmsCaptchaException('验证码错误次数太多，请重新发送');
        }

        if(strtolower($code) != $cache['code']) {
            $this->validError();
            throw new SmsCaptchaException('验证码错误');
        }

        $this->deleteCache();
        return TRUE;
    }

    private function validError() {
        $cache = $this->getCache();
        if(!$cache) return FALSE;
        $value = array(
            'code' => $cache['code'],
            'send_at' => $cache['send_at'],
            'error_times' => $cache['error_times']+1,
        );
        Yii::app()->cache->set($this->key, $value, 1800);
    }

    private function setCache($code) {
        $value = array(
            'code' => $code,
            'send_at' => time(),
            'error_times' => 0,
        );
        Yii::app()->cache->set($this->key, $value, 1800);
    }

    private function getCache() {
        return Yii::app()->cache->get($this->key);
    }

    private function deleteCache() {
        Yii::app()->cache->delete($this->key);
    }

    public function send($mobile, $msg,$number_only=0) {
        $cache = $this->getCache();
        if( $cache && $cache['send_at'] && ((time() - $cache['send_at']) < $this->ttl) )
            throw new SmsCaptchaException("两次短信发送间隔时间不能低于{$this->ttl}秒");
        
        try{
            $code = $this->code(6, $number_only);
            SmsService::send( $mobile, sprintf($msg, $code), TRUE );
            $this->setCache($code);
        } catch(SmsServiceException $e) {
            Yii::log("sms send failed, error_code: {$e->getCode()}, error_msg: {$e->getMessage()}", CLogger::LEVEL_ERROR, 'sms.error');
            throw new SmsCaptchaException('短信发送失败，请稍候再试');
        }
    }

    private function code($len,$number_only=0) {
        $chars = array(
            '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
            'j', 'k', 'm', 'n', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x', 'y', 'z',
        );
        if($number_only){
            $chars = array(
                '0','1','2', '3', '4', '5', '6', '7', '8', '9'
            );
        }
        shuffle($chars);
        $chars = implode('',$chars);
        return substr($chars, 0, $len);
    }
}
