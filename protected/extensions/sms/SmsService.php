<?php
class SmsService {

    public static function send($mobile, $content, $throw_exception = FALSE){ 

        if( Yii::app()->params['evn'] == 'dev') {
            Yii::log("sms send to {$mobile}, content: {$content}", 'error', 'sms.debug');
            return TRUE;
        }

	    //$content .= "【娃娃营】";
	    $sms_config = Yii::app()->params['sms'];

	    $wsdl = "http://211.99.191.148/mms/services/info?wsdl";
	    $client = new SoapClient($wsdl);
	    $param = array(
		    'in0'=> $sms_config['account'],
		    'in1'=> $sms_config['password'],
		    'in2'=> $mobile,
		    'in3'=> urlencode(iconv('UTF-8', 'GBK', $content)),
		    'in4'=>'',
		    'in5'=>'1',
		    'in6'=>'',
		    'in7'=>'1',
		    'in8'=>'',
		    'in9'=>'4'
	    );
	    $ret = $client->sendSMS($param);

        if($throw_exception && $ret->out != 0) {
            throw new SmsServiceException('sms send error: '.iconv( 'GBK', 'UTF-8', urldecode($ret->out)));
        }

        return $ret->out == 0;
    }
}
