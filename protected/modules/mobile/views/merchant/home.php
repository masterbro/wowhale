<div id="owl-merchant-wrap">
	<div id="owl-merchant" class="owl-carousel">
		<?php $slider = $model->sliderArr();?>
		<?php foreach($slider as $k=>$s):?>
			<div>
				<img class="lazyOwl owl-img" style="" data-src="<?php echo HtmlHelper::image($s) ?>" />
			</div>
		<?php endforeach;?>
	</div>
</div>

<script>
	window.owlH = document.documentElement.clientWidth + 'px';
	document.querySelector('#owl-merchant').style.height = window.owlH;
	document.querySelector('#owl-merchant-wrap').style.height = window.owlH;

	var owlImgs = document.querySelectorAll('.owl-img');
	for (var i = 0, len = owlImgs.length; i < len; i++) {
		owlImgs[i].style.height = window.owlH;
		owlImgs[i].style.width = window.owlH;
	}


</script>

<!--<div class="p10 ww-content">-->
<!--	<h2 class="ww-merchant-info-title">学校简介</h2>-->
<!--</div>-->
<div class="p10">

	<h1 class="ww-merchant-title"><?php echo $model->title ?><?php if(Yii::app()->params['evn'] == 'test'):?><span class="text-danger">[DEMO]</span> <?php endif;?></h1>
	<!--	<div class="rich-text-wrap">-->
	<div class="rich-text">
		<?php echo $model->content ?>
		<!--		</div>-->
	</div>

</div>
