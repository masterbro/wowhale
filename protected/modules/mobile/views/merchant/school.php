

<div id="product-list" class="p10 product-list">
	<h3 class="ww-title">缴费项目</h3>

	<?php foreach($products as $p):?>
		<?php $attr = $p->attrData(); ?>
		<div class="item" id="pd-<?php echo $p->id ?>" data-pid="<?php echo $p->id ?>" data-price="<?php echo $p->price ?>">
			<h4><span><?php if($p->original_price) echo '<del>￥'.CommonHelper::price($p->original_price).'</del>' ?>￥<em class="price pd-price-<?php echo $p->id ?>"><?php echo CommonHelper::price($p->price) ?></em></span><?php echo $p->name ?></h4>
			<?php if($attr):?>
				<?php foreach($attr as $a):?>
					<div class="attr">
						<div class="group" data-pid="<?php echo $p->id ?>">
							<h5><?php echo $a['parent']->name ?></h5>
							<?php foreach($a['sub'] as $sa):?>
								<i data-attr-id="<?php echo $sa->id ?>" data-attr-price="<?php echo $sa->price ?>"><?php echo $sa->name ?></i>
							<?php endforeach;?>
						</div>

					</div>
				<?php endforeach;?>
			<?php endif;?>
		</div>
	<?php endforeach;?>
</div>

<div class="p10 customer-info">
	<h3 class="ww-title">个人信息</h3>

	<div class="field">
		<label>学生姓名：<span>*</span></label>
		<input type="text" name="student_name" class="ipt" value="<?php echo $this->current_user->student_name ?>" />
	</div>
	<div class="field">
		<label>学生班级：<span>*</span></label>
		<input type="text" name="student_grade" class="ipt" value="<?php echo $this->current_user->student_grade ?>" />
	</div>
	<div class="field">
		<label>家长姓名：<span>*</span></label>
		<input type="text" name="parent_name" class="ipt" value="<?php echo $this->current_user->parent_name ?>" />
	</div>
	<div class="field">
		<label>联系电话：<span>*</span></label>
		<input type="text" name="tel" class="ipt" value="<?php echo $this->current_user->tel ?>" />
	</div>
	<div class="field">
		<label>生日：<span>*</span></label>
		<input type="date" name="birth" class="ipt"  value="<?php echo $this->current_user->birth ?>" />
	</div>

	<div class="field">
		<label>订单备注：</label>
		<textarea name="remark"></textarea>
	</div>
</div>

<div style="height: 60px;"></div>
<div class="footer-panel">
	<div class="col"><a href="tel:<?php echo $model->contact_tel ?>" class="tel"><?php echo $model->contact_tel ?></a></div><div class="col pay-wrap">
		<a href="#product-list" class="pay btn" id="ww-pay">立即缴费</a>
		<a href="javascript:;" class="cash btn hidden" id="ww-cash" data-paying="false">付款<span>￥<em class="ww-total">0.00</em></span></a>
	</div>
</div>