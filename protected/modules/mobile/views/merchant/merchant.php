<div class="merchant-products-list">
	<?php foreach($products as $p):?>
		<div class="item" onclick="window.location.href='<?php echo $this->createUrl('/product/view?id='.$p->id.'&showwxpaytitle=1') ?>'">
			<img src="<?php echo $p->image ? HtmlHelper::image($p->image) : HtmlHelper::assets('images/no_pic.png') ?>" />
			<div class="ww-title-wrap">
				<div class="price">
					<span><?php if($p->original_price) echo '<del>￥'.CommonHelper::price($p->original_price).'</del>' ?>￥<em class="price"><?php echo CommonHelper::price($p->price) ?></em></span>
				</div>
				<div class="title">
					<h2><?php echo $p->name ?></h2>
				</div>

			</div>
		</div>
	<?php endforeach;?>
</div>