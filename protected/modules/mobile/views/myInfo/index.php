<style>
    body{background-color: #F2F2F2;}
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;
    }}
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #f2f2f2;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signUse.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fff; padding-bottom: 15px;margin-bottom: 5px;border-bottom: 1px solid #ddd; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}

    .update_student{padding:10px;background-color: #22ac38;color:#fff;border-radius: 5px;}
    .p10{background-color: #fff;border-bottom: 1px solid #ddd;margin-bottom: 5px;}
    .ww-title{border: 0;}
    .p10_teacher{background-color: #fff;border-bottom: 1px solid #ddd;padding-top: 1px; }
    .order-item-2{padding:10px;border-top: solid 1px #F2F2F2;padding-left: 15px;height: 25px;}
    .p10_teacher a{height: 25px;color:#999;}
    .order-item-2 p{float: left;}
    .order-item-2 img{float: right;width: 8px;padding-top:5px; }
</style>
<div class="head_img_wrap">
    <a href="<?php echo $this->createUrl('/myInfo/index',array('partner_id'=>$this->partner_id));?>"><img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon"></a>
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>
<!--<div class="p10">

<div class="order-item">
    <h3 class="ww-title">
        信息修改
    </h3>
    <form id="contact_form">
        <input type="hidden" name="partner_id" value="<?php echo $this->partner_id ?>">
        <input type="hidden" name="contact_id" value="<?php echo $contact->id ?>">
        <div class="field">
            <label>姓名：<span>*</span></label>
            <input type="text"  autocomplete="off" name="parent_name" id="parent_name"  class="ipt parent_name"  data-name="姓名" value="<?php echo $contact->parent_name;?>" />
        </div>
        <div class="field">
            <label>手机号：<span>*</span></label>
            <input type="text"  autocomplete="off" name="parent_phone" id="parent_phone"  class="ipt parent_phone"  data-name="姓名" value="<?php echo $contact->parent_phone;?>" />
            <div class="info">修改之后可用此手机号绑定的微信关注企业号</div>
        </div>
        <div class="field">
            <a href="javascript:void(0)" class="pay btn" id="ww-info" data-loading="0" style="padding:10px;">修改信息</a>
        </div>
    </form>
</div>
</div>-->



<?php if($students):?>
<div class="p10">
    <!--<h2 class="page-title ww-title">我的宝宝 </h2>-->
    <div class="order-item">
        <p>爷爷奶奶也想加入？修改信息即可！</p><!--如果要让更多的宝宝家长关注我们，请修改信息增加联系人-->
    </div>
    <?php foreach($students as $k=>$v):?>
        <div class="order-item">
            <p><a class="btn update_student" href="<?php echo $this->createUrl('/myInfo/view',array('id'=>$v->id, 'partner_id'=>$this->partner_id)) ?>">修改 <?php echo $v->name ?> 信息</a></p>
        </div>
    <?php endforeach;?>
</div>
<?php endif;?>

<?php if(count($grade)):?>
<div class="p10_teacher">
    <h2 class="page-title ww-title" style="margin:10px;"> 我的班级</h2>
    <?php foreach($grade as $l):?>
        <a href="<?php echo $this->createUrl('/myInfo/list',array('grade_id'=>$l->id, 'partner_id'=>$this->partner_id)) ?>">
            <div class="order-item-2">
                    <p><?php echo $l->name ?></p>
                    <img src="<?php echo HtmlHelper::assets('images/mobile/album/img-gray.png') ?>">
            </div>
        </a>
    <?php endforeach;?>
</div>
<?php endif;?>


<script>
    $(function(){
        $('#ww-info').click(function(){
            var _mobile = $('#parent_phone').val()
            if(!_mobile){
                alert('请输入手机号');
                return;
            }
            var _name = $('#parent_name').val()
            if(!_name){
                alert('请输入姓名');
                return;
            }
            var _button = $(this);
            if(_button.attr('data-loading')=='1'){
                return;
            }
            _button.attr('data-loading','1');
            _button.text('更新中.');
            $.post('/myInfo/updateContact',$('#contact_form').serialize(),function(data){
                alert(data.msg);
                _button.attr('data-loading',0)
                _button.text('修改信息');
            },'json')
        })
    })
</script>