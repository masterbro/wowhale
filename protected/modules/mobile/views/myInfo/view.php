<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;
    }}
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #f2f2f2;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signUse.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}


</style>
<div class="head_img_wrap">
    <a href="<?php echo $this->createUrl('/myInfo/index',array('partner_id'=>$this->partner_id));?>"><img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon"></a>
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>
<div class="p10">
    <div class="order-item">
        <form id="contact_form">
        <h3 class="ww-title">
            信息修改
        </h3>
            <input type="hidden" name="partner_id" value="<?php echo $this->partner_id ?>">
            <input type="hidden" name="contact_id" value="<?php echo $contact->id ?>">
            <input type="hidden" name="student_id" value="<?php echo $student->id ?>">
            <div class="field">
                <label>姓名：<span>*</span></label>
                <input type="text"  autocomplete="off" name="name" id="name"  class="ipt parent_name"  data-name="姓名" value="<?php echo $student->name;?>" />
            </div>
            <div class="field">
                <label>生日：<span>*</span></label>
                <input type="text"  autocomplete="off" name="birth" id="birth"  class="ipt parent_name"  data-name="生日" value="<?php echo $student->birth;?>" />
            </div>
            <div class="field">
                <label>紧急联系人：<span>*</span></label>
                <input type="text"  autocomplete="off" name="emergency_contact" id="emergency_contact"  class="ipt parent_phone"  data-name="紧急联系人" value="<?php echo $student->emergency_contact;?>" />
            </div>
            <div class="field">
                <label>紧急联系人手机号：<span>*</span></label>
                <input type="text"  autocomplete="off" name="emergency_phone" id="emergency_phone"  class="ipt parent_phone"  data-name="紧急联系人手机号" value="<?php echo $student->emergency_phone;?>" />
            </div>
            <div class="field">
                <label>户口所在地：<span>*</span></label>
                <input type="text"  autocomplete="off" name="register_address" id="register_address"  class="ipt parent_phone"  data-name="户口所在地" value="<?php echo $student->register_address;?>" />
            </div>
            <div class="field">
                <label>家庭住址：<span>*</span></label>
                <input type="text"  autocomplete="off" name="address" id="address"  class="ipt parent_phone"  data-name="家庭住址" value="<?php echo $student->address;?>" />
            </div>
            <div class="field">
                <label>身份证号：<span>*</span></label>
                <input type="text"  autocomplete="off" name="id_num" id="id_num"  class="ipt parent_phone"  data-name="身份证号" value="<?php echo $student->id_num;?>" />
            </div>
            <h3 class="ww-title"> 宝宝家长</h3>
            <?php $parent_count=0;if(count($parents)):?>
                <?php foreach($parents as $k=>$l):$parent_count++;?>
                    <div class="field">
                        <label>家长<?php echo ($parent_count);?>姓名：<span>*</span></label>
                        <input type="text"  autocomplete="off" name="parent_name[]" id="parent_name"  class="ipt parent_name"  data-name="家长姓名" value="<?php echo $l->parent_name ?>" />
                    </div>
                    <div class="field">
                        <label>家长<?php echo ($parent_count);?>手机号：<span>*</span>清空手机号为删除此家长</label>
                        <input type="text"  autocomplete="off" name="parent_phone[]" id="parent_phone"  class="ipt parent_name"  data-name="家长手机号" value="<?php echo $l->parent_phone ?>" />
                    </div>
                <?php endforeach;?>
            <?php endif;?>
            <div class="field">
                <label>家长<?php echo ($parent_count+1);?>姓名：<span>*</span></label>
                <input type="text"  autocomplete="off" name="parent_name[]"  class="ipt parent_name"  data-name="家长姓名" value="" />
            </div>
            <div class="field">
                <label>家长<?php echo ($parent_count+1);?>手机号：<span>*</span>清空手机号为删除此家长</label>
                <input type="text"  autocomplete="off" name="parent_phone[]"   class="ipt parent_phone"  data-name="家长手机号" value="" />
            </div>
            <div class="field">
                <label><a href="javascript:;" style="font-size: 32px;" id="addNewParent">+</a></label>
            </div>

            <div class="field">
                <a href="javascript:void(0)" class="pay btn" id="ww-info" data-loading="0" style="padding:10px;">修改信息</a>
            </div>
        </form>
    </div>
</div>
<!--<div class="p10">
    <h3 class="ww-title parents"> 宝宝家长</h3>
    <?php if(count($parents)):?>
        <?php foreach($parents as $l):?>
            <div class="order-item">
                <p><span>&nbsp;<?php echo $l->parent_name ?></span><span style="float: right"><?php echo $l->parent_phone ?></span></p>
            </div>
        <?php endforeach;?>
    <?php endif;?>
</div>
<div class="p10">
    <h3 class="ww-title"> 宝宝家长</h3>
    <form id="new_parent_form">
        <input type="hidden" name="partner_id" value="<?php echo $this->partner_id ?>">
        <input type="hidden" name="student_id" value="<?php echo $student->id ?>">
        <input type="hidden" name="contact_id" value="<?php echo $contact->id ?>">
        <?php if(count($parents)):?>
            <?php foreach($parents as $k=>$l):?>
                <div class="field">
                    <label>家长<?php echo ($k+1);?>姓名：<span>*</span></label>
                    <input type="text"  autocomplete="off" name="parent_name[]" id="parent_name"  class="ipt parent_name"  data-name="家长姓名" value="<?php echo $l->parent_name ?>" />
                </div>
                <div class="field">
                    <label>家长<?php echo ($k+1);?>手机号：<span>*</span></label>
                    <input type="text"  autocomplete="off" name="parent_phone[]" id="parent_phone"  class="ipt parent_name"  data-name="家长手机号" value="<?php echo $l->parent_phone ?>" />
                </div>
            <?php endforeach;?>
        <?php endif;?>
        <div class="field">
            <label>新增家长姓名：<span>*</span></label>
            <input type="text"  autocomplete="off" name="parent_name[]" id="parent_name"  class="ipt parent_name"  data-name="家长姓名" value="" />
        </div>
        <div class="field">
            <label>新增家长手机号：<span>*</span></label>
            <input type="text"  autocomplete="off" name="parent_phone[]" id="parent_phone"  class="ipt parent_name"  data-name="家长手机号" value="" />
        </div>
        <div class="field">
            <a href="javascript:void(0)" class="pay btn" id="ww-add-parent" data-loading="0" style="padding:10px;">添加家长</a>
        </div>
    </form>
</div>
-->
<script>
    $(function(){
        $('#addNewParent').click(function(){
            var _parent = $(this).parent().parent()
            var _str = '<div class="field">';
            _str +='    <label>家长姓名：<span>*</span></label>';
            _str +='<input type="text"  autocomplete="off" name="parent_name[]"  class="ipt parent_name"  data-name="家长姓名" value="" />';
            _str +='</div>';
            _str +='<div class="field">';
            _str +='<label>家长手机号：<span>*</span>清空手机号为删除此家长</label>';
            _str +='<input type="text"  autocomplete="off" name="parent_phone[]"   class="ipt parent_phone"  data-name="家长手机号" value="" />';
            _str +='</div>';
            _parent.before(_str);
        })
        $('#ww-info').click(function(){
            var _mobile = $('#emergency_phone').val()
            if(!_mobile){
                alert('请输入紧急联系人手机号');
                return;
            }
            var _name = $('#emergency_contact').val()
            if(!_name){
                alert('请输入紧急联系人姓名');
                return;
            }

            var _phones = $('.parent_phone');
            var _no_phones = true;
            $.each(_phones ,function(){
                if($(this).val()){
                    _no_phones = false;
                }
            })
            if(_no_phones){
                alert('请至少保留一个家长');
                return;
            }
            var _button = $(this);
            if(_button.attr('data-loading')=='1'){
                return;
            }
            _button.attr('data-loading','1');
            _button.text('更新中.');
            $.post('/myInfo/updateStudent',$('#contact_form').serialize(),function(data){
                alert(data.msg);
                _button.attr('data-loading',0)
                _button.text('修改信息');
                if(!data.error){
                    document.location.reload();
                }
            },'json')
        })
        $('#ww-add-parent').click(function(){
            var _mobile = $('#parent_phone').val()
            if(!_mobile){
                alert('请输入家长手机号');
                return;
            }
            var _name = $('#parent_name').val()
            if(!_name){
                alert('请输入家长姓名');
                return;
            }
            var _button1 = $(this);
            if(_button1.attr('data-loading')=='1'){
                return;
            }
            _button1.attr('data-loading','1');
            _button1.text('更新中.');
            var _str = '<div class="order-item">';
            _str +=     '<p><span>&nbsp;'+$('#parent_name').val()+'</span><span style="float: right">'+$('#parent_phone').val()+'</span></p>';
            _str +=     '</div>';
            $.post('/myInfo/addParent',$('#new_parent_form').serialize(),function(data){
                alert(data.msg);
                _button1.attr('data-loading',0)
                _button1.text('添加家长');

                if(!data.error){
                    $('.ww-title.parents').parent().append(_str);
                    $('#parent_phone').val('');
                    $('#parent_name').val('');
                }
            },'json')
        })

    })
    window.lastPageY = false;
    $(document).ready(function(){

        function stopScrolling( touchEvent ) {
            //console.info(touchEvent.changedTouches[0].screenX,touchEvent.changedTouches[0].screenY,touchEvent.changedTouches[0].pageX,touchEvent.changedTouches[0].pageY,touchEvent.changedTouches[0].clientX,touchEvent.changedTouches[0].clientY)
            if(touchEvent.changedTouches[0].pageY-touchEvent.changedTouches[0].clientY<=0 && window.lastPageY!==false&&window.lastPageY<touchEvent.changedTouches[0].pageY ){
                touchEvent.preventDefault();
                //console.info(touchEvent.changedTouches[0].screenY,touchEvent.changedTouches[0].pageY,touchEvent.changedTouches[0].clientY)

                //console.info(touchEvent.changedTouches[0].pageY-touchEvent.changedTouches[0].clientY)

                //console.info('error')
            }
            //window.lastPageY = touchEvent.changedTouches[0].pageY;
            //touchEvent.preventDefault();
        }
        function startScrolling( touchEvent ) {
            /*//console.info(touchEvent.changedTouches[0].screenX,touchEvent.changedTouches[0].screenY,touchEvent.changedTouches[0].pageX,touchEvent.changedTouches[0].pageY,touchEvent.changedTouches[0].clientX,touchEvent.changedTouches[0].clientY)
            if(touchEvent.changedTouches[0].pageY-touchEvent.changedTouches[0].clientY<=0 && window.lastPageY!==false&&window.lastPageY<touchEvent.changedTouches[0].pageY ){
                touchEvent.preventDefault();
                //console.info(touchEvent.changedTouches[0].screenY,touchEvent.changedTouches[0].pageY,touchEvent.changedTouches[0].clientY)

                //console.info(touchEvent.changedTouches[0].pageY-touchEvent.changedTouches[0].clientY)

                //console.info('error')
            }*/
            window.lastPageY = touchEvent.changedTouches[0].pageY;
            //touchEvent.preventDefault();
        }
        document.addEventListener( 'touchstart' , startScrolling , false );
        document.addEventListener( 'touchmove' , stopScrolling , false );
    });
</script>