<style>
    .chk_1,.chk_2,.chk_3,.chk_4 {
        display: none;
    }

    /*******STYLE 1*******/
    .chk_1 + label {
        background-color: #FFF;
        border: 1px solid #C1CACA;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
        padding: 9px;
        border-radius: 5px;
        display: inline-block;
        position: relative;
        margin-right: 30px;
    }
    .chk_1 + label:active {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    .chk_1:checked + label {
        background-color: #ECF2F7;
        border: 1px solid #92A1AC;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
        color: #243441;
    }

    .chk_1:checked + label:after {
        content: '\2714';
        position: absolute;
        top: 0px;
        left: 0px;
        color: #758794;
        width: 100%;
        text-align: center;
        font-size: 1.4em;
        padding: 1px 0 0 0;
        vertical-align: text-top;
    }
    .checkbox_label{float:right;margin-right:5px !important}
    .contacts_list .head_icon{width:30px;height: 30px;margin-right: 10px;}
    .contacts_list .students .head_icon {width: 64px; height: 64px; border-radius: 32px;background-color: #ddd; }
    .students{float: left;width:25%;text-align: center;}
    .students a{color: inherit}
    .contacts_list .head_icon{text-align: center;margin: 0 auto;margin-top: 15px;}
    .contacts_list .head_icon img{width:64px;height: 64px}
    .contacts_list .student_name{margin-top: 5px;}
</style>
<div class="p10_0 contacts_list">
	<h3 class="page-title" style="padding-left: 10px"><?php echo $grade->name;?>学生</h3>
    <?php /*foreach($students as $s):?>
        <div class="contacts-item">
            <div class="contact">
                <a href="<?php echo $this->createUrl('/myInfo/view',array('id'=>$s->id, 'partner_id'=>$this->partner_id)) ?>"><p>修改 <?php echo $s->name ?> 档案 </p></a>
            </div>
        </div>
    <?php endforeach;*/?>
    <?php foreach($students as $s):?>
        <div class="students">
            <a href="<?php echo $this->createUrl('/myInfo/view',array('id'=>$s->id, 'partner_id'=>$this->partner_id)) ?>">
                <div >
                    <img class="head_icon" src="<?php echo HtmlHelper::get_head_img($app,$s->parents[0]?$s->parents[0]->id:'');?>"/>
                </div>
                <p class="student_name"> <?php echo $s->name ?>  </p>
            </a>

        </div>
    <?php endforeach;?>
	<?php /*foreach($students as $s):?>
		<div class="contacts-item">
            <a href="<?php echo $this->createUrl('/myInfo/view',array('id'=>$s->id, 'partner_id'=>$this->partner_id)) ?>"><p><?php echo $s->name ?> </p></a>
			<div class="constants-sub">
				<?php foreach($s->parents as $l):?>
				<div class="contact">
                    <img src="<?php echo HtmlHelper::get_head_img($app,$l->id);?>" class="head_icon"/>
                    <!--<input type="checkbox" value="<?php echo $l->id ?>" name="users[]" id="checkbox_sp<?php echo $l->id;?>" class="chk_1"/>
                    <label for="checkbox_sp<?php echo $l->id;?>" class="checkbox_label"></label>-->
					<?php echo $l->parent_name ?>
					<a href="tel:<?php echo $l->parent_phone ?>" style="float: right"><?php echo $l->parent_phone ?></a>
				</div>
				<?php endforeach;?>
			</div>

		</div>
	<?php endforeach;*/?>
</div>
<div style="height: 50px;"></div>
<!--
<div class="contacts-chat-bar">
	<a href="javascript:;" id="create-chat">发起会话</a>
</div>
-->

<script>
	$(function(){
	});
</script>