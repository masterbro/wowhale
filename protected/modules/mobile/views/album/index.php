<style>
    .ui-listview.merchant_view {
        /* background: #e94040; */
        margin-top: 0;
        border-radius: 0;
        border-top: 0;
        border-bottom: 0;
        padding: 6px;
        background: #F2F2F2;
    }
    .ui-listview.merchant_view li.top-radius, .ui-listview.merchant_view a.top-radius {
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }
    .ui-listview.merchant_view li {
        border-left: none;
        border-right: none;
        /* border-bottom: 1px solid #ccc; */
    }
    .ui-listview>li {
        display: block;
        position: relative;
        overflow: visible;
    }
    .ui-listview, .ui-listview>li {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .ui-listview.merchant_view li.top-radius, .ui-listview.merchant_view a.top-radius {
        border-top-left-radius: 4px;
        border-top-right-radius: 4px;
    }
    .ui-listview>.ui-li-has-thumb>.ui-btn, .ui-listview>.ui-li-static.ui-li-has-thumb {
        min-height: 80px;
        padding-left: 85px;
    }
    img, object, embed {
        max-width: 100%;
        border: 0 none;
        vertical-align: middle;
    }
    .ui-listview .ui-li-has-thumb>img:first-child, .ui-listview .ui-li-has-thumb>.ui-btn>img:first-child, .ui-listview .ui-li-has-thumb .ui-li-thumb {
        position: absolute;
        left: 0;
        top: 0;
        /*max-height: 110px;*/
        /*max-width: 110px;*/
        /*padding-top:10px;*/
        /*padding-left:25px;*/
        /*padding-bottom:10px;*/
        padding-top:5px;
        padding-left:5px;
        /*padding-left:25px;*/
    }
    .ui-listview>li h2 {
        padding-top:10px;
        font-size: 18px;
        font-weight: 700;
        display: block;
        /*margin: 15px 0;*/
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        color:#505050;
    }
    .merchant_view.ui-listview>li p {
        /*margin: 10px 0;*/
        font-size: 14px;
        color:#aaaaaa;
    }
    .ui-listview>li p {
        font-size: 14px;
        font-weight: 400;
        display: block;
        /*margin: 10px 0;*/
        margin-top: 0;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    }
    .ui-btn-icon-right:after {
        right: .8625em;
    }
    .ui-btn-icon-notext:after, .ui-btn-icon-left:after, .ui-btn-icon-right:after {
        top: 50%;
        margin-top: -22px;
    }
    .ui-btn-icon-left:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after, .ui-btn-icon-bottom:after, .ui-btn-icon-notext:after {
        content: "";
        position: absolute;
        display: block;
        width: 33px;
        height: 33px;
    }
    .ui-btn-icon-left:after, .ui-btn-icon-right:after, .ui-btn-icon-top:after, .ui-btn-icon-bottom:after, .ui-btn-icon-notext:after {
        background-color: white;
        background-color: rgba(255,255,255,.3);
        background-position: center center;
        background-repeat: no-repeat;
        -webkit-border-radius: 1em;
        border-radius: 1em;
    }
    .ui-icon-carat-r:after {
        /*background-image: url("data:image/svg+xml;charset=US-ASCII,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20width%3D%2228px%22%20height%3D%2228px%22%20viewBox%3D%220%200%2014%2014%22%20style%3D%22enable-background%3Anew%200%200%2028%2028%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cpolygon%20style%3D%22fill%3A%23000000%3B%22%20points%3D%223.404%2C2.051%208.354%2C7%203.404%2C11.95%205.525%2C14.07%2012.596%2C7%205.525%2C-0.071%20%22%2F%3E%3C%2Fsvg%3E");*/
        background-image: url("<?php echo HtmlHelper::assets('images/mobile/album/img-gray.png') ?>");
        background-size: 30%;
        padding-top: 12px;
        right:0;
    }
    .ui-listview>.ui-li-static, .ui-listview>.ui-li-divider, .ui-listview>li>a.ui-btn {
        margin: 0;
        display: block;
        position: relative;
        text-align: left;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        background: #fafafa;
        border-bottom: 1px solid #d3d3d3;
    }
    .merchant_view .ui-li-has-thumb.albums{height:81px;}
</style>
<ul data-inset="true" class="merchant_view ui-listview ui-listview-inset ui-corner-all ui-shadow"
    style="box-shadow: none;min-height: 100%;" >
<?php if($grade):?>
    <?php foreach ($grade as $key => $value) :?>
        <li  class="ui-li-has-thumb   albums">
            <a
                href="<?php echo $this->createUrl('/album/viewGrade',array('grade_id'=>$value->id,'partner_id'=>$this->partner_id));?>"
                class="ui-btn ui-btn-icon-right ui-icon-carat-r "  >
                        <!-- <img
                        src="--><?php //echo $value->name; ?><!--" width="80px"
                        style="width: 80px; padding-top: 19px; padding-left: 10px;" />-->
                <img
                    src="<?php echo  HtmlHelper::wx_image($value->lastAlbumPic) ;?>"
                    style="width: 70px; height:70px" />
                <h2>
                    <?php echo $value->name ;?>相册
                </h2>
                <p>
                    共<?php echo $value->ablumCount;?>张
                </p>
                <p>
                    更新于<?php echo $value->lastAlbumTime?date('Y年m月d日',$value->lastAlbumTime):'--';?>
                </p>
            </a>
        </li>
    <?php endforeach;?>
<?php endif;?>
</ul>