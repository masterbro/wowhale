<link rel="stylesheet" href="<?php echo HtmlHelper::assets('baguettebox/css/baguettebox.min.css?v='.Yii::app()->params['assets_version']) ?>">

<style>
    .baguetteBoxOne.gallery_album a {
        width: 23%;
        display: inline-block;
        /*overflow: hidden;*/
        background-size:cover !important;
        -webkit-background-size:cover !important;
        /*margin: 10px 6px;*/
        /*background:#f2f2f2*/
        margin-bottom: 0;
    }
    .baguetteBoxOne.gallery_album a p {
        background-size:cover !important;
        -webkit-background-size:cover !important;
    }
    .baguetteBoxOne.gallery_album {
        width: 100%;
        margin: 0 auto;
        text-align: center;
        /*background: #fafafa;*/
        border-bottom: 1px solid #eee;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .baguetteBoxOne.gallery_album a p {
        max-height: 100%;
        width: 100%;
        max-width: 100%;
        background-image:url(<?php echo HtmlHelper::assets('images/logo.ico') ?>) ;
    }
    .baguetteBoxOne.gallery_album .date{text-align: left;padding:5px 0 5px 12px;font-size: 18px;font-weight: 600;}
    .album_wrap{min-height: 100%;/*background: #F2F2F2;*/margin-top: 5px;}
    .baguetteBoxOne.gallery_album a.vis{visibility: hidden}

</style>
<div class="album_wrap">
    <?php $list_index = 0;if($list):?>
    <?php foreach($list as $k=>$v):?>
    <div class="baguetteBoxOne gallery_album">
        <div class="date"><?php echo date('m月d日',$v->create_time);?></div>
        <?php $day_count = 0;foreach($v->sameDayAlbums as $kk=>$vv):?>
            <?php if($kk%8==0):?>
            <div class="album_img_wrap album_img_wrap_<?php echo $list_index++;?>">
            <?php endif;?>
            <a class="loading" <?php if($kk>7):?>style="display:none"<?php endif;?> href="<?php echo  HtmlHelper::wx_image($vv->pic_url);?>?.jpg" data-caption="<?php echo $vv->title;?>" onclick="return false;">
                <p data-original="<?php echo HtmlHelper::wx_image($vv->pic_url);?>" class="lazyOwl lazy "></p>
            </a>
            <?php if($kk%8==7):?>
            </div>
            <?php endif;?>
        <?php $day_count++;endforeach;?>
        <?php $day_4_ocunt = ($day_count&&($day_count%4!=0) )?(4-$day_count%4):0;?>
        <?php for($i=0;$i<$day_4_ocunt;$i++):?>
        <a href="javascript:void(0)" <?php if($kk>7):?>style="display:none"<?php endif;?>  class="vis">
            <img  >
        </a>
        <?php endfor;?>
        <?php if($kk%8!=7):?>
            </div>
        <?php endif;?>
        <?php if($day_count>8&&$kk>7):?>
        <div style="clear: both">
            <a href="javascript:;" class="loadMore" style="clear: both"><img src="<?php echo HtmlHelper::assets('images/mobile/album/img-down.png') ?>" style="width: 17px;height: 13px;"></a>
        </div>
        <?php endif;?>
    </div>
    <?php endforeach;?>
    <?php else:?>
        <p class="alert alert-success">当前还没有任何照片哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>
    <?php endif;?>
</div>
<nav style="text-align: center">
    <?php
    $this->widget('CLinkPager',array(
            'header'=>'',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '末页',
            'prevPageLabel' => '上一页',
            'nextPageLabel' => '下一页',
            'pages' => $pager,
            'cssFile'=>false,
            'maxButtonCount'=>3,
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'htmlOptions' => array(
                'class' => 'pagination'
            ),
        )
    );
    ?>
</nav>
<script type="text/javascript" src="<?php echo HtmlHelper::assets('baguettebox/js/baguettebox.min.js?v='.Yii::app()->params['assets_version']) ?>"></script>
<script type="text/javascript" src="<?php echo HtmlHelper::assets('js/jquery.lazyload.js?v='.Yii::app()->params['assets_version']) ?>"></script>

<script>
    var loading_length= 0;
    var srcs = {};
    window.initBaguetteBox = false;
    $(function(){
        var _width = $('.baguetteBoxOne.gallery_album a').width();
        $('.album_wrap').append('<style>.baguetteBoxOne.gallery_album a p{width:'+_width+'px;height:'+_width+'px}</style>')
        $('.album_wrap').append('<style>.baguetteBoxOne.gallery_album a.loading p{width:'+_width+'px;height:'+_width+'px}</style>')

        $('.loadMore').click(function(){
            var _parent = $(this).parent().parent();
            var _img = $('a:hidden:lt(8)',_parent);
            _img.show();
            $(document).trigger('scroll');
            var _img = $('a:hidden:lt(8)',_parent);
            if(_img.length>0){

            }
            else{
                $(this).hide();
            }
        })

        $('.album_img_wrap a p').click(function(){
            var _wrap = $(this).parent().parent();
            try{
                baguetteBox.destroy();
            }
            catch(e){

            }
            //console.info(_wrap.attr('class'))
            baguetteBox.run('.'+_wrap.attr('class').replace('album_img_wrap ',''), {
                animation: 'fadeIn'
            });
            $(this).parent().trigger('click')

        })
        $("p.lazy").lazyload({placeholder : "<?php echo HtmlHelper::assets('images/logo.ico') ?>",'load':function(){
            var _self = $(this);
            var _parent = _self.parent();
            _parent.removeClass('loading');
            //_parent.css('backgroundImage','url('+_src+')');
            //_parent.css('backgroundSize','cover');
        }
        })
    })
    function showBaguetteBox(){
        if(!window.initBaguetteBox){
            window.initBaguetteBox = true;
            baguetteBox.run('.album_img_wrap', {
                animation: 'fadeIn'
            });
        }

    }
</script>
<?php
$config = WechatConfig::getSingleInstance(Yii::app()->params['wx_payment_qy']['wechat_config_id']);
if($config){
    $app = new WxQyApp($config);
    $signPackage = $app->getSignPackage();
}
?>
<?php if($signPackage):?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
    var _needBaguetteBox = false;
    if(typeof(wx)=='undefined' || !wx){
        //showBaguetteBox();
    }
    else{
        wx.config({
            debug: false,
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp:  <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
                'onMenuShareAppMessage',
                'onMenuShareTimeline',
                'onMenuShareWeibo',
                'onMenuShareQQ',
                'hideMenuItems',
                'showMenuItems',
                'hideAllNonBaseMenuItem',
                'showAllNonBaseMenuItem',
                'hideOptionMenu',
                'showOptionMenu',
                'previewImage'
            ]
        });
        //showBaguetteBox();
        function get_img(){
            try{
                var _img = $('img:eq(1)').attr('src');
                if(!_img){
                    _img = $('img:eq(0)').attr('src');
                }
                return _img;
            }
            catch(e){
                return '<?php echo $this->createAbsoluteUrl('/assets/images/island_logo.png') ?>';
            }

        }
        wx.ready(function () {
            //showBaguetteBox();
            $('.baguetteBoxOne.gallery_album a img').click(function(){
                var _img_url = $(this).parent().attr('href');
                //alert(_img_url);
                var _imgs = $(this).parent().parent().find('a');
                var _hrefs = [];
                $.each(_imgs,function(k,v){
                    var _href=$.trim($(v).attr('href'));
                    if(_href && _href!='javascript:void(0)'){
                        _hrefs.push(_href);
                    }

                })
                wx.onMenuShareTimeline({
                    title: '<?php echo $this->partner->name;?>', // 分享标题
                    link: '<?php echo Yii::app()->params['mobile_base'];?>/albums/?partner_id=<?php echo $this->partner_id;?>', // 分享链接
                    imgUrl: _img_url, // 分享图标
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                });
                wx.onMenuShareAppMessage({
                    title: '<?php echo $this->partner->name;?>', // 分享标题
                    desc: document.title, // 分享描述
                    link: '<?php echo Yii::app()->params['mobile_base'];?>/albums/?partner_id=<?php echo $this->partner_id;?>', // 分享链接
                    imgUrl: _img_url, // 分享图标
                    type: '', // 分享类型,music、video或link，不填默认为link
                    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                });
                /*wx.previewImage({
                    current: $(this).attr('href'), // 当前显示图片的http链接
                    urls: _hrefs, // 需要预览的图片http链接列表
                    success: function (res) {
                        //alert('success' + JSON.stringify(res))
                    },
                    error :function(res){
                        //alert('error' + JSON.stringify(res))
                    }
                });
                */
            })
            wx.onMenuShareTimeline({
                title: '<?php echo $this->partner->name;?>', // 分享标题
                link: '<?php echo Yii::app()->params['mobile_base'];?>/albums/?partner_id=<?php echo $this->partner_id;?>', // 分享链接
                imgUrl: get_img(), // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
            wx.onMenuShareAppMessage({
                title: '<?php echo $this->partner->name;?>', // 分享标题
                desc: document.title, // 分享描述
                link: '<?php echo Yii::app()->params['mobile_base'];?>/albums/?partner_id=<?php echo $this->partner_id;?>', // 分享链接
                imgUrl: get_img(), // 分享图标
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        });
        wx.error(function(res){
            //showBaguetteBox();
        });
    }

</script>
<?php else:?>
<script>
    //showBaguetteBox()
</script>
<?php endif;?>
