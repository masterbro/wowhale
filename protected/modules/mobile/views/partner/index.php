<?php if(count($slider)):?>
	<div id="owl-merchant-wrap">
		<div id="owl-merchant" class="owl-carousel">
			<?php foreach($slider as $k=>$s):?>
				<div>
					<a href="<?php echo ($s->url) ? $s->url : '#' ?>">
						<img class="lazyOwl owl-img" style="" data-src="<?php echo HtmlHelper::image($s->img) ?>" />
					</a>
				</div>
			<?php endforeach;?>
		</div>
	</div>

	<script>
		window.owlH = document.documentElement.clientWidth + 'px';
		document.querySelector('#owl-merchant').style.height = window.owlH;
		document.querySelector('#owl-merchant-wrap').style.height = window.owlH;

		var owlImgs = document.querySelectorAll('.owl-img');
		for (var i = 0, len = owlImgs.length; i < len; i++) {
			owlImgs[i].style.height = window.owlH;
			owlImgs[i].style.width = window.owlH;
		}


	</script>

<?php endif;?>

<div class="p10">
	<h3 class="ww-title">新闻公告</h3>
	<ul>
		<?php foreach($slider as $k=>$s):?>
		<li>
			<a href="javascript:;"><?php echo $s->title ?></a>
		</li>
		<?php endforeach;?>
	</ul>

</div>


<div style="height: 60px;"></div>
<div class="footer-panel">
	<div class="col"><a href="tel:<?php echo $partner->contact_tel ?>" class="tel"><?php echo $partner->contact_tel ?></a></div><div class="col pay-wrap">
		<a href="<?php echo $partner->mobileUrl() ?>" class="pay btn" id="ww-pay"><?php echo $partner->type ? '查看商品' : '立即缴费' ?></a>
	</div>
</div>