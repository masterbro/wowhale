<style type="text/css">
	.pay_online,.pay_online a{color: black;}
	.pay_unonline,.pay_unonline a{color: green;}
	.not_pay,.not_pay a{color: red}
	.pay_cancel,.pay_cancel a{color: crimson;}
	.student_status a{text-align: right;float: right;}
	.student_status a.student_status_txt{cursor: pointer;}
</style>
<div class="p10">
	<h2 class="page-title"><?php echo $product->name;?></h2>
	<?php $order_count = $product->schoolGradeOrderCount;?>
	<h2 class="page-title"><?php echo $grade->name;?> 在线支付 <?php echo $order_count['online_count'][$grade->id]?$order_count['online_count'][$grade->id]:0;?>人/ 线下支付 <?php echo $order_count['unonline_count'][$grade->id]?$order_count['unonline_count'][$grade->id]:0;?>人/ 实缴 <?php echo $order_count['order_count'][$grade->id]?$order_count['order_count'][$grade->id]:0;?> 人/ 应缴<?php echo $count;?>人</h2>

	<?php if(count($list)):?>
	<?php foreach($list as $l):?>
	<?php $student_order = $l->getIsOrdered($product_id);?>
	<?php
		$status_text = '未支付';
		$can_edit = 0;
		$status_classes = array('pay_online','pay_unonline','not_pay','pay_cancel');
		$status_classe_index = 0;
		if($student_order &&$student_order->payment_status>0){
			if($student_order &&$student_order->payment_type==1){
				$status_text = '线上支付';
			}
			elseif($student_order &&$student_order->payment_type==2){
				$status_text = '线下支付';
				$status_classe_index = 1;
				$can_edit = 1;
			}
			else{
				$status_text = '其他支付';
				$status_classe_index = 1;
				$can_edit = 1;
			}
		}
		elseif($student_order &&$student_order->payment_status==0){
			$status_text = '支付取消';
			$status_classe_index = 3;
			$can_edit = 1;
		}
		elseif(!$student_order){
			$status_text = '未支付';
			$status_classe_index = 2;
			$can_edit = 1;
		}
		else{
			$status_text = '未支付';
			$status_classe_index = 2;
			$can_edit = 1;
		}

	?>
	<div class="order-item <?php echo $status_classes[$status_classe_index];?> student_status">
		<p>
			<span class="product_title"><?php echo $l->name ?></span>
			<span class="product_student_total">
				<?php if($can_edit):?>
				<a data-href="<?php echo $this->createUrl('/orderReport/UnOnlineOrder',array('partner_id'=>$partner_id,'product_id'=>$product_id,'grade_id'=>$grade->id,'student_id'=>$l->id,'order_type'=>($student_order&&$student_order->payment_status)?0:1)) ?>" class="student_status_txt" >	
				<?php echo $status_text;?>
				</a>
				<?php else:?>
				<a href="javascript:;">
				<?php echo $status_text;?>
				</a>
				<?php endif;?>
			</span>
		</p>
		
	</div>
	<?php endforeach;?>

	<?php else:?>
		<p class="alert alert-success">该班级目前还没有学生哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

	<?php endif;?>

	<nav style="text-align: center">
		<?php
		/*$this->widget('CLinkPager',array(
				'header'=>'',
				'firstPageLabel' => '首页',
				'lastPageLabel' => '末页',
				'prevPageLabel' => '上一页',
				'nextPageLabel' => '下一页',
				'pages' => $pager,
				'cssFile'=>false,
				'maxButtonCount'=>3,
				'selectedPageCssClass' => 'active',
				'hiddenPageCssClass' => 'disabled',
				'htmlOptions' => array(
					'class' => 'pagination'
				),
			)
		);*/
		?>
	</nav>
	<?php $order_money = $product->schoolGradeOrderMoney;?>
	<h2 class="page-title"><?php echo $grade->name;?> 在线支付 <?php echo $order_money['online_count'][$grade->id]?$order_money['online_count'][$grade->id]:0;?>元/ 线下支付 <?php echo $order_money['unonline_count'][$grade->id]?$order_money['unonline_count'][$grade->id]:0;?>元/ 实缴 <?php echo $order_money['order_count'][$grade->id]?$order_money['order_count'][$grade->id]:0;?> 元/ 应缴<?php echo $count*$product->price;?>元</h2>
	</div>
	<script type="text/javascript">
	$(function(){
		$('.student_status_txt').click(function(){
			if(window.confirm('确认切换状态？')){
				$.getJSON($(this).attr('data-href'),function(data){
					//console.info(data)
					//return;
					if(!data.error){
						alert(data.msg)
						document.location.reload();
					}
					else{
						alert(data.msg)
					}
				})
			}
		})
	})
	
	</script>