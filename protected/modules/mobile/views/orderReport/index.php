<style>
	.num_bar {width: 100%;position: relative;display: block;height: 14px;}
	.bar {width: 100%;position: absolute;display: block;background-color: red;text-align: right;padding-right: 3px;}
	.bar_online{background-color: green;text-align: right;z-index: 10}
	.bar_unonline{background-color: blue;text-align: right;z-index: 9}
	a.show_report_next{display: block;}
</style>
<div class="p10">
	<h2 class="page-title">缴费项目</h2>

	<?php if(count($list)):?>
	<?php foreach($list as $l):?>
	<div class="order-item">
		<p>
			<a class="show_report_next" href="<?php echo $this->createUrl('/orderReport/viewProduct',array('partner_id'=>$partner_id,'pr_id'=>$l->id)) ?>">
			<span class="product_title"><?php echo $l->name ?></span>
			<?php $order_count = $l->schoolOrderCount;?>
			</a>
			<div class="num_bar">
				<span class="bar bar_online" style="width:<?php echo ($order_count['online_count']/$student_count)*100;?>%"><?php echo $order_count['online_count'];?></span>
				<span class="bar bar_unonline" style="width:<?php echo (($order_count['online_count']+$order_count['unonline_count'])/$student_count)*100;?>%"><?php echo $order_count['unonline_count'];?></span>
				<!-- <span class="bar"><?php echo $order_count['order_count'];?></span> -->
				<span class="bar"><?php echo $student_count-$order_count['order_count'];?></span>
			</div>
		</p>
		
	</div>
	<?php endforeach;?>

	<?php else:?>
		<p class="alert alert-success">该学校目前没有缴费项目哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

	<?php endif;?>

<nav style="text-align: center">
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>3,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
	</div>