<style>
    select.form-control{
        display: block;
        width: 100%;
        height: 50px;
       /* padding: 6px 12px;*/
        padding: 8px 12px;
        font-size: 18px;
        line-height: 1.42857143;
        color: #00a0e9;
        background-color: #fafafa;
        background-image: none;
        border: 0;
        border-radius: 0;
        -webkit-appearance: inherit;
        border-bottom: 1px solid #d3d3d3;
    }
    .orderReportWrap .page-title-img{float: right;position: absolute;top:6px;right:25px;}
    .orderReportWrap .page-title-img img{height:15px;}
/*
    .orderReportWrap .page-title:after{content:'>';float: right;position: absolute;top:0;right:12px;color: #00a0e9;}
*/
    .orderReportWrap .page-title{margin-bottom: 5px;position: relative}
    /*.orderReportWrap{background: #f2f2f2}*/
    .user_infos_wrap{margin-top: 10px;}
    .pay_type_info{margin-top: 10px;}

    /*.left_wrap{width: 50%;float: left;border: none;text-align: center}
    .right_wrap{width: 50%;float: left;border: none;;text-align: center}*/
    .left_wrap{width: 100%;border: none;text-align: center}
    .right_wrap{width: 100%;border: none;text-align: center;margin:10px auto;}

    .pay_box{cursor:pointer;margin: 0 auto;width: 95%;text-align: center;border-radius: 5px;line-height: 40px;font-size: 18px;height: 100px;border-bottom: 3px solid #ccc;}
    .pay_img{float: left;width: 101px;}
    .pay_img img{height: 103px;width: 100%;}
    .payed_user_money{color:#ed6d46;}
    .no_pay_user_money{color:#62afe5;}
    .pay_content{float: left;padding-top:7px;margin-left:15px;}

    .pay_box.payed{}
    .pay_box.no_pay{}
    .pay_box span{font-size: 18px;}
    .pay_box span.user_num{font-size: 28px;}
    .pay_user_money span{font-size: 26px;}

    /*.payed{background: #f62e00;background:-webkit-gradient(linear, 0 0, 0 100%, from(#ff8a00), to(#f62e00))}
    .no_pay{background: #00F0F0;background:-webkit-gradient(linear, 0 0, 0 100%, from(#cecece), to(#888888))}*/
    .payed{background: #f2f2f2;}
    .no_pay{background: #f2f2f2;}

    .pay_type_info{margin: 0 auto;;margin-bottom: 12px;padding: 5px;overflow: hidden;width: 95%;}
    .online_pay{float: left;width: 50%;}
    .unonline_pay{float: left;width: 45%;margin-left: 5%;}

    .pay_type_info span{color: #3f3f3f;font-size: 18px;}
    .pay_type_info span.pay_num{color: #3f3f3f;font-size: 28px;margin-left: 5px;}
    .pay_type_info .pay_type{color: #898989;margin-left: 5px;}
    .pay_type_info .pay_moneys span{color: #f63100;font-size: 20px;margin-left: 6px;}
    /*.pay_type_info .pay_moneys{margin-top: 12px;}*/

    .detail_wrap table thead{text-align: center;color:#000000;font-weight: 800}
    .detail_wrap table tr:last-child{border-bottom:1px solid #d6d6d6}
    .detail_wrap table tr td{padding: 8px;border:1px solid #d6d6d6}
    .student_status_txt{background: #00a0e9;padding: 4px;border-radius: 2px;color:white;cursor: pointer;width: 64px;display: block;text-align: center;margin:0 auto;}
</style>
<div class="  orderReportWrap">
    <?php if(!$product||!$grades):?>
        <p class="alert alert-success">该学校目前没有班级或缴费项目哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

    <?php return;endif;?>
	<h2 class="page-title">
        <select id="grade_id" class="form-control">
            <option value="0">全园</option>
            <?php foreach($grades as $k=>$v):?>
            <option value="<?php echo $v->id;?>" <?php if($grade_id==$v->id):?>selected<?php endif;?>><?php echo $v->name;?></option>
            <?php endforeach;?>
        </select>
        <div class="page-title-img">
            <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-1.png') ?>" />
        </div>
    </h2>
    <h2 class="page-title">
        <select id="product_id" class="form-control">
            <?php foreach($products as $k=>$v):?>
                <option value="<?php echo $v->id;?>" <?php if($product_id==$v->id):?>selected<?php endif;?>><?php echo $v->name;?></option>
            <?php endforeach;?>
        </select>
        <div class="page-title-img">
            <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-1.png') ?>" />
        </div>
    </h2>
    <?php
    if(!$grade_id){
        $order_count = $product?$product->schoolOrderCount:array();
        $order_money = $product?$product->schoolOrderMoney:array();
    }
    else{
        $order_count_grades = $product?$product->schoolGradeOrderCount:array();
        $order_money_grades = $product?$product->schoolGradeOrderMoney:array();
        $order_count = array();
        $order_count['online_count'] = $order_count_grades['online_count'][$grade_id]?$order_count_grades['online_count'][$grade_id]:0;
        $order_count['unonline_count'] = $order_count_grades['unonline_count'][$grade_id]?$order_count_grades['unonline_count'][$grade_id]:0;
        $order_count['order_count'] = $order_count_grades['order_count'][$grade_id]?$order_count_grades['order_count'][$grade_id]:0;
        $order_money = array();
        $order_money['online_count'] = $order_money_grades['online_count'][$grade_id]?$order_money_grades['online_count'][$grade_id]:0;
        $order_money['unonline_count'] = $order_money_grades['unonline_count'][$grade_id]?$order_money_grades['unonline_count'][$grade_id]:0;
        $order_money['order_count'] = $order_money_grades['order_count'][$grade_id]?$order_money_grades['order_count'][$grade_id]:0;
    }

    ?>
    <div class="user_infos_wrap">
        <div class="left_wrap">
            <div class="payed pay_box" data-status="1">

                <div class="pay_img">
                    <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-2.png') ?>" />
                </div>
                <div class="pay_content">
                    <div class="pay_user_count">
                        <span>已缴费:</span>
                        <span class="user_num"><?php echo $order_count['order_count'];?></span>
                        <span>人</span>
                    </div>
                    <div class="pay_user_money">
                        <span class="payed_user_money">￥<?php echo CommonHelper::price($order_money['order_count']);?></span>
                    </div>
                </div>

            </div>
        </div>

        <div class="right_wrap">
            <div class="no_pay pay_box" data-status="0">

                <div class="pay_img">
                    <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-3.png') ?>" />
                </div>

                <div class="pay_content">
                    <div class="pay_user_count">
                        <span>未缴费:</span>
                        <span class="user_num"><?php echo $none_count = $student_count-$order_count['order_count'];?></span>
                        <span>人</span>
                    </div>
                    <div class="pay_user_money">
                        <span class="no_pay_user_money">￥<?php echo CommonHelper::price($none_count * $product->price);?></span>
                    </div>
                </div>

            </div>
        </div>


    </div>
    <div style="clear:both"></div>

    <div class="pay_type_info">
        <div class="online_pay">
            <div class="pay_info">
                <span class="pay_type">线上支付</span>
                <span class="pay_num"><?php echo $order_count['online_count'];?></span>
                <span>人</span>
            </div>
            <div class="pay_moneys">
                <span>￥<?php echo CommonHelper::price($order_money['online_count']);?></span>
            </div>
        </div>

        <div class="unonline_pay">
            <div class="pay_info">
                <span class="pay_type">现金支付</span>
                <span class="pay_num"><?php echo $order_count['unonline_count'];?></span>
                <span>人</span>
            </div>
            <div class="pay_moneys">
                <span>￥<?php echo CommonHelper::price($order_money['unonline_count']);?></span>
            </div>
        </div>
    </div>

    <div class="detail_wrap">
    <?php if(count($students)):?>
    <table>
        <thead>
        <tr>
            <td>学生姓名</td>
            <td>支付金额</td>
            <td>支付方式</td>
        </tr>
        </thead>
        <tbody>
    <?php foreach($students as $k=>$l):?>
        <?php $student_order = $l->getIsOrdered($product->id);?>
        <?php
        $status_text = '未支付';
        $can_edit = 0;
        $status_classes = array('pay_online','pay_unonline','not_pay','pay_cancel');
        $status_classe_index = 0;
        if($student_order &&$student_order->payment_status>0){
            if($student_order &&$student_order->payment_type==1){
                $status_text = '线上支付';
            }
            elseif($student_order &&$student_order->payment_type==2){
                $status_text = '现金支付';
                $status_classe_index = 1;
                $can_edit = 1;
            }
            else{
                $status_text = '其他支付';
                $status_classe_index = 1;
                $can_edit = 1;
            }
        }
        elseif($student_order &&$student_order->payment_status==0){
            $status_text = '未支付';
            $status_classe_index = 3;
            $can_edit = 1;
        }
        elseif(!$student_order){
            $status_text = '未支付';
            $status_classe_index = 2;
            $can_edit = 1;
        }
        else{
            $status_text = '未支付';
            $status_classe_index = 2;
            $can_edit = 1;
        }
        if(!$product->status){
            $can_edit = 0;
        }

        ?>
        <tr data-status="<?php echo $student_order?$student_order->payment_status:'0';?>">
            <td><?php echo $l->name ?></td>
            <td><?php echo CommonHelper::price($student_order?$student_order->total:'') ?></td>
            <td style="text-align: center;">
                <?php if($can_edit):?>
                    <a data-href="<?php echo $this->createUrl('/orderReport/UnOnlineOrder',array('partner_id'=>$this->partner_id,'product_id'=>$product_id,'grade_id'=>$grade_id?$grade_id:$l->grade_id,'student_id'=>$l->id,'order_type'=>($student_order&&$student_order->payment_status)?0:1)) ?>" class="student_status_txt" >
                        <?php echo $status_text;?>
                    </a>
                <?php else:?>
                    <a href="javascript:;">
                        <?php echo $status_text;?>
                    </a>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
        <p class="alert alert-success">该学校目前没有学生哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

    <?php endif;?>
    </div>
</div>
<script>
    window.partner_id = '<?php echo $this->partner_id;?>';
    function getData(){
        var _data = {};
        _data['partner_id'] = window.partner_id;
        _data['grade_id'] = $('#grade_id').val();
        _data['product_id'] = $('#product_id').val();
        document.location.href="/orderReport/list?"+$.param(_data);
    }
    $(function(){
        $('#grade_id,#product_id').change(function(){
            getData()
        })
        $('.student_status_txt').click(function(){
            if(window.confirm('确认切换状态？')){
                $.getJSON($(this).attr('data-href'),function(data){
                    //console.info(data)
                    //return;
                    if(!data.error){
                        alert(data.msg)
                        document.location.reload();
                    }
                    else{
                        alert(data.msg)
                    }
                })
            }
        })
        $('.pay_box').click(function(){
            var _thisActive = $(this).hasClass('active');
            $('.pay_box').removeClass('active');
            if(!_thisActive){
                $(this).addClass('active')
            }/*
            else{
                $('.pay_box').removeClass('active');
                $(this).addClass('active')
            }*/
            var _activeObj  = $('.pay_box.active');
            if(!_activeObj.length){
                $('.detail_wrap tbody tr').show();
            }
            else{
                var _status = _activeObj.attr('data-status');
                $('.detail_wrap tbody tr').hide();
                $('.detail_wrap tbody tr[data-status='+_status+']').show();

            }
        })
    })
</script>