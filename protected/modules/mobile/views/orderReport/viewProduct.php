<style>
	.num_bar {width: 100%;position: relative;display: block;height: 14px;}
	.bar {width: 100%;position: absolute;display: block;background-color: red;text-align: right;padding-right: 3px;}
	.bar_online{background-color: green;text-align: right;z-index: 10}
	.bar_unonline{background-color: blue;text-align: right;z-index: 9}
	a.show_report_next{display: block;}
</style>
<div class="p10">
	<h2 class="page-title"><?php echo $product->name;?></h2>
	<?php $order_count = $product->schoolGradeOrderCount;?>
	<h2 class="page-title">班级列表</h2>

	<?php if(count($list)):?>

	<?php foreach($list as $l):?>
	<div class="order-item">
		<p>
			<a  class="show_report_next" href="<?php echo $this->createUrl('/orderReport/viewGrade',array('partner_id'=>$partner_id,'pr_id'=>$product_id,'g_id'=>$l->id)) ?>">
			<span class="product_title"><?php echo $l->name ?></span>
			</a>
			<span class="num_bar">
			<?php $online_count_num = $order_count['online_count'][$l->id]?$order_count['online_count'][$l->id]:0;?>
			<?php $unonline_count_num = $order_count['unonline_count'][$l->id]?$order_count['unonline_count'][$l->id]:0;?>
			<?php $order_count_num = $order_count['order_count'][$l->id]?$order_count['order_count'][$l->id]:0;?>
			<?php $student_count_num = $student_count[$l->id]?$student_count[$l->id]:0; ?>
			<?php if($student_count_num>0):?>
				<span class="bar bar_online" style="width:<?php echo ($online_count_num/$student_count_num)*100;?>%"><?php echo $online_count_num;?></span>
				<span class="bar bar_unonline" style="width:<?php echo (($online_count_num+$unonline_count_num)/$student_count_num)*100;?>%"><?php echo $unonline_count_num;?></span>
				<!-- <span class="bar"><?php echo $order_count['order_count'];?></span> -->
				<span class="bar"><?php echo $student_count_num-$order_count_num;?></span>
			<?php else:?>
				<span class="bar">暂无学生</span>
			<?php endif;?>
			</span>
		</p>
		
	</div>
	<?php endforeach;?>

	<?php else:?>
		<p class="alert alert-success">该学校目前没有班级哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

	<?php endif;?>

<nav style="text-align: center">
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>3,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
	</div>