<div class="p10">
	<h1><?php echo $partner->name ?></h1>
	<br />
	<form method="post" action="" enctype="multipart/form-data">

		<div class="field">
			<label>手机号：</label>
			<input type="tel" name="mobile" value="" class="ipt" />
		</div>
		<div class="field">
			<label>验证码：</label><br />
			<input type="tel" name="captcha" value="" class="ipt" style="width: 100px;display: inline-block" />
			<a href="javascript:;" id="send-school-sms">发送验证码</a>
		</div>
		<div class="field">
			<a href="javascript:;" class="btn" style="line-height: 40px;" id="school-login-sub">登录</a>
		</div>
	</form>
</div>


<script>
	(function($){
		$(function(){
			$('#school-login-sub').click(loginSub);
			$('#send-school-sms').click(sendSms);
		});

		var loginSub = function() {
			var btn = $('#school-login-sub');

			if(btn.attr('data-sending') == 'true')
				return false;

			var mobile = $('input[name="mobile"]').val();

			if(!mobile) {
				alert('请输入你的手机号');
				return false;
			}

			if(!/^1\d{10}$/.test(mobile)) {
				alert('请输入正确的手机号');
				return false;
			}

			var captcha = $('input[name="captcha"]').val();

			if(!captcha) {
				alert('请输入验证码');
				return false;
			}

			sending = true;
			btn.html('发送中...').attr('data-sending', 'true');
			$.ajax({
				url:'/session/school?partner_id=<?php echo $partner->id ?>&rd=<?php echo $_GET['rd'];?>',
				type:'post',
				dataType:'json',
				data:{mobile:mobile, captcha:captcha},
				success: function(data) {
					alert(data.msg);
					if(data.error) {
						btn.html('登录').attr('data-sending', 'false');
					} else {
						window.location.href = data.redirect;
					}
				},
				error: function(){
					btn.html('登录').attr('data-sending', 'false');
				}
			})
		};

		var bindSmsCountDownInterval = null, sending = false;

		var sendSms = function() {
			var btn = $('#send-school-sms');

			if(btn.attr('data-sending') == 'true')
				return false;

			var mobile = $('input[name="mobile"]').val();

			if(!mobile) {
				alert('请输入你的手机号');
				return false;
			}

			if(!/^1\d{10}$/.test(mobile)) {
				alert('请输入正确的手机号');
				return false;
			}

			sending = true;
			btn.html('发送中...').attr('data-sending', 'true');
			$.ajax({
				url:'/session/captcha?partner_id=<?php echo $partner->id ?>',
				type:'post',
				dataType:'json',
				data:{mobile:mobile},
				success: function(data) {
					if(data.error) {
						alert(data.msg);
						btn.html('发送验证码').attr('data-sending', 'false');
					} else {
						bindSmsCountDown();
					}
				},
				error: function(){
					sending = false;
					btn.html('发送验证码').attr('data-sending', 'false');
				}
			})
		}

		var bindSmsCountDown = function() {
			$('#send-school-sms').html('<em>60</em> 秒后重新发送');

			bindSmsCountDownInterval = setInterval(function(){
				var i = parseInt($('#send-school-sms').find('em').html());
				i--;
				if(i>0) {
					$('#send-school-sms').html('<em>'+ i +'</em> 秒后重新发送');
				} else {
					sending = false;
					$('#send-school-sms').html('发送验证码').attr('data-sending', 'false');
					clearInterval(bindSmsCountDownInterval);
				}
			}, 1000);
		};

	}(jQuery))
</script>