<div class="p10">
	<h2 class="page-title">公告</h2>

	<?php if(count($list)):?>
	<?php foreach($list as $l):?>
	<div class="order-item">
		<p><a href="<?php echo $this->createUrl('/notice/view',array('id'=>$l->id)) ?>"><?php echo $l->title ?></a></p>
	</div>
	<?php endforeach;?>

	<?php else:?>
		<p class="alert alert-success">当前还没有任何公告哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

	<?php endif;?>

<nav style="text-align: center">
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>3,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
</div>