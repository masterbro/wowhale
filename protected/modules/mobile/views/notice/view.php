<style>
    .wwy:before{content: '.';content: ' ';
        position: absolute;
        top: 0;
        left: -8px;
        top: 6px;
        width: 6px;
        display: block;
        height: 6px;
        text-align: center;
        background-color: red;
        border-radius: 3px;}
    .wwy{position: relative}
</style>
<div class="p10 ww-notice-view">
	<h1 class="article-title"><?php echo $model->title ?></h1>
	<div class="article-subtitle">
		<span class="article-time" data-article-time=""><?php echo date('Y-m-d H:i:s', $model->create_time) ?></span>
		来源: <?php echo $partner->name ?>
	</div>

	<div class="article-body">
		<?php if($model->thumb): ?>
			<p><img src="<?php echo HtmlHelper::image($model->thumb) ?>" /></p>
		<?php endif;?>
		<?php echo $model->content ?>
	</div>
    <div class="article-subtitle">
        阅读 <?php echo $model->upHits();?>
        <span style="display: block;float: right" class="wwy"><a href="http://www.wwcamp.com" style="color: #888;text-decoration: underline !important;">娃娃营-校园的科技枢纽</a></span>
    </div>
</div>
<?php $this->renderPartial('/layouts/wxshare', array('keyword'=>$model->title));?>