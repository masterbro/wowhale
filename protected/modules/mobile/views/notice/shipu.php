<style>
    .calendar_wrap table{width: 100%;}
    .calendar_wrap table tr{height:34px;line-height: 34px;}
    .calendar_wrap table td{text-align: center;padding: 0 10px;position: relative}
    #top_title{margin-top: 3px;}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{ }
    .detail_wrap table tr td{padding: 5px;}
    .table-shipu .day{min-width: 40px;}
    .table-shipu .day_week{min-width: 46px;}
    .table-shipu .day_type{min-width: 40px;}
    .page-shipu{min-height: 100%}
    .table-shipu th{height: 24px;text-align: center;border-right: 1px solid #d6d6d6}
    .table-shipu td{}
    .table-shipu .day_table{padding: 0;}
    .table-shipu tbody  tr:nth-of-type(odd) {
        background-color: #99cccc;
        color: white;
    }
    .table-shipu tbody tr:nth-of-type(even) {
        background-color: #e2f0d6;
        color: black;
    }
    .table-shipu table   tr:nth-of-type(odd) {
        background-color: white;
        color: #000000;
    }
    .table-shipu table  tr:nth-of-type(even) {
        background-color: #f6ffe0;
        color: #000000;
    }
    .week_list{
        margin: 18px;
        background-color: #FAfAFA;
        padding: 10px;
        /*border-radius: 15px;*/
        position: relative;
        padding-top:30px ;
        box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        line-height: 18px;
        font-size: 18px;
        margin-bottom: 35px;;
    }
    .week_list .day_type{min-width: 40px;display: block;float: left;}
    .week_list .week_day {
        background-color: rgb(126, 206, 244);
        box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.3);
        position: absolute;
        left: 5%;
        top: -25px;
        width: 50px;
        height: 45px;
        z-index: 7;
        font-family: "楷体";
        border-radius: 25px;
        border-bottom-left-radius: 0px;
        text-align: center;
        vertical-align: middle;
        line-height: 45px;
    }
    .week_list .week_day_0{
        background-color: #7ecef4;
    }
    .week_list .week_day_1{
        background-color: #acd7b6;
    }
    .week_list .week_day_2{
         background-color: #facd89;
    }
    .week_list .week_day_3{
        background-color: #f8b551;
    }
    .week_list .week_day_4{
        background-color: #cce198;
    }
    .week_list .week_day_5{
        background-color: #7ecef4;
    }
    .week_list .week_day_6{
        background-color: #acd7b6;
    }
    .calendar_wrap table tr td a img{
        height: 20px;
    }
</style>
<div class="page-shipu">
    <div class="calendar_wrap">
        <table>
            <tr>
                <?php if(!$hide):?>
                    <td>
                        <a href="<?php echo $this->createUrl('/notice/shipu',array('partner_id'=>$partner_id,'start'=> date('Y-m-d',strtotime(urldecode($start_date))-3600*24*8 ),'to'=> date('Y-m-d',strtotime(urldecode($start_date))-3600*24*1 ) )) ?>">
                            <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-0.png') ?>" />
                        </a>
                    </td>
                <?php endif;?>
                <td colspan="5" align="center"><div id="top_title"><?php echo urldecode($start_date); ?>&nbsp;&nbsp;到&nbsp;&nbsp;<?php echo urldecode($end_date) ?></div></td>

                <?php if(!$hide):?>
                    <td>
                        <a href="<?php echo $this->createUrl('/notice/shipu',array('partner_id'=>$partner_id,'start'=> date('Y-m-d',strtotime(urldecode($end_date))+3600*24*1 ),'to'=> date('Y-m-d',strtotime(urldecode($end_date))+3600*24*8 ) )) ?>">
                            <img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-1.png') ?>" />
                        </a>
                    </td>
                <?php endif;?>
            </tr>
        </table>
    </div>
    <hr />
    <br/>
    <br/>
    <div class="detail_wrap">

        <?php foreach($list as $d=>$l):?>
            <div class="week_list">
                <div class="week_day week_day_<?php echo date('w',strtotime($d));?>">
                    <?php echo $l[1];?>
                </div>
                <?php foreach($data_keys as $k=>$v):?>
                <div>
                    <span class="day_type"><?php echo $k;?></span>
                    <span><?php echo $l[$k]?$l[$k]:'';?></span>
                </div>
                <?php endforeach;?>
            </div>
            <div style="clear: both"></div>
        <?php endforeach;?>
    </div>
</div>
