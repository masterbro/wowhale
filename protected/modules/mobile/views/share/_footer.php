<footer class="ww-footer">

	<div class="footer-nav">
		<?php foreach(Page::$page_title as $k=>$v):?>
			<a href="<?php echo $this->createUrl('/page?key='.$k) ?>"><?php echo $v ?></a>
		<?php endforeach;?>
	</div>
	<p>Copyright &copy;<?php echo date('Y'); ?> WoWhale  蜀ICP备15013283号</p>

</footer>