<header class="ww-primary-nav">
	<button type="button" class="navbar-toggle" id="navbar-toggle">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a href="/"><img src="<?php echo HtmlHelper::assets('images/logo_white.png') ?>" /></a>
	<ul class="nav" id="header-nav">
		<li><a href="<?php echo $this->createUrl('/shippingInfo') ?>">我的账户</a></li><li>
			<a href="<?php echo $this->createUrl('/myOrder') ?>">我的订单</a></li><li>
			<a href="<?php echo $this->createUrl('/page?key=page_about') ?>">关于我们</a></li>
	</ul>
</header>