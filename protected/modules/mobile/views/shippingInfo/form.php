<?php echo $this->renderPartial('/share/_primary_nav');?>

<div class="p10">
	<h2 class="page-title"><?php echo $model->id ? '编辑' : '添加' ?>常用联系人</h2>

	<form method="post" action="" id="shipping-info">
		<div class="field">
			<label>学生姓名：<span>*</span></label>
			<input type="text" name="student_name" class="ipt" value="<?php echo $model->student_name ?>" />
		</div>
		<div class="field">
			<label>家长姓名：<span>*</span></label>
			<input type="text" name="parent_name" class="ipt" value="<?php echo $model->parent_name ?>" />
		</div>
		<div class="field">
			<label>联系电话：<span>*</span></label>
			<input type="text" name="tel" class="ipt" value="<?php echo $model->tel ?>" />
		</div>
		<div class="field">
			<label>生日：<span>*</span></label>
			<input type="date" name="birth" class="ipt"  value="<?php echo $model->birth ?>" />
		</div>


	<p class="p10"></p>
	<a href="javascript:;" class="btn btn-green" id="shipping-info-sub" style="padding: 10px 0px">保存</a>

	</form>
</div>