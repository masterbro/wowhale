<?php echo $this->renderPartial('/share/_primary_nav');?>
<div class="p10">
	<h2 class="page-title">我的帐户</h2>
	<p>常用联系人</p>

	<?php if(count($info)):?>
		<?php foreach($info as $i) :?>
			<div class="order-item">
				<table>
					<tr style="line-height: 30px;">
						<td>学生姓名：<?php echo $i->student_name ?></td>
						<td>家长姓名：<?php echo $i->parent_name ?></td>
					</tr>
					<tr style="line-height: 30px;">
						<td>联系电话：<?php echo $i->tel ?></td>
						<td>生日：<?php echo $i->birth ?></td>
					</tr>
					<tr style="line-height: 30px;">
						<td><a href="<?php echo $this->createUrl('/shippingInfo/update?id='.$i->id) ?>">编辑</a> </td>
						<td><a href="<?php echo $this->createUrl('/shippingInfo/delete?id='.$i->id) ?>" onclick="return confirm('你确定要删除吗?')">删除</a> </td>
					</tr>
				</table>
			</div>
		<?php endforeach;?>
	<?php else:?>
		<p class="p10"></p>
		<p class="alert alert-success">你还没有常用联系人哦！</p>
	<?php endif;?>


	<?php if(count($info) < 5):?>
	<p class="p10"></p>
	<a href="<?php echo $this->createUrl('/shippingInfo/create') ?>" class="btn btn-green" style="padding: 10px 0px">添加</a>
	<?php endif;?>
</div>