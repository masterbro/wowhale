<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;   }
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #ccc;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%;}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}

    span.days {
        width: 100%;
        height: 100%;
        text-align: center;
        background: #f2f2f2;
        padding: 5px;
        display: block;
        border-radius: 50%;
        cursor: pointer;
    }
    .info_wrap,.detail_wrap{
        margin-left:10px;
    }
    .info_wrap  span.days{
        border-radius: inherit;
        width: 20px;
        height: 20px;
        float: left;
        padding:0;
        margin: 0;
    }
    .info_wrap  span{
        line-height: 20px;
        float: left;
        margin: 0 10px;
    }
    span.days.right{background: green;}
    span.days.invalid{background: lightcoral;}
    span.days.none{background: #f2f2f2;}
    span.days.active:after{content: ' ';
        position: absolute;
        bottom: 6px;
        right: 6px;
        width: 6px;
        height: 6px;
        background-color: red;
        border-radius: 3px;}
    .calendar_wrap table{width: 100%}
    .calendar_wrap table td{text-align: center;padding: 5px 10px;position: relative}
    .log_key_1{color:#ff0000;}
    .log_key_2{color: green;}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{border-bottom:1px solid #d6d6d6}
    .detail_wrap table tr td{padding: 5px;}

    .loglist_list .teachers .head_icon {width: 64px; height: 64px; border-radius: 32px;background-color: #ddd; }
    .teachers{float: left;width:25%;text-align: center;margin-top:12px;    margin-bottom: 12px;}
    .teachers a{color: inherit}
    .loglist_list .teacher_name{margin-top: 16px;padding-top:10px;padding-bottom:10px;    margin-left: 5px;
        margin-right: 5px; }
    .loglist_list .teacher_name.right{background: green;color:#fff;}
    .loglist_list .teacher_name.invalid{background:lightcoral;color:#fff;}
    .loglist_list .teacher_name.none{background: #f2f2f2;}
    .sign_in_time.leave{position: absolute;top:-12px;left: 0px;background-color: rgba(255,0,0,0.2);border-radius: 12px;border-bottom-right-radius: 0;border-top-left-radius: 6px;
        padding: 5px;}
    .sign_out_time{position: absolute;bottom:-12px;right: 0px;background-color: rgba(255,0,0,0.2);border-radius: 12px;border-top-left-radius: 0;border-bottom-right-radius: 6px;
        padding: 5px;}
    .teacher_head_wrap{position: relative}


    .week_list{
        margin: 18px;
        background-color: #FAfAFA;
        padding: 10px;
        /*border-radius: 15px;*/
        position: relative;
        padding-top:30px ;
        box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        line-height: 18px;
        font-size: 18px;
        margin-bottom: 35px;;
    }
    .week_list .day_type{min-width: 40px;display: block;float: left;}
    .week_list .week_day_status {
        background-color: rgb(126, 206, 244);
        box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.3);
        position: absolute;
        left: 5%;
        top: -25px;
        /*width: 50px;*/
        padding-left: 20px;
        padding-right: 20px;
        height: 45px;
        z-index: 7;
        font-family: "楷体";
        border-radius: 25px;
        border-bottom-left-radius: 0px;
        text-align: center;
        vertical-align: middle;
        line-height: 45px;
    }
    .week_list .week_day_0{
        background-color: #7ecef4;
    }
    .week_list .week_day_1{
        background-color: #acd7b6;
    }
    .week_list .week_day_2{
        background-color: #facd89;
    }
    .week_list .week_day_3{
        background-color: #f8b551;
    }
    .week_list .week_day_4{
        background-color: #cce198;
    }
    .week_list .week_day_5{
        background-color: #7ecef4;
    }
    .week_list .week_day_6{
        background-color: #acd7b6;
    }
    .detail_wrap.leave_wrap a{color: inherit;}
    .btn.status0{background-color: #7ecef4;}
    .btn.status1{background-color: #acd7b6;}
    .btn.status2{background-color: #facd89;}
    .btn.btn_title{padding: 5px;}
    .btn.btn_title.active{background-color: #acd7b6;color:#000000;font-size: 20px;}
    .calendar_wrap.leave table td{/*background: #e9f6ec;border:1px solid #afafaf;*/height: 40px;line-height: 40px;padding: 0}
    .calendar_wrap.leave .btn_title{color: black;font-size: 16px;}
    .calendar_wrap.leave .btn_title.active{display: block;width:100%;border-bottom:2px solid #00a0E9;color:#00a0E9  }
</style>
<div class="calendar_wrap leave">
    <table>
        <tr>

            <td align="center"><a class=" btn_title status1 <?php if($status==1):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> 1,'id'=>$id)) ?>">待审批</a> </td>

            <td align="center"><a class=" btn_title status2 <?php if($status==2):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> 2,'id'=>$id)) ?>">已审批</a></td>
            <?php if($this->current_user->leader):?>
                <td align="center"><a class=" btn_title status3 <?php if($status==''):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> '')) ?>">班级概况</a></td>
            <?php elseif($this->current_user->type==Contacts::TY_TEACHER):?>
                <td align="center"><a class=" btn_title status3 <?php if($status==''):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/gradeLog',array('partner_id'=>$partner_id,'grade_id'=> $this->current_user->grade_id)) ?>">班级概况</a></td>
            <?php endif;?>
        </tr>
    </table>
</div>
<hr  style="background-color: #ccc;"/>
<div class="head_img_wrap">
    <img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon">
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>
<div class="calendar_wrap">
    <table>
        <tr>
            <td><a href="<?php echo $this->createUrl('/leave/gradeLog',array('partner_id'=>$this->partner_id,'grade_id'=>$grade_id,'day'=>date('Y-m-d',$month_start_time-3600*24)));?>"><img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-0.png') ?>" /></a></td>
            <td colspan="5" align="center"> <?php echo $this_day = date('Y-m-d',$month_start_time);?> </td>
            <td><a href="<?php echo $this->createUrl('/leave/gradeLog',array('partner_id'=>$this->partner_id,'grade_id'=>$grade_id,'day'=>date('Y-m-d',$month_start_time+3600*24)));?>"><img src="<?php echo HtmlHelper::assets('images/mobile/orderReport/img-1.png') ?>" /></a></td>
        </tr>
    </table>
</div>
<div class="p10_0 loglist_list">
    <?php foreach($students as $s):?>
        <?php
        $isLeave = $s->getIsLeave($this_day);
        if(!$isLeave){
            $show_class = 'none';
        }
        else if($isLeave && $isLeave->status==2){
            $show_class = 'right';
        }
        else{
            $show_class = 'invalid';
        }
        ?>
        <div class="teachers">
            <a href="<?php echo $this->createUrl('/leave/logs',array('id'=>$s->id, 'partner_id'=>$this->partner_id,'status'=>$show_class=='invalid'?1:2)) ?>">
                <div class="teacher_head_wrap">
                    <img class="head_icon" src="<?php echo HtmlHelper::get_head_img($app,$s->parents[0]?$s->parents[0]->id:'');?>"/>

                    <?php if($isLeave):?>
                        <p class="sign_in_time leave"><?php echo $isLeave->start_at<$month_start_time?date('m/d H:i',$isLeave->start_at):date('m/d H:i',$isLeave->start_at);?></p>
                        <p class="sign_out_time"><?php echo $isLeave->end_at>$month_start_time+3600*24?date('m/d H:i',$isLeave->end_at):date('m/d H:i',$isLeave->end_at);?></p>
                    <?php endif;?>
                </div>

                <p class="teacher_name <?php echo $show_class;?>"> <?php echo $s->name ?>  </p>
            </a>
        </div>
    <?php endforeach;?>
</div>
<div class="info_wrap" style="margin-bottom:10px;">
    <h3 class="ww-title" style="margin-bottom: 5px;">请假状态</h3>
    <span class="days right "> </span><span>请假批准</span>
    <span class="days invalid"> </span><span>请假待审</span>
    <span class="days none"> </span><span>未请假</span>
</div>
<div class="p10_0 ">
    <p></p>
</div>
<!--
<div class="detail_wrap">
    <h3 class="ww-title" style="margin: 10px 0 5px ;">签到明细</h3>
    <table>
    </table>
</div>
-->
<script>
    $(function(){
        $('.calendar_wrap .days').height($('.days.active').width()-2);
        $('.calendar_wrap .days').css('line-height',$('.days').height()+'px')
        $('.calendar_wrap .days').click(function(){
            $('.days').removeClass('active');
            $(this).addClass('active');
        })
    })
</script>