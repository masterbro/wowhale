<style>
    .calendar_wrap table{width: 100%;}
    .calendar_wrap table tr{height:24px;line-height: 24px;}
    .calendar_wrap table td{text-align: center;padding: 5px 10px;position: relative}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{ }
    .detail_wrap table tr td{padding: 5px;}
    .table-shipu .day{min-width: 40px;}
    .table-shipu .day_week{min-width: 46px;}
    .table-shipu .day_type{min-width: 40px;}
    .page-shipu{min-height: 100%}
    .table-shipu th{height: 24px;text-align: center;border-right: 1px solid #d6d6d6}
    .table-shipu td{}
    .table-shipu .day_table{padding: 0;}
    .table-shipu tbody  tr:nth-of-type(odd) {
        background-color: #99cccc;
        color: white;
    }
    .table-shipu tbody tr:nth-of-type(even) {
        background-color: #e2f0d6;
        color: black;
    }
    .table-shipu table   tr:nth-of-type(odd) {
        background-color: white;
        color: #000000;
    }
    .table-shipu table  tr:nth-of-type(even) {
        background-color: #f6ffe0;
        color: #000000;
    }
    .week_list{
        margin: 18px;
        background-color: #FAfAFA;
        padding: 10px;
        /*border-radius: 15px;*/
        position: relative;
        padding-top:30px ;
        box-shadow: 0px 1px 2px 0px rgba(0, 0, 0, 0.1);
        line-height: 18px;
        font-size: 18px;
        margin-bottom: 35px;;
    }
    .week_list .day_type{min-width: 40px;display: block;float: left;}
    .week_list .week_day_status {
        background-color: rgb(126, 206, 244);
        box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.3);
        position: absolute;
        left: 5%;
        top: -25px;
        /*width: 50px;*/
        padding-left: 20px;
        padding-right: 20px;
        height: 45px;
        z-index: 7;
        font-family: "楷体";
        border-radius: 25px;
        border-bottom-left-radius: 0px;
        text-align: center;
        vertical-align: middle;
        line-height: 45px;
    }
    .week_list .week_day_0{
        background-color: #7ecef4;
    }
    .week_list .week_day_1{
        background-color: #acd7b6;
    }
    .week_list .week_day_2{
        background-color: #facd89;
    }
    .week_list .week_day_3{
        background-color: #f8b551;
    }
    .week_list .week_day_4{
        background-color: #cce198;
    }
    .week_list .week_day_5{
        background-color: #7ecef4;
    }
    .week_list .week_day_6{
        background-color: #acd7b6;
    }
    .detail_wrap.leave_wrap a{color: inherit;}
    .btn.status0{background-color: #7ecef4;}
    .btn.status1{background-color: #acd7b6;}
    .btn.status2{background-color: #facd89;}
    .btn.btn_title{padding: 5px;}
    .btn.btn_title.active{background-color: #acd7b6;color:#000000;font-size: 20px;}
    .calendar_wrap.leave table td{/*background: #e9f6ec;border:1px solid #afafaf;*/height: 40px;line-height: 40px;padding: 0}
    .calendar_wrap.leave .btn_title{color: black;font-size: 16px;}
    .calendar_wrap.leave .btn_title.active{display: block;width:100%;border-bottom:2px solid #00a0E9;color:#00a0E9  }
</style>
<div class="page-shipu">
    <div class="calendar_wrap leave">
        <table>
            <tr>

                <td align="center"><a class=" btn_title status1 <?php if($status==1):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> 1,'id'=>$id)) ?>">待审批</a> </td>

                <td align="center"><a class=" btn_title status2 <?php if($status==2):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> 2,'id'=>$id)) ?>">已审批</a></td>
                <?php if($this->current_user->leader):?>
                    <td align="center"><a class=" btn_title status3 <?php if($status==''):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$partner_id,'status'=> '')) ?>">班级概况</a></td>
                <?php elseif($this->current_user->type==Contacts::TY_TEACHER):?>
                    <td align="center"><a class=" btn_title status3 <?php if($status==''):?>active<?php endif;?>"  href="<?php echo $this->createUrl('/leave/gradeLog',array('partner_id'=>$partner_id,'grade_id'=> $this->current_user->grade_id)) ?>">班级概况</a></td>
                <?php endif;?>
            </tr>
        </table>
    </div>
    <hr  style="background-color: #ccc;"/>
    <br/>
    <br/>
    <div class="detail_wrap leave_wrap">

        <?php foreach($grades as $d=>$l):?>
            <a href="<?php echo $this->createUrl('/leave/gradeLog',array('partner_id'=>$this->partner_id,'grade_id'=>$l->id));?>">
            <div class="week_list">
                <div class="week_day_status week_day_<?php echo $d%7;?>">
                    <?php echo $l->name;?>
                </div>
                <div style="clear: both">
                    <span class="day_type">班级人数：</span>
                    <span><?php echo $student_count = $l->studentCount;?></span>
                </div>
                <div style="clear: both">
                    <span class="day_type">当前请假人数：</span>
                    <span><?php echo $leave_count = $l->leaveCount;?></span>
                </div>
                <div style="clear: both">
                    <span class="day_type">当前应到人数：</span>
                    <span><?php echo $student_count-$leave_count;?></span>
                </div>
                <div style="clear: both"></div>
            </div>
            </a>
        <?php endforeach;?>
    </div>
</div>
</div>
