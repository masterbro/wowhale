<style>
    body{background-color: #f2f2f2;}
    .btn{
        margin: 0 auto;width: 70%;line-height: 40px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius:5px;
        background-color: #ea8a8b;
    }
    .pay-wrap{text-align: center;margin: 5px auto;}
    .field{background-color: #fff;padding: 10px;border-bottom: 1px solid #ddd;margin-top: 3px;min-height: 35px;}
    .field label{font-size: 12px;}
    .field-left{float: left; width: 23%;}
    .field-left label{margin: 0;line-height: 32px;}
    .field-right{float: right;width: 77%;}
    .order-item{border-bottom:0;padding:0;}
    .log_wrap{margin: 0;}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}
    .head_img_wrap.student_request{display: table;table-layout: fixed;width:100%;border-bottom:1px solid #ddd;background-color: #fff;margin-bottom: 5px;}
    .head_img_wrap .myHead{display: table-cell}
    .head_img_wrap .teacherHead{display: table-cell}
</style>

<script src="<?php echo HtmlHelper::assets('laydate/laydate.js?v='.Yii::app()->params['assets_version']) ?>"></script>
<link href="<?php echo HtmlHelper::assets('/mobi/css/mobiscroll.custom-2.6.2.min.css');?>" rel="stylesheet" type="text/css" />
<script src="<?php echo HtmlHelper::assets('/mobi/js/mobiscroll.custom-2.6.2.min.js?v=1');?>" type="text/javascript"></script>
<div class="head_img_wrap student_request">
    <div class="myHead">
        <a href="<?php echo $this->createUrl('/leave/logs',array('partner_id'=>$this->partner_id));?>"><img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon"></a>
        <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
    </div>
    <div class="teacherHead" style="display:none">
        <a href="javscript:;"><img  class="head_icon"></a>
        <p></p>
    </div>
</div>
<div class="log_wrap">
    <div class="order-item">

        <form id="leave_form">

            <div class="field" id="title">
                <input type="hidden" name="partner_id" value="<?php echo $this->partner_id ?>">
                <?php if($students):$teachers = array();?>
                    <div>
                        <label>学生姓名：</label>
                        <?php if(count($students) > 1):?>
                            <select name="student_id">
                                <?php foreach($students as $s) :$teachers[$s->grade_id]=$this->getGradeTeacher($app,$s->grade_id);?>
                                    <option value="<?php echo $s->id ?>" data-grade="<?php echo $s->grade_id ?>"><?php echo $s->name ?></option>
                                <?php endforeach;?>
                            </select>
                        <?php else:?>
                            <?php echo $students[0]->name ?>
                            <select name="student_id" style="display: none">
                                <?php foreach($students as $s) :$teachers[$s->grade_id]=$this->getGradeTeacher($app,$s->grade_id);?>
                                    <option value="<?php echo $s->id ?>"  data-grade="<?php echo $s->grade_id ?>"><?php echo $s->name ?></option>
                                <?php endforeach;?>
                            </select>
                        <?php endif;?>
                    </div>
                <?php else:$no_student=true;?>
                    <br />
                    <p class="error">你还没有关联学生</p>
                <?php endif;?>
            </div>

            <div class="field">
                <div class="field-left"><label>开始时间：</label></div>
                <div class="field-right"><input  type="text" onfocus="return false" autocomplete="off" name="start_at" id="start_at"  class="ipt student_name"  readonly data-name="请假时间" value="" onclick="//laydate({istime: true, format: 'YYYY-MM-DD hh:mm'})"/></div>
             </div>

            <div class="field">
                <div class="field-left"><label>结束时间：</label></div>
                <div class="field-right"> <input type="text" name="end_at"  id="end_at"  class="ipt parent_name" readonly data-name="家长姓名" value="" onclick="return false;"/></div>
            </div>

            <div class="field">
                <label>请假事由：</label>
                <textarea name="reason"></textarea>
                <div class="col pay-wrap"
                     <?php if($no_student):?>
                         style="display:none"
                     <?php endif;?>
                >
                    <a href="javascript:void(0)" class="pay btn" id="ww-request" data-loading="0">申请请假</a>
                </div>
            </div>
        </form>

    </div>
</div>

<script>
    window.partner_id = '<?php echo $this->partner_id ?>';
    window.teachers = '<?php echo CJSON::encode($teachers);?>';
    try{
        window.teachers = JSON.parse(window.teachers );
    }
    catch(e){

    }
    $(function(){
        var curr = new Date().getFullYear();
        var opt = {

        }

        opt.date = {preset : 'date'};
        opt.datetime = { preset : 'datetime', stepMinute: 5 ,'readonly':false };
        opt.time = {preset : 'time'};
        opt.tree_list = {preset : 'list', labels: ['Region', 'Country', 'City']};
        opt.image_text = {preset : 'list', labels: ['Cars']};
        opt.select = {preset : 'select'};
        var _opt = $.extend(opt['datetime'], { theme: 'android-ics', mode: 'mixed', display: 'modal', lang: 'zh' ,'dateFormat':'yy/mm/dd'});
        //$('#start_at').mobiscroll(_opt).datetime(_opt);
        //$('#end_at').mobiscroll(_opt).datetime(_opt);
        $('#start_at').val('').scroller('destroy').scroller(_opt);
        $('#end_at').val('').scroller('destroy').scroller(_opt);
        //$("#end_at1").mobiscroll().datetime($.extend(opt['datetime'], { theme: 'android-ics', mode: 'mixed', display: 'top', lang: 'zh' }));

        $('#ww-request').click(function(){
            var _button = $(this);
            if(_button.attr('data-loading')=='1'){
                return;
            }
            var _student = $('select[name=student_id]').val()
            if(!_student){
                alert('您不是学生家长没法申请请假哦!');
                return;
            }
            var start_at = $('input[name=start_at]').val()
            if(!start_at){
                alert('请选择请假开始时间!');
                return;
            }
            var end_at = $('input[name=end_at]').val()
            if(!end_at){
                alert('请选择请假结束时间!');
                return;
            }
            _button.attr('data-loading','1');
            _button.text('请假中...');
            $.post('/leave/leaveDo',$('#leave_form').serialize(),function(data){
                alert(data.msg);
                if(data.error){
                    _button.attr('data-loading',0)
                    _button.text('申请请假');
                }
                else{
                    _button.text('请等待批复');
                    try{
                        window.close();
                    }
                    catch (e){

                    }

                }

            },'json')
        })

        $('[name=student_id]').change(function(){
            var _option = $(':selected',$(this));
            var _grade_id = _option.attr('data-grade');
            if(window.teachers[_grade_id]&&window.teachers[_grade_id]['head_img']){
                $('.teacherHead a img').attr('src',window.teachers[_grade_id]['head_img']);
                $('.teacherHead p').text(window.teachers[_grade_id]['head_nick']);
                $('.teacherHead').show();
            }
            else{
                $('.teacherHead').hide();
            }
        })
        $('[name=student_id]').trigger('change');
    })
</script>