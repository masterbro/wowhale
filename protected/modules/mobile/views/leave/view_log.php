<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;
    }}

    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #ccc;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: auto;}
    .log_wrap div.log_list{padding: 10px;line-height: 150%;font-size: 16px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {background-color:#fff;text-align: center; padding-top: 15px; padding-bottom: 15px;margin-bottom: 5px;border-bottom: 1px solid #ddd; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}
    body{background-color: #F2F2F2;}

    span.days {
        width: 100%;
        height: 100%;
        text-align: center;
        background: #f2f2f2;
        padding: 5px;
        display: block;
        border-radius: 50%;
        cursor: pointer;
    }
    .info_wrap,.detail_wrap{
        margin-left:10px;
    }
    .info_wrap  span.days{
        border-radius: inherit;
        width: 20px;
        height: 20px;
        float: left;
        padding:0;
        margin: 0;
    }
    .info_wrap  span{
        line-height: 20px;
        float: left;
        margin: 0 10px;
    }
    span.days.right{background: green;}
    span.days.invalid{background: #ff0000;}
    span.days.none{background: #f2f2f2;}
    span.days.active:after{content: ' ';
        position: absolute;
        bottom: 6px;
        right: 6px;
        width: 6px;
        height: 6px;
        background-color: red;
        border-radius: 3px;}
    .calendar_wrap table{width: 100%}
    .calendar_wrap table td{text-align: center;padding: 5px 10px;position: relative}
    .log_key_1{color:#ff0000;}
    .log_key_2{color: green;}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{border-bottom:1px solid #d6d6d6}
    .detail_wrap table tr td{padding: 5px;}
    .log_wrap.log_state{padding: 0;}

    .item{margin-bottom: 5px;background-color: #fff;}
    .ww-title{border: 0;padding: 0;margin: 5px;font-size: 12px;color:#aaa;}
    .field label{font-size: 12px;}
    .main_list{overflow: hidden;border-bottom: solid 1px #ddd;}
    .mleft_list{float: left;width: 50%;}
    .mright_list{float: right;width: 30%;text-align: right;}
    .time{margin-left: 10px;font-size: 24px;color: #f29b76;line-height: 30px;width:90%;}
    .btn{
        padding: 0;
        margin: 0 auto;width: 70%;line-height: 40px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius:5px;
        background-color: #ea8a8b;
    }
</style>
<div class="head_img_wrap">
    <img src="<?php echo HtmlHelper::get_head_img($app,$leave->contacts_id);?>" class="head_icon">
    <p><?php echo HtmlHelper::get_nick_name($app,$leave->contacts_id)?HtmlHelper::get_nick_name($app,$leave->contacts_id):(($contact=Contacts::model()->findByPk($leave->contacts_id))?$contact->parent_name:'');?></p>
</div>
<?php
$student = Student::model()->findByPk($leave->student_id);
$grade_id = $student->grade_id;
$current_user = $this->current_user;
?>
<div class="log_wrap log_state">
    <div class="item">
        <div class="main_list">
            <p class="ww-title">请假学生</p>
            <div class="log_list mleft_list"><?php echo $student->name;?></div>
            <div class="log_list mright_list" style="color: #83c1b5;"><?php echo Leave::$status[$leave->status];?></div>
        </div>
        <div class="main_list">
            <p class="ww-title">请假时间</p>
            <div class="mleft_list time"><?php echo date(/*'Y'.'年'.*/'m'.'月'.'d'.'日'.'H:i',$leave->start_at);?></div>
            <div style="float:left;width: 100%;padding-left: 50%;">到</div>
            <div class="mleft_list time"><?php echo date(/*'Y'.'年'.*/'m'.'月'.'d'.'日'.'H:i',$leave->end_at);?></div>
        </div>
    </div>
    <div class="item">
        <div class="main_list">
            <p class="ww-title">请假事由</p>
            <div class="log_list"><?php echo $leave->reason;?></div>
        </div>
    </div>
    <?php if($leave->status!=1):?>
    <div class="item">
        <div class="main_list">
            <p class="ww-title">审核时间</p>
            <div class="log_list"><?php echo date('Y/m/d H:i',$leave->confirm_at);?></div>
            <p class="ww-title">审核教师</p>
            <div class="log_list"><?php echo ($contact_teacher=Contacts::model()->findByPk($leave->teacher_id))?$contact_teacher->parent_name:'';?></div>
        </div>
    </div>
    <?php endif;?>
    <?php if($leave->status==0):?>
    <div class="item">
         <div class="main_list">
            <p class="ww-title">拒绝缘由</p>
            <div class="log_list"><?php echo $leave->confirm_txt;?></div>
         </div>
     </div>
    <?php elseif($leave->status==1 && ($current_user->type==Contacts::TY_TEACHER&&!$current_user->leader ) && $current_user->grade_id == $grade_id):?>
    <div class="item">
        <div class="main_list">
            <p class="ww-title">审核批复</p>
            <div class="log_list"><input type="radio" name="confirm_status" value="2" checked><span style="margin-left: 10px">同意</span> <input type="radio" name="confirm_status" value="0" style="margin-left: 15px"><span style="margin-left: 10px">拒绝</span></div>
            <div class="log_list field confirm_txt_wrap" style="height: inherit;display: none">
                <label>拒绝缘由：</label>
                <textarea name="confirm_txt" id="confirm_txt"></textarea>
            </div>
            <div class="log_list" style="height: inherit;text-align: center">
                <a href="javascript:;" class=" btn" id="accept" data-loading="0">确定</a>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
<script>
    $(function(){
        $('input[name=confirm_status]').click(function(){
            var _status = $(this).val();
            if(_status==0){
                $('.confirm_txt_wrap').show();
            }
            else{
                $('.confirm_txt_wrap').hide();
            }
        })
        $('#accept').click(function(){
            if($(this).attr('data-loading')=='1'){
                return;
            }
            var _status = $('input[name=confirm_status]:checked').val();
            var _confirm_txt = $.trim($('#confirm_txt').val());
            if(_status==0 && _confirm_txt==''){
                alert('请填写拒绝缘由');
                return;
            }
            $(this).attr('data-loading','1');
            $(this).text('确认中....');
            $.post('<?php echo $this->createUrl('/leave/leaveAccept',array('partner_id'=>$this->partner_id,'id'=>$leave->id));?>',{status:_status,'confirm_txt':_confirm_txt},function(data){
                alert(data.msg);
                if(data.error){
                }
                else{
                    document.location.reload();
                }
                $(this).attr('data-loading',0);
                $(this).text('确定');
            },'json')
        })

        $('input[name=confirm_status]:checked').trigger('click');
    })
</script>