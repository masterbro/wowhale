<style>
    .chk_1,.chk_2,.chk_3,.chk_4 {
        display: none;
    }

    /*******STYLE 1*******/
    .chk_1 + label {
        background-color: #FFF;
        border: 1px solid #C1CACA;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
        padding: 9px;
        border-radius: 5px;
        display: inline-block;
        position: relative;
        margin-right: 30px;
    }
    .chk_1 + label:active {
        box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
    }

    .chk_1:checked + label {
        background-color: #ECF2F7;
        border: 1px solid #92A1AC;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05), inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05), inset 15px 10px -12px rgba(255, 255, 255, 0.1);
        color: #243441;
    }

    .chk_1:checked + label:after {
        content: '\2714';
        position: absolute;
        top: 0px;
        left: 0px;
        color: #758794;
        width: 100%;
        text-align: center;
        font-size: 1.4em;
        padding: 1px 0 0 0;
        vertical-align: text-top;
    }
    .checkbox_label{float:right;margin-right:5px !important}
    .contacts_list .head_icon{width:30px;height: 30px;margin-right: 10px;}

</style>
<div class="p10_0 contacts_list">
	<h3 class="page-title" style="padding-left: 10px">老师</h3>
	<?php foreach($teachers as $l):?>
		<div class="contacts-item">
			<div class="contact" data-id="<?php echo $l->id ?>">
                <img src="<?php echo HtmlHelper::get_head_img($app,$l->id);?>" class="head_icon"/>
				<input type="checkbox" value="<?php echo $l->id ?>" name="users[]" class="chk_1" id="checkbox_a<?php echo $l->id;?>"/>
                <label for="checkbox_a<?php echo $l->id;?>" class="checkbox_label"></label>
				<?php echo $l->parent_name ?>
                <!--<a href="tel:<?php echo $l->parent_phone ?>"><?php echo $l->parent_phone ?></a>-->
			</div>
		</div>
	<?php endforeach;?>
	<br />
	<h3 class="page-title" style="padding-left: 10px">学生</h3>
	<?php foreach($students as $s):?>
		<div class="contacts-item">
			<p><?php echo $s->name ?> </p>
			<div class="constants-sub">
				<?php foreach($s->parents as $l):?>
				<div class="contact">
                    <img src="<?php echo HtmlHelper::get_head_img($app,$l->id);?>" class="head_icon"/>
					<input type="checkbox" value="<?php echo $l->id ?>" name="users[]" id="checkbox_sp<?php echo $l->id;?>" class="chk_1"/>
                    <label for="checkbox_sp<?php echo $l->id;?>" class="checkbox_label"></label>
					<?php echo $l->parent_name ?>
					<!--<a href="tel:<?php echo $l->parent_phone ?>"><?php echo $l->parent_phone ?></a>-->
				</div>
				<?php endforeach;?>
			</div>

		</div>
	<?php endforeach;?>
</div>
<div style="height: 50px;"></div>
<div class="contacts-chat-bar">
	<a href="javascript:;" id="create-chat">发起会话</a>
</div>


<script>
	$(function(){
		/*$('.contact a').click(function(e){
			e.stopPropagation();
		});*/

		$('.contacts-item').click(function(e){
            if(e.target.localName!='input' && e.target.localName!='a'){
                $(this).find('input:first').trigger('click');
                return false;
            }
            //console.info(e.target.localName)
			//$(this).find('input').trigger('click');
		});
        $('.contacts-item .contact').click(function(e){
            if(e.target.localName!='input' && e.target.localName!='a'){
                $(this).find('input:first').trigger('click');
                return false;
            }
            //console.info(e.target.localName)
            //$(this).find('input').trigger('click');
        });
		$('#create-chat').click(function(){
			var btn = $(this);

			if(btn.attr('data-sending') == 'true')
				return false;

			var users = [];
			$('input[name="users[]"]').each(function(){
				if($(this).prop('checked')) {
					users.push($(this).val());
				}
			});
			if(users.length < 1) {
				alert('请至少选择一名联系人');
				return false;
			}


			btn.html('创建聊天中...').attr('data-sending', 'true');
			$.ajax({
				url:'/contacts/chat?partner_id=<?php echo $this->partner_id ?>',
				type:'post',
				dataType:'json',
				data:{users:users},
				success: function(data) {
					if(data.error) {
						alert(data.msg);
						btn.html('发起会话').attr('data-sending', 'false');
					} else {
						alert(data.msg+','+'请在微信消息列表找到对应消息或进入企业号点击查看企业号消息,开始聊天');
                        btn.unbind('click').html('开始聊天').attr('href','<?php echo Yii::app()->params['mobile_base'];?>/page?key=page_contact_help');
					}
				},
				error: function(){
					btn.html('发起会话').attr('data-sending', 'false');
				}
			})
		});
	});
</script>