<div class="p10">
	<h2 class="page-title">班级</h2>

	<?php if(count($grade)):?>
		<?php foreach($grade as $l):?>
			<div class="order-item">
				<p><a href="<?php echo $this->createUrl('/contacts/list',array('grade_id'=>$l->id, 'partner_id'=>$this->partner_id)) ?>"><?php echo $l->name ?></a></p>
			</div>
		<?php endforeach;?>

	<?php endif;?>

</div>