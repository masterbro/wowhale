<?php
$config = WechatConfig::getSingleInstance(Yii::app()->params['wx_payment_qy']['wechat_config_id']);
if($config){
    $app = new WxQyApp($config);
    $signPackage = $app->getSignPackage();
}
?>
<?php if($signPackage):?>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script>
        wx.config({
            debug: false,
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp:  <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
                'onMenuShareAppMessage',
                'onMenuShareTimeline',
                'onMenuShareWeibo',
                'onMenuShareQQ',
                'hideMenuItems',
                'showMenuItems',
                'hideAllNonBaseMenuItem',
                'showAllNonBaseMenuItem',
                'hideOptionMenu',
                'showOptionMenu',
                'previewImage'
            ]
        });
        function get_img(){
            try{
                var _img = $('img:eq(1)').attr('src');
                if(!_img){
                    _img = $('img:eq(0)').attr('src');
                }
                return _img;
            }
            catch(e){
                return '<?php echo $this->createAbsoluteUrl('/assets/images/island_logo.png') ?>';
            }

        }
        var the_keyword = "<?php echo $keyword;?>";
        wx.ready(function () {

            wx.onMenuShareTimeline({
                title: the_keyword?the_keyword:document.title, // 分享标题
                link: document.location.href, // 分享链接
                desc: the_keyword?the_keyword:"\n[娃娃营]校园的科技枢纽", // 分享描述
                imgUrl: get_img(), // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
            wx.onMenuShareAppMessage({
                title: document.title, // 分享标题
                desc: the_keyword?the_keyword:"\r\n[娃娃营]校园的科技枢纽", // 分享描述
                link: document.location.href, // 分享链接
                imgUrl: get_img(), // 分享图标
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        })
    </script>
<?php endif;?>