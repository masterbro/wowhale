<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="any" mask="" href="/favicon.ico">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<title><?php if(Yii::app()->params['evn'] == 'test'):?>beta <?php endif;?><?php echo $this->pageTitle;?></title>
	<meta name="keywords" content="WoWhale 儿童课外活动第一品牌" />
	<meta name="description" content="WoWhale 儿童课外活动第一品牌" />
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,minimal-ui" />
	<link rel="stylesheet" href="<?php echo HtmlHelper::assets('css/mobile/main.css?v='.Yii::app()->params['assets_version']) ?>">
	<link rel="stylesheet" href="<?php echo HtmlHelper::assets('js/owl_carousel/owl.carousel.css') ?>">
	<script type="text/javascript" src="<?php echo HtmlHelper::assets('js/jquery-2.1.3.min.js') ?>"></script>
</head>
<?php $controller = Yii::app()->controller;?>
<body class="page-<?php echo $controller->id; ?>">
<img src="<?php echo $this->createAbsoluteUrl('/assets/images/island_logo.png') ?>" width="0" height="0" style="position:absolute">

<?php echo $content; ?>
</body>
<script type="text/javascript">
	window.page = "page-<?php echo $controller->id; ?>";
</script>
<script type="text/javascript" src="<?php echo HtmlHelper::assets('js/owl_carousel/owl.carousel.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo HtmlHelper::assets('js/mobile/merchant.js?v='.Yii::app()->params['assets_version'])  ?>"></script>
<?php if($this->page_tracking):?>
<script type="text/javascript" src="<?php echo $this->createUrl("/pageTracking?partner_id={$this->partner_id}&controller={$controller->id}&action={$controller->action->id}") ?>"></script>
<?php endif;?>
<?php echo $this->renderPartial('//layouts/_baidu'); ?>
</html>
