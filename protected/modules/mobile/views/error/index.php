<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<title>娃娃营</title>
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,minimal-ui" />
	<style>
		*{padding: 0;margin: 0;}
		body {
			background: #eae8ee;
		}
		.error {
			width: 100%;
			text-align: center;
			background: url("<?php echo HtmlHelper::assets('images/logo.ico') ?>") no-repeat center bottom;
			height: 200px;
			background-size: 75px 75px;

		}
		.error  p{
			font-size: 14px;
			color: #8f8f8f;
			padding-top: 220px;

		}
	</style>
</head>
<body>

<div class="error">
	<p><?php echo $msg ?></p>
</div>


<script type="application/javascript">
	function onBridgeReady(){
		WeixinJSBridge.call('hideOptionMenu');
	}

	if (typeof WeixinJSBridge == "undefined"){
		if( document.addEventListener ){
			document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
		}else if (document.attachEvent){
			document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
			document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		}
	}else{
		onBridgeReady();
	}
</script>
</body>
</html>
