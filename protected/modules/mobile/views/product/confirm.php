
<div class="p10" style="background: #fff">

		<h2 class="page-title">
			<?php echo $model->name;$attr_name = ''; ?>
			<?php
				$attr_name = '';$attr_ids = '';
				foreach($attr as $a) {
					$attr_name .= ','.$a->name;
					$attr_ids .= ','.$a->id;
				}

				$attr_name = trim($attr_name, ',');
				if($attr_name) echo " ({$attr_name})";
			?>
		</h2>
	<script>
		window.pdAttr = [<?php echo trim($attr_ids, ','); ?>];
	</script>
		<br />
		<p>价格：￥<span class="price pd-price-<?php echo $model->id ?>"><?php echo CommonHelper::price($price) ?></span></p>
		<br />
		<section class="select">
			<p class="label">数量</p>
			<p class="option"><a class="btn-del" id="pd-minus">-</a><span class="fm-txt" id="pd-qty">1</span><a class="btn-add" id="pd-plus">+</a></p>
		</section>

		<div style="clear: both"></div>
		<div class="order-item"></div>
		<p class="p10"></p>


	<div id="shipping-info-list">
		<div class="order-item">
			<h3 class="ww-title">
				<select class="my-shipping-info" onchange="fillShippingInfo($(this))" style="float: right">
					<option value="">常用联系人</option>
					<?php foreach($info as $i):?>
						<option value="<?php echo $i->id ?>" data-info='<?php echo CJSON::encode($i) ?>'><?php echo $i->student_name ?></option>
					<?php endforeach;?>
				</select>
				联系人 1
			</h3>
			<div class="field">
				<label>学生姓名：<span>*</span></label>
				<input type="text" name="student_name[]" class="ipt student_name" data-name="学生姓名" value="" />
			</div>
			<div class="field">
				<label>家长姓名：<span>*</span></label>
				<input type="text" name="parent_name[]" class="ipt parent_name" data-name="家长姓名" value="" />
			</div>
			<div class="field">
				<label>联系电话：<span>*</span></label>
				<input type="text" name="tel[]" class="ipt tel" value="" data-name="联系电话" />
			</div>
			<div class="field">
				<label>生日：<span>*</span></label>
				<input type="date" name="birth[]" class="ipt birth"  value="" data-name="生日" />
			</div>
		</div>
	</div>



		<p class="p10"></p>

		<div class="field">
			<label>订单备注：</label>
			<textarea name="remark"></textarea>
		</div>
</div>


<script>
	window.pid = <?php echo $model->id ?>;
	window.price = <?php echo $price ?>;
</script>



<div style="height: 60px;"></div>
<div class="footer-panel">
	<div class="col"><a href="tel:<?php echo $merchant->contact_tel ?>" class="tel"><?php echo $merchant->contact_tel ?></a></div><div class="col pay-wrap">
		<a href="javascript:;" class="cash btn" id="ww-cash-single" data-paying="false">付款<span>￥<em class="ww-total pd-price-<?php echo $model->id ?>"><?php echo CommonHelper::price($price) ?></em></span></a>
	</div>
</div>

<div style="display: none" id="confirm-field-tpl">
	<div class="order-item">
		<h3 class="ww-title">
			<select class="my-shipping-info" onchange="fillShippingInfo($(this))" style="float: right">
				<option value="">常用联系人</option>
				<?php foreach($info as $i):?>
					<option value="<?php echo $i->id ?>" data-info='<?php echo CJSON::encode($i) ?>'><?php echo $i->student_name ?></option>
				<?php endforeach;?>
			</select>
			联系人 <span class="num"></span>
		</h3>
		<div class="field">
			<label>学生姓名：<span>*</span></label>
			<input type="text" name="student_name[]" class="ipt student_name" data-name="学生姓名" value="" />
		</div>
		<div class="field">
			<label>家长姓名：<span>*</span></label>
			<input type="text" name="parent_name[]" class="ipt parent_name" data-name="家长姓名" value="" />
		</div>
		<div class="field">
			<label>联系电话：<span>*</span></label>
			<input type="text" name="tel[]" class="ipt tel" value="" data-name="联系电话" />
		</div>
		<div class="field">
			<label>生日：<span>*</span></label>
			<input type="date" name="birth[]" class="ipt birth"  value="" data-name="生日" />
		</div>
	</div>
</div>