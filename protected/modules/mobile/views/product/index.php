<img src="<?php echo $model->image ? HtmlHelper::image($model->image) : HtmlHelper::assets('images/no_pic.png') ?>" style="width: 100%" />

<div class="product-title">
	<div class="ww-title-wrap">
		<div class="price">
			<span><?php if($model->original_price) echo '<del>￥'.CommonHelper::price($model->original_price).'</del>' ?>￥<em class="price pd-price-<?php echo $model->id ?>"><?php echo CommonHelper::price($model->price) ?></em></span>
		</div>
		<div class="title">
			<h2><?php echo $model->name ?></h2>
		</div>

	</div>
</div>


<?php $attr = $model->attrData(); ?>
<?php if($attr):?>
<div class="product-attr" id="product-detail-attr">
	<h3 class="ww-title">请选择</h3>
	<div class="item" id="pd-<?php echo $model->id ?>" data-pid="<?php echo $model->id ?>" data-price="<?php echo $model->price ?>">
			<?php foreach($attr as $a):?>
				<div class="attr">
					<div class="group" data-pid="<?php echo $model->id ?>">
						<h5><?php echo $a['parent']->name ?></h5>
						<?php foreach($a['sub'] as $sa):?>
							<i data-attr-id="<?php echo $sa->id ?>" data-attr-price="<?php echo $sa->price ?>"><?php echo $sa->name ?></i>
						<?php endforeach;?>
					</div>

				</div>
			<?php endforeach;?>

	</div>
</div>
<?php endif;?>
<!---->
<!--<div class="product-user-info">-->
<!--	<h3 class="ww-title">个人信息</h3>-->
<!---->
<!--	<div class="field">-->
<!--		<label>学生姓名：<span>*</span></label>-->
<!--		<input type="text" name="student_name" class="ipt" value="--><?php //echo $this->current_user->student_name ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>家长姓名：<span>*</span></label>-->
<!--		<input type="text" name="parent_name" class="ipt" value="--><?php //echo $this->current_user->parent_name ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>联系电话：<span>*</span></label>-->
<!--		<input type="text" name="tel" class="ipt" value="--><?php //echo $this->current_user->tel ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>生日：<span>*</span></label>-->
<!--		<input type="date" name="birth" class="ipt"  value="--><?php //echo $this->current_user->birth ?><!--" />-->
<!--	</div>-->
<!---->
<!--	<div class="field">-->
<!--		<label>订单备注：</label>-->
<!--		<textarea name="remark"></textarea>-->
<!--	</div>-->
<!--</div>-->

<div class="product-content">
	<h3 class="ww-title">详情</h3>
	<div class="rich-text">
		<?php echo $model->content; ?>
	</div>
</div>



<script>
	window.pid = <?php echo $model->id ?>;
</script>



<div style="height: 60px;"></div>
<div class="footer-panel">
	<div class="col"><a href="tel:<?php echo $merchant->contact_tel ?>" class="tel"><?php echo $merchant->contact_tel ?></a></div><div class="col pay-wrap">
		<a href="javascript:;" class="pay btn" id="ww-product-sub">立即报名</a>
<!--		<a href="javascript:;" class="cash btn" id="ww-cash" data-paying="false">付款<span>￥<em class="ww-total pd-price---><?php //echo $model->id ?><!--">--><?php //echo CommonHelper::price($model->price) ?><!--</em></span></a>-->
	</div>
</div>