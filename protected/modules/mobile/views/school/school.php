<style>
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; border-bottom:1px solid #ddd;}
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}
	.order_list_wrap{background-color: #fff;}
    .order_list_wrap .order_list_table{width:100%}
    .order_list_wrap .ww-title{margin-bottom:15px;}
    .order_list_wrap .order_list_table tr td {padding: 5px 0;}
    .order_list_wrap .order_list_table tr  {border-top:1px solid #d6d6d6}
    .order_list_wrap .order_list_table tr:last-child  {border-bottom:1px solid #d6d6d6}

	body{background-color: #ebebeb;}
	.product-list .item{border: 0;border-bottom: 1px solid #d6d6d6;background-color: #fff;border-radius: 4px;margin-bottom: 5px;}
	#stu-info{background-color: #fff; border-bottom: 1px solid #ddd;padding:5px 10px;}
	.field{background-color: #fff;padding:10px;}
	.p10{padding-bottom: 5px;}
	.p10-field{padding:0;}
	.ww-title{border: 0;}
	#btn-partner{background-color: #fff;overflow: hidden;padding: 10px;}
	#btn-left{float: left;width: 47%;}
	#btn-left a{padding: 0;}
	#btn-left a img{width: 25px;margin-bottom: 5px;}
	#btn-right{float: right;width: 47%;}
	.btn-tel{background-color: #98c18a;line-height: 40px;border-radius: 4px;}
	.btn-p{background-color: #00b7ee;line-height: 40px;border-radius: 4px;}
</style>
<div class="head_img_wrap">
    <img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon">
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>

<div id="product-list" class="p10 product-list">
	<!--<h3 class="ww-title">缴费项目</h3>-->

	<?php foreach($products as $p):?>
		<?php $attr = $p->attrData(); ?>
		<div class="item" id="pd-<?php echo $p->id ?>" data-pid="<?php echo $p->id ?>" data-price="<?php echo $p->price ?>">
			<h4><span><?php if($p->original_price) echo '<del>￥'.CommonHelper::price($p->original_price).'</del>' ?>￥<em class="price pd-price-<?php echo $p->id ?>"><?php echo CommonHelper::price($p->price) ?></em></span><?php echo $p->name ?></h4>
			<?php if($attr):?>
				<?php foreach($attr as $a):?>
					<div class="attr">
						<div class="group" data-pid="<?php echo $p->id ?>">
							<h5><?php echo $a['parent']->name ?></h5>
							<?php foreach($a['sub'] as $sa):?>
								<i data-attr-id="<?php echo $sa->id ?>" data-attr-price="<?php echo $sa->price ?>"><?php echo $sa->name ?></i>
							<?php endforeach;?>
						</div>

					</div>
				<?php endforeach;?>
			<?php endif;?>
		</div>
	<?php endforeach;?>
</div>

<div class="customer-info p10-field">
	<!--<h3 class="ww-title">学生信息</h3>-->
	<div id="stu-info">
		<?php if($students):?>
			<div class="field">
				<label>学生姓名：</label>
				<?php if(count($students) > 1):?>
				<select name="student_id">
					<?php foreach($students as $s) :?>
						<option value="<?php echo $s->id ?>"><?php echo $s->name ?></option>
					<?php endforeach;?>
				</select>
				<?php else:?>
					<?php echo $students[0]->name ?>
					<select name="student_id" style="display: none">
						<?php foreach($students as $s) :?>
							<option value="<?php echo $s->id ?>"><?php echo $s->name ?></option>
						<?php endforeach;?>
					</select>
				<?php endif;?>

			</div>
		<?php else:$no_student=true;?>
			<br>
			<p class="error">你还没有关联学生</p>
		<?php endif;?>
	</div>
<!--	<div class="field">-->
<!--		<label>学生姓名：<span>*</span></label>-->
<!--		<input type="text" name="student_name" class="ipt" value="--><?php //echo $this->current_user->student_name ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>学生班级：<span>*</span></label>-->
<!--		<input type="text" name="student_grade" class="ipt" value="--><?php //echo $this->current_user->student_grade ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>家长姓名：<span>*</span></label>-->
<!--		<input type="text" name="parent_name" class="ipt" value="--><?php //echo $this->current_user->parent_name ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>联系电话：<span>*</span></label>-->
<!--		<input type="text" name="tel" class="ipt" value="--><?php //echo $this->current_user->tel ?><!--" />-->
<!--	</div>-->
<!--	<div class="field">-->
<!--		<label>生日：<span>*</span></label>-->
<!--		<input type="date" name="birth" class="ipt"  value="--><?php //echo $this->current_user->birth ?><!--" />-->
<!--	</div>-->

	<div class="field">
		<label>订单备注：</label>
		<textarea name="remark"></textarea>
	</div>

	<div id="btn-partner">
		<div id="btn-left">
			<a href="tel:<?php echo $model->contact_tel ?>" class="pay btn btn-tel">
				<img src="<?php echo HtmlHelper::assets('images/mobile/school/tel-icon.png') ?>">
				<?php echo $model->contact_tel ?>
			</a>
		</div>
		<div id="btn-right" <?php if($no_student):?>style="display:none"<?php endif;?>>
			<a href="#product-list" class="pay btn btn-p" id="ww-pay">立即缴费</a>
			<a href="javascript:;" class="cash btn btn-p hidden" id="ww-cash" data-paying="false">付款<span>￥<em class="ww-total">0.00</em></span></a>
		</div>
	</div>

</div>


<?php if($orders):?>
<div class="order_list_wrap p10">
    <h5 class="ww-title">缴费历史</h5>
    <table class="order_list_table">
    <?php foreach($orders as $p):?>
        <tr>
            <?php $details = $p->order_details(); ?>
            <td width="80px"><?php echo date('y/m/d',$p->begin_time);?></td>
            <td width="80px"><?php echo CommonHelper::price($p->total);?></td>
            <td>
            <?php foreach ($details as $k=>$v):?>
                <?php echo $v->title ?>&nbsp;
            <?php endforeach;?>
            </td>
        </tr>
    <?php endforeach;?>
    </table>
</div>

<?php endif;?>
<!--<div style="height: 60px;"></div>-->
<!--<div class="footer-panel">

</div>-->
<script>
	window.partner_id = <?php echo $model->id ?>;
</script>