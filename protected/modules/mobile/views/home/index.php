<?php echo $this->renderPartial('/share/_primary_nav');?>

<?php if(count($slider)):?>
	<div id="owl-merchant-wrap">
		<div id="owl-merchant" class="owl-carousel">
			<?php foreach($slider as $k=>$s):?>
				<div>
					<a href="<?php echo ($s->url) ? $s->url : '#' ?>">
						<img class="lazyOwl owl-img" style="" data-src="<?php echo HtmlHelper::image($s->img) ?>" />
					</a>
				</div>
			<?php endforeach;?>
		</div>
	</div>

	<script>
		window.owlH = document.documentElement.clientWidth + 'px';
		document.querySelector('#owl-merchant').style.height = window.owlH;
		document.querySelector('#owl-merchant-wrap').style.height = window.owlH;

		var owlImgs = document.querySelectorAll('.owl-img');
		for (var i = 0, len = owlImgs.length; i < len; i++) {
			owlImgs[i].style.height = window.owlH;
			owlImgs[i].style.width = window.owlH;
		}


	</script>

<?php endif;?>
	<div class="p10">
		<div class="home-slogan" style="border: 0 none">儿童课外活动第一品牌</div>
	</div>
<?php foreach($category as $c):?>
	<div class="home-category">
		<a href="<?php echo $this->createUrl('/home/product?cid='.$c->id) ?>">
			<img src="<?php echo HtmlHelper::image($c->image) ?>" style="width: 100%" />
		</a>
	</div>
<?php endforeach;?>

<div class="p10">
	<div class="home-slogan">开启精彩童年</div>
</div>


<?php echo $this->renderPartial('/share/_footer');?>