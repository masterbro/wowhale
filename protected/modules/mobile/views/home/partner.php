<style type="text/css">
	/*图标的样式*/
	#icon{
		margin:5% 2%;
		background-color:#fdfdfd;
		border: solid 1px #eee;
		-moz-border-radius: 15px;      /* Gecko browsers */
		-webkit-border-radius: 15px;   /* Webkit browsers */
		border-radius:15px;            /* W3C syntax */
	}
	.item{
		float: left;
		width: 33%;
		text-align: center;
		margin: 0 auto;
		padding: 2% 0;
		/*text-shadow:2px 1px 3px #ccc;*/
	}
	.item img{
		margin-bottom: 5px;
		-moz-border-radius: 11px;      /* Gecko browsers */
		-webkit-border-radius: 11px;   /* Webkit browsers */
		border-radius:11px;            /* W3C syntax */

		-moz-box-shadow:1px 1px 1px 1px #ddd;
		-webkit-box-shadow:1px 1px 1px 1px #ddd;
		box-shadow:1px 1px 1px 1px #ddd;
	}
	#item-1,#item-2{
		border-right: solid 1px #eee;
	}
</style>

<div id="owl-merchant-wrap">
	<div id="owl-merchant" class="owl-carousel">
		<?php $slider = $model->sliderArr();?>
		<?php foreach($slider as $k=>$s):?>
			<div>
				<img class="lazyOwl owl-img" style="" data-src="<?php echo HtmlHelper::image($s) ?>" />
			</div>
		<?php endforeach;?>
	</div>
</div>

<script>
	window.owlH = document.documentElement.clientWidth + 'px';
	document.querySelector('#owl-merchant').style.height = window.owlH;
	document.querySelector('#owl-merchant-wrap').style.height = window.owlH;

	var owlImgs = document.querySelectorAll('.owl-img');
	for (var i = 0, len = owlImgs.length; i < len; i++) {
		owlImgs[i].style.height = window.owlH;
		owlImgs[i].style.width = window.owlH;
	}


</script>

<!--<div class="p10 ww-content">-->
<!--	<h2 class="ww-merchant-info-title">学校简介</h2>-->
<!--</div>-->

<div id="icon">

	<div class="item" id="item-1">
		<a href="#myself"><img src="<?php echo HtmlHelper::assets('images/mobile/img-1.png') ?>" /></a>
		<p>园所介绍</p>
	</div>

	<div class="item" id="item-2">
		<a href="<?php echo $this->createUrl('/notice/shipu?partner_id='.$model->id );?>"><img src="<?php echo HtmlHelper::assets('images/mobile/img-2.png') ?>" /></a>
		<p>宝宝食谱</p>
	</div>

	<div class="item" id="item-3">
		<a href="<?php echo $this->createUrl('/albums/?partner_id='.$model->id);?>"><img src="<?php echo HtmlHelper::assets('images/mobile/img-3.png') ?>" /></a>
		<p>宝宝相册</p>
	</div>

</div>

<div class="p10">
	<div id="myself"></div>

	<h1 class="ww-merchant-title"><?php echo $model->title ?><?php if(Yii::app()->params['evn'] == 'test'):?><span class="text-danger">[DEMO]</span> <?php endif;?></h1>
	<!--	<div class="rich-text-wrap">-->
	<div class="rich-text">
		<?php echo $model->content ?>
		<!--		</div>-->
	</div>
    <?php /*if($model->contact_tel):?>
        联系电话：<a href="tel:<?php echo preg_replace('/[^0-9\*\#]/','',$model->contact_tel);?>"><?php echo $model->contact_tel;?></a>
    <?php endif;*/?>
    <style>
        .wwy:before{content: '.';content: ' ';
            position: absolute;
            top: 0;
            left: -8px;
            top: 6px;
            width: 6px;
            display: block;
            height: 6px;
            text-align: center;
            background-color: red;
            border-radius: 3px;}
        .wwy{position: relative}
    </style>
    <div class="rich-text">
        阅读<?php echo $model->upHits();?>次
        <span style="display: block;float: right" class="wwy"><a href="http://www.wwcamp.com" style="color: #888;text-decoration: underline !important;">娃娃营-校园的科技枢纽</a></span>
    </div>

</div>
<div style="height: 60px;"></div>
<?php if($model->contact_tel):?>
<div class="footer-panel">
    <div class="col" style="width:100%;"><a href="tel:<?php echo preg_replace('/[^0-9\*\#]/','',$model->contact_tel);?>" class="tel"><?php echo $model->contact_tel ?></a></div>
</div>
<?php endif;?>
