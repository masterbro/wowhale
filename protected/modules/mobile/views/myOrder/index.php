<?php echo $this->renderPartial('/share/_primary_nav');?>
<div class="p10">
	<h2 class="page-title">我的订单</h2>

	<?php if(count($list)):?>
	<?php foreach($list as $l):?>
	<div class="order-item">
		<p>订单号：<?php echo $l->display_id ?></p>
		<p>订单金额：￥<?php echo CommonHelper::price($l->total) ?></p>
		<p>下单时间：<?php echo date('y/m/d H:i:s', $l->begin_time) ?></p>
		<p>订单内容：<?php
			foreach($l->order_details as $lk=>$lo) {
				echo '<br />';
				echo $lo->title. '<span class="text-danger">￥'.CommonHelper::price($lo->price).'x'.$lo->qty.'</span>';
			}
			?></p>
	</div>
	<?php endforeach;?>

	<?php else:?>
		<p class="alert alert-success">你还没有任何订单哦！<a href="<?php echo $this->createUrl('/') ?>">大手牵小手，我们一起走</a></p>

	<?php endif;?>

<nav style="text-align: center">
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>3,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
	</div>