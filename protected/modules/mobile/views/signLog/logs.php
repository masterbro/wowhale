<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;   }
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #ccc;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}

    span.days {
        width: 100%;
        height: 100%;
        text-align: center;
        background: #f2f2f2;
        padding: 5px;
        display: block;
        border-radius: 50%;
        cursor: pointer;
    }
    .info_wrap,.detail_wrap{
        margin-left:10px;
    }
    .info_wrap  span.days{
        border-radius: inherit;
        width: 20px;
        height: 20px;
        float: left;
        padding:0;
        margin: 0;
    }
    .info_wrap  span{
        line-height: 20px;
        float: left;
        margin: 0 10px;
    }
    span.days.right{background: green;}
    span.days.invalid{background: #ff0000;}
    span.days.none{background: #f2f2f2;}
    span.days.active:after{content: ' ';
        position: absolute;
        bottom: 6px;
        right: 6px;
        width: 6px;
        height: 6px;
        background-color: red;
        border-radius: 3px;}
    .calendar_wrap table{width: 100%}
    .calendar_wrap table td{text-align: center;padding: 5px 10px;position: relative}
    .log_key_1{color:#ff0000;}
    .log_key_2{color: green;}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{border-bottom:1px solid #d6d6d6}
    .detail_wrap table tr td{padding: 5px;}
</style>
<div class="head_img_wrap">
    <img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon">
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>

<div class="calendar_wrap">
    <table>
        <tr>
            <td><a href="<?php echo $this->createUrl('/signLog/logs',array('partner_id'=>$this->partner_id,'month'=>date('Y-m',$pre_month_time)));?>"><</a></td>
            <td colspan="5" align="center"> <?php echo date('Y-m',$month_start_time);?> </td>
            <td><a href="<?php echo $this->createUrl('/signLog/logs',array('partner_id'=>$this->partner_id,'month'=>date('Y-m',$month_end_time)));?>">></a></td>
        </tr>
        <tr>
            <td>日</td>
            <td>一</td>
            <td>二</td>
            <td>三</td>
            <td>四</td>
            <td>五</td>
            <td>六</td>
        </tr>
        <?php $start_week_day = date('w',$month_start_time);?>
        <tr>
        <?php for($i=0;$i<$start_week_day;$i++):?>
            <td>&nbsp;</td>
        <?php endfor;?>
        <?php while( ($day_time = ($month_start_time + ($i-$start_week_day)*3600*24) ) < $month_end_time ):?>
        <?php
            $day =  date('d',$day_time);
            if($show_list[$day] && count($show_list[$day])>1){
                $show_class = 'right';
            }
            else if($show_list[$day] && count($show_list[$day])==1){
                $show_class = 'invalid';
            }
            else{
                $show_class = 'none';
            }
            if($day==date('d')){
                $show_class .= ' active';
            }
            $day = "<span class=\"days {$show_class}\" data-day=\"".$day."\">{$day}</span>";
        ?>
        <?php if(date('w',$day_time)==6):?>
            <td>
                <?php echo $day;?>
            </td>
        </tr>
        <?php elseif(date('w',$day_time)==0):?>
        <tr>
            <td>
                <?php echo $day;?>
            </td>
        <?php else:?>
            <td>
                <?php echo $day;?>
            </td>
        <?php endif;?>
        <?php $i++;?>
        <?php endwhile;?>
        <?php if( ($end_week_day = date('w',$day_time) )!=0):?>
            <?php for($j=0;$j<7-$end_week_day;$j++):?>
                <td>&nbsp;</td>
            <?php endfor;?>
        </tr>
        <?php endif;?>
    </table>
</div>
<div class="info_wrap" style="margin-bottom:10px;">
    <h3 class="ww-title" style="margin-bottom: 5px;">签到状态</h3>
    <span class="days right "> </span><span>正常签到</span>
    <span class="days invalid"> </span><span>非正常签到</span>
    <span class="days none"> </span><span>未签到</span>
</div>
<div class="detail_wrap">
    <h3 class="ww-title" style="margin: 10px 0 5px ;">签到明细</h3>
    <table>
    </table>
</div>
<script>
    var details = '<?php echo  CJSON::encode($show_list);?>';
    try{
        details = JSON.parse(details);
    }
    catch(e){
        details = {};
    }
    function getActiveLog(){
        var _keys = ['','签到上班','签到下班'];
        var _detail_obj =$('.days.active');
        var _detail_day = _detail_obj.attr('data-day');
        var _detail = details[_detail_day];
        $('.detail_wrap table').empty();

        if(!_detail){
        }
        else{
            var _tmps = [];
            $.each(_detail,function(k,v){
                $.each(v,function(kk,vv){
                    var _obj = {'k':k,'v':vv,'html':'<tr class="log_key_'+k+'"><td>'+vv+' '+_keys[k]+'</td></tr>'}
                    _tmps.push(_obj)
                })

            })
            _tmps.sort(function(v1,v2){
                var _time1 = parseInt(v1.v.replace(':',''));
                var _time2 = parseInt(v2.v.replace(':',''));
                return _time1-_time2;
            })
            var _str = '';
            $.each(_tmps,function(k,v){
                _str+= v.html;
            })
            $('.detail_wrap table').append(_str);
        }
    }
    $(function(){
        getActiveLog();
        $('.calendar_wrap .days').height($('.days.active').width()-2);
        $('.calendar_wrap .days').css('line-height',$('.days').height()+'px')
        $('.calendar_wrap .days').click(function(){
            $('.days').removeClass('active');
            $(this).addClass('active');
            getActiveLog();
        })
    })
</script>