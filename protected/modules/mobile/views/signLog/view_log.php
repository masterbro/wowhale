<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;
    }}
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #ccc;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
    .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}

    span.days {
        width: 100%;
        height: 100%;
        text-align: center;
        background: #f2f2f2;
        padding: 5px;
        display: block;
        border-radius: 50%;
        cursor: pointer;
    }
    .info_wrap,.detail_wrap{
        margin-left:10px;
    }
    .info_wrap  span.days{
        border-radius: inherit;
        width: 20px;
        height: 20px;
        float: left;
        padding:0;
        margin: 0;
    }
    .info_wrap  span{
        line-height: 20px;
        float: left;
        margin: 0 10px;
    }
    span.days.right{background: green;}
    span.days.invalid{background: #ff0000;}
    span.days.none{background: #f2f2f2;}
    span.days.active:after{content: ' ';
        position: absolute;
        bottom: 6px;
        right: 6px;
        width: 6px;
        height: 6px;
        background-color: red;
        border-radius: 3px;}
    .calendar_wrap table{width: 100%}
    .calendar_wrap table td{text-align: center;padding: 5px 10px;position: relative}
    .log_key_1{color:#ff0000;}
    .log_key_2{color: green;}
    .detail_wrap table tr{border-top:1px solid #d6d6d6}
    .detail_wrap table tr:last-child{border-bottom:1px solid #d6d6d6}
    .detail_wrap table tr td{padding: 5px;}
    .log_wrap.log_state{padding: 5px;}
</style>
<div class="head_img_wrap">
    <img src="<?php echo HtmlHelper::get_head_img($app,$signLog->contacts_id);?>" class="head_icon">
    <p><?php echo HtmlHelper::get_nick_name($app,$signLog->contacts_id)?HtmlHelper::get_nick_name($app,$signLog->contacts_id):(($contact=Contacts::model()->findByPk($signLog->contacts_id))?$contact->parent_name:'');?></p>
</div>
<div class="log_wrap log_state">
    <h3 class="ww-title" style="margin: 10px 0 5px ;">签到状态</h3>
    <div class="log_list"><?php echo $signLog->type==2?'下班签到':'上班签到';?></div>
    <h3 class="ww-title" style="margin: 10px 0 5px ;">签到时间</h3>
    <div class="log_list"><?php echo date('Y/m/d H:i:s',$signLog->time);?></div>
</div>
