<style>
    .sign_button{text-align: center;padding: 10px 5px;}
    .map_wrap{width:100%;height:50%}
    #l-map {width: 100%;height: 100%;overflow: hidden;margin:0;}
    .address_wrap{padding: 5px 5px;line-height: 32px;height: 32px;font-size: 14px;
    }}
    .address_wrap .address_title{width: 28%;float: left;}
    .address_wrap .address_info{width: 70%;float: right;text-align: right;padding-right: 5px;color:#0c0c0c}
    .sign_button_wrap{width: 100%;background-color: #f2f2f2;}
    .sign_button_wrap .sign_button.sign{width: 45%;float:left}
    .sign_button_wrap .sign_button.signout{width: 45%;float:right}
    .sign_button_wrap .sign_button a{background:gray;width: 70%;color:white;font-weight: bolder;display: block;border-radius: 5px;padding: 8px 2px;margin: 0 auto;font-size:18px}
    .sign_button_wrap .sign_button.sign.can_click a{background:red;}
    .sign_button_wrap .sign_button.signUse.can_click a{background:red;}
    .sign_button_wrap .sign_button.signout.can_click a{background:green;}
    .log_wrap{width: 100%}
    .log_wrap div.log_list{border-bottom: solid 1px #ddd;padding: 10px 0px;line-height: 150%;clear:both;height: 14px;}
    .log_wrap div.log_list div.day{width:38%;float: left;}
    .log_wrap div.log_list div.sign{width:30%;float: left;}
    .log_wrap div.log_list div.signout{width:30%;float: left;}
     .head_img_wrap {text-align: center; padding-top: 15px;background-color: #fafafa; padding-bottom: 15px; }
    .head_img_wrap img.head_icon {width: 64px; border-radius: 32px;background-color: #ddd; }
    .head_img_wrap p{font-size: 14px;font-weight: 800;margin-top:10px}


</style>
<div class="head_img_wrap">
    <a href="<?php echo $this->createUrl('/signLog/logs',array('partner_id'=>$this->partner_id));?>"><img src="<?php echo HtmlHelper::get_head_img($app,$this->current_user->id);?>" class="head_icon"></a>
    <p><?php echo HtmlHelper::get_nick_name($app,$this->current_user->id)?HtmlHelper::get_nick_name($app,$this->current_user->id):$this->current_user->parent_name;?></p>
</div>
<div class="map_wrap" id="map_wrap">
    <div id="l-map">

    </div>
</div>
<div class="address_wrap">
    <span class="address_title">当前位置：</span>
    <span id="address" class="address_info">获取中....</span>
</div>
<div class="sign_button_wrap">
    <div class="sign_button sign " data-type="1"><a href="javascript:void(0)">上班</a></div>
    <div class="sign_button signout " data-type="2"><a href="javascript:void(0)">下班</a></div>
</div>
<?php if($this->current_user->leader):?>
<div class="sign_button_wrap" style="margin-top:10px">
    <div class="sign_button signUse "><a href="javascript:void(0)">设为签到地点</a></div>
</div>
<?php endif;?>
<div class="log_wrap">
    <?php $shangban=$xiaban=false;
        foreach($list as $k=>$v){
            if($v->type==2 &&! $xiaban){
                $xiaban = $v->time;
            }
            elseif($v->type==1){
                $shangban =  $v->time;
            }
        }
    ?>
    <div class="log_list">
        <div class="day">&nbsp;<?php echo date('y/m/d');?></div>
        <div class="sign">&nbsp;<?php if($shangban):?>已上班<?php else:?>未上班<?php endif;?></div>
        <div class="signout">&nbsp;<?php if($xiaban):?>已下班<?php else:?>未下班<?php endif;?></div>
    </div>
</div>
<?php
$config = WechatConfig::getSingleInstance($this->partner_id);
if($config){
    $app = new WxQyApp($config);
    $signPackage = $app->getSignPackage();
}

$point = explode(',',$partner->sign_log_point);
$lat = $point[0];
$lng = $point[1];
?>

<script type="text/javascript" src="http://api.map.baidu.com/api?type=quick&ak=FVGAubtTnZGf1tFFKRf0wfPQ&v=1.0"></script>
<script>
    document.getElementById('map_wrap').height = document.getElementById('map_wrap').width/2;

    var myLocation = {'lat':'<?php echo $lat?$lat:30.5514;?>','lng':'<?php echo $lng?$lng:104.06;?>'};
    var map = new BMap.Map("l-map");
    var school_point = new BMap.Point(myLocation.lng, myLocation.lat);
    map.centerAndZoom(school_point, 14);
    var marker = new BMap.Marker(school_point); // 创建标注
    map.addOverlay(marker);

    function doSign(type){
        var _obj = $('.sign_button[data-type='+type+']');
        if(_obj.attr('loading')=='1'){
            return false;
        }
        var _data = {type:type,'partner_id':'<?php echo $this->partner_id;?>'};
        _data.point= window._my_point;
        _data.address= window.formatted_address;
        _obj.attr('loading',1)
        _obj.find('a').text('加载中...');
        $.post('/SignLog/doSign',_data,function(data){
            if((typeof(data)).toLowerCase()=='string'){
                try{
                    data = JSON.parse(data);
                }
                catch(e){

                }
            }

            alert(data.msg);

            _obj.attr('loading',0)
            if(type==1){
                _obj.find('a').text('上班');
            }
            else{
                _obj.find('a').text('下班');
            }
            if(data.signLog){
                if(type==1){
                    $('.log_list .sign').text('已上班');
                }
                else{
                    $('.log_list .signout').text('已下班');
                }
                document.location.href='/signLog/viewLogs?partner_id=<?php echo $this->partner_id;?>&s_id='+data.signLog.id;
                /*var _str = '';
                _str += '<div class="log_list">';
                _str += '<div class="day">&nbsp;'+data.signLog.time+'</div>';
                _str += '<div class="sign">&nbsp;'+(data.signLog.type==1?'上班':'')+'</div>';
                _str += '<div class="signout">&nbsp;'+(data.signLog.type==2?'下班':'')+'</div>';
                _str += '</div>';
                $('.log_wrap').prepend(_str);*/
            }


        })
    }
    function useSign(){
        var _obj = $('.sign_button.signUse');
        if(_obj.attr('loading')=='1'){
            return false;
        }
        var _data = {'partner_id':'<?php echo $this->partner_id;?>'};
        _data.point= window._my_point;
        //_data.address= window.formatted_address;
        _obj.attr('loading',1)
        _obj.find('a').text('加载中...');
        $.post('/SignLog/useSign',_data,function(data){
            if((typeof(data)).toLowerCase()=='string'){
                try{
                    data = JSON.parse(data);
                }
                catch(e){
                }
            }

            alert(data.msg);
            _obj.attr('loading',0)
            _obj.find('a').text('设为签到地点');


        })
    }
    $(document).on('click','.sign_button_wrap .sign_button.sign.can_click a',function(){
        doSign(1)
    })
    $(document).on('click','.sign_button_wrap .sign_button.signout.can_click a',function(){
        doSign(2)
    })
    $(document).on('click','.sign_button_wrap .sign_button.signUse.can_click a',function(){
        useSign()
    })
</script>
<?php if($signPackage):?>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
    if(!wx){
        alert('加载失败！点击确定按钮刷新页面重试');
        window.location.reload();
    }
    var EARTH_RADIUS = 6378137.0;
    var getRad = function(d){
        return d*PI/180.0;
    };
    var formatFloat =  function(src, pos) {
        return Math.round(src*Math.pow(10, pos))/Math.pow(10, pos);
    };
    var frendly_distance = function(s){
        if(s < 1000){
            return formatFloat(Math.round(s*10000)/10000.0, 2) +' M';
        }
        else {
            s = s/1000;
            return formatFloat(Math.round(s*10000)/10000.0, 2) +' KM';
        }
    };
    var getRad = function(d){
        return d*Math.PI/180.0;
    };
    var distances = function(target, my) {
        if(!my.lat){
            my.lat = 30.5514;
        }
        if(!my.lng){
            my.lng = 104.06;
        }
        var radLat1 = getRad(my.lat);
        var radLat2 = getRad(target.lat);

        var a = radLat1 - radLat2;
        var b = getRad(my.lng) - getRad(target.lng);

        var s = 2*Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s*EARTH_RADIUS;
        return s;
    };
    window.loadJsonP = function(address){
        var _script = document.createElement('script');
        _script.src=address
        document.head.appendChild(_script);
    };
    function getLocation(){
        wx.getLocation({
            success: function (res) {
                window.latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                window.longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                var _myPoint = {lat:latitude,lng:longitude};
                window._my_point  = _myPoint;
                var _distance = distances(_myPoint,myLocation);
                if(_distance<parseInt('<?php echo $partner->sign_log_limit;?>') ){
                    $('.sign_button').addClass('can_click');
                }
                else{
                    $('.sign_button').removeClass('can_click');
                    alert('不在签到范围内');
                }
                $('.sign_button.signUse').removeClass('can_click').addClass('can_click');
                var my_point = new BMap.Point(_myPoint.lng, _myPoint.lat);
                map.centerAndZoom(my_point, 14);
                var marker_1 = new BMap.Marker(my_point); // 创建标注
                map.addOverlay(marker_1);
                $.getJSON('http://api.map.baidu.com/geocoder/v2/?ak=FVGAubtTnZGf1tFFKRf0wfPQ&callback=renderReverse&location='+latitude+','+longitude+'&output=json&pois=1&callback=?',function(data){
                    if(data.result&&data.result.formatted_address){
                        window.formatted_address  = data.result.formatted_address;
                        $('#address').html(data.result.formatted_address);
                    }
                    else{

                    }

                })
            },
            error:function(res){
                window._my_point  = false;
                window.formatted_address  = false;
                $('.sign_button').removeClass('can_click');
                $('#address').html('获取地理位置失败');
            }
        });
    }
    wx.config({
        debug: false,
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp:  <?php echo $signPackage["timestamp"];?>,
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            'onMenuShareAppMessage',
            'onMenuShareTimeline',
            'onMenuShareWeibo',
            'onMenuShareQQ',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'hideOptionMenu',
            'showOptionMenu',
            'getLocation',
            'openLocation'
        ]
    });
    wx.ready(function () {
        getLocation();
    });
    wx.error(function(res){
        alert(res.errMsg);
    });
</script>
<?php endif;?>