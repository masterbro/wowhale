<?php
class OrderReportController extends MobileSchoolUserController {
    public function actionList(){
        $criteria = new CDbCriteria();
        $partner_id = (int) Yii::app()->request->getParam('partner_id');
        $grade_id = (int) Yii::app()->request->getParam('grade_id');
        $product_id = (int) Yii::app()->request->getParam('product_id');
        if(!$this->partner){
            $this->showError('notice_404');
        }
        if(!$this->current_user || $this->current_user->partner_id!= $partner_id || $this->current_user->type==1 ){
            $this->showError('permison_404');
        }
        $criteria->addCondition('partner_id = '.$partner_id);
        $condition_array = array('partner_id'=>$partner_id);

        $products = Product::model()->findAllByAttributes(array('partner_id'=>$partner_id));
        $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$partner_id));
        if($product_id)  {
            $product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));
        }
        elseif($products && $products[0]){
            $product = $products[0];
        }
        if($grade_id){
            $condition_array['grade_id'] = $grade_id;
        }
        $student_count = Student::model()->countByAttributes($condition_array);
        $students = Student::model()->findAllByAttributes($condition_array);
        $this->render('list',compact('products','grades','product','student_count','students','grade_id','product_id'));
    }
	public function actionIndex() {
        $this->actionList();
        return;
		$criteria = new CDbCriteria();
		$partner_id = (int) Yii::app()->request->getParam('partner_id');
		if(!$this->partner){
			$this->showError('notice_404');
		}
		if(!$this->current_user || $this->current_user->partner_id!= $partner_id || $this->current_user->type==1 ){
			$this->showError('permison_404');
		}
		$criteria->addCondition('partner_id = '.$partner_id);
		$count = Product::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Product::model()->findAll($criteria);

		//学生人数
		$student_count = Student::model()->countByAttributes(array('partner_id'=>$partner_id));


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'student_count'=>$student_count,
			'partner_id'=>$partner_id
		));

	}
	public function actionViewProduct() {
		$partner_id = (int) Yii::app()->request->getParam('partner_id');
		$product_id = (int) Yii::app()->request->getParam('pr_id');
		if(!$this->current_user || $this->current_user->partner_id!= $partner_id || $this->current_user->type==1 ){
			$this->showError('permison_404');
		}
		if(!$partner_id){
			$this->showError('notice_404');
		}
		if(!$product_id){
			$this->showError('notice_404');
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition('partner_id = '.$partner_id);

		$product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));
		if(!$product){
			$this->showError('notice_404');
		}
		$count = Grade::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Grade::model()->findAll($criteria);

		//学生人数
		$student_count_model = Student::model()->findAllByAttributes(array('partner_id'=>$partner_id),array('group'=>'grade_id','select'=>'count(id) as id,grade_id'));
		if(!$student_count_model){
			$student_count_model = array();
		}
		$student_count = array();
		foreach ($student_count_model as $key => $value) {
			$student_count[$value->grade_id] = $value->id;
		}
		$this->render('viewProduct', array(
			'product' => $product,
			'list' => $list,
			'pager' => $pager,
			'student_count'=>$student_count,
			'partner_id'=>$partner_id,
			'product_id'=>$product_id
		));

	}
	public function actionViewGrade() {
		$partner_id = (int) Yii::app()->request->getParam('partner_id');
		$product_id = (int) Yii::app()->request->getParam('pr_id');
		$grade_id = (int) Yii::app()->request->getParam('g_id');
		if(!$this->current_user || $this->current_user->partner_id!= $partner_id || $this->current_user->type==1 ){
			$this->showError('permison_404');
		}
		if(!$partner_id){
			$this->showError('notice_404');
		}
		if(!$product_id){
			$this->showError('notice_404');
		}
		if(!$grade_id){
			$this->showError('notice_404');
		}

		$product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));
		if(!$product){
			$this->showError('notice_404');
		}
		$grade = Grade::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$grade_id));
		if(!$grade){
			$this->showError('notice_404');
		}
		$criteria = new CDbCriteria();
		$criteria->addCondition('partner_id = '.$partner_id);
		$criteria->addCondition('grade_id = '.$grade_id);
		$count = Student::model()->count($criteria);
		// $pager = new CPagination($count);
		// $pager->pageSize = 20;
		// $pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Student::model()->findAll($criteria);
		$this->render('viewGrade', array(
			'product' => $product,
			'grade' => $grade,
			'count' => $count,
			'list' => $list,
			//'pager' => $pager,
			//'student_count'=>$student_count,
			'partner_id'=>$partner_id,
			'product_id'=>$product_id
		));

	}
	public function actionUnOnlineOrder(){
		$product_id = (int) Yii::app()->request->getParam('product_id'); 
		$partner_id = (int) Yii::app()->request->getParam('partner_id'); 
		$grade_id = (int) Yii::app()->request->getParam('grade_id'); 
		$student_id = (int) Yii::app()->request->getParam('student_id'); 
		$order_type = (int) Yii::app()->request->getParam('order_type');//0取消缴费 1缴费
		$product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));
		if(!$this->current_user || $this->current_user->partner_id!= $partner_id || $this->current_user->type==1 ){
			JsonHelper::show(array(
				'error' => true,
				'msg' => '无权访问',
			));
		}
		if(!$product){
			//$this->showMsg('缴费项目不存在');
			JsonHelper::show(array(
				'error' => true,
				'msg' => '缴费项目不存在',
			));
		}
		$grade = Grade::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$grade_id));
		if(!$grade){
			//$this->showMsg('缴费班级不存在');
			JsonHelper::show(array(
				'error' => true,
				'msg' => '缴费班级不存在',
			));
		}
		$student = Student::model()->findByPk($student_id);
		if(!$student){
			//$this->showMsg('学生不存在');
			JsonHelper::show(array(
				'error' => true,
				'msg' => '学生不存在',
			));
		}
		$student_order = $student->getIsOrdered($product_id);
		if($order_type==1){
			if($student_order && $student_order->payment_status>0){
				//$this->showMsg('此学生已缴费');
				JsonHelper::show(array(
					'error' => true,
					'msg' => '此学生已缴费',
				));
			}
		}
		else{
			if(!$student_order || $student_order->payment_status==0){

				//$this->showMsg('此学生并没有缴费');
				JsonHelper::show(array(
					'error' => true,
					'msg' => '此学生并没有缴费',
				));
			}
			else if($student_order && $student_order->payment_type==1){
				//$this->showMsg('在线缴费项目不能取消缴费');
				JsonHelper::show(array(
					'error' => true,
					'msg' => '在线缴费项目不能取消缴费',
				));
			}
		}

		if(!$student_order){
			$info = array();
			$info['student_name'] = $student->name;
			$info['parent_name'] = $student->emergency_contact;
			$info['tel'] = $student->emergency_phone;
			$info['birth'] = $student->birth;
			$info['student_grade'] = $grade->name;
			$student_order = new Order();
			//$order->uid = $this->current_user->id;
			$student_order->school_id = $product->partner_id;
			$student_order->student_id = $student_id;
            if($student->parents && $student->parents[0]){
                $student_order->contacts_id = $student->parents[0]->id;
            }
            $student_order->student_id = $student_id;
			$student_order->total = $product->price;
			$student_order->begin_time = time();
			$student_order->payment_type = Order::UnOnline;
			$student_order->shipping_info = json_encode($info);
			//if($_POST['remark'])
			//	$student_order->remark = $_POST['remark'];

			$student_order->save();
			$order_detail = new OrderDetail();
			$order_detail->order_id = $student_order->id;
			$order_detail->pid = $product->id;
			$order_detail->title = $product->name;
			$order_detail->product_attr_ids = '';
			$order_detail->qty = 1;
			$order_detail->price = $product->price;
			$order_detail->save();
			//$student_order->generateDisplayId();
		}
		if($order_type==1){
			$student_order->payment_status=1;
		}
		else{
			$student_order->payment_status=0;
		}
		$student_order->save();

		//$this->showMsg('支付状态切换成功');
		JsonHelper::show(array(
			'error' => false,
			'msg' => '支付状态切换成功',
		));
	}
}