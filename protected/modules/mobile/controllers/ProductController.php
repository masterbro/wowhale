<?php
class ProductController extends MobileBaseController {


	public function actionView($id) {
		$id = intval($id);
		if(!$id)
			$this->showError('404');

		$model = Product::model()->findByPk($id);

		if(!$model || !$model->status)
			$this->showError('404');


		$this->render('index', array(
			'model' => $model,
			'merchant' => $this->getPartner($model->partner_id)
		));
	}


	public function actionConfirm() {
		$id = intval($_GET['pid']);
		if(!$id)
			$this->showError('404');

		$model = Product::model()->findByPk($id);

		if(!$model || !$model->status)
			$this->showError('404');


		$arr = array();

		$price = $model->price;
		if($_GET['attr'] && is_array($_GET['attr'])) {
			foreach($_GET['attr'] as $a) {
				$a = intval($a);
				if(!$a)
					continue;
				$tmp = ProductAttr::model()->find("id = {$a} and product_id = {$model->id}");

				$price += $tmp->price;

				$arr[] = $tmp;
			}
		}

		if(!$price)
			$this->showError('pd_error');

		$this->render('confirm', array(
			'model' => $model,
			'attr' => $arr,
			'price' => $price,
			'merchant' => $this->getPartner($model->partner_id),
			'info' => ShippingInfo::model()->findAll('user_id = '.$this->current_user->id),
		));
	}
}