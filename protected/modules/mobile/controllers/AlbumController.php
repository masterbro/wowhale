<?php
class AlbumController extends MobileSchoolUserController {


	public function actionIndex() {
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->addCondition('partner_id = '.$this->partner_id);
        if($this->current_user->type==Contacts::TY_TEACHER){

        }
        else{
            $contacts = Contacts::model()->findByPk($this->current_user->id);
            $students = $contacts->students;
            $grades = array();
            foreach($students  as $k=>$v){
                if(!$grades[$v->grade_id]){
                }
                $grades[$v->grade_id]=1;
            }
            if(!empty($grades)){
                $grades = array_keys($grades);
                $condition_or = "id='".implode("' OR id='",$grades)."'";
                $criteria->addCondition($condition_or);
            }
            else{
                $criteria->addCondition("1=0");
            }
        }
		$grade = Grade::model()->findAll($criteria);
        if(count($grade)==1){
            $this->redirect($this->createUrl('/album/ViewGrade',array('partner_id'=>$this->partner_id,'grade_id'=>$grade[0]->id)));
        }
		$this->render('index', array(
			'model' => $this->partner,
			'grade' => $grade,
		));
	}
    public function actionViewGrade() {
        $grade_id = Yii::app()->request->getParam('grade_id');
        $grade = Grade::model()->find(array(
            'condition' => 'partner_id = '.$this->partner_id .' AND id='.$grade_id,
            'order' => 'id DESC'
        ));
        $criteria = new CDbCriteria();

        $criteria->addCondition('partner_id = '.$this->partner_id);
        $criteria->addCondition('grade_id = '.$grade_id);
        $criteria->group = 'day';
        $count = Albums::model()->count($criteria);
        $pager = new CPagination($count);
        $pager->pageSize = 5;
        $pager->applyLimit($criteria);
        $this->pageTitle = $grade->name;
        $criteria->order = 'id DESC';
        $list = Albums::model()->findAll($criteria);
        $this->render('viewGrade', array(
            'model' => $this->partner,
            'grade' => $grade,
            'pager'=>$pager,
            'list'=>$list,
            'count'=>$count
        ));
    }


}