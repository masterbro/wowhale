<?php
class MerchantController extends MobileBaseController {


	public function actionView($id) {


		$model = $this->getPartner($id);


		$products = Product::model()->findAll(array(
			'condition' => 'partner_id = '.$id.' and status = 1',
			'order' => 'sort_num DESC, id DESC'
		));

		$this->render($model->type == 1 ? 'merchant' : 'school', array(
			'model' => $model,
			'products' => $products,
		));
	}

}