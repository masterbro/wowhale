<?php
class SchoolController extends MobileSchoolUserController {


	public function actionView($partner_id) {
        if($this->partner->pay_to=='none'||$this->partner->pay_to==''){
            $this->showError('pay_not_use');
        }
		$products = Product::model()->findAll(array(
			'condition' => 'partner_id = '.$this->partner_id.' and status = 1',
			'order' => 'sort_num DESC, id DESC'
		));

		$contacts = Contacts::model()->with('students')->findByPk($this->current_user->id);
        $config = WechatConfig::getSingleInstance($this->partner_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        $orders = Order::model()->with('order_details')->findAllByAttributes(array('school_id'=>$this->partner_id,'contacts_id'=>$this->current_user->id,'payment_status'=>1,'payment_type'=>1));
		//var_dump($orders);exit;
        $this->render('school', array(
			'model' => $this->partner,
			'products' => $products,
			'students' => $contacts->students,
            'app'=>$app,
            'orders'=>$orders
		));
	}

}