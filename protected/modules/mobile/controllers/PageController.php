<?php
class PageController extends MobileApplicationController {

	public function actionIndex() {

		$key = $_GET['key'];

		$page = Page::model()->find('`key` = :key', array(':key' => $key));
		if(!$page)
			$this->showError('404');


		$this->render('index', array(
			'page' => $page,
			'key' => $key,
		));

	}
}