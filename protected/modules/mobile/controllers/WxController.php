<?php
class WxController extends CController {

	public function actionCallback() {
		if(!$_GET['code'])
			throw new CHttpException(404);

		Yii::import('application.extensions.wechat.*');

		$wx_config = Yii::app()->params['wx_payment'];
		$wx_app = new WxApp(array(
			'appid' => $wx_config['app_id'],
			'secret' => $wx_config['app_secret'],
		));

		try {

			$token = $wx_app->getOpenId($_GET['code']);

		} catch (WxAppException $e) {
			$redirect = urlencode($this->createAbsoluteUrl('/wx/callback?r='.$_GET['rd']));
			$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect');
		}


		$user_open_id = UserOpenId::model()->findByAttributes(array('open_id' => $token->openid));

		if(!$user_open_id) {
			$user = new User();
			$user->save();
			$user->generateToken();

			$user_open_id = new UserOpenId();
			$user_open_id->open_id = $token->openid;
			$user_open_id->user_id = $user->id;
			$user_open_id->save();
		} else {
			$user = User::model()->findByPk($user_open_id->id);
			if(!$user) {
				$user = new User();
				$user->save();
				$user->generateToken();

				$user_open_id->user_id = $user->id;
				$user_open_id->save();
			}
		}

		$user_info = false;
		try {
			$wx_app->setAccessToken($wx_app->getCachedAccessToken());
			$user_info = $wx_app->getUserInfo($token->openid);
		} catch (WxAppException $e) {
			Yii::log('oauth error: '.$e->getMessage());
		}

		if($user_info && $user_info->subscribe == 1) {
			$user_open_id->nickname = $user_info->nickname;
			$user_open_id->avatar = $user_info->headimgurl;
			$user_open_id->save();
		}

		SessionHelper::create($user, true);


		$this->redirect(urldecode($_GET['rd']));
	}



	public function actionSchoolSystem() {
		if(!$_GET['code'] || !$_GET['state'])
			throw new CHttpException(404);

		Yii::import('application.extensions.wechat.*');

		$wx_config = Yii::app()->params['wx_payment'];
		$wx_app = new WxApp(array(
			'appid' => $wx_config['app_id'],
			'secret' => $wx_config['app_secret'],
		));

		$partner_id = intval($_GET['state']);

		try {

			$token = $wx_app->getOpenId($_GET['code']);

		} catch (WxAppException $e) {
			$redirect = urlencode($this->createAbsoluteUrl('/wx/schoolSystem?rd='.urlencode($_GET['rd']).'&c_id='.$_GET['c_id']));
			$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$partner_id.'#wechat_redirect');
		}
        if(isset($_GET['c_id']) && $_GET['c_id']){
            $c_id = $_GET['c_id'];
        }
        if(!$c_id){
            $contacts = Contacts::model()->findByAttributes(array(
                'system_openid' => $token->openid,
                'partner_id' => $partner_id,
            ));
        }
        else{
            $contacts = Contacts::model()->findByAttributes(array(
                'id' => $c_id,
                'partner_id' => $partner_id,
            ));
            $contacts->system_openid = $token->openid;
            $contacts->save();
        }
        if(!$contacts) {
            Yii::app()->session['contacts_openid'] = $token->openid;
        } else {
            unset(Yii::app()->session['contacts_openid']);
            SessionHelper::createContacts($contacts, true);
        }
		$this->redirect(urldecode($_GET['rd']));
	}
    public function actionSchoolSystemQy() {
        if(!$_GET['code'] || !$_GET['state'])
            throw new CHttpException(404);

        Yii::log('System GET'.CJSON::encode($_GET),'error');
        Yii::import('application.extensions.wechat.*');

        $wx_config = (Object)Yii::app()->params['wx_payment_qy'];
        $wechat_config = WechatConfig::getSingleInstance($wx_config->wechat_config_id);
        $wx_config->access_token  = $wechat_config->access_token;//'bNU-4Jhs812YZMC1ZjjRPls-iVxO2jJIIefWv61VUheq73Vfro6uWEWRMe5bGwkjrlhiDo0GO10_bRuk1m7YTg';//WxQyUser::getAccessToken($wx_config)->access_token;
        $wx_app = new WxQyUser($wx_config);

        $partner_id = intval($_GET['state']);

        try {

            $token = $wx_app->getUserId($_GET['code']);


        } catch (WxAppException $e) {
            Yii::log('get Token error'.CJSON::encode($_GET),'error');
            $redirect = urlencode($this->createAbsoluteUrl('/wx/schoolSystemQy?rd='.urlencode($_GET['rd']).'&c_id='.$_GET['c_id']));
            $this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$partner_id.'#wechat_redirect');
        }
        if(!$token->UserId) {
            if($token->OpenId){
                $open_id = $token->OpenId;
            }
            else{
                $this->redirect('/error?e=contacts_404');
            }
        }
        else if(!preg_match("#^sh#", $token->UserId)){
            $this->redirect('/error?e=contacts_404');
        }
        else{
            try {
                $result = $wx_app->getOpenId($token->UserId);
            } catch (WxAppException $e) {
            }

            if($result && $result->openid) {
                $open_id = $result->openid;
            }
        }
        //var_dump($open_id,urldecode($_GET['rd']),CJSON::encode($result));exit;
        if(!$open_id){
            $this->redirect(urldecode($_GET['rd']));
        }
        if(isset($_GET['c_id']) && $_GET['c_id']){
            $c_id = $_GET['c_id'];
        }
        if(!$c_id){
            $contacts = Contacts::model()->findByAttributes(array(
                'system_openid' => $open_id ,
                'partner_id' => $partner_id ,
            ));
        }
        else{
            $contacts = Contacts::model()->findByAttributes(array(
                'id' => $c_id,
                'partner_id' => $partner_id,
            ));
            if($contacts){
                $contacts->system_qy_openid = $open_id;
                $contacts->save();
            }

        }
        if(!$contacts) {
            Yii::app()->session['contacts_openid'] = $open_id;
        } else {
            unset(Yii::app()->session['contacts_openid']);
            SessionHelper::createContacts($contacts, true);
        }
        $this->redirect(urldecode($_GET['rd']));
    }

	public function actionSchool() {
		if(!$_GET['code'] || !$_GET['state'])
			throw new CHttpException(404);
        Yii::log('SCHOOL GET'.CJSON::encode($_GET),'error');

		Yii::import('application.extensions.wechat.*');

		$partner_id = intval($_GET['state']);
		$config = WechatConfig::getSingleInstance($partner_id);

		if(!$config) {
			$this->redirect('/error?e=wx_config_404');
		}

		$app = new WxQyUser($config);
		$app->throw_exception = true;
		try {

			$token = $app->getUserId($_GET['code']);

		} catch (WxAppException $e) {
            Yii::log('SCHOOL GET ERROR'.CJSON::encode($_GET),'error');
			$redirect = urlencode($this->createAbsoluteUrl('/wx/school?rd='.urlencode($_GET['rd']) ));
			$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$config->corpid.'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$partner_id.'#wechat_redirect');
		}

		if(!$token->UserId || !preg_match("#^sh#", $token->UserId)) {
			$this->redirect('/error?e=contacts_404');
		}

		$contacts = Contacts::model()->findByAttributes(array(
			'id' => str_replace('sh', '', $token->UserId),
			'partner_id' => $partner_id,
		));

		if($contacts && !$contacts->partner_openid) {
			try {
				$result = $app->getOpenId($token->UserId);
			} catch (WxAppException $e) {
			}

			if($result && $result->openid) {
				$contacts->partner_openid = $result->openid;
				$contacts->save();
			}

		}
        if($contacts){
            SessionHelper::createContacts($contacts, true);
        }
        else{
            $this->redirect('/error?e=contacts_404');
        }


		$this->redirect(urldecode($_GET['rd']));
	}
}