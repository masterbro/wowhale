<?php
class SchoolOrderController extends MobileSchoolUserController {
	public function actionCreate() {
		$info = $_POST['info'];
		//if(!$_POST['pid'] || !$info['birth'] || !$info['parent_name'] || !$info['student_name'] || !$info['tel'])
		if(!$_POST['pid'] || !$_POST['student_id'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '参数错误',
			));


		$total = 0;
		$error_pd = false;
		$attr_id = $_POST['attr_id'];
		$detail = array();
		$last_school_id = false;
		foreach($_POST['pid'] as $p) {
			$p = intval($p);
			if(!$p) {
				$error_pd = '部分商品异常，请重试';
				break;
			}
			$product = Product::model()->findByPk(intval($p));

			if(!$product) {
				$error_pd = '部分商品异常，请重试';
				break;
			}


			if($product->status != 1) {
				$error_pd = '商品 '.$product->name.' 已下架';
				break;
			}

			if($last_school_id !== false && $product->partner_id != $last_school_id) {
				//包含其它学校商品
				$error_pd = '部分商品异常，请重试';
				break;
			}
			$last_school_id = $product->partner_id;

			$product_price = $product->price;
			$attr_name = '';
			$attr_ids = '';

			if($attr_id[$product->id] && is_array($attr_id[$product->id])) {
				$product_attr = array();
				foreach($product->product_attr as $pp)
					$product_attr[$pp->id] = $pp;

				foreach($attr_id[$product->id] as $a) {
					if($product_attr[$a]) {
						$attr_name .= ','.$product_attr[$a]->name;
						$attr_ids .= ','.$product_attr[$a]->id;
						$product_price += $product_attr[$a]->price;
					}
				}
			}

			if(!$product_price) {
				$error_pd = '请选择商品属性';
				break;
			}

			$qty = intval($_POST['qty'][$product->id]) > 0 ? intval($_POST['qty'][$product->id]) : 1;
			$total += $product_price * $qty;
			$attr_name = $attr_name ? ' ('.trim($attr_name, ',').')' : '';
			$detail[] = array(
				'pid' => $p,
				'title' => $product->name.$attr_name,
				'product_attr_ids' => trim($attr_ids, ','),
				'qty' => $qty,
				'price' => $product_price,
			);
		}

		if($error_pd)
			JsonHelper::show(array(
				'error' => true,
				'msg' => $error_pd,
			));


		$order = new OrderTmp();
		//$order->uid = $this->current_user->id;
		$order->school_id = $product->partner_id;
		$order->total = $total;
		$order->payment_type = Order::PAY_WX;

		$order->student_id = intval($_POST['student_id']);
		$order->contacts_id = $this->current_user->id;
		if($this->partner->pay_to == Partner::$pay_to_key[1]) {
			$order->pay_to = Order::PAY_TO_PARTNER;
		}
        else if($this->partner->pay_to == Partner::$pay_to_key[2]) {
            $order->pay_to = Order::PAY_TO_SYSTEM_QY;
        } else {
			$order->pay_to = Order::PAY_TO_SYSTEM;
		}

		if($_POST['remark'])
			$order->remark = $_POST['remark'];

		$order->save();
		$order->generateDisplayId();

		foreach($detail as $d) {
			$order_detail = new OrderDetailTmp();
			$order_detail->order_id = $order->id;
			$order_detail->pid = $d['pid'];
			$order_detail->title = $d['title'];
			$order_detail->product_attr_ids = $d['product_attr_ids'];
			$order_detail->qty = $d['qty'];
			$order_detail->price = $d['price'];
			$order_detail->save();
		}


		JsonHelper::show(array(
			'error' => false,
			'msg' => 'success',
			'package' => json_decode($this->wechatPayPackage($order, $detail)),
			'order' => array(
				'id' => $order->id,
				's' => md5($order->id.User::SAFE_SALT),
			)
		));
	}


	private function wechatPayPackage($order, $detail) {
		include_once("./protected/extensions/wxPayment/SDKRuntimeException.php");
		include_once("./protected/extensions/wxPayment/WxPayPubHelper.php");

		$default_notify_config = Yii::app()->params['wx_payment'];

		if($this->partner->pay_to == Partner::$pay_to_key[1]) {
			$wx_payment_config = WxPayment::getInstance($order->school_id);
			$notify_config = $wx_payment_config->attributes;
			$open_id = $this->current_user->partner_openid;
		}
        else if($this->partner->pay_to == Partner::$pay_to_key[2]){
            $notify_config = Yii::app()->params['wx_payment_qy'];
            $open_id = $this->current_user->system_qy_openid;
        }
        else {
			$notify_config = Yii::app()->params['wx_payment'];
			$open_id = $this->current_user->system_openid;
		}

		$jsApi = new JsApi_pub($notify_config);

		//$user_open_id = UserOpenId::model()->find('user_id = '.$order->uid);
		$school = School::model()->findByPk($order->school_id);

		$unifiedOrder = new UnifiedOrder_pub($notify_config);
		$unifiedOrder->setParameter("openid","$open_id");
		$unifiedOrder->setParameter("body", "$school->name 费用");

		$unifiedOrder->setParameter("out_trade_no", $order->display_id);

		$unifiedOrder->setParameter("total_fee", $order->total);
		//$wxPayHelper->setParameter("total_fee", "1");
		$unifiedOrder->setParameter("notify_url", $this->createAbsoluteUrl('/payment/notify?t='.$default_notify_config['notify_token']));//通知地址
		$unifiedOrder->setParameter("trade_type", "JSAPI");//交易类型


		$prepay_id = $unifiedOrder->getPrepayId();
		//=========步骤3：使用jsapi调起支付============
		$jsApi->setPrepayId($prepay_id);

		$jsApiParameters = $jsApi->getParameters();

		return $jsApiParameters;
	}
}