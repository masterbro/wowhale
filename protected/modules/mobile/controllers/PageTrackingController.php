<?php
class PageTrackingController extends CController {

	public function actionIndex() {
		header('Content-Type: application/javascript');
		foreach (Yii::app()->log->routes as $route)
		{
			if ($route instanceof CWebLogRoute)
			{
				$route->enabled = false;
				break;
			}
		}

		$current_url = ($_GET['uri']&&$_GET['uri']!='')?$_GET['uri']:$_SERVER['HTTP_REFERER'];
		$current_url_info = parse_url($current_url);
		$path = trim($current_url_info['path'], '/');

		$user = SessionHelper::currentUser();

		PageTracking::create(intval($_GET['partner_id']), $user ? $user->id : NULL, array(
			'controller' => $_GET['controller'],
			'action' => $_GET['action'],
			'uri' => $path,
			'referer' => $_GET['referer'] ? urldecode($_GET['referer']) : '',
		));
	}
}