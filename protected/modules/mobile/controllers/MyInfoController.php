<?php
class MyInfoController extends MobileSchoolUserController {
    private $config = 'init';
    private $ww_suite_id = false;

    public function beforeAction($action) {
        $ww_apps = CommonHelper::qyApp();
        $this->ww_suite_id = $ww_apps['teacher']['ww_suite_id'];

        return parent::beforeAction($action);
    }

	public function actionIndex() {

        $contacts_id = $this->current_user->id;
        $contact = Contacts::model()->findByPk($contacts_id);
        if(!$contact){
            unset(Yii::app()->session['current_contacts']);
            $this->showError('contacts_404');
        }
        $students = $contact->students;
        //var_dump($students);
        if($contact->type==Contacts::TY_TEACHER && !$contact->leader ){
            $grade = Grade::model()->findAll(array(
                'condition' => 'partner_id = '.$this->partner_id .' AND id='.$contact->grade_id,
                'order' => 'id DESC'
            ));
        }
        else if($contact->type==Contacts::TY_TEACHER && $contact->leader ){
            $grade = Grade::model()->findAll(array(
                'condition' => 'partner_id = '.$this->partner_id,
                'order' => 'id DESC'
            ));
        }
        else{

        }

        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
		$this->render('index', array(
			'model' => $this->partner,
			'grade' => $grade,
            'students'=>$students,
            'contact'=>$contact,
            'app'=>$app
		));
	}

    public function actionUpdateContact(){
        $error = 1;
        $msg = '不正确的请求';
        if($_POST){
            $contacts_id = $this->current_user->id;
            $_contacts_id = Yii::app()->request->getParam('contact_id');
            $partner_id = Yii::app()->request->getParam('partner_id');
            $parent_name = Yii::app()->request->getParam('parent_name');
            $parent_phone = Yii::app()->request->getParam('parent_phone');
            if($contacts_id!=$_contacts_id){

            }
            else{
                $contact = Contacts::model()->findByPk($contacts_id);
                if($contact->partner_id!=$this->partner_id){
                    $error = 0;
                    $msg = '用户信息获取出错。请刷新页面重试';
                }
                else{
                    $contact->parent_name = $parent_name;
                    $contact->parent_phone = $parent_phone;
                    $contact->save();
                    if($config = $this->wxConfig()) {
                        $app = new WxQyContacts($config);
                        $department = $this->getDepartment($contact);
                        $app->update($contact, $department);
                    }
                    $error = 0;
                    $msg = '修改成功。';
                }

            }
        }
        JsonHelper::show(compact('error','msg'));
    }

    private function getDepartment($contact){
        $config = $this->wxConfig();
        if(!$config){
            return array();
        }
        $student_department = $config->student_department;
        $teacher_department = $config->teacher_department;
        $leader_department = $config->leader_department;
        $departs = array();
        if($contact->leader){
            $departs[] = $leader_department;
        }
        if($contact->type==2){
            $departs[] = $teacher_department;
            if(count($contact->students)>0 ){
                //$departs[] = $student_department;
                $students = $contact->students;
                foreach($students as $k=>$v){
                    $departs[] = $config->getGradeDepartment($v->grade_id);
                }
            }

        }
        else{
            //$departs[] = $student_department;
            $students = $contact->students;
            foreach($students as $k=>$v){
                $departs[] = $config->getGradeDepartment($v->grade_id);
            }
        }
        return $departs;
    }
    public function actionView(){
        $contacts_id = $this->current_user->id;
        $contact = Contacts::model()->findByPk($contacts_id);
        if(!$contact){
            unset(Yii::app()->session['current_contacts']);
            $this->showError('contacts_404');
        }
        $student_id = Yii::app()->request->getParam('id');
        $student = Student::model()->findByPk($student_id);
        $parents = $student->parents;
        if($contact->type==Contacts::TY_TEACHER && $contact->leader){

        }
        else if($contact->type==Contacts::TY_TEACHER && $student->grade_id==$contact->grade_id){

        }
        else{
            $is_parent = false;
            foreach($parents as $k=>$v){
                if($v->id==$contact->id){
                    $is_parent = true;
                    break;
                }
            }
            if(!$is_parent){
                $this->showError('not_correct_contact');
            }
        }
        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        $this->render('view',compact('student','contact','parents','app'));
    }
    public function actionUpdateStudent(){
        $error = 1;
        $msg = '不正确的请求';
        if($_POST){
            $contacts_id = $this->current_user->id;
            $_contacts_id = Yii::app()->request->getParam('contact_id');
            $student_id = Yii::app()->request->getParam('student_id');
            $partner_id = Yii::app()->request->getParam('partner_id');
            //$parent_name = Yii::app()->request->getParam('parent_name');
            //$parent_phone = Yii::app()->request->getParam('parent_phone');
            if($contacts_id!=$_contacts_id){

            }
            else{
                $contact = Contacts::model()->findByPk($contacts_id);
                $student = Student::model()->findByPk($student_id);
                if($contact->partner_id!=$this->partner_id){
                    $error = 1;
                    $msg = '用户信息获取出错。请刷新页面重试';
                }
                else if($student->partner_id!=$this->partner_id){
                    $error = 1;
                    $msg = '用户信息获取出错。请刷新页面重试';
                }
                else{
                    if(!CommonHelper::validMobile($_POST['emergency_phone'])){
                        $error = 1;
                        $msg = '请输入正确的紧急联系人手机号';
                    }
                    else if(!$_POST['emergency_contact']){
                        $error = 1;
                        $msg = '请输入正确的紧急联系人';
                    }
                    else{
                        foreach($_POST['parent_phone'] as $k=>$p) {
                            if($p&&!CommonHelper::validMobile($p)){
                                JsonHelper::show(array('error'=>1,'msg'=>$p.'不是一个正确的手机号哦'));
                            }
                        }
                        $student->name = $_POST['name'];
                        $student->birth = $_POST['birth'];
                        $student->id_num = $_POST['id_num'];
                        $student->register_address = $_POST['register_address'];
                        $student->address = $_POST['address'];
                        $student->emergency_contact = $_POST['emergency_contact'];
                        $student->emergency_phone = $_POST['emergency_phone'];
                        $student->save();
                        $error = 0;
                        $old_parents = $student->parents;
                        StudentParent::model()->deleteAll("student_id = {$student->id}");
                        $del_contacts = array();
                        //删除修改前的联系人
                        foreach($old_parents as $p) {
                            //var_dump($p->parent_phone,in_array($p->parent_phone, $_POST['parent_phone']));
                            if(!in_array($p->parent_phone, $_POST['parent_phone'])) {
                                //var_dump(StudentParent::model()->find("parent_id = {$p->id} AND student_id!={$student->id}"));
                                if($p->type != Contacts::TY_TEACHER && !StudentParent::model()->find("parent_id = {$p->id} AND student_id!={$student->id}")) {
                                    $del_contacts[] = 'sh'.$p->id;
                                }
                            }
                        }
                        foreach($_POST['parent_phone'] as $k=>$p) {
                            if(!CommonHelper::validMobile($p)){
                                continue;
                            }
                            $par_contact = $this->createParent($p, $_POST['parent_name'][$k], $student->id);
                            $department = $this->getDepartment($par_contact);
                            $need_push_contacts[$contact->id] = $par_contact;
                            $departs[$par_contact->id] = $department;

                        }
                        $config = $this->wxConfig();
                        //var_dump($del_contacts);
                        if($config){
                            $app = new WxQyContacts($config);
                            $app->throw_exception = true;
                            Yii::log('delete ids '.CJSON::encode($del_contacts),'error');
                            if(!empty($del_contacts)){
                                try{
                                    $deleted = $app->batchDelete($del_contacts);

                                    Yii::log('delete result '.CJSON::encode($deleted),'error');
                                }
                                catch(Exception $e){
                                    Yii::log('delete error '.$e->getMessage(),'error');
                                }
                            }
                            if(!empty($need_push_contacts)){
                                try{
                                    $app->batchUpdate($this->partner_id,$need_push_contacts ,$departs);
                                }
                                catch(Exception $e){
                                    //$info['tips'] .= '同步通讯录出现问题，请重新同步';
                                }
                            }

                        }
                        $msg = '修改成功。';
                    }

                }

            }
        }
        JsonHelper::show(compact('error','msg'));
    }

    public function actionAddParent(){
        $error = 1;
        $msg = '不正确的请求';
        if($_POST){
            $contacts_id = $this->current_user->id;
            $_contacts_id = Yii::app()->request->getParam('contact_id');
            $student_id = Yii::app()->request->getParam('student_id');
            $partner_id = Yii::app()->request->getParam('partner_id');
            if($contacts_id!=$_contacts_id){

            }
            else{
                $contact = Contacts::model()->findByPk($contacts_id);
                $student = Student::model()->findByPk($student_id);
                if($contact->partner_id!=$this->partner_id){
                    $error = 1;
                    $msg = '用户信息获取出错。请刷新页面重试';
                }
                else if($student->partner_id!=$this->partner_id){
                    $error = 1;
                    $msg = '用户信息获取出错。请刷新页面重试';
                }
                else{
                    if(!CommonHelper::validMobile($_POST['parent_phone'])){
                        $error = 1;
                        $msg = '请输入正确的家长手机号';
                    }
                    else if(!$_POST['parent_name']){
                        $error = 1;
                        $msg = '请输入正确的家长姓名';
                    }
                    else{
                        $par_contact = $this->createParent($_POST['parent_phone'], $_POST['parent_name'], $student->id);
                        $department = $this->getDepartment($par_contact);
                        $config = $this->wxConfig();
                        //$aaa = $par_contact->isNewRecord;
                        //$bbb = $contact->
                        $need_push_contacts[$contact->id] = $par_contact;
                        $departs[$par_contact->id] = $department;
                        if($config){
                            $app = new WxQyContacts($config);
                            $app->throw_exception = true;
                            try{
                                $app->batchUpdate($this->partner_id,$need_push_contacts ,$departs);
                            }
                            catch(Exception $e){
                                //$info['tips'] .= '同步通讯录出现问题，请重新同步';
                            }
                        }
                        $error = 0;
                        $msg = '添加成功。';
                    }

                }

            }
        }
        JsonHelper::show(compact('error','msg','aaa'));
    }

    private function createParent($parent_phone, $parent_name, $student_id) {
        $contacts = Contacts::model()->find(
            "partner_id = {$this->partner_id} AND parent_phone = :parent_phone",
            array(
                ':parent_phone' => $parent_phone
            )
        );
        $create = false;
        if(!$contacts) {
            $contacts = new Contacts();
            $contacts->partner_id = $this->partner_id;
            $contacts->parent_phone = $parent_phone;
            $contacts->parent_name = $parent_name;
            $contacts->type = Contacts::TY_STUDENT;
            $contacts->save();
            $contacts->generateToken();
            $create = true;
        }
        else if($contacts->type!=Contacts::TY_TEACHER){
            $contacts->parent_name = $parent_name;
            $contacts->save();
        }
        $sp = new StudentParent();
        $sp->student_id = $student_id;
        $sp->parent_id = $contacts->id;
        $sp->save();
        return $contacts;
    }
    public function actionList() {

		$grade_id = intval($_GET['grade_id']);

		$grade = Grade::model()->find(array(
			'condition' => 'id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));

		if(!$grade)
			$this->showError('404');
        $this->pageTitle = $grade->name;
		$teachers = Contacts::model()->findAll(array(
			'condition' => 'type = '.Contacts::TY_TEACHER.' AND grade_id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));

		$students = Student::model()->findAll(array(
			'condition' => 'grade_id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));


		$ww_apps = CommonHelper::qyApp();
		$config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }


        //getUserInfo();

		$this->render('list', array(
			'model' => $this->partner,
			'grade' => $grade,
			'teachers' => $teachers,
			'students' => $students,
            'app'=>$app
		));
	}


    public function wxConfig() {
        if($this->config != 'init') {
            return $this->config;
        }
        $ww_apps = CommonHelper::qyApp();
        $this->config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
        return $this->config;
    }
}