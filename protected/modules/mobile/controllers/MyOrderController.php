<?php
class MyOrderController extends MobileBaseController {

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->addCondition('uid = '.$this->current_user->id);
		$count = Order::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Order::model()->findAll($criteria);

		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
		));

	}
}