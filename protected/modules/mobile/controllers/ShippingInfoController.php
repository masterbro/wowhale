<?php
class ShippingInfoController extends MobileBaseController {
	public function actionIndex() {
		$info = ShippingInfo::model()->findAll('user_id = '.$this->current_user->id);


		$this->render('index', array(
			'info' => $info
		));
	}


	public function actionCreate() {

		$model = new ShippingInfo();

		if($_POST['student_name']) {
			$model->user_id = $this->current_user->id;
			$model->student_name = $_POST['student_name'];
			$model->parent_name = $_POST['parent_name'];
			$model->tel = $_POST['tel'];
			$model->birth = date("Y-m-d", strtotime($_POST['birth']));
			$model->save();

			$this->showMsg('保存成功', 'success', '/shippingInfo');
		}

		$this->render('form', array(
			'model' => $model
		));
	}


	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = ShippingInfo::model()->find("id = {$id} and user_id = {$this->current_user->id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST['student_name']) {
			$model->student_name = $_POST['student_name'];
			$model->parent_name = $_POST['parent_name'];
			$model->tel = $_POST['tel'];
			$model->birth = date("Y-m-d", strtotime($_POST['birth']));
			$model->save();

			$this->showMsg('保存成功', 'success', '/shippingInfo');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}


	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = ShippingInfo::model()->find("id = {$id} and user_id = {$this->current_user->id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}
}