<?php
class PartnerController extends MobileApplicationController {

	public function actionIndex($id) {
		$id = intval($id);
		$partner = $this->getPartner($id);

		$slider = PartnerSlider::model()->findAll(array(
			'condition' => 'partner_id = '.$id,
			'order' => 'sort_num DESC, id DESC'
		));

		$news = PartnerPost::model()->findAll(array(
			'condition' => 'type = "news" AND partner_id = '.$id,
			'limit' => 5,
			'order' => 'sort_num DESC, id DESC'
		));

		$this->render('index', array(
			'partner' => $partner,
			'slider' => $slider,
			'news' => $news,
		));
	}


}