<?php
class SignLogController extends MobileSchoolUserController
{

	private $ww_suite_id = false;

	public function beforeAction($action) {
		$ww_apps = CommonHelper::qyApp();
		$this->ww_suite_id = $ww_apps['teacher']['ww_suite_id'];

		return parent::beforeAction($action);
	}

    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $partner_id = (int)$this->partner_id;
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        if (!$this->current_user || $this->current_user->partner_id != $partner_id || $this->current_user->type == 1) {
            $this->showError('permison_404');
        }
        $criteria->addCondition('partner_id = ' . $partner_id);
        $criteria->addCondition('contacts_id = ' . $this->current_user->id);
        $criteria->addCondition('time >= ' . strtotime(date('Y-m-d')));

        $count = SignLog::model()->count($criteria);
        $pager = new CPagination($count);
        $pager->pageSize = 50;
        $pager->applyLimit($criteria);

        $criteria->order = 'id DESC';
        $list = SignLog::model()->findAll($criteria);

        $pager = new stdClass();
        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        //$list = array();

        $this->render('index', array(
            'list' => $list,
            'pager' => $pager,
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'app'=>$app
        ));

    }
    public function actionDoSign(){
        $type = Yii::app()->request->getParam('type');
        if($type ==1){
            $msg_type = '上班签到';
        }
        else{
            $msg_type = '下班签到';
            $type = 2;
        }
        if (!$this->partner) {
            JsonHelper::show(array('error'=>1,'msg'=>'参数错误'));
        }
        if (!$this->current_user || $this->current_user->partner_id != $this->partner_id || $this->current_user->type == 1) {
            JsonHelper::show(array('error'=>1,'msg'=>'无权'.$msg_type));
        }

        $signLog = new SignLog();
        $signLog->partner_id = $this->partner_id;
        $signLog->contacts_id = $this->current_user->id;;
        $signLog->type = $type;
        $signLog->time = time();
        $return = $signLog->save();
        if(!$return){
            $error = 1;
            $msg = $msg_type.'失败';
        }
        else{
            $error = 0;
            $msg = $msg_type.'成功';
            $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
            if(!$config || !$config->app_ids){
                /*JsonHelper::show(array(
                    'error' => true,
                    'msg' => '你还没有配置微信服务号',
                ));*/
            }
            else{
                $app = new WxQyMsg($config);
                $app->throw_exception = true;
                $app_ids = json_decode($config->app_ids);

                $to = 'sh'.$this->current_user->id;
                if($app_ids->teacher){
                    $wx_app_id = $app_ids->teacher ;
                }
                else{
                    $wx_app_id = $app_ids->sign_log ;
                }

                try {
                    $news = array(
                        array(
                            'title' => "【{$msg_type}】成功 {$msg_type}时间 ".date('m/d H:i',$signLog->time),
                            'description' => "【{$msg_type}】成功 {$msg_type}时间 ".date('m/d H:i',$signLog->time),
                            'url' => Yii::app()->params['mobile_base'].'/signLog/viewLogs?s_id='.$signLog->id.'&partner_id='.$this->partner_id,
                            'picurl' => 'http://m.wowhale.com/assets/upload/slider/2e0e1dd7caffed49238429ed58d5e7cf.jpg',
                        )
                    );
                    $app->news($to, $news, $wx_app_id);
                } catch(WxAppException $e) {
                    /*JsonHelper::show(array(
                        'error' => true,
                        'msg' => $e->getMessage(),
                    ));*/
                }
            }



        }
        $signLog->time = date('H:i:s',$signLog->time);
        JsonHelper::show(compact('error','msg','signLog'));
    }
    public function actionUseSign(){

        if (!$this->partner) {
            JsonHelper::show(array('error'=>1,'msg'=>'参数错误'));
        }
        if (!$this->current_user || $this->current_user->partner_id != $this->partner_id || !$this->current_user->leader ) {
            JsonHelper::show(array('error'=>1,'msg'=>'无权设置'));
        }
        $msg_type = '设置签到地点';
        $partner = $this->partner;
        $point = Yii::app()->request->getParam('point');
        $lat = $point['lat'];
        $lng = $point['lng'];
        $partner->sign_log_point = $lat.','.$lng;
        $return = $partner->save();
        if(!$return){
            $error = 1;
            $msg = $msg_type.'失败';
        }
        else{
            $error = 0;
            $msg = $msg_type.'成功';
            $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
            if(!$config || !$config->app_ids){
                /*JsonHelper::show(array(
                    'error' => true,
                    'msg' => '你还没有配置微信服务号',
                ));*/
            }
            else{
                $app = new WxQyMsg($config);
                $app->throw_exception = true;
                $app_ids = json_decode($config->app_ids);

                $to = 'sh'.$this->current_user->id;
                if($app_ids->teacher){
                    $wx_app_id = $app_ids->teacher ;
                }
                else{
                    $wx_app_id = $app_ids->sign_log ;
                }
                try {
                    /*$news = array(
                        array(
                            'title' => "【{$msg_type}】成功 {$msg_type}时间 ".date('m/d H:i',$signLog->time),
                            'description' => "【{$msg_type}】成功 {$msg_type}时间 ".date('m/d H:i',$signLog->time),
                            'url' => Yii::app()->params['mobile_base'].'/signLog/viewLogs?s_id='.$signLog->id.'&partner_id='.$this->partner_id,
                            'picurl' => 'http://m.wowhale.com/assets/upload/slider/2e0e1dd7caffed49238429ed58d5e7cf.jpg',
                        )
                    );*/
                    $app->text($to, '设置签到地点成功，定位点：'.$lng.','.$lat, $wx_app_id);
                } catch(WxAppException $e) {
                    /*JsonHelper::show(array(
                        'error' => true,
                        'msg'z => $e->getMessage(),
                    ));*/
                }
            }
        }
        //$signLog->time = date('H:i:s',$signLog->time);
        JsonHelper::show(compact('error','msg','signLog','point','lat','lng'));
    }

    public function  actionLog(){
        $this->actionLogs();
    }
    public function   actionAllLogs(){
        $this->pageTitle .='全校签到';
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        $partner_id = (int)$this->partner_id;
        if (!$this->current_user || $this->current_user->partner_id != $partner_id || $this->current_user->type == 1) {
            $this->showError('user_404');
        }
        if(!$this->current_user->leader){
            $this->showError('permison_404');

        }
        $day = Yii::app()->request->getParam('day');
        if(!$day){
            $day = date('Y-m-d');
        }
        $month_start = $day;
        $month_end = date('Y-m-d',strtotime($month_start) +3600*24);
        $month_start_time = strtotime($month_start );
        $month_end_time = strtotime($month_end );
        $pre_month_time = $month_start_time-3600*24;
        $criteria = new CDbCriteria();
        $criteria->addCondition('partner_id = ' . $partner_id);
        //$criteria->addCondition('contacts_id = ' . $this->current_user->id);
        $criteria->addCondition('time >= ' . $month_start_time);
        $criteria->addCondition('time < ' . $month_end_time);
        $criteria->order = 'id DESC';
        $list = SignLog::model()->findAll($criteria);
        $teachers = Contacts::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'type'=>Contacts::TY_TEACHER));
        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        $show_list = array();
        if($list){
            foreach($list as $k=>$v){
                $contacts_id = $v->contacts_id;//date('d',$v->time);
                $time = date('H:i',$v->time);
                $v->type = trim($v->type);
                if(!$show_list[$contacts_id]){
                    $show_list[$contacts_id] = array();
                }
                if(!$show_list[$contacts_id][$v->type]){
                    $show_list[$contacts_id][$v->type] = array();
                }
                $show_list[$contacts_id][$v->type][] = $time;
            }
        }
        //var_dump($show_list);

        $this->render('all_logs', array(
            'list' => $list,
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'app'=>$app,
            'month_start_time' =>$month_start_time,
            'month_end_time' =>$month_end_time,
            'show_list'=>$show_list,
            'pre_month_time'=>$pre_month_time,
            'teachers'=>$teachers
        ));
        //var_dump($month_start,$month_end);
    }
    public function  actionLogs(){
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        $partner_id = (int)$this->partner_id;
        if (!$this->current_user || $this->current_user->partner_id != $partner_id || $this->current_user->type == 1) {
            $this->showError('user_404');
        }
        if($this->current_user->leader){
            $id = Yii::app()->request->getParam('id');
            $contact = Contacts::model()->findByPk($id);
            if($contact && $contact->partner_id== $partner_id){
                $this->current_user->id = $id;
                $this->current_user->parent_name = $contact->parent_name;
            }

        }
        $month = Yii::app()->request->getParam('month');
        if(!$month){
            $month = date('Y-m');
        }
        $month_start = $month.'-01';
        $month_end = date('Y-m-d',strtotime(date('Y-m-01',strtotime($month_start) +3600*24*31)));
        $month_start_time = strtotime($month_start );
        $month_end_time = strtotime($month_end );
        $pre_month_time = $month_start_time-3600*24;
        $criteria = new CDbCriteria();
        $criteria->addCondition('partner_id = ' . $partner_id);
        $criteria->addCondition('contacts_id = ' . $this->current_user->id);
        $criteria->addCondition('time >= ' . $month_start_time);
        $criteria->addCondition('time < ' . $month_end_time);
        $criteria->order = 'id DESC';
        $list = SignLog::model()->findAll($criteria);

        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        $show_list = array();
        if($list){
            foreach($list as $k=>$v){
                $day = date('d',$v->time);
                $time = date('H:i',$v->time);
                $v->type = trim($v->type);
                if(!$show_list[$day]){
                    $show_list[$day] = array();
                }
                if(!$show_list[$day][$v->type]){
                    $show_list[$day][$v->type] = array();
                }
                $show_list[$day][$v->type][] = $time;
            }
        }

        $this->render('logs', array(
            'list' => $list,
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'app'=>$app,
            'month_start_time' =>$month_start_time,
            'month_end_time' =>$month_end_time,
            'show_list'=>$show_list,
            'pre_month_time'=>$pre_month_time
        ));
        //var_dump($month_start,$month_end);
    }
    public function  actionViewLogs(){
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        $partner_id = (int)$this->partner_id;
        /*if (!$this->current_user || $this->current_user->partner_id != $partner_id || $this->current_user->type == 1) {
            $this->showMsg('无权访问');
        }*/
        $s_id = Yii::app()->request->getParam('s_id');
        if(!$s_id){
            $this->showError('notice_404');
        }
        $signLog = SignLog::model()->findByPk($s_id);

        $this->render('view_log', array(
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'signLog'=>$signLog
        ));
        //var_dump($month_start,$month_end);
    }
}
