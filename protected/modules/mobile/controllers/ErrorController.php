<?php
class ErrorController extends CController {

	public function actionIndex() {

		$this->render('index', array(
			'msg' => CommonHelper::mobileError($_GET['e']),
		));
	}
}