<?php
class LeaveController extends MobileSchoolUserController
{

	private $ww_suite_id = false;

	public function beforeAction($action) {
        $beforeAction = parent::beforeAction($action);
		$ww_apps = CommonHelper::qyApp();
		$this->ww_suite_id = $ww_apps['student']['ww_suite_id'];
        $this->pageTitle = '学生请假';
		return $beforeAction ;
	}
    public function getGradeTeacher($app,$grade_id){
        $teacher = Contacts::model()->findByAttributes(array('grade_id'=>$grade_id,'type'=>Contacts::TY_TEACHER),array('condition'=>'`leader` !=1
OR  `leader` IS NULL'));
        //var_dump($grade_id,$teacher,$app);
        if(!$teacher){
            return false;
        }
        else{
            $head_img = HtmlHelper::get_head_img($app,$teacher->id);
            $head_nick = HtmlHelper::get_nick_name($app,$teacher->id)?HtmlHelper::get_nick_name($app,$teacher->id):$teacher->parent_name;
            return compact('head_img','head_nick');
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $partner_id = (int)$this->partner_id;
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        if (!$this->current_user || $this->current_user->partner_id != $partner_id ) {
            $this->showError('user_404');
        }
        $criteria->addCondition('partner_id = ' . $partner_id);
        $criteria->addCondition('contacts_id = ' . $this->current_user->id);
        $criteria->addCondition('time >= ' . strtotime(date('Y-m-d')));


        $count = SignLog::model()->count($criteria);
        $pager = new CPagination($count);
        $pager->pageSize = 50;
        $pager->applyLimit($criteria);

        $criteria->order = 'id DESC';
        $list = SignLog::model()->findAll($criteria);

        $pager = new stdClass();
        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        //$list = array();
        $contacts = Contacts::model()->with('students')->findByPk($this->current_user->id);

        $this->render('index', array(
            'list' => $list,
            'pager' => $pager,
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'app'=>$app,
            'students' => $contacts->students,
        ));

    }
    public function actionGradeLog(){
        $partner_id = $this->partner_id;
        $criteria = new CDbCriteria();
        $partner_id = (int)$this->partner_id;
        $grade_id = Yii::app()->request->getParam('grade_id');
        $day = Yii::app()->request->getParam('day');
        if(!$day){
            $day = date('Y-m-d');
        }
        $month_start = $day;
        $month_end = date('Y-m-d',strtotime($month_start) +3600*24);
        $month_start_time = strtotime($month_start );
        $month_end_time = strtotime($month_end );
        $pre_month_time = $month_start_time-3600*24;
        $students = Student::model()->findAllByAttributes(array('grade_id'=>$grade_id));

        $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }
        $this->render('grade_log',compact('students','day','month_start_time','pre_month_time','grade_id','partner_id','app'));
    }
    public function actionLeaveAccept(){
        $error = 1;
        $msg = '非法请求.';
        if($_POST) {
            $request = Yii::app()->request;
            $id = $request->getParam('id');
            //var_dump($id);
            $status = $request->getParam('status');
            $confirm_txt = $request->getParam('confirm_txt');
            $leave = Leave::model()->findByPk($id);
            if(!$leave){
                $error = 1;
                $msg = '未找到请假数据';
            }
            else if($leave->partner_id != $this->partner_id){
                $error = 1;
                $msg = '未找到请假数据';
            }
            else{
                $student = Student::model()->findByPk($leave->student_id);
                if(!$student){
                    $error = 1;
                    $msg = '未找到学生';
                }
                else if($this->current_user->type!=Contacts::TY_TEACHER){
                    $error = 1;
                    $msg = '您不是该生的教师';
                }
                else if($student->grade_id!=$this->current_user->grade_id){
                    $error = 1;
                    $msg = '该学生不是您的学生';
                }
                else if(!$status && !$confirm_txt){
                    $error = 1;
                    $msg = '拒绝请假请填写拒绝缘由';
                }
                else if($leave->status!=1){
                    $error = 1;
                    $msg = '该请假已被批复,请不要重复批复';
                }
                else{
                    $leave->status = $status;
                    $leave->teacher_id = $this->current_user->id;
                    $leave->confirm_txt = trim($confirm_txt);
                    $leave->confirm_at = time();
                    $leave->save();
                    $error = 0;
                    $msg = '批复成功';
                    $teachers = Contacts::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'grade_id'=>$student->grade_id,'type'=>Contacts::TY_TEACHER));
                    $to_teacher = array();
                    foreach($teachers as $k=>$v){
                        if(!$v->leader){
                            $to_teacher[] = 'sh'.$v->id;
                        }
                    }
                    $description = "{$student->name}请假\r\n请假时间：\r\n".date('Y/m/d H:i',$leave->start_at)."到".date('Y/m/d H:i',$leave->end_at)."\r\n";
                    $description .= "批复教师：{$this->current_user->parent_name}\r\n";
                    $description .= "批复状态：".Leave::$status[$status]."\r\n";
                    if($status==0){
                        $description .= "拒绝缘由：\r\n{$confirm_txt}";
                    }
                    else{

                    }
                    $news = array(
                        array(
                            'title' => "请假批复",
                            'description' => $description,
                            'url' => Yii::app()->params['mobile_base'].'/leave/viewLogs?id='.$leave->id.'&partner_id='.$this->partner_id,
                            //'picurl' => 'http://m.wowhale.com/assets/upload/slider/2e0e1dd7caffed49238429ed58d5e7cf.jpg',
                        )
                    );
                    $ww_apps = CommonHelper::qyApp();
                    if(!empty($to_teacher) ){
                        $to = implode('|',$to_teacher);
                        $ww_suite_id = $ww_apps['teacher']['ww_suite_id'];
                        $config = WechatConfig::getInstance($this->partner_id, $ww_suite_id);
                        if(!$config || !$config->app_ids){
                        }
                        else{
                            $app = new WxQyMsg($config);
                            $app->throw_exception = true;
                            $app_ids = json_decode($config->app_ids);

                            //$to = 'sh'.$this->current_user->id;
                            $wx_app_id = $app_ids->teacher ;


                            try {
                                if($app) {
                                    $app->news($to, $news, $wx_app_id);
                                }
                            }
                            catch(WxAppException $e) {
                            }

                        }
                    }
                    $ww_student_suite_id = $ww_apps['student']['ww_suite_id'];
                    if($ww_suite_id && $ww_student_suite_id!=$ww_suite_id &&!$config){
                        $config = WechatConfig::getInstance($this->partner_id, $ww_suite_id);
                        $app_ids = json_decode($config->app_ids);
                        if(!$app){

                            $app = new WxQyMsg($config);
                            $app->throw_exception = true;
                        }
                    }

                    $to = 'sh'.$leave->contacts_id;
                    $wx_app_id = $app_ids->student ;
                    try {
                        if($app){
                            $app->news($to, $news, $wx_app_id);
                        }

                    }
                    catch(WxAppException $e) {
                    }
                }

            }
        }
        JsonHelper::show(compact('error','msg'));
    }
    public function actionLeaveDo(){
        $error = 1;
        $msg = '非法请求.';
        if($_POST){
            $request = Yii::app()->request;
            $start_at = $request->getParam('start_at');
            $end_at = $request->getParam('end_at');
            $student_id = $request->getParam('student_id');
            $reason = $request->getParam('reason');
            $start_at_time = strtotime($start_at);
            $end_at_time = strtotime($end_at);
            if(!$student_id){
                $error = 1;
                $msg = '没有学生.';
            }
            else if($end_at_time <=$start_at_time){
                $error = 1;
                $msg = '请假结束时间必须大于开始时间.';
            }
            else{
                $student = Student::model()->findByPk($student_id);
                if(!$student){
                    $error = 1;
                    $msg = '没有学生.';
                }
                else if($student->partner_id != $this->partner_id){
                    $error = 1;
                    $msg = '没有学生..';
                }
                else{
                    $leave = new Leave();
                    $leave ->student_id = $student_id;
                    $leave ->partner_id = $this->partner_id;
                    $leave ->create_time = time();
                    $leave ->contacts_id = $this->current_user->id;
                    //update_at
                    //confirm_at
                    //confirm_text
                    $leave ->status = 1;
                    $leave ->start_at = $start_at_time;
                    $leave ->end_at = $end_at_time;
                    $leave ->reason = $reason;
                    $leave ->save();
                    $error = 0;
                    $msg = '请假成功.';
                    $grade_id = $student->grade_id;
                    $teachers = Contacts::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'grade_id'=>$grade_id,'type'=>Contacts::TY_TEACHER));
                    $to_teacher = array();
                    if($teachers){
                        foreach($teachers as $k=>$v){
                            if(!$v->leader){
                                $to_teacher[] = 'sh'.$v->id;
                            }
                        }
                    }

                    $ww_apps = CommonHelper::qyApp();
                    $news = array(
                        array(
                            'title' => "学生请假",
                            'description' => "{$student->name}请假\r\n请假时间：\r\n".date('Y/m/d H:i',$leave->start_at)."到".date('Y/m/d H:i',$leave->end_at)."\r\n请假事由：\r\n{$reason}",
                            'url' => Yii::app()->params['mobile_base'].'/leave/viewLogs?id='.$leave->id.'&partner_id='.$this->partner_id,
                            //'picurl' => 'http://m.wowhale.com/assets/upload/slider/2e0e1dd7caffed49238429ed58d5e7cf.jpg',
                        )
                    );
                    if(!empty($to_teacher) ){
                        $to = implode('|',$to_teacher);
                        $ww_suite_id = $ww_apps['teacher']['ww_suite_id'];
                        $config = WechatConfig::getInstance($this->partner_id, $ww_suite_id);
                        if(!$config || !$config->app_ids){
                        }
                        else{
                            $app = new WxQyMsg($config);
                            $app->throw_exception = true;
                            $app_ids = json_decode($config->app_ids);

                            //$to = 'sh'.$this->current_user->id;
                            $wx_app_id = $app_ids->teacher ;

                            try {
                                $app->news($to, $news, $wx_app_id);
                            }
                            catch(Exception $e) {
                            }

                        }
                    }
                    $ww_student_suite_id = $ww_apps['student']['ww_suite_id'];
                    if(!$config || ($ww_student_suite_id && $ww_student_suite_id!=$ww_suite_id &&!$config) ){
                        $config = WechatConfig::getInstance($this->partner_id, $ww_student_suite_id);
                        $app_ids = json_decode($config->app_ids);
                    }
                    if(!$config || !$config->app_ids){
                    }
                    else {
                        $to = 'sh' . $leave->contacts_id;
                        $wx_app_id = $app_ids->student;
                        try {
                            $app->news($to, $news, $wx_app_id);
                        } catch (Exception $e) {
                        }
                    }

                }
            }
        }
        JsonHelper::show(compact('error','msg'));

    }



    public function  actionLogs(){
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        $partner_id = (int)$this->partner_id;
        if (!$this->current_user ) {
            $this->showError('user_404');
        }
        $status = Yii::app()->request->getParam('status');

        $criteria = new CDbCriteria();
        if($this->current_user->leader && $status==''){
            $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$this->partner_id));
            $this->render('show_all',compact('grades','partner_id'));
        }
        else{
            $criteria->addCondition('t.partner_id = ' . $partner_id);
            $id = Yii::app()->request->getParam('id');
            if($id>0){
                $criteria->addCondition('student_id = ' .$id);
            }
            if($this->current_user->type==Contacts::TY_STUDENT){
                $criteria->addCondition('contacts_id = ' . $this->current_user->id);
            }
            elseif($this->current_user->leader){

            }
            else{
                $criteria->join = "LEFT JOIN ".Student::model()->tableName()." student ON student.id=t.student_id";
                $criteria->addCondition('student.grade_id = ' . $this->current_user->grade_id);
            }
            if($status==''){
                $status = 1;
            }
            if($status!=1){
                $criteria->addCondition("t.status != 1" );
            }
            else{
                $criteria->addCondition("t.status = '{$status}'" );
            }


            $criteria->order = 'id DESC';

            $count = Leave::model()->count($criteria);
            $pager = new CPagination($count);
            $pager->pageSize = 20;
            $pager->applyLimit($criteria);

            $criteria->order = 'id DESC';
            $list = Leave::model()->findAll($criteria);


            $this->render('logs', array(
                'list' => $list,
                'partner_id' => $partner_id,
                'partner' => $this->partner,
                'pager'=>$pager,
                'status'=>$status,
                'id'=>$id

            ));
        }

        //var_dump($month_start,$month_end);
    }
    public function  actionViewLogs(){
        if (!$this->partner) {
            $this->showError('notice_404');
        }
        $partner_id = (int)$this->partner_id;
        /*if (!$this->current_user || $this->current_user->partner_id != $partner_id || $this->current_user->type == 1) {
            $this->showMsg('无权访问');
        }*/
        $id = Yii::app()->request->getParam('id');
        if(!$id){
            $this->showError('notice_404');
        }
        $leave = Leave::model()->findByPk($id);

        $this->render('view_log', array(
            'partner_id' => $partner_id,
            'partner' => $this->partner,
            'leave'=>$leave
        ));
        //var_dump($month_start,$month_end);
    }
}
