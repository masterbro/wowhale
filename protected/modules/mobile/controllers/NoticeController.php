<?php
class NoticeController extends MobileApplicationController {

	public function actionView($id) {
		$id = intval($id);
		if(!$id)
			$this->showError('notice_404');

		$model = Notice::model()->find('id = '.$id);
		if(!$model)
			$this->showError('notice_404');

		$partner = Partner::model()->findByPk($model->partner_id);

		$this->pageTitle = $partner->name;
		$this->partner_id = $partner->id;

		$this->render('view', array(
			'model' => $model,
			'partner' => $partner,
		));
	}

	public function actionIndex() {
		$partner_id = Yii::app()->request->getParam('partner_id');
		//if(!$partner_id)
		//	$this->showError('notice_404');

		$criteria = new CDbCriteria();
		if($partner_id){
			$criteria->addCondition('partner_id = '.$partner_id);		
			$partner = Partner::model()->findByPk($partner_id);
			$this->pageTitle = $partner->name;
			$this->partner_id = $partner->id;
		}
		else{
			$this->pageTitle = '系统公告';
			$this->partner_id = '';
		}
		
		$count = Notice::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Notice::model()->findAll($criteria);



		$this->render('index', array(
			'list' => $list,
			'partner' => $partner,
		));
	}

    public function actionShipu(){
        $start_date = Yii::app()->request->getParam('start');
        $end_date = Yii::app()->request->getParam('to');
        $hide = Yii::app()->request->getParam('hide');
        $criteria = new CDbCriteria();
        $partner_id  = Yii::app()->request->getParam('partner_id');
        $criteria->addCondition("partner_id = {$partner_id}");
        if(!$start_date || !$end_date){
            $now_time = time();
            $now_week = date('w',$now_time);
            if($now_week==0){
                $now_week = 7;
            }
            $start_date = date('Y-m-d',time()-3600*24*$now_week);
            $end_date = date('Y-m-d',time()-3600*24*($now_week-7) );
            //$end_date = date('Y-m-d');
        }
        $criteria->addCondition("date >= '{$start_date}'");
        $criteria->addCondition("date <= '{$end_date}'");
        $data = Shipu::model()->findAll($criteria);
        $list = array();
        $data_keys = array();
        $week = array('日','一','二','三','四','五','六','日');
        foreach($data as $k=>$v){
            if(!$list[$v->date]){
                $list[$v->date] = array(date('m/d',strtotime($v->date)),'周'.$week[date('w',strtotime($v->date))]);
            }
            if(!$data_keys[$v->type]){
                $data_keys[$v->type] = 1;
            }
            $list[$v->date][$v->type] = $v->description;
        }

        $this->render('shipu',compact('list','start_date','end_date','data_keys','hide','partner_id'));

    }

}