<?php
class PaymentController extends CController {

	public function actionNotify() {
		$xml = $GLOBALS['HTTP_RAW_POST_DATA'];

//		Yii::log('notify u','info', 'payment.debug');
//		Yii::log('$xml : '.$xml,'info', 'payment.debug');
		if(!$xml)
			throw new CHttpException(404);

		include_once("./protected/extensions/wxPayment/SDKRuntimeException.php");
		include_once("./protected/extensions/wxPayment/WxPayPubHelper.php");

		$n = new Notify_pub(array());
		$n->saveData($xml);

        $order = OrderTmp::model()->findByAttributes(array('display_id' => $n->data['out_trade_no']));
        if(!$order || $order->payment_status == 1) {
            $n->setReturnParameter("return_code","SUCCESS");//设置返回码
            echo $n->returnXml();
            exit;
        }
		$notify_config = Yii::app()->params['wx_payment'];

        if($order->pay_to == Order::PAY_TO_SYSTEM) {
            $notify_config = Yii::app()->params['wx_payment'];

        } else if($order->pay_to == Order::PAY_TO_PARTNER) {
            //收款方是学校自己的则取学校企业号的open id来识别用户
            $wx_payment_config = WxPayment::getInstance($order->school_id);
            $notify_config = $wx_payment_config->attributes;
            if(!$notify_config){
                $n->setReturnParameter("return_code","ERROR");//设置返回码
                echo $n->returnXml();
                exit;
            }
                //$this->showError('wx_config_404');
        }
        else{
            $notify_config = Yii::app()->params['wx_payment_qy'];
        }
        $default_notify_config = Yii::app()->params['wx_payment'];
		if($n->data['t'] != $default_notify_config['notify_token'])
			throw new CHttpException(404);

		$notify = new Notify_pub($notify_config);
		$notify->saveData($xml);
		unset($notify->data['t']);

		if($notify->checkSign() == FALSE){
			$notify->setReturnParameter("return_code","FAIL");//返回状态码
			$notify->setReturnParameter("return_msg","签名失败");//返回信息
			echo $notify->returnXml();
			exit;
		}


//		Yii::log('$out_trade_no : '.$notify->data['out_trade_no'],'info', 'payment.debug');
//		Yii::log('$order : '.CJSON::encode($order),'info', 'payment.debug');
		if(!$order || $order->payment_status == 1) {
			$notify->setReturnParameter("return_code","SUCCESS");//设置返回码
			echo $notify->returnXml();
			exit;
		}



		if ($notify->data["return_code"] == "SUCCESS") {

			$order->payment_info = json_encode($notify->data);
			$order->payment_type = 1;
			$order->payment_status = 1;
			$order->payment_transaction_id = $notify->data["transaction_id"];
			$order->save();

			$order->moveToOrder();

			$log = "success!tmp order ID:{$order->id}, order money:{$order->total}, payment money:".$notify->data['total_fee'];
			Yii::log($log,'info', 'payment.log');

			$notify->setReturnParameter("return_code","SUCCESS");//设置返回码
			echo $notify->returnXml();
			exit;
		} else {
			$notify->setReturnParameter("return_code","FAIL");//返回状态码
			$notify->setReturnParameter("return_msg","未知错误");//返回信息
			echo $notify->returnXml();
			exit;
		}
	}

	public function actionStatus() {
		if($_POST['id']) {
			$order_id = intval($_POST['id']);
			if(md5($order_id.User::SAFE_SALT) != $_POST['s'])
				throw new CHttpException(404);


			$order = Order::model()->find('tmp_oid = '.$order_id);

			$status = !$order || $order->payment_status != 1 ? false : true;

			echo CJSON::encode(array('status' => $status));
		}
	}

	public function actionWarn() {
		$post_data = $GLOBALS["HTTP_RAW_POST_DATA"];
		if(!$post_data)
			throw new CHttpException(404);

		Yii::import('application.extensions.wxPayment.*');
		$pay_config = Yii::app()->params['wx_payment'];

		$data = simplexml_load_string($post_data, 'SimpleXMLElement', LIBXML_NOCDATA);
		$data = (Object) $data;


		$app_signature = WxCommonUtil::signature(array(
			'AppId' => $data->AppId,
			'AlarmContent' => $data->AlarmContent,
			'appkey' => $pay_config['app_key'],
			'description' => $data->Description,
			'errortype' => $data->ErrorType,
			'timestamp' => $data->TimeStamp,
		));
		Yii::log('$app_signature : '.$app_signature,'info', 'payment.debug');
		if($app_signature != $data->AppSignature)
			throw new CHttpException(404);

		//TODO warning developer
		Yii::log("\n{$data->AlarmContent}\n{$data->Description}\n",'info', 'payment_warn');
		echo 'success';
	}

}