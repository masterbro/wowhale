<?php
class AlbumsController extends MobileApplicationController {


	public function actionIndex() {
        $partner_id = Yii::app()->request->getParam('partner_id');
        $model = $this->getPartner($partner_id);
        $this->pageTitle = $model->name.'校园相册';
        $criteria = new CDbCriteria();
        $criteria->order = 'id desc';
        $criteria->addCondition('partner_id = '.$this->partner_id);
        $dayModel = Albums::model()->find($criteria);
		$this->render('index', array(
			'model' => $model,
			'list' => $dayModel?array($dayModel):array(),
		));
	}


}