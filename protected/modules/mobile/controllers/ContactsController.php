<?php
class ContactsController extends MobileSchoolUserController {


	public function actionIndex() {



		$grade = Grade::model()->findAll(array(
			'condition' => 'partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));

		$this->render('index', array(
			'model' => $this->partner,
			'grade' => $grade,
		));
	}


	public function actionList() {

		$grade_id = intval($_GET['grade_id']);

		$grade = Grade::model()->find(array(
			'condition' => 'id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));

		if(!$grade)
			$this->showError('404');
        $this->pageTitle = $grade->name;
		$teachers = Contacts::model()->findAll(array(
			'condition' => 'type = '.Contacts::TY_TEACHER.' AND grade_id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));

		$students = Student::model()->findAll(array(
			'condition' => 'grade_id = '.$grade_id.' AND partner_id = '.$this->partner_id,
			'order' => 'id DESC'
		));


		$ww_apps = CommonHelper::qyApp();
		$config = WechatConfig::getInstance($this->partner_id, $ww_apps['contacts']['ww_suite_id']);
        if(!$config || !$config->app_ids){

        }
        else{
            $app = new WxQyUser($config);
            $app->throw_exception = false;
        }


        //getUserInfo();

		$this->render('list', array(
			'model' => $this->partner,
			'grade' => $grade,
			'teachers' => $teachers,
			'students' => $students,
            'app'=>$app
		));
	}


	public function actionChat() {
		$users = $_POST['users'];
		if(!$users || !is_array($users))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '请至少选则一名联系人',
			));

		$users = array_unique($users);

		$ww_apps = CommonHelper::qyApp();
		$config = WechatConfig::getInstance($this->partner_id, $ww_apps['contacts']['ww_suite_id']);
		if(!$config)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '还未配置微信',
			));

		$chat = new WxQyMsg($config);
		$chat->throw_exception = true;

		$user_list = array();
		foreach($users as $u)
			$user_list[] = 'sh'.$u;

		$owner = 'sh'.$this->current_user->id;
//		$user_list[] = 'crazyhorse';
		$user_list[] = $owner;

		$error = false;
		$chat_id = $config->chat_id + 1;
		$msg = '创建成功';
		try {
			$chat->chat(array(
				'chatid' => $chat_id,
				'name' => '聊天',
				'owner' => $owner,
				'userlist' => $user_list,
			));


			$chat->chatSend(array(
				'receiver' => array(
					'type' => 'group',
					'id' => $chat_id,
				),
				'sender' => $owner,
				'msgtype' => 'text',
				'text' => array("content" => 'Hi!'),
			));
		} catch(WxAppException $e) {
			$error = true;
			$msg = $e->getMessage();
		}

		$config->chat_id += 1;
		$config->save();

		JsonHelper::show(array(
			'error' => $error,
			'msg' => $msg,
		));
	}

}