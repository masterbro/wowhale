<?php
class SessionController extends MobileApplicationController {


	public function actionSchool() {
		$partner = $this->getPartner($_GET['partner_id']);
		if(!$partner)
			$this->showError('wx_config_404');

		$openid = Yii::app()->session['contacts_openid'];
		if(!$openid)
			$this->showError('404');

		if($_POST['mobile'] && $_POST['captcha']) {


			$mobile = trim($_POST['mobile']);
			if(!CommonHelper::validMobile($mobile))
				JsonHelper::show(array(
					'error' => true,
					'msg' => '手机号错误',
				));

			if(!$_POST['captcha'])
				JsonHelper::show(array(
					'error' => true,
					'msg' => '验证码错误',
				));

			$contacts = Contacts::model()->findByAttributes(array(
				'partner_id' => $partner->id,
				'parent_phone' => $mobile,
			));

			if(!$contacts)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '手机号不存在',
				));

			if(Yii::app()->params['evn'] == 'production') {
				Yii::import('application.extensions.sms.*');
				try {
					$sms_captcha = new SmsCaptcha('school_contacts_login_' . $mobile);
					$sms_captcha->valid($_POST['captcha']);
				} catch(Exception $e) {
					JsonHelper::show(array(
						'error' => true,
						'msg' => $e->getMessage()
					));
				}
			}

			$contacts->system_openid = $openid;
			$contacts->save();
			SessionHelper::createContacts($contacts, true);


			JsonHelper::show(array(
				'error' => false,
				'msg' => '登录成功',
				'redirect' => urldecode($_GET['rd']),
			));
		}


		$this->render('school', array(
			'partner' => $partner
		));
	}

	public function actionCaptcha() {
		$mobile = trim($_POST['mobile']);
		if(!CommonHelper::validMobile($mobile))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号错误',
			));

		$partner_id = intval($_GET['partner_id']);
		$contacts = Contacts::model()->findByAttributes(array(
				'partner_id' => $partner_id,
				'parent_phone' => $mobile,
			));

		if(!$contacts)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号不存在，请联系学校管理员',
			));

		Yii::import('application.extensions.sms.*');

		try {
			$sms_captcha = new SmsCaptcha('school_contacts_login_' . $mobile);
			$sms_captcha->send($mobile, SmsContent::SCHOOL_CONTACTS_LOGIN, 1);
		}catch (SmsCaptchaException $e) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => $e->getMessage()
			));
		}

		JsonHelper::show(array(
			'error' => false,
			'msg' => '发送成功',
		));
	}

}