<?php
class HomeController extends MobileApplicationController {

	public function actionIndex() {



		$slider = Slider::model()->findAll(array(
			'order' => 'sort_num DESC, id DESC'
		));

		$this->render('index', array(
			'slider' => $slider,
			'category' => Category::all()
		));
	}

	public function actionPartner($id) {
		$model = $this->getPartner($id);



		$this->render('partner', array(
			'model' => $model,
		));
	}

	public function actionProduct() {
		$criteria = new CDbCriteria();

		$cid = intval($_GET['cid']);
		if($cid)
			$criteria->addCondition('cid = '.$cid);

		$criteria->addCondition("partner_id in (select id from {{partner}} where type = 1)");
		$criteria->addCondition('status = 1');
		$count = Product::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'sort_num DESC, id DESC';
		$list = Product::model()->findAll($criteria);


//		$slider = Slider::model()->findAll(array(
//			'order' => 'sort_num DESC, id DESC'
//		));

		$this->render('product', array(
			'products' => $list,
			'count' => $count,
//			'slider' => $slider,
			'pager' => $pager,
		));
	}
}