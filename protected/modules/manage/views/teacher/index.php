<?php
	$role_id = $this->current_admin->role_id;
	$role_id_school = Yii::app()->params['role_id']['school'];
	if($role_id == $role_id_school ):
?>
	<h3 class="content-title">教师列表</h3>
<?php else:?>
	<h4 class="content-title">教师列表</h4>
<?php endif;?>


<hr />
<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/teacher') ?>" method="get">
	<input type="hidden" name="type" value="teacher" />
	<div class="form-group">
		<label for="name">姓名</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="姓名" />
	</div>

	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/teacher') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>学校</th>
		<th>姓名</th>
		<th>手机</th>
		<th>角色</th>
		<th>班级</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $schools[$l->partner_id] ?></td>
			<td><?php echo $l->parent_name ?></td>
			<td><?php echo $l->parent_phone ?></td>
			<td><?php echo $l->leader ? '园长' : '老师' ?></td>
			<td><?php echo !$l->leader && $l->grade_id ? $grades[$l->grade_id] : '---' ?></td>

		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
