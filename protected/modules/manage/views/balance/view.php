<h3 class="content-title">结算申请详情</h3>
<hr />

<div class="panel panel-default">
	<div class="panel-heading">商家信息</div>
	<div class="panel-body">
		<p>商家名称：<?php echo $partner->name ?></p>
		<p>联系人：<?php echo $partner->contacts ?></p>
		<p>联系电话：<?php echo $partner->tel ?></p>
		<p>当前余额：￥<?php echo CommonHelper::price($partner->balance->balance) ?></p>
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">结算信息</div>
	<div class="panel-body">
		<p>当前状态：
			<?php
			if($model->status == 1) {
				echo '<span class="label label-warning">'.BalanceApply::$status_txt[$model->status].'</span>';
			} elseif ($model->status == 2) {
				echo '<span class="label label-success">'.BalanceApply::$status_txt[$model->status].'</span>';
			} else {
				echo '<span class="label label-default">'.BalanceApply::$status_txt[$model->status].'</span>';
			}
			?>
		</p>
		<p>金额：￥<?php echo CommonHelper::price($model->money); ?></p>
		<p>手续费：￥<?php echo CommonHelper::price($model->fee); ?></p>
		<p>到帐金额：￥<?php echo CommonHelper::price($model->money - $model->fee); ?></p>
		<p>申请时间：<?php echo date('Y-m-d H:i:s', $model->create_time) ?></p>
		<p>完成时间：<?php echo $model->status ? date('Y-m-d H:i:s', $model->complete_time) : '---'; ?></p>
		<?php if($model->remark):?>
			<p>备注：<?php echo $partner->tel ?></p>
		<?php endif;?>
		<hr />
		<?php $bank = json_decode($model->card_info);?>
		<p>卡号：<?php echo $bank->card_num; ?></p>
		<p>姓名：<?php echo $bank->name; ?></p>
		<p>银行：<?php echo $bank->bank_name; ?></p>
		<p>支行：<?php echo $bank->bank_info; ?></p>



		<?php if($model->status == 0):?>
		<hr />
		<button class="btn btn-success" id="approve" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 操作中..."><i class="fa fa-check"></i> 通过审核并结算</button>
		<button class="btn btn-danger" id="reject"><i class="fa fa-ban"></i> 拒绝结算申请</button>
		<?php endif;?>
	</div>
</div>


<div class="modal fade" id="reject-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">拒绝结算申请</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>拒绝理由：<em class="text-danger">*</em></label>
					<textarea class="form-control" rows="2" name="remark"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="reject-sub" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 操作中...">确定</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
	$(function(){
		var applyId = <?php echo $model->id ?>;

		$('#reject').click(function(){
			$('#reject-modal').modal();
		});

		$('#approve').click(function() {
			$(this).button('loading');

			$.ajax({
				url:'/manage/balance/approve',
				data: {id:applyId},
				type: 'post',
				dataType: 'json',
				success: function(data) {
					alert(data.msg);
					if(!data.error)
						window.location.reload();
				}
			})
		});

		$('#reject-sub').click(function(){
			var btn = $(this);
			var remark = $('textarea[name="remark"]').val();
			if(!remark) {
				alert('拒绝理由为必填');
				return false;
			}

			btn.button('loading');

			$.ajax({
				url:'/manage/balance/reject',
				data: {id:applyId, remark:remark},
				type: 'post',
				dataType: 'json',
				success: function(data) {
					alert(data.msg);
					if(!data.error)
						window.location.reload();
				}
			})
		})
	})
</script>