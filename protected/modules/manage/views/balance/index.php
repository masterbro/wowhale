<h3 class="content-title">结算申请</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/balance') ?>" method="get">
	<div class="form-group">
		<label for="name">学校/商家</label>
		<select class="form-control" name="partner_id">
			<option value="">所有</option>
			<?php foreach($partners as $sk=>$sv):?>
				<option value="<?php echo $sk ?>" <?php if($_GET['partner_id'] == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<div class="form-group">
		<label for="name">状态</label>
		<select class="form-control" name="st">
			<option value="">所有</option>
			<option value="pending" <?php if($_GET['st'] == 'pending') echo 'selected="selected"' ?>>待审核</option>
			<option value="approved" <?php if($_GET['st'] == 'approved') echo 'selected="selected"' ?>>拒绝</option>
			<option value="rejected" <?php if($_GET['st'] == 'rejected') echo 'selected="selected"' ?>>成功</option>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/balance') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-hover ">
	<thead>
	<tr>
		<th>学校/商家</th>
		<th>金额</th>
		<th>手续费</th>
		<th>申请人</th>
		<th>处理人</th>
		<th>状态</th>
		<th>申请时间</th>
		<th>完成时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $partners[$l->partner_id] ?></td>
			<td>￥<?php echo CommonHelper::price($l->money); ?></td>
			<td>￥<?php echo CommonHelper::price($l->fee); ?></td>
			<td><?php echo $users[$l->user_id] ?></td>
			<td><?php echo $l->manager_id ? $manager[$l->manager_id] : '---' ?></td>
			<td><?php
				if($l->status == 1) {
					echo '<span class="label label-warning">'.BalanceApply::$status_txt[$l->status].'</span>';
				} elseif ($l->status == 2) {
					echo '<span class="label label-success">'.BalanceApply::$status_txt[$l->status].'</span>';
				} else {
					echo '<span class="label label-default">'.BalanceApply::$status_txt[$l->status].'</span>';
				}
				?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td><?php echo $l->status ? date('Y-m-d H:i:s', $l->complete_time) : '---'; ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/balance/view?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-search"></i> 查看</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>