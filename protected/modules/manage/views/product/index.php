<h3 class="content-title">产品列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/product') ?>" method="get">
	<div class="form-group">
		<label for="name">产品名称</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="产品名称" />
	</div>
	<div class="form-group">
		<label for="name">状态</label>
		<select class="form-control" name="st">
			<option>所有</option>
			<option value="normal" <?php if($_GET['st'] == 'normal') echo 'selected="selected"' ?>>正常</option>
			<option value="disable" <?php if($_GET['st'] == 'disable') echo 'selected="selected"' ?>>已下架</option>
		</select>
	</div>
	<div class="form-group">
		<label for="name">学校/商家</label>
		<select class="form-control" name="school">
			<option>所有</option>
			<?php foreach($partners as $sk=>$sv):?>
				<option value="<?php echo $sk ?>" <?php if($_GET['school'] == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/product') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>产品名称</th>
		<th>价格</th>
		<th>学校/商家</th>
		<th>状态</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->name ?></td>
			<td><?php
				echo '￥'.CommonHelper::price($l->price);
				if($l->original_price) {
					echo '/<del>￥'.CommonHelper::price($l->original_price).'</del>';
				}
				?></td>
			<td><?php echo $partners[$l->partner_id] ?></td>
			<td><?php
				if(!$l->status) {
					echo '<span class="label label-danger">已下架</span>';
				} else {
					echo '<span class="label label-success">正常</span>';
				}
				?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<?php if(!$l->status):?>
					<a href="<?php echo $this->createUrl('/manage/product/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
					<a href="<?php echo $this->createUrl('/manage/product/status?id='.$l->id); ?>" onclick="return confirm('你确定要上架吗?')" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i> 上架</a>
				<?php else:?>
					<a href="javascript:;" class="btn btn-xs btn-primary disabled"><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/product/status?id='.$l->id); ?>" onclick="return confirm('你确定要下架吗?')" class="btn btn-xs btn-danger"><i class="fa fa-arrow-down"></i> 下架</a>
				<?php endif;?>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<a href="<?php echo $this->createUrl('/manage/product/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加产品</a>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>