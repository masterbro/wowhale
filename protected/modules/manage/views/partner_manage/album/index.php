<h3 class="content-title">相册管理</h3>
<hr />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th width="50%">名称</th>
		<th>图片数量</th>
		<th>点击量</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->title ?></td>
			<td><?php echo $l->image_count ?></td>
			<td><?php echo $l->hits ?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<a href="javascript:;" class="btn btn-xs btn-primary show-modal" data-id="<?php echo $l->id ?>" data-title="<?php echo addslashes($l->title) ?>"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/slider/albumAssign?id='.$l->id); ?>" class="btn btn-xs btn-success"><i class="fa fa-lock"></i> 授权教师</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/slider/album?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>



<a href="javascript:;" class="btn btn-primary btn-sm show-modal" style="width: 100px;" data-id="0"><i class="fa fa-plus"></i> 添加相册</a>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>


<!-- Modal -->
<div class="modal fade" id="modal-album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">相册管理</h4>
			</div>
			<div class="modal-body">
				<form action="" method="post">
					<input type="hidden" name="id" value="" />
					<div class="form-group">
						<label>名称：</label>
						<input type="text" class="form-control" name="title" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="submit">确定</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		var action = '';
		var modalObj = $('#modal-album');
		$('.show-modal').click(function(){
			if($(this).attr('data-id') == '0') {
				action = BASE_URL + '/manage/partner_manage/album/create';
				modalObj.find('input[name="title"]').val('');
			} else {
				action = BASE_URL + '/manage/partner_manage/album/update';
				modalObj.find('input[name="title"]').val($(this).attr('data-title'));
				modalObj.find('input[name="id"]').val($(this).attr('data-id'));
			}

			modalObj.modal('show');
		});


		$('#submit').click(function(){
			$.ajax({
				url:action,
				data:modalObj.find('form').serialize(),
				dataType:'json',
				type:'post',
				success: function(data) {
					alert(data.msg);
					if(!data.error)
						window.location.reload();
				}
			})
		});
	})
</script>