<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
    
        
        <ul class="nav nav-pills nav-stacked sidebar-2">
        
            <h4 class="content-title text-center">家长</h4>
            <hr />

            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>">缴费项目</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order') ?>">缴费历史</a>
            </li>
            <li role="presentation"  class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>">缴费报表</a>
            </li>
                    
        </ul>
               
    </div>
    
	<div class="col-md-10">

<!--网页以前的代码start-->
<h4 class="content-title">缴费报表</h4>
<hr />

<form class="form-inline yl-form-search order_search" action="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>" method="get">
    <!--<div class="input-daterange">
        <label for="start">时间</label>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['start']) ?>" name="start" />
        <span class="">到</span>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['to']) ?>" name="to" />
    </div>

    <div class="clearfix"></div>
    -->
    <br />
    <div class="form-group">
        <label for="">班级</label>
        <select id="grade_id" name="grade_id" class="form-control">
            <option value="0">全园</option>
            <?php foreach($grades as $k=>$v):?>
                <option value="<?php echo $v->id;?>" <?php if($grade_id==$v->id):?>selected<?php endif;?>><?php echo $v->name;?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="form-group">
        <label for="">缴费项目</label>
        <select id="product_id"  name="product_id" class="form-control">
            <?php foreach($products as $k=>$v):?>
                <option value="<?php echo $v->id;?>" <?php if($product_id==$v->id):?>selected<?php endif;?>><?php echo $v->name;?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="form-group">
        <label for="">支付状态</label>
        <select id="payed"  name="payed" class="form-control">
            <option value="0">不限</option>
            <option value="1" <?php if($payed==1):?>selected<?php endif;?>>已支付</option>
            <option value="2" <?php if($payed==2):?>selected<?php endif;?>>未支付</option>

        </select>
    </div>
    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
    <a href="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
    <!--<button type="button" class="btn btn-success export"><i class="fa fa-file-excel-o"></i> 导出</button>-->
</form>

<br />
<div class="  orderReportWrap">
    <?php if(!$product||!$grades):?>
        <p class="alert alert-success">该学校目前没有班级或缴费项目哦！</p>
    <?php return;endif;?>

    <?php
    if(!$grade_id){
        $order_count = $product?$product->schoolOrderCount:array();
        $order_money = $product?$product->schoolOrderMoney:array();
    }
    else{
        $order_count_grades = $product?$product->schoolGradeOrderCount:array();
        $order_money_grades = $product?$product->schoolGradeOrderMoney:array();
        $order_count = array();
        $order_count['online_count'] = $order_count_grades['online_count'][$grade_id]?$order_count_grades['online_count'][$grade_id]:0;
        $order_count['unonline_count'] = $order_count_grades['unonline_count'][$grade_id]?$order_count_grades['unonline_count'][$grade_id]:0;
        $order_count['order_count'] = $order_count_grades['order_count'][$grade_id]?$order_count_grades['order_count'][$grade_id]:0;
        $order_money = array();
        $order_money['online_count'] = $order_money_grades['online_count'][$grade_id]?$order_money_grades['online_count'][$grade_id]:0;
        $order_money['unonline_count'] = $order_money_grades['unonline_count'][$grade_id]?$order_money_grades['unonline_count'][$grade_id]:0;
        $order_money['order_count'] = $order_money_grades['order_count'][$grade_id]?$order_money_grades['order_count'][$grade_id]:0;
    }

    ?>
    <table class="table table-bordered table-striped">
        <tr>
            <td></td>
            <td>已缴费</td>
            <td>线上支付</td>
            <td>现金支付</td>
            <td>未缴费</td>
        </tr>
        <tr>
            <td>人数</td>
            <td><?php echo $order_count['order_count'];?>人</td>
            <td><?php echo $order_count['online_count'];?>人</td>
            <td><?php echo $order_count['unonline_count'];?>人</td>
            <td><?php echo $none_count = $student_count-$order_count['order_count'];?>人</td>
        </tr>
        <tr>
            <td>金额</td>
            <td>￥<?php echo CommonHelper::price($order_money['order_count']);?></td>
            <td>￥<?php echo CommonHelper::price($order_money['online_count']);?></td>
            <td>￥<?php echo CommonHelper::price($order_money['unonline_count']);?></td>
            <td>￥<?php echo CommonHelper::price($none_count * $product->price);?></td>
        </tr>

        <!---<tr>
            <td>已缴费</td>
            <td><?php /*echo $order_count['order_count'];?>人</td>
            <td>￥<?php echo CommonHelper::price($order_money['order_count']);?></td>
            <td>未缴费</td>
            <td><?php echo $none_count = $student_count-$order_count['order_count'];?>人</td>
            <td>￥<?php echo CommonHelper::price($none_count * $product->price);?></td>
            </tr>
        <tr>
            <td>线上支付</td>
            <td><?php echo $order_count['online_count'];?>人</td>
            <td>￥<?php echo CommonHelper::price($order_money['online_count']);?></td>
            <td>现金支付</td>
            <td><?php echo $order_count['unonline_count'];?>人 </td>
            <td>￥<?php echo CommonHelper::price($order_money['unonline_count']);*/?></td>
        </tr>
        -->
    </table>

    <div class="detail_wrap">
    <?php if(count($students)):?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>学生姓名</td>
            <td>支付金额</td>
            <td>支付方式</td>
        </tr>
        </thead>
        <tbody>
    <?php foreach($students as $k=>$l):?>
        <?php $student_order = $l->getIsOrdered($product->id,$l->order_id);?>
        <?php
        $status_text = '未支付';
        $can_edit = 0;
        $status_classes = array('pay_online','pay_unonline','not_pay','pay_cancel');
        $status_classe_index = 0;
        if($student_order &&$student_order->payment_status>0){
            if($student_order &&$student_order->payment_type==1){
                $status_text = '线上支付';
            }
            elseif($student_order &&$student_order->payment_type==2){
                $status_text = '现金支付';
                $status_classe_index = 1;
                $can_edit = 1;
            }
            else{
                $status_text = '其他支付';
                $status_classe_index = 1;
                $can_edit = 1;
            }
        }
        elseif($student_order &&$student_order->payment_status==0){
            $status_text = '未支付';
            $status_classe_index = 3;
            $can_edit = 1;
        }
        elseif(!$student_order){
            $status_text = '未支付';
            $status_classe_index = 2;
            $can_edit = 1;
        }
        else{
            $status_text = '未支付';
            $status_classe_index = 2;
            $can_edit = 1;
        }
        if(!$product->status){
            $can_edit = 0;
        }

        ?>
        <tr data-status="<?php echo $student_order?$student_order->payment_status:'0';?>">
            <td><?php echo $l->name ?></td>
            <td><?php echo CommonHelper::price($student_order?$student_order->total:'') ?></td>
            <td>
                <?php if($can_edit):?>
                    <a  href="javascript:;" data-href="<?php echo $this->createUrl('/manage/partner_manage/order/UnOnlineOrder',array('product_id'=>$product_id,'grade_id'=>$grade_id?$grade_id:$l->grade_id,'student_id'=>$l->id,'order_type'=>($student_order&&$student_order->payment_status)?0:1)) ?>" class="student_status_txt" >
                        <?php echo $status_text;?>
                    </a>
                <?php else:?>
                    <a href="javascript:;">
                        <?php echo $status_text;?>
                    </a>
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
        <p class="alert alert-success">该学校目前没有学生哦！</p>

    <?php endif;?>
    </div>
</div>
<nav>
    <?php
    $this->widget('CLinkPager',array(
            'header'=>'',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '末页',
            'prevPageLabel' => '上一页',
            'nextPageLabel' => '下一页',
            'pages' => $pager,
            'cssFile'=>false,
            'maxButtonCount'=>5,
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'htmlOptions' => array(
                'class' => 'pagination'
            ),
        )
    );
    ?>
</nav>
<script>
    window.partner_id = '<?php echo $this->partner_id;?>';
    function getData(){
        var _data = {};
        _data['partner_id'] = window.partner_id;
        _data['grade_id'] = $('#grade_id').val();
        _data['product_id'] = $('#product_id').val();
        document.location.href="/orderReport/list?"+$.param(_data);
    }
    $(function(){
        /*$('#grade_id,#product_id').change(function(){
            getData()
        })*/
        $('.student_status_txt').click(function(){
            if(window.confirm('确认切换状态？')){
                $.getJSON($(this).attr('data-href'),function(data){
                    //console.info(data)
                    //return;
                    if(!data.error){
                        alert(data.msg)
                        document.location.reload();
                    }
                    else{
                        alert(data.msg)
                    }
                })
            }
        })
        $('.pay_box').click(function(){
            var _thisActive = $(this).hasClass('active');
            $('.pay_box').removeClass('active');
            if(!_thisActive){
                $(this).addClass('active')
            }/*
            else{
                $('.pay_box').removeClass('active');
                $(this).addClass('active')
            }*/
            var _activeObj  = $('.pay_box.active');
            if(!_activeObj.length){
                $('.detail_wrap tbody tr').show();
            }
            else{
                var _status = _activeObj.attr('data-status');
                $('.detail_wrap tbody tr').hide();
                $('.detail_wrap tbody tr[data-status='+_status+']').show();

            }
        })
    })
</script>

<!--网页以前的代码end-->   

	</div>
</div>