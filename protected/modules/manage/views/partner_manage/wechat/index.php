<style>
    .apps .media img{border-radius: 25px;}
    #center-right{padding-left: 30px;}
</style>
<h3 class="content-title"> 应用管理</h3>
<hr />
<?php $app = array_chunk($app, 4, true); ?>
<?php foreach($app as $a):?>

<div class="row apps" style="margin-bottom: 30px;">
	<?php foreach($a as $ak=>$av):?>
	<div class="col-md-3">
		<div class="media">
			<div class="media-left media-middle">
            
    		<?php if($partner_apps[$ak])://如果添加了应用 ?>
				<?php //用switch对相应的a标签添加路径
                  switch($ak){
					  case "contacts":
					  	$akurl = $this->createUrl('/manage/partner_manage/contacts?type=teacher');
						break;
					  case "notice":
					  	$akurl = $this->createUrl('/manage/partner_manage/notice');
						break;
					  case "wap":
						$akurl = $this->createUrl('/manage/partner_manage/partner');
						break;
					  case "product":
					  	$akurl = $this->createUrl('/manage/partner_manage/product');
					    break;
					  case "album":
					    $akurl = $this->createUrl('/manage/partner_manage/albums');
						break;
					  case "sign_log":
					  	$akurl = $this->createUrl('/manage/partner_manage/signLog/');
					  	break;
					  case "order":
					  	$akurl = $this->createUrl('/manage/partner_manage/order/list');
					  	break;
					  default:
					  	$akurl = "#";
                  } 			  
                 ?>
                <a href="<?php echo $akurl ?>"><img class="media-object" src="<?php echo HtmlHelper::assets('images/wechat_icon/v2/'.$ak.'.png'); ?>" alt="" width="128" /></a>
            <?php else://如果没有添加应用，直接输出图片?>
            	<img class="media-object" src="<?php echo HtmlHelper::assets('images/wechat_icon/v2/'.$ak.'.png'); ?>" alt="" width="128" />
            <?php endif;?>
				
                     
			</div>
			<div class="media-body app-center-txt">
				<h4 class="media-heading"><?php echo $av['name'] ?></h4>
				<?php if($partner_apps[$ak]):?>
					<span class="text-success">已安装</span>
					<button class="btn btn-success btn-xs add-app" data-key="<?php echo $ak ?>" data-loading-text="安装中...">重新安装</button>
				<?php else:?>
					<button class="btn btn-success btn-xs add-app" data-key="<?php echo $ak ?>" data-loading-text="安装中...">安装</button>
				<?php endif;?>
			</div>
		</div>
	</div>
	<?php endforeach;?>
</div>

<?php endforeach;?>
<a href="javascript:;" class="btn btn-primary add-app" data-key="all" data-loading-text="安装中...">全部安装</a>


<script>
	$(function(){
		$('.add-app').click(function(){
			var btn = $(this);

			btn.button('loading');
			$.ajax({
				url: '/manage/partner_manage/wechat/oauth',
				type:'post',
				data: {key:btn.attr('data-key')},
				dataType: 'json',
				success: function(data) {
					if(data.success) {
						window.location.href = data.oauth_url;
					}
				},
				complete: function() {
					btn.button('reset');
				}
			});

		});
	})
</script>