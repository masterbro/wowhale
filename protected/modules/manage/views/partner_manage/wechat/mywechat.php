<style>
    .apps .media img{border-radius: 25px;}
    #center-right{padding-left: 30px;}
</style>
<h3 class="content-title"> 我的应用</h3>
<hr />
<?php $app = array_chunk($app, 6, true); ?>
<?php $count = 0; ?>
<?php foreach($app as $a):?>

<div class="row" style="margin-bottom: 30px;">
	<?php foreach($a as $ak=>$av):?>
	<div class="col-sm-2 apps">
		<div class="media">
			<div class="media-left media-middle">
            
    		<?php if( $partner_apps[$ak])://如果添加了应用 ?>
				<?php //用switch对相应的a标签添加路径
                  switch($ak){
//					  case "teacher":
//					  	$akurl = $this->createUrl('/manage/partner_manage/contacts?type=teacher');
//						break;
					  case "notice":
					  	    $akurl = $this->createUrl('/manage/partner_manage/notice');
						    break;
//					  case "wap":
//						$akurl = $this->createUrl('/manage/partner_manage/partner');
//						break;
					  case "product":
					  	    $akurl = $this->createUrl('/manage/partner_manage/product');
					        break;
					  case "album":
					        $akurl = $this->createUrl('/manage/partner_manage/albums');
						    break;
					  case "teacher":
					  	    $akurl = $this->createUrl('/manage/partner_manage/signLog/');
					  	    break;
					  case "wap":
					  	    $akurl = $this->createUrl('/manage/partner_manage/order/list');
					  	    break;
                      case "student":
                            $akurl = $this->createUrl('/manage/partner_manage/product');
                            break;
					  default:
					  	    $akurl = "#";
                  } 			  
                 ?>
                <a href="<?php echo $akurl ?>"><img class="media-object" src="<?php echo HtmlHelper::assets('images/wechat_icon/v2/'.$ak.'.png'); ?>" alt="" width="128" /></a>
                <h4 class="mg-app-img-txt" ><?php echo $av['name'] ?></h4>          
            	<?php $count++ ?> 
			<?php endif;?>
				
                     
			</div>

		</div>
	</div>
	<?php endforeach;?>
</div>


<?php endforeach;?>
<?php if($count == 0){
	    echo "<script>";
		echo "document.location=\"".$this->createUrl('/manage/partner_manage/wechat')."\"";
		echo "</script>";
	  }
?>

<script>
	$(function(){
		$('.add-app').click(function(){
			var btn = $(this);

			btn.button('loading');
			$.ajax({
				url: '/manage/partner_manage/wechat/oauth',
				type:'post',
				data: {key:btn.attr('data-key')},
				dataType: 'json',
				success: function(data) {
					if(data.success) {
						window.location.href = data.oauth_url;
					}
				},
				complete: function() {
					btn.button('reset');
				}
			});

		});
	})
</script>