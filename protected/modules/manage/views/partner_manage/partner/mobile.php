<?php
$partner = Partner::model()->findByPk($this->partner_id);
if($partner->type == 1)://1为商家?>
    <h3 class="content-title">学生</h3>
    <hr />

<?php else://是学校则显示侧边栏?>
<div class="row">
    <div class="col-md-2 sidebar-2-parent">


        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">校园公告</h4>
            <hr />

            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">公告</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>">食谱</a>
            </li>
            <li role="presentation"  class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">微网站</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title">微网站</h4>
        <hr />
        <p> <a href="#" class="btn btn-xs btn-info" id="show-qrcode"><i class="fa fa-mobile-phone"></i> 查看效果</a></p>
        <?php $slider = $model->sliderArr();$hidden_fields = ''; ?>
        <div class="row">
            <div class="col-md-6">
                <h4>滑动图</h4>

                <div id="img-container">
                    <?php if($slider):?>
                        <?php foreach($slider as $k=>$s):?>
                            <div class="school-slider-item">
                                <img src="<?php echo HtmlHelper::image($s) ?>" />
                                <a href="#" class="ww-del-slider" data-id="<?php echo $k ?>"><i class="fa fa-trash"></i></a>
                            </div>
                            <?php $hidden_fields .= '<input type="hidden" name="slider[]" value="'. $s .'" class="ww-school-slider" id="ww-school-slider-'. $k .'" />';?>
                        <?php endforeach;?>
                    <?php else:?>
                        <div class="alert alert-danger" role="alert" style="margin-top: 20px;">
                            <p>没有图片</p>
                        </div>
                    <?php endif;?>
                </div>


                <hr />
                <button type="button" class="btn btn-block btn-primary" id="ww-add-img" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 上传中"><i class="fa fa-image"></i> 添加图片</button>
                <br/>
                <p>请上传1：1比例的图片，规格不为1：1的图片上传之后会被系统裁剪</p>
            </div>
            <div class="col-md-6">
                <form class="validate" method="post" action="" id="ww-school-page-form">
                    <?php echo $hidden_fields;?>
                    <div class="form-group">
                        <label for="">标题：<span class="text-danger">*</span></label>
                        <input type="text" class="form-control required" name="title" placeholder="标题" value="<?php echo $model->title ?>" />
                        <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                        <label for="">联系电话：<span class="text-danger">*</span></label>
                        <input type="text" class="form-control required" name="contact_tel" placeholder="联系电话" value="<?php echo $model->contact_tel ?>" />
                        <p class="help-block"></p>
                    </div>
                    <div class="form-group">
                        <label for="">内容：</label>
                        <textarea name="content" id="ueditor" style="width: 100%;height:400px"><?php echo $model->content; ?></textarea>
                        <p class="help-block"></p>
                    </div>

                    <button class="btn btn-block btn-success" type="button"><i class="fa fa-save"></i> 保存</button>

                </form>
            </div>
        </div>

        <input type="file" name="files" id="fileupload_input" style="visibility: hidden" />
        <br />
        <br />
        <br />
        <script>
            var editor;
            KindEditor.ready(function(K) {
                editor = K.create('textarea[name="content"]', {
                    resizeType : 1,
                    allowPreviewEmoticons : false,
                    //uploadJson : window.kindEditorBase+'/php/upload_json.php?partner_id=<?php echo $model->id ?>',
                    //fileManagerJson : window.kindEditorBase+'/php/file_manager_json.php?partner_id=<?php echo $model->id ?>',
                    uploadJson : '/manage/upload/uploadImg?partner_id=<?php echo $model->id ?>',
                    fileManagerJson : window.kindEditorBase+'/php/file_manager_json.php?asset=1&partner_id=<?php echo $model->id ?>',
                    allowFileManager : true,
                    items : [
                        'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
                        'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
                        'insertunorderedlist', '|', 'baidumap', 'image', 'link', 'fullscreen', 'source', 'undo', 'redo']
                });
            });
        </script>

        <div class="modal fade" id="modal-qrcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">使用微信扫一扫</h4>
                    </div>
                    <div class="modal-body text-center">
                        <img src="<?php echo $this->createUrl('/qrCode?data='.urlencode($model->mobileUrl())) ?>" />

                        <p class="well text-left">网址：<?php echo $model->mobileUrl(); ?></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.ui.widget.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.fileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/mobile/partner.js?v=1" type="text/javascript"></script>
<?php endif;?>