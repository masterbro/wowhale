<h3 class="content-title">相册管理</h3>
<hr />
<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/partner_manage/albums') ?>" method="get">
    <div class="input-daterange">
        <label for="start">时间</label>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['start']) ?>" name="start" />
        <span class="">到</span>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['to']) ?>" name="to" />
        <div class="form-group">
            <label for="">班级</label>
            <select name="grade_id" class="form-control">
                <option value="">全部</option>
                <?php $grades_key_val=array();foreach($grades as $k=>$v):?>
                    <option value="<?php echo $v->id;?>" <?php if($grade_id==$v->id):?>selected<?php endif;?>><?php echo $v->name;?></option>
                <?php $grades_key_val[$v->id] = $v;endforeach;?>
            </select>
        </div>
        <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
        <a href="<?php echo $this->createUrl('/manage/partner_manage/albums') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>

    </div>



    <div class="clearfix"></div>
    <br />
</form>
<style>
    .img_contain_wrap{width:100%;}
    .img_contain{width:130px;height:130px;float: left;text-align: center;margin:10px;background: #F2F2F2;position: relative}
    .img_contain img{height: 100%;max-width: 100%}
    .img_contain .img_info{position: absolute;bottom: 0;background: rgba(0,0,0,0.5);width:100%;color:white}
    .img_contain .oprate{position: absolute;top: 0;right: 0;display: none}
    .img_contain .oprate.hover{display: block}
    .img_contain .checkbox{position: absolute;top: 0;left:24px;margin: 0 !important}
    .img_contain .checkbox .checkbox_input{display: none;}
    .img_contain .checkbox .checkbox_input:checked{display: block;}
</style>
<div class="img_contain_wrap">
<?php foreach($list as $l):?>
    <div class="img_contain">
        <img src="<?php echo $l->pic_url ?>"/>
        <div class="img_info">
            <span class="time"><?php echo date('Y-m-d', $l->create_time) ?></span>
            <span class="grade"><?php echo $grades_key_val[$l->grade_id]?$grades_key_val[$l->grade_id]->name:''; ?></span>
        </div>
        <div class="oprate">
            <a href="javascript:;" class="btn btn-xs btn-primary show-modal" data-id="<?php echo $l->id ?>" data-title="<?php echo addslashes($l->title) ?>"><i class="fa fa-edit"></i> 编辑</a>
            <a href="<?php echo $this->createUrl('/manage/partner_manage/albums/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
        </div>
        <span class="checkbox"><input type="checkbox" value="<?php echo $l->id ?>" class="checkbox_input" ></span>
    </div>
<?php endforeach;?>
</div>
<!--
<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th width="100px" height="80px">照片</th>
		<th>名称</th>
		<th>班级</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr valign="middle">
			<td align="center"><img src="<?php echo $l->pic_url ?>" height="70px"></td>
			<td><?php echo $l->title ?></td>
			<td><?php echo $grades_key_val[$l->grade_id]?$grades_key_val[$l->grade_id]->name:''; ?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<a href="javascript:;" class="btn btn-xs btn-primary show-modal" data-id="<?php echo $l->id ?>" data-title="<?php echo addslashes($l->title) ?>"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/albums/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>
-->
<div class="clear" style="clear: both"></div>
<div style="padding: 10px;">
    <label for="check_all"><input id="check_all" type="checkbox"> 全选 </label>
    <label for="back_all"><input id="back_all" type="checkbox"> 反选 </label>
<a href="javascript:void(0)"  class="btn btn-xs btn-danger btn-dellAll"><i class="fa fa-trash"></i> 删除</a>
</div>
照片上传请各班级老师使用微信在园方管理对话框中直接发送照片上传

<!--<a href="javascript:;" class="btn btn-primary btn-sm show-modal" style="width: 100px;" data-id="0"><i class="fa fa-plus"></i> 添加相册</a>-->


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>


<!-- Modal -->
<div class="modal fade" id="modal-album" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">相册管理</h4>
			</div>
			<div class="modal-body">
				<form action="" method="post">
					<input type="hidden" name="id" value="" />
					<div class="form-group">
						<label>名称：</label>
						<input type="text" class="form-control" name="title" />
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="submit">确定</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(function(){
		var action = '';
		var modalObj = $('#modal-album');
		$('.show-modal').click(function(){
			if($(this).attr('data-id') == '0') {
				action = BASE_URL + '/manage/partner_manage/albums/create';
				modalObj.find('input[name="title"]').val('');
			} else {
				action = BASE_URL + '/manage/partner_manage/albums/update';
				modalObj.find('input[name="title"]').val($(this).attr('data-title'));
				modalObj.find('input[name="id"]').val($(this).attr('data-id'));
			}

			modalObj.modal('show');
		});


		$('#submit').click(function(){
			$.ajax({
				url:action,
				data:modalObj.find('form').serialize(),
				dataType:'json',
				type:'post',
				success: function(data) {
					alert(data.msg);
					if(!data.error)
						window.location.reload();
				}
			})
		});
        $('.img_contain').hover(function(){
            $('.oprate',$(this)).addClass('hover')
        },function(){
            $('.oprate',$(this)).removeClass('hover')
        })
        $('#check_all').click(function(){
            if($(this).is(':checked')){
                $('.img_contain .checkbox_input').prop('checked',true);
            }
            else{
                $('.img_contain .checkbox_input').prop('checked',false);

            }
        })
        $('#back_all').click(function(){
            var _none_check = $('.img_contain .checkbox_input:not(:checked)');
            var _check = $('.img_contain .checkbox_input:checked');
            _none_check.prop('checked',true);
            _check.prop('checked',false);
        })
        $('.img_contain').click(function(e){
            if(e.target.localName!='input' && e.target.localName!='a'){
                $(this).find('input.checkbox_input:first').trigger('click');
                return false;
            }
            /*if($('.checkbox_input',$(this) ).is(':checked')){
                $('.checkbox_input',$(this)).prop('checked',false);
            }
            else{
                $('.checkbox_input',$(this)).prop('checked',true);

            }*/
        })
        $('.btn-dellAll').click(function(){
            var _checked_box = $('.img_contain .checkbox_input:checked');
            if(_checked_box.length<1){
                alert('请至少选择一个照片');
                return false;
            }
            var _ids = [];
            $.each(_checked_box,function(k,checked_input){
                _ids.push($(checked_input).val())
            })
            action = BASE_URL + '/manage/partner_manage/albums/delAll';
            if(confirm('你确定要删除选择的这些照片吗?')){
                $.ajax({
                    url:action,
                    data:{'ids':_ids},
                    dataType:'json',
                    type:'post',
                    success: function(data) {
                        alert(data.msg);
                        if(!data.error)
                            window.location.reload();
                    }
                })
            }
        })

	})
</script>

<script>
	//更改侧边栏的激活状态的图标
	$('.sidebar #app-center').addClass('active');
	$('.sidebar #school-notice').removeClass('active');
	$('.sidebar #mail-list').removeClass('active');
</script>