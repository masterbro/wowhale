<h3 class="content-title">申请结算</h3>
<hr />


<p class="text-success">当前可用余额：￥<?php echo CommonHelper::price($model->balance); ?>&nbsp;&nbsp;(结算至：<?php echo date('Y-m-d H:i:s', ($last_day - 1)) ?>)</p>


<?php if(!$cards):?>
<div class="alert alert-danger alert-dismissible fade in" role="alert">
	<p>你还没有设置提现银行卡，<a href="<?php echo $this->createUrl('/manage/partner_manage/balance/card'); ?>">点此添加</a></p>
</div>
<?php else:?>
	<br />
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">提现金额：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required digits" name="money" id="money" placeholder="提现金额" value="" />
				<p class="help-block">必须为100的倍数<?php if($partner->type) echo ', 手续费：'.(BalanceApply::FEE_RATE * 100).'%' ?></p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">银行卡：<span class="text-danger">*</span></label>
				<select class="form-control" name="card">
					<?php foreach($cards as $l):?>
						<option value="<?php echo $l->id; ?>"><?php echo $l->card_num; ?></option>
					<?php endforeach;?>
				</select>
				<p class="help-block"></p>
			</div>
		</div>
	</div>
	<button class="btn btn-success" id="apply" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 操作中..."><i class="fa fa-check"></i> 申请结算</button>



	<script>
		$(function(){
			var b = <?php echo $model->balance;?>

			$('#apply').click(function() {
				var money = $('#money').val();

				if(isNaN(money) || money.indexOf('.') !== -1) {
					alert('请输入整数');
					return false;
				}

				money = parseInt(money);
				if(money < 100 || money%100) {
					alert('请输入100的倍数');
					return false;
				}

				if(money * 100 > b) {
					alert('你没有这么多可用余额');
					return false;
				}

				$(this).button('loading');
				var btn = $(this);
				$.ajax({
					url:'/manage/partner_manage/balance/request',
					data: {money:money, card:$('select[name="card"]').val()},
					type: 'post',
					dataType: 'json',
					success: function(data) {
						alert(data.msg);
						if(!data.error) {
							window.location.reload();
						} else {
							btn.button('reset');
						}

					}
				})
			});

		})
	</script>

<?php endif;?>