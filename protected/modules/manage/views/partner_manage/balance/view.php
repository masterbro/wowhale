<h3 class="content-title">结算申请详情</h3>
<hr />
<div>
	<p>当前状态：
		<?php
		if($model->status == 1) {
			echo '<span class="label label-warning">'.BalanceApply::$status_txt[$model->status].'</span>';
		} elseif ($model->status == 2) {
			echo '<span class="label label-success">'.BalanceApply::$status_txt[$model->status].'</span>';
		} else {
			echo '<span class="label label-default">'.BalanceApply::$status_txt[$model->status].'</span>';
		}
		?>
	</p>
	<p>金额：￥<?php echo CommonHelper::price($model->money); ?></p>
	<p>手续费：￥<?php echo CommonHelper::price($model->fee); ?></p>
	<p>到帐金额：￥<?php echo CommonHelper::price($model->money - $model->fee); ?></p>
	<p>申请时间：<?php echo date('Y-m-d H:i:s', $model->create_time) ?></p>
	<p>完成时间：<?php echo $model->status ? date('Y-m-d H:i:s', $model->complete_time) : '---'; ?></p>
	<?php if($model->remark):?>
		<p>备注：<?php echo $partner->tel ?></p>
	<?php endif;?>
	<hr />
	<?php $bank = json_decode($model->card_info);?>
	<p>卡号：<?php echo $bank->card_num; ?></p>
	<p>姓名：<?php echo $bank->name; ?></p>
	<p>银行：<?php echo $bank->bank_name; ?></p>
	<p>支行：<?php echo $bank->bank_info; ?></p>

	<br />
	<br />
	<a href="javascript:window.history.back()" class="btn btn-default" style="width: 100px;">返回</a>
</div>

