<ul class="nav nav-tabs">
	<li role="presentation" class="active"><a href="#"><?php echo $model->id ? '编辑' : '添加'; ?>银行卡</a></li>
	<li role="presentation"><a href="<?php echo $this->createUrl('/manage/partner_manage/balance/card'); ?>">提现银行卡</a></li>
</ul>
<br />
<br />


<form class="validate" method="post" action="">

	<div class="form-group">
		<label for="">卡号：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="card_num" placeholder="卡号" value="<?php echo $model->card_num ?>" />
		<p class="help-block"></p>
	</div>

	<div class="form-group">
		<label for="">姓名：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="name" placeholder="姓名" value="<?php echo $model->name ?>" />
		<p class="help-block"></p>
	</div>
	<div class="form-group">
		<label for="">银行：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="bank_name" placeholder="银行" value="<?php echo $model->bank_name ?>" />
		<p class="help-block"></p>
	</div>

	<div class="form-group">
		<label for="">支行：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="bank_info" placeholder="支行" value="<?php echo $model->bank_info ?>" />
		<p class="help-block"></p>
	</div>





	<div>
		<br />
		<br />
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>

