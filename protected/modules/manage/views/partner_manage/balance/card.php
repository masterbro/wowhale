<ul class="nav nav-tabs">
	<li role="presentation" class="active"><a href="#">提现银行卡</a></li>
	<li role="presentation"><a href="<?php echo $this->createUrl('/manage/partner_manage/balance/cardCreate'); ?>">添加</a></li>
</ul>


<br />
<br />
<table class="table table-bordered table-hover ">
	<thead>
	<tr>
		<th>卡号</th>
		<th>姓名</th>
		<th>银行</th>
		<th>支行</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->card_num; ?></td>
			<td><?php echo $l->name; ?></td>
			<td><?php echo $l->bank_name; ?></td>
			<td><?php echo $l->bank_info; ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/balance/cardUpdate?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/balance/cardDelete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


