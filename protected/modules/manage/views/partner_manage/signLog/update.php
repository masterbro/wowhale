<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
    
        
        <ul class="nav nav-pills nav-stacked sidebar-2">
        
            <h4 class="content-title text-center">微信签到</h4>
            <hr />
        
          <li role="presentation">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/') ?>">签到记录</a>
          </li>
          <li role="presentation" class="active">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/update') ?>">签到设置</a>
          </li>
                    
        </ul>
               
    </div>
    
	<div class="col-md-10">

<!--网页以前的代码start-->
<h4 class="content-title">签到设置</h4>
<hr />

<form class="validate" method="post" action="">

    <?php $point = explode(',',$partner->sign_log_point);?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">签到经度：<span class="text-danger">*</span></label>
                <input type="text" class="form-control required" name="lng" placeholder="签到经度" value="<?php echo $point[1] ?>" />
                <p class="help-block"></p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="lng">签到纬度：<span class="text-danger">*</span></label>
                <input type="text" class="form-control required" name="lat" placeholder="签到纬度" value="<?php echo $point[0] ?>" />
                <p class="help-block"></p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="limit">签到精度：<span class="text-danger">*</span></label>
                <input type="text" class="form-control required" name="limit" placeholder="签到精度" value="<?php echo $partner->sign_log_limit ?>" />
                <p class="help-block">仅支持正整数米</p>
            </div>
        </div>
        <div class="col-md-6">
            <label><a target="_blank" href="http://api.map.baidu.com/lbsapi/getpoint/index.html">取经纬度</a></label>
        </div>

    </div>


    <div>
        <br />
        <button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
        <br />
        <br />
        <br />
        <div class="clearfix"></div>
    </div>

</form>


<!--网页以前的代码end-->   

	</div>
</div>