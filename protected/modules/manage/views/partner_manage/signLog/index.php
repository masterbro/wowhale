<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
    
        
        <ul class="nav nav-pills nav-stacked sidebar-2">
        
            <h4 class="content-title text-center">微信签到</h4>
            <hr />
        
          <li role="presentation" class="active">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/') ?>">签到记录</a>
          </li>
          <li role="presentation">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/update') ?>">签到设置</a>
          </li>
                    
        </ul>
               
    </div>
    
	<div class="col-md-10">

<!--网页以前的代码start-->
<h4 class="content-title">签到记录</h4>
<hr />


<form class="form-inline yl-form-search order_search" action="<?php echo $this->createUrl('/manage/partner_manage/signLog') ?>" method="get">
    <div class="input-daterange">
        <label for="start">时间</label>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['start']) ?>" name="start" />
        <span class="">到</span>
        <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['to']) ?>" name="to" />

        <label for="contact_id">教师</label>
        <select class="form-control " id="contact_id" name="contact_id">
            <option value="0">全部</option>
        <?php foreach($contacts_obj as $k=>$v):?>
            <option value="<?php echo $v->id;?>" <?php if($v->id==$contact_id):?>selected<?php endif;?>><?php echo $v->parent_name;?></option>
        <?php endforeach;?>
        </select>
    </div>
    <div class="clearfix"></div>
    <br />

    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
    <a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
    <button type="button" class="btn btn-success export"><i class="fa fa-file-excel-o"></i> 导出</button>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>姓名</th>
        <th>日期</th>
        <th>时间</th>
        <th>签到类型</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $grades = array();
    $contacts = array();
    ?>
    <?php foreach($list as $l):?>
        <tr>
            <td><?php echo $contacts_obj[$l->contacts_id]?$contacts_obj[$l->contacts_id]->parent_name:'--' ?></td>
            <td><?php echo date('Y-m-d',$l->time);?></td>
            <td><?php echo date('H:i:s',$l->time);?></td>
            <td><?php echo $l->type==1?'上班':'下班' ?></td>
        </tr>
    <?php endforeach;?>
    </tbody>
</table>


<nav>
    <?php
    $this->widget('CLinkPager',array(
            'header'=>'',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '末页',
            'prevPageLabel' => '上一页',
            'nextPageLabel' => '下一页',
            'pages' => $pager,
            'cssFile'=>false,
            'maxButtonCount'=>5,
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'htmlOptions' => array(
                'class' => 'pagination'
            ),
        )
    );
    ?>
</nav>
<script>
    $(function(){
        $('.export').click(function(){
            var _query = $('.order_search').serialize();
            _query += '&export=1';
            document.location.search ='?'+_query;
        })
    })
</script>

<!--网页以前的代码end-->   

	</div>
</div>