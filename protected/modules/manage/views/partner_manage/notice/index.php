<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
    <div class="col-md-2 sidebar-2-parent">
        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">校园公告</h4>
            <hr />

            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">公告</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>">食谱</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">微网站</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title">公告管理</h4>
        <hr />

        <br />
        <br />

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>标题</th>
                <th>类型</th>
                <th>公告类别</th>
                <th>点击量</th>
                <th>推送时间</th>
                <th>创建时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($list as $l):?>
                <tr>
                    <td><?php echo $l->title ?></td>
                    <td><?php echo Notice::$type_txt[$l->type] ?></td>
                    <td><?php echo Notice::$push_txt[$l->push_to] ?></td>
                    <td><?php echo $l->hits ?></td>
                    <td><?php echo $l->last_push ? date('Y-m-d H:i:s', $l->last_push) : '未推送' ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
                    <td>
                        <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
                        <a href="javascript:;" data-id="<?php echo $l->id; ?>" class="btn btn-xs btn-info push-production"><i class="fa fa-cloud-upload"></i> 推送</a>
                        <a href="javascript:;" data-id="<?php echo $l->id; ?>" class="btn btn-xs btn-success push-test"><i class="fa fa-cloud-upload"></i> 推送测试</a>
                        <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>




        <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加</a>


        <div class="modal fade" id="modal-push" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">推送预览消息</h4>
                    </div>
                    <div class="modal-body form-inline">
                        <div class="form-group">
                            <label for="">推送到：</label>
                            <select class="form-control" name="id">
                                <?php foreach($teachers as $t):?>
                                    <option value="<?php echo $t->id; ?>"><?php echo $t->parent_name; ?></option>
                                <?php endforeach;?>
                            </select>
                            <p class="help-block">消息将会推送该老师的微信上</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" id="submit" data-loading-text="推送中...">确定</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
	$(function(){
		var notice_id;
		$('.push-test').click(function(){
			$('#modal-push').modal('show');
			notice_id = $(this).attr('data-id');
		});

		$('#submit').click(function(){
			var btn = $(this);
			btn.button('loading');
			$.ajax({
				url:'/manage/partner_manage/notice/push',
				data:{id:notice_id, push_to:$('select[name="id"]').val()},
				dataType:'json',
				type:'post',
				success: function(data) {
					alert(data.msg);
					if(!data.error) {
						$('#modal-push').modal('hide');
					}
				},
				complete: function() {
					btn.button('reset');
				}
			})
		});

		$('.push-production').click(function(){
			var btn = $(this);
			btn.button('loading');
			$.ajax({
				url:'/manage/partner_manage/notice/push',
				data:{id:btn.attr('data-id')},
				dataType:'json',
				type:'post',
				success: function(data) {
					alert(data.msg);
					if(!data.error) {
						window.location.reload();
					}
				},
				complete: function() {
					btn.button('reset');
				}
			})
		})
	})
</script>