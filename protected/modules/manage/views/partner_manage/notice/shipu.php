<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
    <div class="col-md-2 sidebar-2-parent">
        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">校园公告</h4>
            <hr />

            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">公告</a>
            </li>
            <li role="presentation"  class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>">食谱</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">微网站</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title">食谱管理</h4>
        <hr />
        <form class="form-inline yl-form-search order_search" action="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>" method="get">
            <div class="input-daterange">
                <label for="start">时间</label>
                <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($start_date); ?>" name="start" />
                <span class="">到</span>
                <input type="text" class="form-control input-datepicker" value="<?php echo urldecode($end_date) ?>" name="to" />


            <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
            <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
            <button type="button" class="btn btn-success export"><i class="fa fa-file-excel-o"></i> 导出</button>
                <a href="javascript:;"  class="btn  btn-info push-production"><i class="fa fa-cloud-upload"></i> 推送</a>
            </div>
        </form>

        <br />
        <br />

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>日期</th>
                <th>星期</th>
                <?php foreach($data_keys as $k=>$v):?>
                <th><?php echo $k;?></th>
                <?php endforeach;?>

            </tr>
            </thead>
            <tbody>
            <?php foreach($list as $d=>$l):?>
                <tr>
                    <td><?php echo $l[0];?></td>
                    <td><?php echo $l[1];?></td>
                    <?php foreach($data_keys as $k=>$v):?>
                        <td><?php echo $l[$k]?$l[$k]:'';?></td>
                    <?php endforeach;?>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>



        <hr />
        <h3>导入</h3>
        <p>导入说明：导入时务必保证excel表第一行为表头且第一列为日期第二列为星 期第三列不为空</p>
        <p>相同日期相同类型的数据会替换之前的数据，相同日期相同类型为空的数据为删除此类型此日期下食谱</p>
        <p>目前仅支持一次导入10个类型的食谱 ，如有超过10个类型请拆分成俩个excel上传</p>
        <p><a href="<?php echo HtmlHelper::assets('file/shipu.xlsx'); ?>" class="btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i> 下载导入模版 </a >类型名称可自定义，不需要的请删除</p>
        <br />
        <form  onsubmit="return checkFile()" action="<?php echo $this->createUrl('/manage/partner_manage/notice/shipuImport') ?>" enctype="multipart/form-data" method="post">
            <input type="file" name="file" id="upload_file"/>
            <br />
            <button class="btn btn-info btn-sm" type="submit"><i class="fa fa-cloud-upload"></i> 导入食谱</button>
        </form>
        <!--
        <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加</a>


        <div class="modal fade" id="modal-push" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">推送预览消息</h4>
                    </div>
                    <div class="modal-body form-inline">
                        <div class="form-group">
                            <label for="">推送到：</label>
                            <select class="form-control" name="id">
                                <?php foreach(array() as $t):?>
                                    <option value="<?php echo $t->id; ?>"><?php echo $t->parent_name; ?></option>
                                <?php endforeach;?>
                            </select>
                            <p class="help-block">消息将会推送该老师的微信上</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" id="submit" data-loading-text="推送中...">确定</button>
                    </div>
                </div>
            </div>
        </div>
        -->
    </div>
</div>


<script>
    function checkFile(){
        //alert($('#upload_file').val());
        if(!$('#upload_file').val()){
            alert('请选择文件！')
            return false;
        }
        else{
            return true;
        }
    }
    $(function(){
        $(function(){
            $('.export').click(function(){
                var _query = $('.order_search').serialize();
                _query += '&export=1';
                document.location.search ='?'+_query;
            })
            $('.push-production').click(function(){
                var btn = $(this);
                btn.button('loading');
                var _query = $('.order_search').serialize()
                $.ajax({
                    url:'/manage/partner_manage/notice/pushShipu?'+_query,
                    data:{},
                    dataType:'json',
                    type:'post',
                    success: function(data) {
                        alert(data.msg);
                        if(!data.error) {
                            window.location.reload();
                        }
                    },
                    complete: function() {
                        btn.button('reset');
                    }
                })
            })
        })
    })
</script>
