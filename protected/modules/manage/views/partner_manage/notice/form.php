<div class="row">
    <div class="col-md-2 sidebar-2-parent">
        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">校园公告</h4>
            <hr />

            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">公告</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/notice/shipu') ?>">食谱</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title"><?php echo ($model->id ? '编辑 ' : '添加 '); ?>公告</h4>
        <hr />

        <form class="validate" method="post" action="" enctype="multipart/form-data">
            <div class="form-group">
                <label for="">标题：<em class="text-danger">*</em></label>
                <input type="text" class="form-control required" name="title" placeholder="标题" value="<?php echo $model->title ?>" />
                <p class="help-block"></p>
            </div>

            <div class="form-group">
                <label for="">类型：</label>
                <select class="form-control" name="type">
                    <?php foreach(Notice::$type_txt as $k=>$v):?>
                    <option value="<?php echo $k; ?>" <?php if($k == $model->type) echo 'selected="selected"' ?>><?php echo $v; ?></option>
                    <?php endforeach;?>
                </select>
                <p class="help-block">文字消息仅会发送描述中的文字</p>
            </div>

            <div class="form-group">
                <label for="">公告类别：</label>
                <select class="form-control" name="push_to">
                    <?php foreach(Notice::$push_txt as $k=>$v):?>
                        <option value="<?php echo $k; ?>" <?php if($k == $model->push_to) echo 'selected="selected"' ?>><?php echo $v; ?></option>
                    <?php endforeach;?>
                </select>
                <p class="help-block"></p>
            </div>

            <div class="form-group">
                <label for="">图片：</label>
                <input type="file" class="form-control" name="files" />
                <p class="help-block">推荐图片大小640*320，文字消息时不用上传</p>
            </div>
            <?php if($model->thumb): ?>
                <div class="form-group">
                    <label for="">原图片：</label>
                    <img src="<?php echo HtmlHelper::image($model->thumb) ?>" width="300" />
                    <p class="help-block">如不修改则从不用上传</p>
                </div>
            <?php endif;?>

            <div class="form-group">
                <label for="">描述：<em class="text-danger">*</em></label>
                <textarea name="description" class="form-control required" rows="2"><?php echo $model->description ?></textarea>
                <p class="help-block"></p>
            </div>



            <div class="form-group">
                <label for="">内容：</label>
                <textarea name="content" id="ueditor" style="width: 640px" rows="30"><?php echo $model->content; ?></textarea>
                <p class="help-block"></p>
            </div>


            <div>
                <button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
                <br />
                <br />
                <br />
                <div class="clearfix"></div>
            </div>

        </form>
    </div>
</div>


<script>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			//uploadJson : window.kindEditorBase+'/php/upload_json.php?partner_id=<?php echo $this->partner_id ?>',
            uploadJson : '/manage/upload/uploadImg?partner_id=<?php echo $this->partner_id ?>',
			fileManagerJson : window.kindEditorBase+'/php/file_manager_json.php?asset=1&partner_id=<?php echo $this->partner_id ?>',
			allowFileManager : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'baidumap', 'image', 'link', 'fullscreen', 'source', 'undo', 'redo']
		});
	});
</script>