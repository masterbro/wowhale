<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row sidebar-2-parent">
    <div class="col-md-2 sidebar-2-parent">


        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">通讯录</h4>
            <hr />

            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">员工</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/student') ?>">学生</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/grade') ?>">班级</a>
            </li>

        </ul>

    </div>

    <div class="col-md-10">

        <!--网页以前的代码start-->
        <h4 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>员工</h4>
        <hr />


        <form class="validate" method="post" action="">


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">员工姓名：<span class="text-danger">*</span></label>
                    <input type="text" class="form-control required" name="parent_name" placeholder="员工姓名" value="<?php echo $model->parent_name ?>" />
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">员工手机号：<span class="text-danger">*</span></label>
                    <input type="text" class="form-control required" name="parent_phone" placeholder="员工手机号" value="<?php echo $model->parent_phone ?>" />
                    <p class="help-block"></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">角色 <span class="text-danger">*</span></label>
                    <select class="form-control" name="leader">
                            <option value="0">教师</option>
                            <option value="1" <?php if($model->leader) echo 'selected="selected"' ?>>园长</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">班级 <span class="text-danger">*</span></label>
                    <select class="form-control" name="grade_id">
                        <?php foreach($grades as $k=>$v):?>
                            <option value="<?php echo $k ?>" <?php if($model->grade_id == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>
        </div>


        <div>
            <br />
            <button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
            <br />
            <br />
            <br />
            <div class="clearfix"></div>
        </div>

    </form>
    </div>
</div>