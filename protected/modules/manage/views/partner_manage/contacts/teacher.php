<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
    
        
        <ul class="nav nav-pills nav-stacked sidebar-2">
        
            <h4 class="content-title text-center">通讯录</h4>
            <hr />
        
          <li role="presentation" class="active">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">员工</a>
          </li>
          <li role="presentation">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/student') ?>">学生</a>
          </li>
          <li role="presentation">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/grade') ?>">班级</a>
          </li>
                    
        </ul>
               
    </div>
    
	<div class="col-md-10">
    
<!--网页以前的代码start-->   
<h4 class="content-title">员工通讯录列表</h4>
<hr />
   
<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/partner_manage/contacts') ?>" method="get">
	<input type="hidden" name="type" value="teacher" />
	<div class="form-group">
		<label for="name">姓名</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="姓名" />
	</div>
	<div class="form-group">
		<label for="name">班级</label>
		<select class="form-control" name="grade">
			<option value="">所有</option>
			<?php foreach($grades as $k=>$v):?>
			<option value="<?php echo $k ?>" <?php if($_GET['grade'] == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
        <th><input type="checkbox" value="" id="check_all"></th>
		<th>姓名</th>
		<th>手机</th>
		<th>角色</th>
		<th>班级</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
            <td><input type="checkbox" value="<?php echo $l->id ?>" class="del_student_ids"></td>
			<td><?php echo $l->parent_name ?></td>
			<td><?php echo $l->parent_phone ?></td>
			<td><?php echo $l->leader ? '园长' : '老师' ?></td>
			<td><?php echo !$l->leader && $l->grade_id ? $grades[$l->grade_id] : '---' ?></td>

			<td>
					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/update?id='.$l->id); ?>" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/create?type=teacher'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> 添加员工</a>
<a href="javascript:;" class="btn btn-primary btn-sm btn-del-all"><i class="fa fa-trash"></i> 批量删除</a>

<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>
<!--网页以前的代码end-->   

	</div>
</div>
<script>
    $(function(){
        $('#check_all').click(function(){
            if($(this).is(':checked')){
                $('.del_student_ids').prop('checked',true);
            }
            else{
                $('.del_student_ids').prop('checked',false);
            }
        })
    })
    $('.btn-del-all').click(function(){
        if($(this).attr('data-loading')=='1'){
            return;
        }
        var _checked = $('.del_student_ids:checked');
        if(_checked.length<1){
            alert('请至少选择一个员工');
            return false;
        }
        var _check_ids = [];
        $.each(_checked,function(k,v){
            _check_ids.push($(this).val()) ;
        })
        if(_check_ids.length<1){
            alert('请至少选择一个员工');
            return false;
        }
        if(window.confirm('您确定要删除这'+_check_ids.length+'个员工么？')){
            $('.btn-del-all').text('删除中...').attr('data-loading',1);
            $.post('<?php echo $this->createUrl('/manage/partner_manage/contacts/delAll'); ?>',{'ids':_check_ids},function(data){
                if((typeof(data)).toLowerCase()=='string'){
                    try{
                        data = JSON.parse(data);
                    }
                    catch(e){

                    }
                }
                alert(data.msg);
                if(data.error){

                }
                else{
                    document.location.reload();
                }
                $('.btn-del-all').text('批量删除').attr('data-loading',0);
            })
        }
    })
</script>