<h3 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>学生</h3>
<hr />

<form class="validate" method="post" action="">

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="">学生姓名：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required" name="student_name" placeholder="学生姓名" value="<?php echo $model->student_name ?>" />
				<p class="help-block"></p>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label for="">出生年月：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required number" name="student_birth" placeholder="出生年月" value="<?php echo $model->student_birth ?>" />
				<p class="help-block">格式：<?php echo date('Ym') ?></p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="">家长姓名：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required" name="parent_name" placeholder="家长姓名" value="<?php echo $model->parent_name ?>" />
				<p class="help-block"></p>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label for="">家长手机号：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required" name="parent_phone" placeholder="家长手机号" value="<?php echo $model->parent_phone ?>" />
				<p class="help-block"></p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label for="name">班级 <span class="text-danger">*</span></label>
				<select class="form-control" name="grade_id">
					<?php foreach($grades as $k=>$v):?>
						<option value="<?php echo $k ?>" <?php if($model->grade_id == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
					<?php endforeach;?>
				</select>
			</div>
		</div>
	</div>


	<div>
		<br />
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>