<?php 
	$partner = Partner::model()->findByPk($this->partner_id);
	if($partner->type == 1)://1为商家
?>
<h3 class="content-title">学生</h3>
<hr />

<?php else://是学校则显示侧边栏?>
<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
    
        
        <ul class="nav nav-pills nav-stacked sidebar-2">
        
            <h4 class="content-title text-center">家长</h4>
            <hr />
        
            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>">缴费项目</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order') ?>">缴费历史</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>">缴费报表</a>
            </li>
        </ul>
               
    </div>
    
	<div class="col-md-10">

<!--网页以前的代码start-->  
<h4 class="content-title">产品列表</h4>
<hr />
<?php endif;?>


<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/partner_manage/product') ?>" method="get">
	<div class="form-group">
		<label for="name">产品名称</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="产品名称" />
	</div>
	<div class="form-group">
		<label for="name">状态</label>
		<select class="form-control" name="st">
			<option value="">所有</option>
			<option value="normal" <?php if($_GET['st'] == 'normal') echo 'selected="selected"' ?>>正常</option>
			<option value="disable" <?php if($_GET['st'] == 'disable') echo 'selected="selected"' ?>>已下架</option>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>产品名称</th>
		<th>价格</th>
		<th>状态</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->name ?></td>
			<td><?php
				echo '￥'.CommonHelper::price($l->price);
				if($l->original_price) {
					echo '/<del>￥'.CommonHelper::price($l->original_price).'</del>';
				}
				?></td>
			<td><?php
				if(!$l->status) {
					echo '<span class="label label-danger">已下架</span>';
				} else {
					echo '<span class="label label-success">正常</span>';
				}
				?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<?php if(!$l->status):?>
					<a href="<?php echo $this->createUrl('/manage/partner_manage/product/update?id='.$l->id); ?>" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/partner_manage/product/status?id='.$l->id); ?>" onclick="return confirm('你确定要上架吗?')" class="btn btn-xs btn-success"><i class="fa fa-arrow-up"></i> 上架</a>
				<?php else:?>
					<a href="javascript:;" class="btn btn-xs btn-primary disabled" ><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/partner_manage/product/status?id='.$l->id); ?>" onclick="return confirm('你确定要下架吗?')" class="btn btn-xs btn-danger"><i class="fa fa-arrow-down"></i> 下架</a>
				<?php endif;?>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<a href="<?php echo $this->createUrl('/manage/partner_manage/product/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加产品</a>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>

<!--网页以前的代码end-->   
<?php if($partner->type == 1)://1为商家?>
<?php else:?>
	</div>
</div>
<?php endif?>