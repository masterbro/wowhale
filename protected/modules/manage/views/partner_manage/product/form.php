<div class="row">
    <div class="col-md-2 sidebar-2-parent">


        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">家长</h4>
            <hr />

            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>">缴费项目</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order') ?>">缴费历史</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>">缴费报表</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">微网站</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>产品</h4>
        <hr />
        <form class="validate" method="post" action="">

	<div class="form-group">
		<label for="">名称：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="name" placeholder="名称" value="<?php echo $model->name ?>" />
		<p class="help-block"></p>
	</div>

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
			<label for="">价格：<span class="text-danger">*</span></label>
			<input type="text" class="form-control required number" name="price" placeholder="价格" value="<?php if($model->price) echo CommonHelper::price($model->price) ?>" />
			<p class="help-block"></p>

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">原价：</label>
				<input type="text" class="form-control number" name="original_price" placeholder="原价" value="<?php if($model->original_price) echo CommonHelper::price($model->original_price) ?>" />
				<p class="help-block"></p>

			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">优先级：</label>
				<input type="text" class="form-control required number" name="sort_num" placeholder="优先级" value="<?php echo $model->sort_num ?>" />
				<p class="help-block">0-255的数字，数字越大排名越靠前</p>

			</div>
		</div>
	</div>

	<h5>商品属性 <a href="javascript:;" class="btn btn-xs btn-info" id="add-pd-attr"><i class="fa fa-plus"></i></a></h5>
	<hr />
	<p class="text-success">说明：<br />属性中的价格是指属性对价格的影响，正数为上浮，负数为下浮。例如：X商品价格为20.00元 如果填3 则用户选择了此属性后X商品价格为23.00元；如果填-3则为17.00元</p>

	<div id="product-attr">

		<?php if($model->id && ($attr = $model->attrData())):?>
			<?php $i=0;foreach($attr as $a):?>
				<div class="pd-attr-group" id="pd-attr-group-<?php echo $i; ?>">
					<div class="row">
						<input type="hidden" name="attr_id[<?php echo $i; ?>]" value="<?php echo $a['parent']->id ?>" />

						<div class="col-md-4">
							<div class="form-group">
								<label for="">属性类型：<span class="text-danger">*</span></label>
								<input type="text" class="form-control required" placeholder="属性类型" name="attr_parent[<?php echo $i; ?>]" value="<?php echo $a['parent']->name;?>" />
								<p class="help-block"></p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group pt10">
								<label for=""> </label><br />
								<a href="javascript:;" class="btn btn-xs btn-info add" data-eq="<?php echo $i; ?>"><i class="fa fa-plus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-danger remove" data-eq="<?php echo $a['parent']->id ?>"><i class="fa fa-trash"></i></a>
							</div>
						</div>
					</div>
					<?php foreach($a['sub'] as $sa):?>
						<input type="hidden" name="attr_sub_id[<?php echo $i; ?>][]" value="<?php echo $sa->id ?>" />
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<label for="">名称：<span class="text-danger">*</span></label>
									<input type="text" class="form-control required" placeholder="属性名称"  name="attr_name[<?php echo $i; ?>][]" value="<?php echo $sa->name ?>" />
									<p class="help-block"></p>
								</div>
							</div>
							<div class="col-md-1">
								<div class="checkbox pt20">
									<label>
										<input type="checkbox" class=""  name="attr_change_price[<?php echo $i; ?>][]" value="1" <?php if($sa->price !== NULL) echo 'checked="checked"' ?> /> 价格
										<input type="hidden" name="attr_change_price_val[<?php echo $i; ?>][]" value="<?php echo ($sa->price !== NULL) ? 1 : 0 ?>" />
									</label>
								</div>
							</div>
							<div class="col-md-2 attr_price  <?php if($sa->price === NULL) echo 'hidden' ?>">
								<div class="form-group">
									<label for="">价格：</label>
									<input type="text" class="form-control required" name="attr_price[<?php echo $i; ?>][]" value="<?php if($sa->price) echo CommonHelper::price($sa->price) ?>" placeholder="价格" />
									<p class="help-block"></p>
								</div>
							</div>
							<div class="col-md-1">
								<div class="form-group pt5">
									<label for="">&nbsp;</label><br />
									<a href="javascript:;" class="btn btn-xs btn-danger remove-sub" data-id="<?php echo $sa->id ?>"><i class="fa fa-trash"></i></a>
								</div>
							</div>

						</div>
					<?php endforeach;?>

					<?php $i++; ?>
				</div>
			<?php endforeach;?>
		<?php endif;?>

		<input type="hidden" name="remove_attr" value="" />



	</div>



	<div>
		<br />
		<br />
		<br />
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>
    </div>
</div>

<script src="<?php echo Yii::app()->baseUrl ?>/assets/manage/product_one_attr.js" type="text/javascript"></script>


