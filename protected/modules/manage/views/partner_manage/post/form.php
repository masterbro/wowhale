<h3 class="content-title"><?php echo ($model->id ? '编辑 ' : '添加 ').$type['name']; ?></h3>
<hr />

<form class="validate" method="post" action="" enctype="multipart/form-data">
	<div class="form-group">
		<label for="">标题：</label>
		<input type="text" class="form-control required" name="title" placeholder="标题" value="<?php echo $model->title ?>" />
		<p class="help-block"></p>

	</div>

	<div class="form-group">
		<label for="">优先级：</label>
		<input type="text" class="form-control required number" name="sort_num" placeholder="优先级" value="<?php echo $model->sort_num ?>" />
		<p class="help-block">0-255的数字，数字越大排名越靠前</p>

	</div>

	<?php if($type['has_thumb']): ?>
	<div class="form-group">
		<label for="">图片：</label>
		<input type="file" class="form-control" name="files" />
		<p class="help-block">图片大小为<?php echo $type['thumb_size'][0].'x'.$type['thumb_size'][1] ?> 图片会被自动裁剪，请上传正方型图片</p>
	</div>
	<?php if($model->thumb): ?>
		<div class="form-group">
			<label for="">原图片：</label>
			<img src="<?php echo HtmlHelper::image($model->thumb) ?>"
			<p class="help-block">如不修改则从不用上传</p>
		</div>
	<?php endif;?>
	<?php endif;?>

	<div class="form-group">
		<label for="">内容：</label>
		<textarea name="content" id="ueditor" style="width: 100%" rows="30"><?php echo $model->content; ?></textarea>
		<p class="help-block"></p>
	</div>


	<div>
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>


<script>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			uploadJson : window.kindEditorBase+'/php/upload_json.php?partner_id=<?php echo $this->partner_id ?>',
			fileManagerJson : window.kindEditorBase+'/php/file_manager_json.php?partner_id=<?php echo $this->partner_id ?>',
			allowFileManager : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'baidumap', 'image', 'link', 'fullscreen', 'source', 'undo', 'redo']
		});
	});
</script>