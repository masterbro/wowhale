<h3 class="content-title">微网站 <?php echo $type['name'] ?> 管理</h3>
<hr />


<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th style="width: 50%">标题</th>
		<?php if($type['has_thumb']): ?>
		<th>图片</th>
		<?php endif;?>
		<th>点击量</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->title ?></td>
			<?php if($type['has_thumb']): ?>
				<td>
					<?php if($l->thumb):?>
					<img src="<?php echo HtmlHelper::image($l->thumb) ?>" width="200px;" />
					<?php else:?>
						---
					<?php endif;?>
				</td>

			<?php endif;?>

			<td><?php echo $l->hits ?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/post/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/post/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/partner_manage/post/create?type='.$_GET['type']); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加</a>
