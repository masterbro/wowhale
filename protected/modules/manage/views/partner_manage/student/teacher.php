<h3 class="content-title">员工通讯录列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/partner_manage/contacts') ?>" method="get">
	<input type="hidden" name="type" value="teacher" />
	<div class="form-group">
		<label for="name">姓名</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="姓名" />
	</div>
	<div class="form-group">
		<label for="name">班级</label>
		<select class="form-control" name="grade">
			<option value="">所有</option>
			<?php foreach($grades as $k=>$v):?>
			<option value="<?php echo $k ?>" <?php if($_GET['grade'] == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>姓名</th>
		<th>手机</th>
		<th>角色</th>
		<th>班级</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->parent_name ?></td>
			<td><?php echo $l->parent_phone ?></td>
			<td><?php echo $l->leader ? '园长' : '老师' ?></td>
			<td><?php echo !$l->leader && $l->grade_id ? $grades[$l->grade_id] : '---' ?></td>

			<td>
					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/update?id='.$l->id); ?>" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/create?type=teacher'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> 添加员工</a>

<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>