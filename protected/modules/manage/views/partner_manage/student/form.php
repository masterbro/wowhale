<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
    <div class="col-md-2 sidebar-2-parent">
        <ul class="nav nav-pills nav-stacked sidebar-2">

            <h4 class="content-title text-center">通讯录</h4>
            <hr />

            <li role="presentation" >
                <a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">员工</a>
            </li>
            <li role="presentation" class="active">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/student') ?>">学生</a>
            </li>
            <li role="presentation">
                <a href="<?php echo $this->createUrl('/manage/partner_manage/grade') ?>">班级</a>
            </li>
        </ul>

    </div>

    <div class="col-md-10">
        <h4 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>学生</h4>
        <hr />

        <form class="validate" method="post" action="">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">学生姓名：<span class="text-danger">*</span></label>
                        <input type="text" class="form-control required" name="name" placeholder="学生姓名" value="<?php echo $model->name ?>" />
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">出生年月：<span class="text-danger">*</span></label>
                        <input type="text" class="form-control required number" name="birth" placeholder="出生年月" value="<?php echo $model->birth ?>" />
                        <p class="help-block">格式：<?php echo date('Ym') ?></p>
                    </div>
                </div>
            </div>
            <hr />
            <div class="form-group">
                <label for="">家长</label> <a href="javascript:;" class="btn btn-success btn-xs add-parent">添加</a>
            </div>

            <div id="parent">
                <?php if($model->id):?>
                    <?php foreach($model->parents as $mp):?>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="">家长姓名：<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="parent_name[]" placeholder="家长姓名" value="<?php echo $mp->parent_name ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="">家长手机号：<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="parent_phone[]" placeholder="家长手机号" value="<?php echo $mp->parent_phone ?>" />
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="">&nbsp;</label>
                                    <p class="help-block"><a href="javascript:;" onclick="deleteParent($(this))" class="btn btn-danger btn-xs" data-id="<?php echo $mp->id ?>">删除</a></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php else:?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">家长姓名：<span class="text-danger">*</span></label>
                                <input type="text" class="form-control required" name="parent_name[]" placeholder="家长姓名" value="<?php //echo $model->parent_name ?>" />
                                <p class="help-block"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">家长手机号：<span class="text-danger">*</span></label>
                                <input type="text" class="form-control required" name="parent_phone[]" placeholder="家长手机号" value="<?php //echo $model->parent_phone ?>" />
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>

            <hr />

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">紧急联系人姓名：<span class="text-danger"></span></label>
                        <input type="text" class="form-control " name="emergency_contact" placeholder="紧急联系人姓名" value="<?php echo $model->emergency_contact ?>" />
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">紧急联系人手机号：<span class="text-danger"></span></label>
                        <input type="text" class="form-control " name="emergency_phone" placeholder="紧急联系人手机号" value="<?php echo $model->emergency_phone ?>" />
                        <p class="help-block"></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">户口所在地：<span class="text-danger"></span></label>
                        <input type="text" class="form-control " name="register_address" placeholder="户口所在地" value="<?php echo $model->emergency_contact ?>" />
                        <p class="help-block"></p>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">家庭住址：<span class="text-danger"></span></label>
                        <input type="text" class="form-control " name="address" placeholder="家庭住址" value="<?php echo $model->emergency_phone ?>" />
                        <p class="help-block"></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">身份证号 <span class="text-danger"></span></label>
                        <input type="text" class="form-control " name="id_num" placeholder="身份证号" value="<?php echo $model->id_num ?>" />

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">班级 <span class="text-danger">*</span></label>
                        <select class="form-control" name="grade_id">
                            <?php foreach($grades as $k=>$v):?>
                                <option value="<?php echo $k ?>" <?php if($model->grade_id == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
            </div>


            <div>
                <br />
                <button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
                <br />
                <br />
                <br />
                <div class="clearfix"></div>
            </div>
            <input type="hidden" name="deleted_contacts" value="" />
        </form>
    </div>
</div>
<script>
	var deleteParent = function(object) {
		if(object.attr('data-id')) {
			var del = $('input[name="deleted_contacts"]').val() + ',' + object.attr('data-id');//
			$('input[name="deleted_contacts"]').val(del);
		}
		object.parent().parent().parent().parent().remove();

	};
	$(function(){


		$('.add-parent').click(function() {
			var html = '';
			html += '<div class="row">';
			html += '<div class="col-md-5">';
			html += '<div class="form-group">';
			html += '<label for="">家长姓名：<span class="text-danger">*</span></label>';
			html += '<input type="text" class="form-control required" name="parent_name[]" placeholder="家长姓名" value="<?php //echo $model->parent_name ?>" />';
			html += '<p class="help-block"></p>';
			html += '</div>';
			html += '</div>';

			html += '<div class="col-md-5">';
			html += '<div class="form-group">';
			html += '<label for="">家长手机号：<span class="text-danger">*</span></label>';
			html += '<input type="text" class="form-control required" name="parent_phone[]" placeholder="家长手机号" value="<?php //echo $model->parent_phone ?>" />';
			html += '<p class="help-block"></p>';
			html += '</div>';
			html += '</div>';

			html += '<div class="col-md-2">';
			html += '<div class="form-group">';
			html += '<label for="">&nbsp;</label>';
			html += '<p class="help-block"><a href="javascript:;" onclick="deleteParent($(this))" class="btn btn-danger btn-xs ">删除</a></p>';
			html += '</div>';
			html += '</div>';

			html += '</div>';

			$('#parent').append(html);
			
		})
	});
</script>