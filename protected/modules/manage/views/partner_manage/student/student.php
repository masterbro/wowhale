<h3 class="content-title">学生通讯录列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/partner_manage/contacts') ?>" method="get">
	<input type="hidden" name="type" value="student" />
	<div class="form-group">
		<label for="name">学生</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="学生名称" />
	</div>
	<div class="form-group">
		<label for="name">班级</label>
		<select class="form-control" name="grade">
			<option value="">所有</option>
			<?php foreach($grades as $k=>$v):?>
			<option value="<?php echo $k ?>" <?php if($_GET['grade'] == $k) echo 'selected="selected"' ?>><?php echo $v ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=student') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>学生姓名</th>
		<th>学生生日</th>
		<th>家长姓名</th>
		<th>家长手机</th>
		<th>班级</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->student_name ?></td>
			<td><?php echo $l->student_birth ?></td>
			<td><?php echo $l->parent_name ?></td>
			<td><?php echo $l->parent_phone ?></td>
			<td><?php echo $l->grade_id ? $grades[$l->grade_id] : '---' ?></td>

			<td>
					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/update?id='.$l->id); ?>" class="btn btn-xs btn-primary" ><i class="fa fa-edit"></i> 编辑</a>

					<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts/create?type=student'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> 添加学生</a>
<hr />
<h3>导入</h3>
<p>导入说明：导入时务必保证班级栏的名字和班级管理中的班级名称一至；电话务必为手机号码。</p>
<p><a href="<?php echo HtmlHelper::assets('file/student.xlsx'); ?>" class="btn btn-success btn-xs"><i class="fa fa-file-excel-o"></i> 下载导入模版</a></p>
<br />
<form action="<?php echo $this->createUrl('/manage/partner_manage/contacts/import') ?>" enctype="multipart/form-data" method="post">
	<input type="file" name="file" />
	<br />
	<button class="btn btn-info btn-sm" type="submit"><i class="fa fa-cloud-upload"></i> 导入学生</button>
</form>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>