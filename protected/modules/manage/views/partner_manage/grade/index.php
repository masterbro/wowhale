<!--新增嵌套列，方便插入里面的侧边栏-->
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
      
        <ul class="nav nav-pills nav-stacked sidebar-2">
			
            <h4 class="content-title text-center">通讯录</h4>
		    <hr />
                    
          <li role="presentation" >
              <a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">员工</a>
          </li>
          <li role="presentation" >
              <a href="<?php echo $this->createUrl('/manage/partner_manage/student') ?>">学生</a>
          </li>
          <li role="presentation" class="active">
              <a href="<?php echo $this->createUrl('/manage/partner_manage/grade') ?>">班级</a>
          </li>
        </ul>
                           
                          
    </div>
    
	<div class="col-md-10">
    
<!--网页以前的内容start-->

<h4 class="content-title">班级管理</h4>
<hr />


		<a href="javascript:;" class="btn btn-primary btn-sm show-modal"
		   data-action="create"
		   data-fid="0"
			><i class="fa fa-plus"></i> 添加班级</a>
		<hr />

		<table class="table table-hover ">
			<thead>
			<tr>
				<th style="width: 80px;">名称</th>
				<th style="width: 60%;"></th>
				<th>操作</th>
			</tr>
			</thead>


			<tbody>
			<?php foreach($lists as $k=>$v):?>
				<tr>
					<td colspan="2"><?php echo $v->name ?></td>
					<td>
<!--						<a href="javascript:;" class="btn btn-xs btn-primary show-modal"-->
<!--						   data-action="create"-->
<!--						   data-fid="--><?php //echo $v->id ?><!--"-->
<!--							><i class="fa fa-plus"></i> 添加</a>-->
						<a href="javascript:;" class="btn btn-xs btn-success show-modal"
							data-action="update"
							data-id="<?php echo $v->id ?>"
							data-name="<?php echo $v->name ?>"
							><i class="fa fa-edit"></i> 编辑</a>
						<a href="<?php echo $this->createUrl('/manage/partner_manage/grade/delete?id='.$v->id) ?>" class="btn btn-xs btn-danger" onclick="return confirm('你确定要删除此班级吗？')"><i class="fa fa-trash"></i> 删除</a>
					</td>
				</tr>

			<?php endforeach;?>


		</tbody>
			</table>




<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">班级管理</h4>
			</div>
			<div class="modal-body">
			<form id="trade-form" method="post">
				<input type="hidden" name="id" value="" />
				<input type="hidden" name="fid" value="" />
				<div class="form-group">
					<label for="">名称 <span class="text-danger">*</span></label>
					<input type="text" class="form-control hr-mi required" name="name" placeholder="名称" value="" />
					<p class="help-block"></p>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-undo"></i> 取消</button>
				<button type="button" class="btn btn-primary" id="submit"><i class="fa fa-check"></i> 保存</button>
			</div>

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(function(){
		var action = '';
		var modal = $('#myModal');

		$('.show-modal').click(function(){
			if($(this).attr('data-action') == 'create') {
				action = BASE_URL + '/manage/partner_manage/grade/create';
				modal.find('input[name="fid"]').val($(this).attr('data-fid'));
				modal.find('input[name="name"]').val('');
			} else {
				action = BASE_URL + '/manage/partner_manage/grade/update';
				modal.find('input[name="id"]').val($(this).attr('data-id'));
				modal.find('input[name="name"]').val($(this).attr('data-name'));
			}
			modal.find('form').attr('action', action);
			modal.modal('show');
		});

		$('#submit').click(function() {
			if(!modal.find('input[name="name"]')) {
				alert('名称不能为空');
				return false;
			}

			modal.find('form').submit();
		})
	})
</script>

<!--网页以前的内容end-->

	</div>
</div>
