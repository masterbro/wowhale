<h3 class="content-title">微网站首页滑动图</h3>
<hr />
<p>微网站地址：<img src="<?php echo $this->createUrl('/qrCode?data='.urlencode(Yii::app()->params['mobile_base'].'/merchant?id='.$this->partner_id)) ?>" width="100" />
<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>图片</th>
		<th>链接</th>
		<th>优先级</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><img src="<?php echo HtmlHelper::image($l->img) ?>" width="200px;" /></td>
			<td><?php echo $l->url ?></td>
			<td><?php echo $l->sort_num ?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/slider/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/partner_manage/slider/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/partner_manage/slider/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加</a>
