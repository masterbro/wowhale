<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台管理 登录</title>
	<script type="text/javascript">
		BASEURL = '<?php echo Yii::app()->baseUrl ?>';
	</script>
    <link href="<?php echo Yii::app()->baseUrl ?>/assets/css/admin/login.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.img-captcha {
			width: 170px;
			height: 45px;
			display: block;
			position: absolute;
			right: 0px;
			top: 0px;
		}
	</style>
</head>
<body>

<div class="wrap">
    <h1><a href="#">后台管理中心</a></h1>
    <div class="login">
        <ul>
            <li>
                <input class="input" id="J_admin_name" required name="username" type="text" placeholder="帐号名" title="帐号名" />
            </li>
            <li>
                <input class="input" id="admin_pwd" type="password" required name="password" placeholder="密码" title="密码" />
            </li>
	        <li style="position: relative">
		        <input class="input" id="captcha" required name="captcha" type="text" placeholder="验证码" title="验证码" />
		        <img src="<?php echo Yii::app()->baseUrl ?>/index.php/manage/account/captcha?v=<?php echo time() ?>" onclick="this.src='<?php echo Yii::app()->request->baseUrl; ?>/index.php/manage/account/captcha?v='+Math.random()" class="img-captcha" id="login-captcha" />
<!--		        <img src="http://placehold.it/175x45" onclick="this.src='--><?php //echo Yii::app()->request->baseUrl; ?><!--/index.php/manage/account/captcha?v='+Math.random()" class="img-captcha" id="login-captcha" />-->
	        </li>
        </ul>
        <button type="submit" name="submit" class="btn" id="submit">登录</button>
    </div>
</div>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.1.9.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(function(){
			//不支持placeholder浏览器下对placeholder进行处理
			if(document.createElement('input').placeholder !== '') {
				$('[placeholder]').focus(function() {
					var input = $(this);
					if(input.val() == input.attr('placeholder')) {
						input.val('');
						input.removeClass('placeholder');
					}
				}).blur(function() {
					var input = $(this);
					if(input.val() == '' || input.val() == input.attr('placeholder')) {
						input.addClass('placeholder');
						input.val(input.attr('placeholder'));
					}
				}).blur().parents('form').submit(function() {
					$(this).find('[placeholder]').each(function() {
						var input = $(this);
						if(input.val() == input.attr('placeholder')) {
							input.val('');
						}
					});
				});
			}
		})

		$('#submit').click(function(){
			var username = $('input[name="username"]').val();
			if(!username) {
				alert('请输入你的用户名');
				return false;
			}



			var password = $('input[name="password"]').val();
			if(!password) {
				alert('请输入你的密码');
				return false;
			}

			var captcha = $('input[name="captcha"]').val();
			if(captcha.length != 4) {
				alert('请输入验证码');
				return false;
			}

			$.ajax({
				url:BASEURL+'/manage/account/login',
				data:{username:username, password:password, captcha:captcha},
				dataType:'json',
				type:'post',
				success: function(data) {

					if(!data.error) {
						window.location.href = BASEURL+'/manage';
					} else {
						alert(data.msg);
					}
				}
			})
		})
	})
</script>
</body>
</html>
