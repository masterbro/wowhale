<h3 class="content-title">后台帐号管理</h3>
<hr />


<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/manager') ?>" method="get">
	<div class="form-group">
		<label for="name">类型</label>
		<select class="form-control" name="type" id="ww-type">
			<option value="">所有</option>
			<option value="system" <?php if($_GET['type'] == 'system') echo 'selected="selected"' ?>>系统管理员</option>
			<option value="school" <?php if($_GET['type'] == 'school') echo 'selected="selected"' ?>>合作伙伴</option>
		</select>
	</div>
	<div class="form-group" style="display: none" id="ww-schools">
		<label for="name">学校/商家</label>
		<select class="form-control" name="school">
			<option value="">所有</option>
			<?php foreach($partners as $sk=>$sv):?>
				<option value="<?php echo $sk ?>" <?php if($_GET['school'] == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/manager') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>
<?php
	$role_id = Yii::app()->params['role_id'];
?>
<br />
<table class="table table-bordered table-hover ">
	<thead>
	<tr>
		<th>用户名</th>
		<th>真实姓名</th>
		<th>电话</th>
		<th>类型</th>
		<th>角色</th>
		<th>最后登录时间</th>
		<th>状态</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $m):?>
		<tr>
			<td><?php echo $m->username ?></td>
			<td><?php echo $m->realname ?></td>
			<td><?php echo $m->tel ?></td>
			<td><?php
				if($m->type) {
					echo '合作伙伴【'.$partners[$m->partner_id].'】';
				} else {
					echo '系统管理员';
				}
				?></td>
			<td><?php echo $m->role_name ? $m->role_name : '---' ?></td>
			<td><?php echo $m->last_login ? date('Y-m-d H:i:s', $m->last_login) : '未登录' ?></td>
			<td>
				<?php if($m->status):?>
					<span class="label label-success">正常</span>
				<?php else:?>
					<span class="label label-danger">停用</span>
				<?php endif;?>
			</td>
			<td>
				<?php if($m->id > 1):?>
					<a href="<?php echo $this->createUrl('/manage/manager/update?id='.$m->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<?php endif;?>

				<?php if(!$m->type && (in_array($m->role_id, $role_id))):?>
					<a href="<?php echo $this->createUrl('/manage/manager/scope?id='.$m->id); ?>" class="btn btn-xs btn-success"><i class="fa fa-lock"></i> 管理范围</a>
				<?php endif;?>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<a href="<?php echo $this->createUrl('/manage/manager/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加帐号</a>




<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>

<script>
	$(function(){
		var schools = $('#ww-schools');
		$('#ww-type').change(function(){
			typeSwitch($(this).val(), schools);
		});

		typeSwitch($('#ww-type').val(), schools);
	});
	var typeSwitch = function(type, schools) {
		if(type == 'school') {
			schools.show();
		} else {
			schools.hide();
			schools.find('select option').eq(0).prop('selected', true);
		}
	}
</script>