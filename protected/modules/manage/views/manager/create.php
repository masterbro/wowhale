<h3 class="content-title"><?php echo $model->id ? '编辑后台帐号' : '添加后台帐号' ?></h3>
<hr />
<?php
$form = $this->beginWidget('CActiveForm', array(
	'htmlOptions' => array('class'=> 'validate', 'autocomplete' => 'off',),
	'action' => $model->id ? $this->createUrl('/manage/manager/update?id='.$model->id) : $this->createUrl('/manage/manager/create')
));
?>
<div class="form-group">
	<label for="">用户名</label>
	<input type="text" class="form-control hr-mi required" name="username" placeholder="用户名" value="<?php echo $model->username ?>" />
	<p class="help-block"></p>
</div>
<div class="form-group">
	<label for="">登录密码</label>
	<?php if($model->id):?>
		<input type="password" class="form-control hr-mi" name="password" placeholder="登录密码" value="" />
		<p class="help-block">如不修改则留空</p>
	<?php else:?>
		<input type="password" class="form-control hr-mi require" name="password" placeholder="登录密码" value="" />
		<p class="help-block"></p>
	<?php endif;?>
</div>

<div class="form-group">
	<label for="">真实姓名</label>
	<input type="text" class="form-control hr-mi required" name="realname" placeholder="真实姓名" value="<?php echo $model->username ?>" />
	<p class="help-block"></p>
</div>

<div class="form-group">
	<label for="">联系电话</label>
	<input type="text" class="form-control hr-mi required" name="tel" placeholder="联系电话" value="<?php echo $model->tel ?>" />
	<p class="help-block"></p>
</div>
<div class="form-group">
	<label for="">类型</label>
	<select class="form-control hr-mi" name="type" id="ww-type">
		<option value="0">系统管理员</option>
		<option value="1" <?php if($model->type) echo 'selected="selected"' ?>>合作伙伴</option>
	</select>
</div>
<div class="form-group" style="display: none" id="ww-schools">
	<label for="name">学校/商家</label>
	<select class="form-control hr-mi" name="partner_id">
		<?php foreach($partners as $sk=>$sv):?>
			<option value="<?php echo $sk ?>" <?php if($model->partner_id == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
		<?php endforeach;?>
	</select>
</div>
<div class="form-group" id="ww-roles">

	<label for="">角色</label>
	<select class="form-control hr-mi" name="role_id">
		<?php foreach($roles as $r):?>
			<option value="<?php echo $r->name ?>" <?php if($r->name == $model->role_id) echo 'selected="selected"' ?>><?php echo $r->description ?></option>
		<?php endforeach;?>
	</select>
	<p class="help-block"></p>
	<?php $first_role = array_shift($roles) ?>

	<input type="hidden" name="role_name" value="<?php echo $model->role_name ? $model->role_name : $first_role->description ?>" />
</div>

<div class="form-group">

	<label for="">状态</label>
	<select class="form-control hr-mi" name="status">
		<option value="1">正常</option>
		<option value="0" <?php if($model->id  && $model->status == 0) echo 'selected="selected"' ?>>停用</option>
	</select>
</div>



<button type="button" class="btn btn-success hr-save" data-callback="roleSuccess()" data-loading-text="保存中..."><i class="fa fa-save"></i> 保存</button>
<?php $this->endWidget(); ?>

<br />
<br />
<br />
<script>
	function roleSuccess() {
		window.location.href = BASE_URL + '/manage/manager';
	}

	$(function(){
		$('select[name="role_id"]').change(function(){
			$('input[name="role_name"]').val($(this).find("option:selected").text());
		});
	})

	$(function(){
		var schools = $('#ww-schools');
		var roles = $('#ww-roles');

		$('#ww-type').change(function(){
			typeSwitch($(this).val(), schools, roles);
		});

		typeSwitch($('#ww-type').val(), schools, roles);
	});
	var typeSwitch = function(type, schools, roles) {
		console.log(type);
		if(type == '1') {
			schools.show();
			roles.hide();
		} else {
			schools.hide();
			roles.show();
		}
	}
</script>


