<h3 class="content-title">管理员 管理范围设置</h3>
<hr />
<p>管理员：<?php echo $model->realname.' / '.$model->username ?></p>
<?php
$form = $this->beginWidget('CActiveForm', array(
	'htmlOptions' => array('class'=> 'validate', 'autocomplete' => 'off',),
));
?>
<div class="form-group">
	<label for="">范围设置</label>
	<select class="form-control hr-mi" name="partner_scope" id="ww-type">
		<option value="0">全部</option>
		<option value="1" <?php if($model->partner_scope) echo 'selected="selected"' ?>>部分</option>
	</select>
</div>

<div class="form-group" id="scopes">
	<label for="">范围设置</label>
	<br />

	<?php foreach($partners as $rk=>$rv):?>
		<label class="checkbox-inline">
			<input type="checkbox" <?php if(in_array($rv->id, $scopes)) echo 'checked="checked"' ?> class="required" name="partners[]" value="<?php echo $rv->id ?>" /> <?php echo $rv->name ?>
		</label>
	<?php endforeach;?>
</div>

<button type="button" class="btn btn-success hr-save" data-callback="roleSuccess()" data-loading-text="保存中..."><i class="fa fa-save"></i> 保存</button>
<?php $this->endWidget(); ?>

<br />
<br />
<br />
<script>
	function roleSuccess() {
		window.location.href = BASE_URL + '/manage/manager';
	}

	$(function(){
		var scopes = $('#scopes');

		$('#ww-type').change(function(){
			typeSwitch($(this).val(), scopes);
		});

		typeSwitch($('#ww-type').val(), scopes);
	});
	var typeSwitch = function(type, scopes) {
		//console.log(type);
		if(type == '1') {
			scopes.show();
		} else {
			scopes.hide();
		}
	}
</script>


