<!DOCTYPE html>
<html lang="zh-cn">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>娃娃营 | 管理后台</title>

	<!-- Bootstrap -->
	<link href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->baseUrl ?>/assets/font_awesome/css/font-awesome.min.css" rel="stylesheet">

	<link href="<?php echo Yii::app()->baseUrl ?>/assets/manage/css/main.css" rel="stylesheet">
	<link href="<?php echo Yii::app()->baseUrl ?>/assets/manage/css/custom.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/umeditor1_2_2/themes/default/css/umeditor.min.css"/>
	<link href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/css/bootstrap-datepicker.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
		BASE_URL = '<?php echo Yii::app()->baseUrl ?>';
	</script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.1.9.1.min.js"></script>
	<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.validate.js"></script>
	<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.validate.msg_cn.js"></script>
	<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/bootstrap-datepicker.min.js"></script>

	<script src="<?php echo Yii::app()->baseUrl ?>/assets/manage/main.js"></script>

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl ?>/assets/kindeditor-4.1.10/themes/default/default.css" />
	<script charset="utf-8" src="<?php echo Yii::app()->baseUrl ?>/assets/kindeditor-4.1.10/kindeditor-min.js"></script>
	<script charset="utf-8" src="<?php echo Yii::app()->baseUrl ?>/assets/kindeditor-4.1.10/lang/zh_CN.js"></script>
	<script>
		window.kindEditorBase = '<?php echo Yii::app()->baseUrl ?>/assets/kindeditor-4.1.10';
	</script>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="any" mask="" href="/favicon.ico">
	    
</head>
<body>
<?php
$action_id = Yii::app()->controller->action->id;
$controller_id = Yii::app()->controller->id;
?>
<nav class="navbar navbar-fixed-top yl-navbar" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar" style="background:#fff"></span>
				<span class="icon-bar" style="background:#fff"></span>
				<span class="icon-bar" style="background:#fff"></span>
			</button>
			<a class="navbar-brand" href="<?php echo $this->createUrl('home/index') ?>"><?php echo Yii::app()->user->getState('school') ?><?php if(Yii::app()->params['evn'] == 'test'):?><span class="text-danger">[beta]</span> <?php endif;?></a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">

			<ul class="nav navbar-nav navbar-right">                       
                
                <?php
					$partner = Partner::model()->findByPk($this->partner_id); 
					if($partner->type == 1)://1为商家?>
                    
                    <li style="margin-right: 15px;" class="<?php if($controller_id == 'home') echo 'active' ?>"><a href="<?php echo $this->createUrl('home/index') ?>">后台首页</a></li>
                
				<?php else://else为学校时显示全部应用?>
					
                    <li class="<?php if($controller_id == 'home') echo 'active' ?>"><a href="<?php echo $this->createUrl('home/index') ?>">后台首页</a></li>                	
                    <li style="margin-right: 15px;" class="<?php if($action_id == 'index' && $controller_id == 'partner_manage/wechat') echo 'active' ?>"><a href="<?php echo $this->createUrl('/manage/partner_manage/wechat') ?>">全部应用</a></li>
                
				<?php endif;?>             
                <!--<li style="margin-right: 15px;"><a href="<?php echo $this->createUrl('#') ?>">帮助</a></li>-->
                
				<li style="padding-top: 15px;">
					<div class="dropdown">
						<a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
							<i class="fa fa-user"></i> <?php echo Yii::app()->user->getState('realname')?Yii::app()->user->getState('realname'):Yii::app()->user->getState('username') ?>
							<span class="caret"></span>
						</a>

						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<li><a href="<?php echo $this->createUrl('/manage/home/profile') ?>"><i class="fa fa-lock"></i> 修改密码</a></li>
							<li><a href="<?php echo $this->createUrl('/manage/account/logout') ?>"><i class="fa  fa-sign-out"></i> 退出</a></li>
						</ul>
					</div>
				</li>
                                                   
			</ul>
		</div><!--/.nav-collapse -->

	</div>
</nav>
<div style="padding-top: 80px;">
</div>


<div class="container-fluid">
	<div id="web-center" class="row">
    
		<div id="center-left" class="col-xs-1">
			<div class="panel-group yl-panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin:0px;">

				<?php
				    $partner = Partner::model()->findByPk($this->partner_id);
					if($partner->type == 1) {
						$this->renderPartial('/layouts/_navMerchantNew', array('controller_id' => $controller_id));
					} else {
						$this->renderPartial('/layouts/_navSchoolNew', array('controller_id' => $controller_id));
					}
				?>

			</div>
		</div>

		<script type="text/javascript">
			(function($){
				$(function(){
					$('.product_tabs li').click(function(){
						if($(this).hasClass('active'))
							return true;

						$('.product_tabs li.active').removeClass('active');
						$(this).addClass('active');
						var con = $(this).attr('id').replace("tabs", "tabs_contents");
//						console.log(con);
						$('.product_tabs_content').hide();
						$('#'+con).show();
					});

					$('.click-me').trigger('click');
				})
			}(jQuery));
		</script>
        
		<div id="center-right" class="col-xs-11 yl-container <?php if($partner->type == 1): ?>right-merchant<?php  else:?>right-school<?php endif;?>">
			<?php if(Yii::app()->user->hasFlash('success')):?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h4>操作成功!</h4>
					<p><?php echo Yii::app()->user->getFlash('success'); ?></p>
				</div>
			<?php endif; ?>

			<?php if(Yii::app()->user->hasFlash('error')):?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h4>操作失败!</h4>
					<p><?php echo Yii::app()->user->getFlash('error'); ?></p>
				</div>
			<?php endif; ?>

			<?php echo $content ?>
		</div>
	</div>

</div><!-- /container -->

<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/mobile/change_size.js" type="text/javascript"></script>

<footer class="footer manage-footer" style="position:fixed; z-index: 2000">
	<div class="container web-foot">
		<p class="text-muted text-center">娃娃营 版权所有 &copy;<?php echo date('Y'); ?> 蜀ICP备15013283号	</p>
	</div>
</footer>
<?php echo $this->renderPartial('//layouts/_baidu'); ?>
</body>
</html>