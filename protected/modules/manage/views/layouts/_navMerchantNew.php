<div class="sidebar">
	<ul class="nav nav-pills nav-stacked text-center nav-merchant col-xs-1" style="padding-right: 0">
        <?php
        $balance_on = array('partner_manage/balance');
        $product_on = array('partner_manage/product');
        $wap_on = array('partner_manage/partner');
        $order_on = array('partner_manage/order');

        ?>
        <li role="presentation">
            <a class="<?php if(in_array($controller_id, $wap_on)):?>active<?php endif;?>" href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-merchant-icon-1.png"><br>
                微网站管理
            </a>
        </li>
        
        <li role="presentation">
           <a class="<?php if(in_array($controller_id, $order_on)):?>active<?php endif;?>" href="<?php echo $this->createUrl('/manage/partner_manage/order') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-merchant-icon-2.png"><br>
            	订单管理
            </a>
        </li>
        
        <li role="presentation">
            <a class="<?php if(in_array($controller_id, $product_on)):?>active<?php endif;?>" href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-merchant-icon-3.png"><br>
              	产品管理
            </a>
        </li>
        
        <li role="presentation">
            <a class="<?php if(in_array($controller_id, $balance_on)):?>active<?php endif;?>" href="<?php echo $this->createUrl('/manage/partner_manage/balance') ?>" aria-expanded="false" aria-controls="collapseOne">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-merchant-icon-4.png"><br>
              	结算管理
            </a>
        </li>
      
    </ul>  
</div>

<script>
	var h = $("#center-right").height()+80;
	h = h + 'px';
	$(".sidebar").height(h);
</script>