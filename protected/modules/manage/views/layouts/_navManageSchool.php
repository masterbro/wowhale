<?php
//后台学校管理员菜单
?>

<!--<div class="panel-group yl-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
			<h4 class="panel-title">
				<a class="collapsed <?php /*if($controller_id == 'school') echo 'click-me' */?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-mortar-board icon"></i><i class="fa fa-chevron-down pull-right"></i>学校管理
				</a>
			</h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				<a href="<?php /*echo $this->createUrl('/manage/school') */?>">学校列表</a>
				<a href="<?php /*echo $this->createUrl('/manage/school/create') */?>">添加学校</a>
				<a href="<?php /*echo $this->createUrl('/manage/school/apply') */?>">学校申请</a>

			</div>
		</div>
	</div>


	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
			<h4 class="panel-title">
				<a class="" href="<?php /*echo $this->createUrl('/manage/order') */?>">
					<i class="fa fa-shopping-cart icon"></i><i class="fa fa-chevron-right pull-right"></i> 订单管理
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading6">
			<h4 class="panel-title">
				<a class="" href="<?php /*echo $this->createUrl('/manage/product') */?>">
					<i class="fa fa-truck icon"></i><i class="fa fa-chevron-right pull-right"></i> 产品管理
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
			<h4 class="panel-title">
				<a class="" href="<?php /*echo $this->createUrl('/manage/teacher') */?>">
					<i class="fa fa-book icon"></i><i class="fa fa-chevron-right pull-right"></i> 老师列表
				</a>
			</h4>
		</div>
	</div>


	<div class="panel panel-default" style="border-bottom: 1px solid #ddd">
		<div class="panel-heading" role="tab" id="heading8">
			<h4 class="panel-title">
				<a  href="<?php /*echo $this->createUrl('/manage/student') */?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-child icon"></i><i class="fa fa-chevron-right pull-right"></i> 学生列表
				</a>
			</h4>
		</div>
	</div>

</div>-->

<?php
	$schoolM_on = array('school');
	$order_on = array('order');
	$product_on = array('product');
	$teacher_on = array('teacher');
	$student_on = array('student');

	if(in_array($controller_id, $schoolM_on)){
		$active = 'schoolM';
	}
	else if(in_array($controller_id, $order_on)){
		$active = 'order';
	}
	else if(in_array($controller_id, $product_on)){
		$active = 'product';
	}
	else if(in_array($controller_id, $teacher_on)){
		$active = 'teacher';
	}
	else if(in_array($controller_id, $student_on)){
		$active = 'student';
	}
	else{
		$active = '';
	}

?>

<div class="sidebar">
	<ul class="nav nav-pills nav-stacked text-center nav-school col-xs-1" style="padding-right: 0">

		<li role="presentation" class="<?php if($active=='schoolM'):?>active<?php endif;?>">
			<a href="<?php echo $this->createUrl('/manage/school') ?>">
				<img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/mschool/img-1.png" style="width: 32px;"><br>
				学校管理
			</a>
		</li>

		<li role="presentation"  class="<?php if($active=='order'):?>active<?php endif;?>">
			<a href="<?php echo $this->createUrl('/manage/order') ?>">
				<img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/mschool/img-2.png" style="width: 32px;"><br>
				订单管理
			</a>
		</li>

		<li role="presentation"  class="<?php if($active=='product'):?>active<?php endif;?>">
			<a href="<?php echo $this->createUrl('/manage/product') ?>">
				<img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/mschool/img-3.png" style="width: 32px;"><br>
				产品管理
			</a>
		</li>

		<li role="presentation"  class="<?php if($active=='teacher'):?>active<?php endif;?>">
			<a href="<?php echo $this->createUrl('/manage/teacher') ?>">
				<img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/mschool/img-4.png" style="width: 32px;"><br>
				老师列表
			</a>
		</li>

		<li role="presentation"  class="<?php if($active=='student'):?>active<?php endif;?>">
			<a href="<?php echo $this->createUrl('/manage/student') ?>">
				<img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/mschool/img-5.png" style="width: 32px;"><br>
				学生列表
			</a>
		</li>

	</ul>
</div>