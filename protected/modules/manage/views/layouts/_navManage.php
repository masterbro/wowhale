<?php
//后台管理员菜单
?>
<div class="panel-group yl-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingOne">
			<h4 class="panel-title">
				<a class="collapsed <?php if($controller_id == 'school') echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-mortar-board icon"></i><i class="fa fa-chevron-down pull-right"></i>学校管理
				</a>
			</h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<div class="panel-body">
				<a href="<?php echo $this->createUrl('/manage/school') ?>">学校列表</a>
				<a href="<?php echo $this->createUrl('/manage/school/create') ?>">添加学校</a>
				<a href="<?php echo $this->createUrl('/manage/school/apply') ?>">学校申请</a>

			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="collapsed <?php if($controller_id == 'merchant') echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					<i class="fa fa-building-o icon"></i><i class="fa fa-chevron-down pull-right"></i>商家管理
				</a>
			</h4>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			<div class="panel-body">
				<a href="<?php echo $this->createUrl('/manage/merchant') ?>">商家列表</a>
				<a href="<?php echo $this->createUrl('/manage/merchant/create') ?>">添加商家</a>
				<a href="<?php echo $this->createUrl('/manage/merchant/apply') ?>">商家申请</a>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/user') ?>">
					<i class="fa fa-users icon"></i><i class="fa fa-chevron-right pull-right"></i> 用户管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/order') ?>">
					<i class="fa fa-shopping-cart icon"></i><i class="fa fa-chevron-right pull-right"></i> 订单管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading6">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/product') ?>">
					<i class="fa fa-truck icon"></i><i class="fa fa-chevron-right pull-right"></i> 产品管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/category') ?>">
					<i class="fa fa-bars icon"></i><i class="fa fa-chevron-right pull-right"></i> 产品分类
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/teacher') ?>">
					<i class="fa fa-book icon"></i><i class="fa fa-chevron-right pull-right"></i> 老师列表
				</a>
			</h4>
		</div>
	</div>


	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading8">
			<h4 class="panel-title">
				<a  href="<?php echo $this->createUrl('/manage/student') ?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-child icon"></i><i class="fa fa-chevron-right pull-right"></i> 学生列表
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading7">
			<h4 class="panel-title">
				<a  href="<?php echo $this->createUrl('/manage/balance') ?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-credit-card icon"></i><i class="fa fa-chevron-right pull-right"></i> 结算管理
				</a>
			</h4>
		</div>

	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading9">
			<h4 class="panel-title">
				<a  href="<?php echo $this->createUrl('/manage/page') ?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-file-text-o icon"></i><i class="fa fa-chevron-right pull-right"></i> 文章管理
				</a>
			</h4>
		</div>
	</div>

	<div class="panel panel-default" style="border-bottom: 1px solid #ddd">
		<div class="panel-heading" role="tab" id="heading8">
			<h4 class="panel-title">
				<a  href="<?php echo $this->createUrl('/manage/slider') ?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-image icon"></i><i class="fa fa-chevron-right pull-right"></i> 首页BANNER
				</a>
			</h4>
		</div>

	</div>


</div>