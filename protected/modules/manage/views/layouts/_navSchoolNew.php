<?php
    $action_id = Yii::app()->controller->action->id;
    $contacts_on = array('partner_manage/contacts', 'partner_manage/student', 'partner_manage/grade');
    $notice_on = array('partner_manage/notice','partner_manage/partner');

    $active = 'app-center';
    if(in_array($controller_id, $contacts_on)){
        $active = 'mail-list';
    }
    else if(in_array($controller_id, $notice_on)){
        $active = 'school-notice';
    }
    else if($controller_id == 'partner_manage/wechat' && $action_id == 'index'){
        $active = '';
    }
?>
<div class="sidebar">
	<ul class="nav nav-pills nav-stacked text-center nav-school col-xs-1" style="padding-right: 0">
    
        <li id="app-center" role="presentation" class="<?php if($active=='app-center'):?>active<?php endif;?>">
            <a href="<?php echo $this->createUrl('/manage/partner_manage/wechat/mywechat') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-school-icon-1.png"><br>
                我的应用
            </a>
        </li>
        <?php
        $ww_apps = CommonHelper::qyApp();
        $ww_suite_id = $ww_apps['notice']['ww_suite_id'];
        $config = WechatConfig::getInstance($this->partner_id, $ww_suite_id);
        if($config&&$config->appIdsObj->notice):?>
        <li id="mail-list" role="presentation"  class="<?php if($active=='mail-list'):?>active<?php endif;?>">
            <a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-school-icon-2.png"><br>
            通讯录
            </a>
        </li>

        <li id="school-notice" role="presentation"  class="<?php if($active=='school-notice'):?>active<?php endif;?>">
            <a href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">
                <img src="<?php echo Yii::app()->baseUrl ?>/assets/manage/images/m-school-icon-3.png"><br>
              校园公告
            </a>
        </li>
        <?php endif;?>
    </ul>  
</div>
