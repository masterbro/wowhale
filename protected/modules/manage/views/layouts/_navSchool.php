<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="headingOne">
		<h4 class="panel-title">
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/partner') ?>">
				<i class="fa fa-building-o icon"></i><i class="fa fa-chevron-right pull-right"></i> 微网站
			</a>
		</h4>
	</div>
</div>
<?php $contacts_on = array('partner_manage/contacts', 'partner_manage/student'); ?>

<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="heading9">
		<h4 class="panel-title">
			<a class="collapsed <?php if(in_array($controller_id, $contacts_on)) echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
				<i class="fa fa-users icon"></i><i class="fa fa-chevron-down pull-right"></i> 通讯录
			</a>
		</h4>
	</div>
	<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
		<div class="panel-body">
			<a href="<?php echo $this->createUrl('/manage/partner_manage/contacts?type=teacher') ?>">员工</a>
			<a href="<?php echo $this->createUrl('/manage/partner_manage/student') ?>">学生</a>
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/grade') ?>">
				班级管理
			</a>
		</div>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="">
		<h4 class="panel-title">
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/notice') ?>">
				<i class="fa fa-bullhorn icon"></i><i class="fa fa-chevron-right pull-right"></i> 学校公告
			</a>
		</h4>
	</div>
</div>

<!--				<div class="panel panel-default">-->
<!--					<div class="panel-heading" role="tab" id="headingThree">-->
<!--						<h4 class="panel-title">-->
<!--							<a class="" href="--><?php //echo $this->createUrl('/manage/partner_manage/order') ?><!--">-->
<!--								<i class="fa fa-shopping-cart icon"></i><i class="fa fa-chevron-right pull-right"></i> 订单管理-->
<!--							</a>-->
<!--						</h4>-->
<!--					</div>-->
<!--				</div>-->
<!--				<div class="panel panel-default">-->
<!--					<div class="panel-heading" role="tab" id="heading6">-->
<!--						<h4 class="panel-title">-->
<!--							<a class="" href="--><?php //echo $this->createUrl('/manage/partner_manage/product') ?><!--">-->
<!--								<i class="fa fa-truck icon"></i><i class="fa fa-chevron-right pull-right"></i> 产品管理-->
<!--							</a>-->
<!--						</h4>-->
<!--					</div>-->
<!--				</div>-->

<?php $product_on = array();?>
<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="heading7">
		<h4 class="panel-title">
			<a class="collapsed <?php if(in_array($controller_id, $product_on)) echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
				<i class="fa fa-truck icon"></i><i class="fa fa-chevron-down pull-right"></i> 缴学费
			</a>
		</h4>
	</div>
	<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
		<div class="panel-body">
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/product') ?>">
				缴费项目
			</a>
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/order') ?>">
				缴费历史
			</a>
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/order/list') ?>">
				缴费报表
			</a>
			<!--							<a class="" href="--><?php //echo $this->createUrl('/manage/partner_manage/balance') ?><!--">-->
			<!--								结算-->
			<!--							</a>-->
		</div>
	</div>
</div>


<?php $mobile_site_on = array('partner_manage/slider', 'partner_manage/post'); ?>
<!--<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading7">
						<h4 class="panel-title">
							<a class="collapsed <?php if(in_array($controller_id, $mobile_site_on)) echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
								<i class="fa fa-globe icon"></i><i class="fa fa-chevron-down pull-right"></i> 微网站管理
							</a>
						</h4>
					</div>
					<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
						<div class="panel-body">
							<a href="<?php echo $this->createUrl('/manage/partner_manage/slider') ?>">首页滑动图</a>
							<a href="<?php echo $this->createUrl('/manage/partner_manage/post?type=news') ?>">新闻公告</a>
							<a href="<?php echo $this->createUrl('/manage/partner_manage/post?type=env') ?>">环境展示</a>
							<a href="<?php echo $this->createUrl('/manage/partner_manage/post?type=member') ?>">员工风采</a>
						</div>
					</div>
				</div>-->

<?php $mobile_site_on = array('partner_manage/slider', 'partner_manage/post', 'partner_manage/albums'); ?>
<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="heading8">
		<h4 class="panel-title">
			<a class="collapsed <?php if(in_array($controller_id, $mobile_site_on)) echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse7">
				<i class="fa fa-image icon"></i><i class="fa fa-chevron-down pull-right"></i> 校园相册
			</a>
		</h4>
	</div>
	<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
		<div class="panel-body">
			<a href="<?php echo $this->createUrl('/manage/partner_manage/albums') ?>">相册列表</a>
			<!--							<a href="--><?php //echo $this->createUrl('/manage/partner_manage/album/teacher') ?><!--">教师管理</a>-->
		</div>
	</div>
</div>
<div class="panel panel-default">
	<div class="panel-heading" role="tab" id="">
		<h4 class="panel-title">
			<a class="" href="<?php echo $this->createUrl('/manage/partner_manage/wechat') ?>">
				<i class="fa fa-wechat icon"></i><i class="fa fa-chevron-right pull-right"></i> 应用管理
			</a>
		</h4>
	</div>
</div>






<?php $sign_log_on = array('partner_manage/signLog'); ?>
<div class="panel panel-default" style="border-bottom: 1px solid #ddd">
	<div class="panel-heading" role="tab" id="heading11">
		<h4 class="panel-title">
			<a  class="collapsed <?php if(in_array($controller_id, $sign_log_on)) echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
				<i class="fa fa-calendar icon"></i><i class="fa fa-chevron-right pull-right"></i> 老师考勤
			</a>
		</h4>
	</div>
	<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
		<div class="panel-body">
			<a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/update') ?>">考勤设置</a>
			<a href="<?php echo $this->createUrl('/manage/partner_manage/signLog/') ?>">考勤记录</a>
		</div>
	</div>

</div>