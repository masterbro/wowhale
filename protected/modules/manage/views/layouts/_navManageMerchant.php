<?php
//后台学校管理员菜单
?>
<div class="panel-group yl-panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="collapsed <?php if($controller_id == 'merchant') echo 'click-me' ?>" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					<i class="fa fa-building-o icon"></i><i class="fa fa-chevron-down pull-right"></i>商家管理
				</a>
			</h4>
		</div>
		<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			<div class="panel-body">
				<a href="<?php echo $this->createUrl('/manage/merchant') ?>">商家列表</a>
				<a href="<?php echo $this->createUrl('/manage/merchant/create') ?>">添加商家</a>
				<a href="<?php echo $this->createUrl('/manage/merchant/apply') ?>">商家申请</a>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingTwo">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/user') ?>">
					<i class="fa fa-users icon"></i><i class="fa fa-chevron-right pull-right"></i> 用户管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="headingThree">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/order') ?>">
					<i class="fa fa-shopping-cart icon"></i><i class="fa fa-chevron-right pull-right"></i> 订单管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>

	<div class="panel panel-default">
		<div class="panel-heading" role="tab" id="heading6">
			<h4 class="panel-title">
				<a class="" href="<?php echo $this->createUrl('/manage/product') ?>">
					<i class="fa fa-truck icon"></i><i class="fa fa-chevron-right pull-right"></i> 产品管理
				</a>
			</h4>
		</div>
		<!--			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">-->
		<!--				<div class="panel-body">-->
		<!--					<a href="javascript:;"></a>-->
		<!--					<a href="javascript:;"></a>-->
		<!--				</div>-->
		<!--			</div>-->
	</div>



	<div class="panel panel-default" style="border-bottom: 1px solid #ddd">
		<div class="panel-heading" role="tab" id="heading7">
			<h4 class="panel-title">
				<a  href="<?php echo $this->createUrl('/manage/balance') ?>" aria-expanded="false" aria-controls="collapseOne">
					<i class="fa fa-credit-card icon"></i><i class="fa fa-chevron-right pull-right"></i> 结算管理
				</a>
			</h4>
		</div>

	</div>



</div>