<h3 class="content-title">订单列表</h3>
<hr />


<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/order') ?>" method="get">
	<div class="input-daterange">
		<label for="start">时间</label>
		<input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['start']) ?>" name="start" />
		<span class="">到</span>
		<input type="text" class="form-control input-datepicker" value="<?php echo urldecode($_GET['to']) ?>" name="to" />
	</div>
	<div class="clearfix"></div>
	<br />
	<div class="form-group">
		<label for="">订单号</label>
		<input type="text" class="form-control" id="" value="<?php echo ($_GET['id']) ?>" name="id" placeholder="订单号" />
	</div>
	<div class="form-group">
		<label for="">支付单号</label>
		<input type="text" class="form-control" id="" value="<?php echo ($_GET['trans_id']) ?>" name="trans_id" placeholder="支付单号" />
	</div>
	<div class="form-group">
		<label for="name">学校/商家</label>
		<select class="form-control" name="school">
			<option>所有</option>
			<?php foreach($partners as $sk=>$sv):?>
			<option value="<?php echo $sk ?>" <?php if($_GET['school'] == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/order') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
	<input type="hidden" name="export" value="0" />
	<button type="button" class="btn btn-success" id="export" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 生成文件中"><i class="fa fa-file-excel-o"></i> 导出</button>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>订单号</th>
		<th>支付状态</th>
		<th>订单内容</th>
		<th>订单金额</th>
		<th>学校/商家</th>
		<th>姓名</th>
		<th>电话</th>
		<th>班级</th>
		<th>年龄</th>
		<th>支付单号</th>
		<th>创建时间</th>
	</tr>
	</thead>
	<tbody>
	<?php 
		$grades = array();
		$contacts = array();
	?>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->import_from ? '微小店导入' : ($l->payment_type==2?'线下支付':$l->display_id) ?></td>
			<td><?php echo $l->payment_status?'已支付':'未支付';?></td>
			<td>
				<?php
				foreach($l->order_details as $lk=>$lo) {
					if($lk) echo '<br />';
					echo $lo->title. '<span class="text-danger">￥'.CommonHelper::price($lo->price).'x'.$lo->qty.'</span>';
				}
				?>
			</td>
			<td>￥<?php echo CommonHelper::price($l->total) ?></td>
			<td><?php echo $partners[$l->school_id] ?></td>
			<td>
				<?php
					$student = $l->student;
					$shipping_info = $l->shippingInfo();
				?>
				<?php
				if($student){
					echo $student->name;
				}
				else{
					if(is_array($shipping_info)) {
						foreach($shipping_info as $s) {
							echo ' '.$s->student_name;
						}
					} else {
						//兼容旧数据
						echo $shipping_info->student_name;
					}
				}
				
				?>
			</td>
			<td>
				<?php
				$show_old_data = true;
				if($l->contacts_id){
					if(!$contacts[$l->contacts_id]){
						$contacts[$l->contacts_id] = Contacts::model()->findByPk($l->contacts_id);
					}
					if($contacts[$l->contacts_id] && $contacts[$l->contacts_id]->parent_phone){
						echo $contacts[$l->contacts_id]->parent_phone;
						$show_old_data = false;
					}
				}
				if($student && $show_old_data && $student->parents&& $student->parents[0]&& $student->parents[0]->parent_phone){
					echo $student->parents[0]->parent_phone;
					$show_old_data = false;
				}
				if($show_old_data){
					if(is_array($shipping_info)) {
						foreach($shipping_info as $s) {
							echo ' '.$s->tel;
						}
					} else {
						//兼容旧数据
						echo $shipping_info->tel;
					}
				}
				?>
			</td>

			<td>
				<?php
				$show_old_data = true;
				if($student){
					if(!$grades[$student->grade_id]){
						$grades[$student->grade_id] = Grade::model()->findByPk($student->grade_id);
					}
					if($grades[$student->grade_id] && $grades[$student->grade_id]->name){
						echo $grades[$student->grade_id]->name;
						$show_old_data = false;
					}
					else{
					}
				}
				if($show_old_data){
					if(is_array($shipping_info)) {
						foreach($shipping_info as $s) {
							echo ' '.$s->student_grade;
						}
					} else {
						//兼容旧数据
						echo $shipping_info->student_grade;
					}
				}
				?>
			</td>
			<td>
				<?php
				if($student){
					echo CommonHelper::age($student->birth).'岁';
				}
				else{
					if(is_array($shipping_info)) {
						foreach($shipping_info as $s) {
							echo ' '.CommonHelper::age($s->birth).'岁';
						}
					} else {
						//兼容旧数据
						echo CommonHelper::age($shipping_info->birth).'岁';
					}
				}
				?>
			</td>
			<td><?php echo $l->payment_transaction_id ?></td>
			<td><?php echo date('y/m/d H:i:s', $l->begin_time) ?></td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>


<script>
	$(function(){
		$('#export').click(function(){
			var from = $(this).parent();
			var btn = $(this);
			//btn.button('loading');

			from.find('input[name="export"]').val(1);
			from.submit();

			from.find('input[name="export"]').val('');
//			setTimeout(function(){
//				btn.button('reset');
//			}, 10000);
		})
	})
</script>