<h3 class="content-title">学生列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/student') ?>" method="get">
	<input type="hidden" name="type" value="student" />
	<div class="form-group">
		<label for="name">学生</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="学生名称" />
	</div>

	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/student') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>学校</th>
		<th>班级</th>
		<th>学生姓名</th>
		<th>学生生日</th>
		<th>家长信息</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $schools[$l->partner_id] ?></td>
			<td><?php echo $l->grade_id ? $grades[$l->grade_id] : '---' ?></td>
			<td><?php echo $l->name ?></td>
			<td><?php echo $l->birth ?></td>

			<td>
				<?php foreach($l->parents as $p):?>
					<p><?php echo $p->parent_name.' '.$p->parent_phone ?></p>
				<?php endforeach;?>
			</td>

		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>