<h3 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>分类</h3>
<hr />

<form class="validate" method="post" action="" enctype="multipart/form-data">

	<div class="form-group">
		<label for="">名称：</label>
		<input type="text" class="form-control required" name="name" placeholder="名称" value="<?php echo $model->name ?>" />
		<p class="help-block"></p>
	</div>
	<div class="form-group">
		<label for="">优先级：</label>
		<input type="text" class="form-control required number" name="sort_num" placeholder="优先级" value="<?php echo $model->sort_num ?>" />
		<p class="help-block">0-255的数字，数字越大排名越靠前</p>

	</div>

	<div class="form-group">
		<label for="">图片：</label>
		<input type="file" class="form-control" name="files" />
		<p class="help-block">图片将会展示在首页，请处理好再上传</p>
	</div>
	<?php if($model->image): ?>
		<div class="form-group">
			<label for="">原图片：</label>
			<img src="<?php echo HtmlHelper::image($model->image) ?>"
			<p class="help-block">如不修改则从不用上传</p>
		</div>
	<?php endif;?>


	<div>
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>
