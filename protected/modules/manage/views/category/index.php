<h3 class="content-title">分类列表</h3>
<hr />


<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>名称</th>
		<th>图片</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->name ?></td>
			<td><img src="<?php echo HtmlHelper::image($l->image) ?>" width="200px;" /></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/category/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/category/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>


<a href="<?php echo $this->createUrl('/manage/category/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加</a>
