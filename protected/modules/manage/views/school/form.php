<?php if($this->layout == 'manageSchoolPartner'): ?>
<div class="row">
	<div class="col-md-2 sidebar-2-parent">
		<ul class="nav nav-pills nav-stacked sidebar-2">

			<h4 class="content-title text-center">学校管理</h4>
			<hr />

			<li role="presentation">
				<a href="<?php echo $this->createUrl('/manage/school') ?>">学校列表</a>
			</li>
			<li role="presentation" class="active">
				<a href="<?php echo $this->createUrl('/manage/school/create') ?>"><?php echo $model->id ? '编辑' : '添加'; ?>学校</a>
			</li>
			<li role="presentation">
				<a href="<?php echo $this->createUrl('/manage/school/apply') ?>">学校申请</a>
			</li>
		</ul>
	</div>

	<div class="col-md-10">

		<h4 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>学校</h4>
		<hr />
<?php else: ?>
		<h3 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>学校</h3>
		<hr />
<?php endif;?>
		<form class="validate" method="post" action="">

			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="">校名：<span class="text-danger">*</span></label>
						<input type="text" class="form-control required" name="name" placeholder="校名" value="<?php echo $model->name ?>" />
						<p class="help-block"></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">联系人：</label>
						<input type="text" class="form-control" name="contacts" placeholder="联系人" value="<?php echo $model->contacts ?>" />
						<p class="help-block"></p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">联系电话：</label>
						<input type="text" class="form-control" name="tel" placeholder="联系电话" value="<?php echo $model->tel ?>" />
						<p class="help-block"></p>
					</div>
				</div>
			</div>

			<h4>微信支付设置</h4>
			<div class="form-group checkbox">
				<label style="padding: 0">收款方：</label>
				<label class="radio-inline payment" data-type="system">
					<input type="radio" name="pay_to" id="" value="system"  <?php if($model->pay_to == 'system') echo 'checked="checked"' ?>/> 系统企业号
				</label>
				<label class="radio-inline payment" data-type="system_fuwu">
					<input type="radio" name="pay_to" id="inlineRadio3" value="system_fuwu" <?php if($model->pay_to == 'system_fuwu') echo 'checked="checked"' ?> /> 系统服务号
				</label>
				<label class="radio-inline payment" data-type="partner">
					<input type="radio" name="pay_to" id="inlineRadio2" value="partner" <?php if($model->pay_to == 'partner') echo 'checked="checked"' ?> /> 学校微信帐户
				</label>
				<label class="radio-inline payment" data-type="none">
					<input type="radio" name="pay_to" id="inlineRadio4" value="none" <?php if($model->pay_to != 'partner' &&$model->pay_to != 'system_fuwu' &&$model->pay_to != 'system') echo 'checked="checked"' ?> /> 暂不开放
				</label>
			</div>

			<div id="payment-config" <?php if($model->pay_to != 'partner') echo 'style="display:none"' ?>>
				<hr />

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="">App id：<span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="app_id" placeholder="App id" value="<?php echo $wx_payment->app_id ?>" />
							<p class="help-block"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">App secret：<span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="app_secret" placeholder="App secret" value="<?php echo $wx_payment->app_secret ?>" />
							<p class="help-block"></p>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="">key：<span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="key" placeholder="key" value="<?php echo $wx_payment->key ?>" />
							<p class="help-block"></p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="">mchid：<span class="text-danger">*</span></label>
							<input type="text" class="form-control required" name="mchid" placeholder="mchid" value="<?php echo $wx_payment->mchid ?>" />
							<p class="help-block"></p>
						</div>
					</div>

				</div>

				<p>需在微信公众号的“微信支付”菜单中添加支付授权目录 “<?php echo Yii::app()->params['mobile_base'].'/merchant/' ?>”,<?php echo Yii::app()->params['mobile_base'].'/school/' ?></p>
			</div>




			<br />
			<div>
				<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
				<br />
				<br />
				<br />
				<div class="clearfix"></div>
			</div>

		</form>
<?php if($this->layout == 'manageSchoolPartner'): ?>
	</div>
</div>
<?php endif;?>

<script>
	$(function(){
		$('.payment').click(function(){
			var type = $(this).attr('data-type');
			if(type == 'partner') {
				$('#payment-config').show();
			} else {
				$('#payment-config').hide();
			}
		})
	})
</script>
