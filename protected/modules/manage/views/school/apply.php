<?php if($this->layout == 'manageSchoolPartner'): ?>
<div class="row">

	<div class="col-md-2 sidebar-2-parent">
		<ul class="nav nav-pills nav-stacked sidebar-2">

			<h4 class="content-title text-center">学校管理</h4>
			<hr />

			<li role="presentation">
				<a href="<?php echo $this->createUrl('/manage/school') ?>">学校列表</a>
			</li>
			<li role="presentation">
				<a href="<?php echo $this->createUrl('/manage/school/create') ?>"><?php echo $model->id ? '编辑' : '添加'; ?>学校</a>
			</li>
			<li role="presentation"  class="active">
				<a href="<?php echo $this->createUrl('/manage/school/apply') ?>">学校申请</a>
			</li>
		</ul>
	</div>

	<div class="col-md-10">

		<h4 class="content-title">学校申请列表</h4>
		<hr />
<?php else: ?>
		<h3 class="content-title">学校申请列表</h3>
		<hr />
<?php endif;?>
		<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/school/apply') ?>" method="get">
			<div class="form-group">
				<label for="name">学校名称</label>
				<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="学校名称" />
			</div>
			<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
			<a href="<?php echo $this->createUrl('/manage/school/apply') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
		</form>

		<br />
		<br />

		<table class="table table-bordered table-hover ">
			<thead>
			<tr>
				<th>名称</th>
				<th>联系人</th>
				<th>企业号相关</th>
				<th>电话</th>
				<th>状态</th>
				<th>申请时间</th>
				<th>操作</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($list as $l):?>
				<tr>
					<td><?php echo CHtml::encode($l->name) ?></td>
					<td><?php echo CHtml::encode($l->contacts) ?></td>
					<td>
						<?php
						$extra = $l->extra?(object)CJSON::decode($l->extra):new stdClass();
						//var_dump($extra,$l->extra);
						if($extra->IDCard){
							echo '身份证：'.$extra->IDCard.'<br>';
						}
						if($l->email){
							echo '邮箱：'.$l->email.'<br>';
						}
						if($l->wechat_id){
							echo '微信：'.$l->wechat_id.'<br>';
						}
						if($extra->shenfengzheng){
							echo '<a href="'.HtmlHelper::image($extra->shenfengzheng) .'" target="_blank" >身份证扫描件</a>&nbsp;&nbsp;&nbsp;&nbsp;';
						}
						if($extra->zhuzhijigou){
							echo '<a href="'.HtmlHelper::image($extra->zhuzhijigou) .'" target="_blank" >组织机构扫描件</a>';
						}
						//echo $l->contacts;
						?>
					</td>
					<td><?php echo CHtml::encode($l->tel) ?></td>

					<td><?php
						if($l->status == 1) {
							echo '<span class="label label-success">审核成功</span>';
						} else if($l->status == 2) {
							echo '<span class="label label-danger">已拒绝</span><p style="padding-top:10px;">原因：'. $l->reason .'</p>';
						} else {
							echo '<span class="label label-default">等待审核</span>';
						}
						?></td>
					<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
					<td>
						<?php if($l->status == 0):?>
							<a href="<?php echo $this->createUrl('/manage/school/applyStatus?action=approve&id='.$l->id); ?>" class="btn btn-xs btn-success"><i class="fa fa-check"></i> 通过审核</a>
							<a href="javascript:;" data-id="<?php echo $l->id ?>" class="btn btn-xs btn-danger reject"><i class="fa fa-ban"></i> 拒绝</a>
						<?php endif;?>

						<?php if($l->status != 1):?>
							<a href="<?php echo $this->createUrl('/manage/school/applyStatus?action=delete&id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-info"><i class="fa fa-trash"></i> 删除</a>

						<?php endif;?>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>

		<nav>
			<?php
			$this->widget('CLinkPager',array(
					'header'=>'',
					'firstPageLabel' => '首页',
					'lastPageLabel' => '末页',
					'prevPageLabel' => '上一页',
					'nextPageLabel' => '下一页',
					'pages' => $pager,
					'cssFile'=>false,
					'maxButtonCount'=>5,
					'selectedPageCssClass' => 'active',
					'hiddenPageCssClass' => 'disabled',
					'htmlOptions' => array(
						'class' => 'pagination'
					),
				)
			);
			?>
		</nav>

		<!-- Modal -->
		<div class="modal fade" id="modal-reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">拒绝申请</h4>
					</div>
					<div class="modal-body">
						<form action="<?php echo $this->createUrl('/manage/school/applyStatus'); ?>" method="get">
							<input type="hidden" name="action" value="reject" />
							<input type="hidden" name="id" value="" />
							<div class="form-group">
								<label>拒绝理由：</label>
								<textarea name="reason" class="form-control" rows="2"></textarea>
							</div>

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
						<button type="button" class="btn btn-primary" id="submit">确定</button>
					</div>
				</div>
			</div>
		</div>

<?php if($this->layout == 'manageSchoolPartner'): ?>
	</div>
</div>
<?php endif;?>

<script>
	var url = '<?php echo $this->createUrl('/manage/school/applyStatus?action=reject&id='); ?>';
	$(function(){
		var modal_obj = $('#modal-reason');
		$('.reject').click(function(){
			console.log(modal_obj);
			modal_obj.find('input[name="id"]').val($(this).attr('data-id'));
			modal_obj.find('textarea').val('');
			modal_obj.modal('show');
		});

		$('#submit').click(function(){
			if(!modal_obj.find('textarea').val()) {
				alert('拒绝理由不能为空');
				return false;
			}

			if(confirm('你确定要拒绝此申请吗?')) {
				modal_obj.find('form').submit();
			}
		})
	})
</script>