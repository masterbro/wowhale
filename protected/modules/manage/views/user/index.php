<h3 class="content-title">用户列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/user') ?>" method="get">
	<div class="form-group">
		<label for="parent_name">家长姓名</label>
		<input type="text" class="form-control" id="parent_name" value="<?php echo urldecode($_GET['parent_name']) ?>" name="parent_name" placeholder="家长姓名" />
	</div>
	<div class="form-group">
		<label for="student_name">学生姓名</label>
		<input type="text" class="form-control" id="student_name" value="<?php echo urldecode($_GET['student_name']) ?>" name="student_name" placeholder="学生姓名" />
	</div>
	<div class="form-group">
		<label for="name">供应商</label>
		<select class="form-control" name="school">
			<option>所有</option>
			<?php foreach($partners as $sk=>$sv):?>
				<option value="<?php echo $sk ?>" <?php if($_GET['school'] == $sk) echo 'selected="selected"' ?>><?php echo $sv ?></option>
			<?php endforeach;?>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/user') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th>家长姓名</th>
		<th>学生姓名</th>
		<th>供应商</th>
		<th>联系电话</th>
		<th>生日</th>
		<th>总消费额</th>
		<th>最后消费时间</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->parent_name ?></td>
			<td><?php echo $l->student_name ?></td>
			<td><?php echo $l->from_partner ? $partners[$l->from_partner] : '---' ?></td>
			<td><?php echo $l->tel ?></td>
			<td><?php echo $l->birth ?></td>
			<td><?php echo '￥'.CommonHelper::price($l->consume_total);  ?></td>
			<td><?php echo $l->last_consume_time ? date('Y-m-d H:i:s', $l->last_consume_time) : '未消费'; ?></td>

		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>