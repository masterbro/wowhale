
<h3 class="content-title">个人设置</h3>
<br />
<?php
$form = $this->beginWidget('CActiveForm', array(
	'htmlOptions' => array('class'=> 'validate'),
));
?>
<div class="form-group">
	<label for="">真实姓名</label>
	<input type="text" class="form-control hr-mi" name="realname" placeholder="真实姓名" value="<?php echo $model->realname ?>" />
	<p class="help-block"></p>
</div>
<div class="form-group">
	<label for="">原登录密码</label>
	<input type="password" class="form-control hr-mi" id="" name="old_password" placeholder="原登录密码" value="" />
	<p class="help-block"></p>
</div>
<div class="form-group">
	<label for="">登录密码</label>
	<input type="password" class="form-control hr-mi" id="password" name="password" placeholder="登录密码" value="" />
	<p class="help-block">如不修改则为空</p>
</div>
<div class="form-group">
	<label for="">重复登录密码</label>
	<input type="password" class="form-control hr-mi" equalTo="#password" name="password_confirm" placeholder="重复登录密码" value="" />
	<p class="help-block"></p>
</div>



<button type="button" class="btn btn-success btn-block hr-save  hr-mi" data-callback="success()" data-loading-text="<i class='fa fa-spin fa-spinner'></i> 保存中..."><i class="fa fa-save"></i> 保存</button>

<?php $this->endWidget(); ?>


<script>
	function success() {
		window.location.reload();
	}
</script>
