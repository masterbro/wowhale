<h3 class="content-title">欢迎回来！</h3>
<br />

<?php if($partner->type==1 ):?>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">个人信息</div>
			<div class="panel-body">
				<table class="table" style="margin-bottom: 0px;">
					<tr>
						<td class="info-label no-border">真实姓名</td>
						<td class=" no-border"><?php echo $user->realname ?></td>
					</tr>
					<tr>
						<td class="info-label">联系电话</td>
						<td><?php echo $user->tel ?></td>
					</tr>
					<?php if(Yii::app()->user->getState('type') == 1): ?>
					<tr>
						<td class="info-label">学校</td>
						<td><?php echo Yii::app()->user->getState('school') ?></td>
					</tr>
					<?php else:?>
					<tr>
						<td class="info-label">角色</td>
						<td><?php echo $user->role_name ?></td>
					</tr>
					<?php endif;?>
					<tr>
						<td class="info-label">上次登录</td>
						<?php $last_login = Yii::app()->user->getState('last_login');?>
						<td><?php echo $last_login ? date("Y-m-d H:i:s", $last_login) : '首次登录'; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
    <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">访问统计(每五分钟更新一次)</div>
                <div class="panel-body">
                    <table class="table" style="margin-bottom: 0px;">
                        <tr>
                            <td class="info-label no-border">今日访问人数</td>
                            <td class=" no-border"><?php echo $page_stat['today_people'] ?></td>
                        </tr>
                        <tr>
                            <td class="info-label">今日访问次数</td>
                            <td><?php echo $page_stat['today_times'] ?></td>
                        </tr>
                        <tr>
                            <td class="info-label">总访问人数</td>
                            <td><?php echo $page_stat['people'] ?></td>
                        </tr>
                        <tr>
                            <td class="info-label">总访问次数</td>
                            <td><?php echo $page_stat['times'] ?></td>
                        </tr>
                    </table>
                </div>
            </div>
    </div>

</div>
<!--
<div class="row">

    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">订单信息(每五分钟更新一次)</div>
            <div class="panel-body">
                <table class="table" style="margin-bottom: 0px;">
                    <tr>
                        <td class="info-label no-border">今日订单数</td>
                        <td class=" no-border"><?php echo $stat['today_qty'] ?></td>
                    </tr>
                    <tr>
                        <td class="info-label">今日销售额</td>
                        <td><?php echo CommonHelper::price($stat['today_total']) ?></td>
                    </tr>
                    <tr>
                        <td class="info-label">总订单数</td>
                        <td><?php echo $stat['qty'] ?></td>
                    </tr>
                    <tr>
                        <td class="info-label">总销售额</td>
                        <td><?php echo CommonHelper::price($stat['total']) ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
	<div class="col-md-6">

	</div>
</div>
-->
<?php else:?>
    <script src="http://cdn.hcharts.cn/highcharts/highcharts.js"></script>
    <style>
        svg text:last-child {
            display: none;
        }
        svg g text:last-child {
            display: inherit;
        }
    </style>
    <div class="">
        <style>
            .center{text-align: center}
        </style>
        <?php $sep = $user->partner_id?1:3;?>
        <div class="panel panel-default">
            <div class="panel-heading">昨日关键数据</div>
            <div class="panel-body">
                <table class="table" style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th class="center">总人数</th>
                        <th class="center">总关注数</th>
                        <th class="center">昨日进入应用人数</th>
                        <th class="center">昨日进入应用次数</th>
                    </tr>
                    </thead>
                    <tr>
                        <td class=" center no-border"><?php echo CommonHelper::number($app_stat_yes['contacts'],$sep); ?></td>
                        <td class=" center no-border"><?php echo CommonHelper::number($app_stat_yes['subscribed'],$sep); ?></td>
                        <td class=" center no-border"><?php echo CommonHelper::number($app_stat_yes['today_people'],$sep); ?></td>
                        <td class=" center no-border"><?php echo CommonHelper::number($app_stat_yes['today_times'],$sep); ?></td>
                    </tr>
                </table>
            </div>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-8">企业号活跃度</div>
                    <div class="col-md-4 col-xs-12">
                        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%;text-align: right">
                            <i class="glyphicon glyphicon-calendar fa fa-calendar" style="float: left;"></i>&nbsp;
                            <span><?php echo $start . ' 到 ' . $end;?></span> <b class="caret"></b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="container" style="width:100%; height:400px;"></div>
                <!--<table class="table" style="margin-bottom: 0px;">
                    <thead>
                    <tr>
                        <th class="center">总人数</th>
                        <th class="center">昨日进入应用人数</th>
                        <th class="center">昨日进入应用次数</th>
                    </tr>
                    </thead>
                    <?php foreach($app_stats as $k=>$v):?>
                    <tr>
                        <td class=" center no-border"><?php echo $k; ?></td>
                        <td class=" center no-border"><?php echo CommonHelper::number($v['today_people']); ?></td>
                        <td class=" center no-border"><?php echo CommonHelper::number($v['today_times']); ?></td>
                    </tr>
                    <?php endforeach;?>
                </table>-->
            </div>
            <div class="panel-heading">应用活跃度</div>
                <div class="panel-body">
                <table class="table" style="margin-bottom: 0px;">
                <?php $apps = CommonHelper::qyApp($this->partner_id);?>
                    <thead>
                    <tr>
                        <th class="center">应用</th>
                        <th class="center">昨日进入应用人数</th>
                        <th class="center">昨日进入应用次数</th>
                        <th class="center">总关注人数</th>
                    </tr>
                    </thead>
                    <?php foreach($apps  as $k=>$v):?>
                    <tr>
                        <td class=" center "><?php echo $v['name'] ?></td>
                        <td class=" center "><?php echo CommonHelper::number($app_stat_yes['today_people_app'][$k],$sep); ?></td>
                        <td class=" center "><?php echo CommonHelper::number($app_stat_yes['today_times_app'][$k],$sep); ?></td>
                        <td class=" center "><?php echo CommonHelper::number($app_stat_yes['subscribed_app'][$k],$sep); ?></td>
                    </tr>
                    <?php endforeach;?>

                </table>
            </div>
        </div>

    </div>
    <script>
        var _categories = '<?php echo CJSON::encode($titles);?>';
        try{
            _categories = JSON.parse(_categories);
        }
        catch (E){
            _categories = [];
        }
        var _peoples = '<?php echo CJSON::encode($peoples);?>';
        try{
            _peoples = JSON.parse(_peoples);
        }
        catch (E){
            _peoples = [];

        }
        var _times = '<?php echo CJSON::encode($times);?>';
        try{
            _times = JSON.parse(_times);
        }
        catch (E){
            _times = [];
        }
        $(function(){

            $('#container').highcharts({
                chart: {
                    type: 'area'
                },
                title: {
                    text: '趋势图'
                },
                xAxis: {
                    categories: _categories
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    min:0
                },
                tooltip: {
                    //pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f} millions)<br/>',
                    shared: true
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                                /*[0, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(1).get('rgba')],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.5).get('rgba')],*/
                                //[1,'rgba(74,144,226,0.1)'],
                                [0,'rgba(127,184,135,0.1)'],
                                [1,'rgba(74,144,226,0.1)']
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 2
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                    name: '应用进入人数',
                    data: _peoples,
                    color:'#4A90E2'
                }, {
                    name: '应用进入次数',
                    data: _times,
                    color:'#7FB887'
                }]
            });
        })
    </script>
<?php endif;?>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/moment.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/daterangepicker.js"></script>
<link rel='stylesheet' href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/daterangepicker.css"/>
<script>
    var showDate =  function(start, end, label) {
        $('#reportrange span').html(start.format('YYYY-MM-DD') + ' 到 ' + end.format('YYYY-MM-DD'));
        document.location.search = '?start='+start.format('YYYY-MM-DD')+'&end='+end.format('YYYY-MM-DD');
    }
    var _options = {
        //"showWeekNumbers": true,
        "autoApply": true,
        "opens": "left",
        "maxDate":new Date(),
        /*"dateLimit": {
            "days": 7
        },*/
        "locale": {
            "daysOfWeek": [
                "日",
                "一",
                "二",
                "三",
                "四",
                "五",
                "六"
            ],
            "monthNames": [
                "一月",
                "二月",
                "三月",
                "四月",
                "五月",
                "六月",
                "七月",
                "八月",
                "九月",
                "十月",
                "十一月",
                "十二月"
            ]
        }};
    function dateRangePickerInit(start,end,_option){
        _option.startDate = start;
        _option.endDate = end;
        $('#reportrange').daterangepicker(_option,showDate);
    }
    $(function(){
        dateRangePickerInit(new Date('<?php echo $start;?>'),new Date('<?php echo $end;?>'),_options)
    })
</script>
<script>
    //更改侧边栏的激活状态的图标
    $('.sidebar #app-center').removeClass('active');
    $('.sidebar #school-notice').removeClass('active');
    $('.sidebar #mail-list').removeClass('active');
</script>
