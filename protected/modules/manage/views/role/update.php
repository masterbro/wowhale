<section class="panel">
	<header class="panel-heading"><i class="fa fa-lock"></i> 角色编辑</header>
	<div class="panel-body">

<?php echo $this->renderPartial('_form', array('model' => $model, 'operations' => $operations, 'item_operations' => array()));?>

</div>
</section>
