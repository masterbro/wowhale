<section class="panel">
	<header class="panel-heading"><i class="fa fa-lock"></i> 权限管理</header>
	<div class="panel-body">

<div role="tabpanel">

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#list" aria-controls="list" role="tab" data-toggle="tab">角色列表</a></li>
		<li role="presentation"><a href="#create" aria-controls="create" role="tab" data-toggle="tab">添加角色</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content" style="padding-top: 30px;">
		<div role="tabpanel" class="tab-pane active" id="list">
			<table class="table table-hover ">
				<thead>
				<tr>
					<!--				<th style="width: 30px;"><input type="checkbox" /></th>-->
					<th style="width: 80%;">角色名称</th>
<!--					<th style="width: 60%;">权限</th>-->
					<th>操作</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($roles as $r):?>
				<tr>
					<td><?php echo $r->description;?></td>
<!--					<td>--><?php
//						$children = $r->getAuthManager()->getItemChildren($r->name);
//						foreach($children as $c) {
//							echo $c->description.' ';
//						}
//						?>
<!--					</td>-->
					<td>
						<a href="<?php echo $this->createUrl('/manage/role/update?id='.$r->name) ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
						<a href="<?php echo $this->createUrl('/manage/role/delete?id='.$r->name) ?>" class="btn btn-xs btn-danger" onclick="return confirm('你确定要删除吗？删除后当前角色下的管理员将没有任何权限！')"><i class="fa fa-trash"></i> 删除</a>
					</td>
				</tr>
				<?php endforeach;?>

				</tbody>
			</table>

		</div>
		<div role="tabpanel" class="tab-pane" id="create">
			<?php echo $this->renderPartial('_form', array('model' => new stdClass(), 'operations' => $operations, 'item_operations' => array()));?>
		</div>
	</div>

</div>
</div>
</section>