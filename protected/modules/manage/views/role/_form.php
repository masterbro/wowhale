<?php
	$form = $this->beginWidget('CActiveForm', array(
		'htmlOptions' => array('class'=> 'validate'),
		'action' => $model->name ? $this->createUrl('/manage/role/update?id='.$model->name) : $this->createUrl('/manage/role/create')
	));
?>
	<div class="form-group">
		<label for="">角色名称</label>
		<input type="text" class="form-control hr-si required" name="description" placeholder="角色名称" value="<?php echo $model->description ?>" />
		<p class="help-block"></p>
	</div>

	<div class="form-group">
<!--		<label for="">权限</label>-->
		<br />
		<?php
			$my_ops = array();
			if($model->name) {
				$ops = $model->getAuthManager()->getItemChildren($model->name);

				foreach($ops as $o)
					$my_ops[] = $o->name;
			}

		?>
		<?php foreach($operations as $o):?>

<!--		<label class="checkbox-inline">-->
<!--			<input type="checkbox" --><?php //if(in_array($o->name, $my_ops)) echo 'checked="checked"' ?><!-- class="required" name="operations[]" value="--><?php //echo $o->name ?><!--" /> --><?php //echo $o->description ?>
<!--		</label>-->

		<?php endforeach;?>

		<p class="help-block"></p>
	</div>
<?php foreach(PermissionsHelper::rules() as $p):?>
	<div class="form-group">
		<h5 class="text-bold"><?php echo $p['name'];?></h5>
		<?php foreach($p['rule'] as $rk=>$rv):?>
		<label class="checkbox-inline">
			<input type="checkbox" <?php if(in_array(strtolower($rk), $my_ops)) echo 'checked="checked"' ?> class="required" name="operations[]" value="<?php echo strtolower($rk) ?>" /> <?php echo $rv ?>
		</label>
		<?php endforeach;?>
		<hr />
	</div>

<?php endforeach;?>


	<button type="button" class="btn btn-success hr-save" data-callback="roleSuccess()" data-loading-text="保存中..."><i class="fa fa-save"></i> 保存</button>
<?php $this->endWidget(); ?>
<script>
	function roleSuccess() {
		window.location.href = BASE_URL + '/manage/role';
	}
</script>