<h3 class="content-title">文章管理</h3>
<hr />


<br />
<br />

<table class="table table-bordered table-striped">
	<thead>
	<tr>
		<th style="width: 80%">名称</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo Page::$page_title[$l->key] ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/page/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>
