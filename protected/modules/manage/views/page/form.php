<h3 class="content-title"><?php echo Page::$page_title[$model->key] ?> 编辑</h3>
<hr />

<form class="validate" method="post" action="" enctype="multipart/form-data">

	<div class="form-group">
		<label for="">文章内容：</label>
		<textarea name="content" id="ueditor" style="width: 100%" rows="20"><?php echo $model->content; ?></textarea>
		<p class="help-block"></p>
	</div>


	<div>
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>


<script>
	var editor;
	KindEditor.ready(function(K) {
		editor = K.create('textarea[name="content"]', {
			resizeType : 1,
			allowPreviewEmoticons : false,
			uploadJson : window.kindEditorBase+'/php/upload_json.php',
			fileManagerJson : window.kindEditorBase+'/php/file_manager_json.php',
			allowFileManager : true,
			items : [
				'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'baidumap', 'image', 'link', 'fullscreen', 'source', 'undo', 'redo']
		});
	});
</script>