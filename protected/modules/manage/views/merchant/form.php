<h3 class="content-title"><?php echo $model->id ? '编辑' : '添加'; ?>商家</h3>
<hr />

<form class="validate" method="post" action="">

	<div class="form-group">
		<label for="">名称：<span class="text-danger">*</span></label>
		<input type="text" class="form-control required" name="name" placeholder="名称" value="<?php echo $model->name ?>" />
		<p class="help-block"></p>
	</div>

	<div class="form-group">
		<label for="">联系人：</label>
		<input type="text" class="form-control" name="contacts" placeholder="联系人" value="<?php echo $model->contacts ?>" />
		<p class="help-block"></p>
	</div>

	<div class="form-group">
		<label for="">联系人电话：</label>
		<input type="text" class="form-control" name="tel" placeholder="联系人电话" value="<?php echo $model->tel ?>" />
		<p class="help-block"></p>
	</div>

	<div>
		<button class="btn btn-primary btn-lg" style="width: 200px;" type="submit">保存</button>
		<br />
		<br />
		<br />
		<div class="clearfix"></div>
	</div>

</form>
