<h3 class="content-title">商家申请列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/merchant/apply') ?>" method="get">
	<div class="form-group">
		<label for="name">商家名称</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="商家名称" />
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/merchant/apply') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-hover ">
	<thead>
	<tr>
		<th>名称</th>
		<th>联系人</th>
		<th>电话/邮箱</th>
		<th>其它</th>
		<th>状态</th>
		<th>申请时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo CHtml::encode($l->name) ?> <a href="<?php echo HtmlHelper::image($l->license) ?>" target="_blank">查看营业执照</a> </td>
			<td><?php echo CHtml::encode($l->contacts) ?></td>
			<td><?php echo CHtml::encode($l->tel) ?><br /><?php echo CHtml::encode($l->email) ?></td>
			<td>
				<p>业务描述：<?php echo CHtml::encode($l->description) ?></p>
				<p>网址：<?php echo $l->website ? CHtml::encode($l->website) : '---'?></p>
				<p>公众号：<?php echo $l->wechat_id ? CHtml::encode($l->wechat_id) : '---' ?></p>
			</td>
			<td><?php
				if($l->status == 1) {
					echo '<span class="label label-success">审核成功</span>';
				} else if($l->status == 2) {
					echo '<span class="label label-danger">已拒绝</span><p style="padding-top:10px;">原因：'. $l->reason .'</p>';
				} else {
					echo '<span class="label label-default">等待审核</span>';
				}
				?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<?php if($l->status == 0):?>
					<a href="<?php echo $this->createUrl('/manage/merchant/applyStatus?action=approve&id='.$l->id); ?>" class="btn btn-xs btn-success"><i class="fa fa-check"></i> 通过审核</a>
					<a href="javascript:;" data-id="<?php echo $l->id ?>" class="btn btn-xs btn-danger reject"><i class="fa fa-ban"></i> 拒绝</a>
				<?php endif;?>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>

<!-- Modal -->
<div class="modal fade" id="modal-reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">拒绝申请</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo $this->createUrl('/manage/merchant/applyStatus'); ?>" method="get">
					<input type="hidden" name="action" value="reject" />
					<input type="hidden" name="id" value="" />
					<div class="form-group">
						<label>拒绝理由：</label>
						<textarea name="reason" class="form-control" rows="2"></textarea>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="button" class="btn btn-primary" id="submit">确定</button>
			</div>
		</div>
	</div>
</div>


<script>
	var url = '<?php echo $this->createUrl('/manage/merchant/applyStatus?action=reject&id='); ?>';
	$(function(){
		var modal_obj = $('#modal-reason');
		$('.reject').click(function(){
			console.log(modal_obj);
			modal_obj.find('input[name="id"]').val($(this).attr('data-id'));
			modal_obj.find('textarea').val('');
			modal_obj.modal('show');
		});

		$('#submit').click(function(){
			if(!modal_obj.find('textarea').val()) {
				alert('拒绝理由不能为空');
				return false;
			}

			if(confirm('你确定要拒绝此申请吗?')) {
				modal_obj.find('form').submit();
			}
		})
	})
</script>