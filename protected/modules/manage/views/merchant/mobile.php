<h3 class="content-title">商家页面管理</h3>

<hr />

<p><?php echo $model->name; ?>   <a href="#" class="btn btn-xs btn-info" id="show-qrcode"><i class="fa fa-mobile-phone"></i> 查看效果</a>
</p>
<?php $slider = $model->sliderArr();$hidden_fields = ''; ?>


		<form class="validate" method="post" action="" id="ww-school-page-form">
			<?php echo $hidden_fields;?>
<!--			<div class="form-group">-->
<!--				<label for="">标题：<span class="text-danger">*</span></label>-->
<!--				<input type="text" class="form-control required" name="title" placeholder="标题" value="--><?php //echo $model->title ?><!--" />-->
<!--				<p class="help-block"></p>-->
<!--			</div>-->
			<div class="form-group">
				<label for="">联系电话：<span class="text-danger">*</span></label>
				<input type="text" class="form-control required" name="contact_tel" placeholder="联系电话" value="<?php echo $model->contact_tel ?>" />
				<p class="help-block"></p>
			</div>
			<button class="btn btn-block btn-success" type="submit"><i class="fa fa-save"></i> 保存</button>

		</form>



<!-- Modal -->
<div class="modal fade" id="modal-qrcode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">使用微信扫一扫</h4>
			</div>
			<div class="modal-body text-center">
				<p>
					<img src="<?php echo $this->createUrl('/qrCode?data='.urlencode($model->mobileUrl())) ?>" />
				</p>
				<p class="well text-left">网址：<?php echo $model->mobileUrl(); ?></p>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
		$('#show-qrcode').click(function(){
			$('#modal-qrcode').modal();
		})
	})
</script>