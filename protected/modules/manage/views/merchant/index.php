<h3 class="content-title">商家列表</h3>
<hr />

<form class="form-inline yl-form-search" action="<?php echo $this->createUrl('/manage/merchant') ?>" method="get">
	<div class="form-group">
		<label for="name">商家名称</label>
		<input type="text" class="form-control" id="name" value="<?php echo urldecode($_GET['name']) ?>" name="name" placeholder="商家名称" />
	</div>
	<div class="form-group">
		<label for="name">状态</label>
		<select class="form-control" name="st">
			<option>所有</option>
			<option value="normal" <?php if($_GET['st'] == 'normal') echo 'selected="selected"' ?>>正常</option>
			<option value="deleted" <?php if($_GET['st'] == 'deleted') echo 'selected="selected"' ?>>已删除</option>
		</select>
	</div>
	<button type="submit" class="btn btn-info"><i class="fa fa-search"></i> 搜索</button>
	<a href="<?php echo $this->createUrl('/manage/merchant') ?>" class="btn btn-default"><i class="fa fa-refresh"></i> 重置</a>
</form>

<br />
<br />

<table class="table table-bordered table-hover ">
	<thead>
	<tr>
		<th>名称</th>
		<th>联系人</th>
		<th>电话</th>
		<th>状态</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($list as $l):?>
		<tr>
			<td><?php echo $l->name ?></td>
			<td><?php echo $l->contacts ?></td>
			<td><?php echo $l->tel ?></td>
			<td><?php
				if($l->is_deleted) {
					echo '<span class="label label-danger">已删除</span>';
				} else {
					echo '<span class="label label-success">正常</span>';
				}
				?></td>
			<td><?php echo date('Y-m-d H:i:s', $l->create_time) ?></td>
			<td>
				<a href="<?php echo $this->createUrl('/manage/merchant/update?id='.$l->id); ?>" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> 编辑</a>
				<a href="<?php echo $this->createUrl('/manage/merchant/mobile?id='.$l->id); ?>" class="btn btn-xs btn-info" class=""><i class="fa fa-mobile"></i>  手机页面</a>
				<?php if($l->is_deleted):?>
					<a href="<?php echo $this->createUrl('/manage/merchant/delete?id='.$l->id); ?>" onclick="return confirm('你确定要恢复吗?')" class="btn btn-xs btn-success"><i class="fa fa-recycle"></i> 恢复</a>
				<?php else:?>
					<a href="<?php echo $this->createUrl('/manage/merchant/delete?id='.$l->id); ?>" onclick="return confirm('你确定要删除吗?')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> 删除</a>
				<?php endif;?>

				<div class="dropdown" style="display: inline-block">
					<button id="school-dLabel<?php echo $l->id ?>" class="btn btn-xs btn-default" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						更多
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="school-dLabel<?php echo $l->id ?>">
						<li>
							<a href="<?php echo $this->createUrl('/manage/order?school='.$l->id); ?>" class=""><i class="fa fa-shopping-cart"></i>  订单列表</a>
						</li>
						<li>
							<a href="<?php echo $this->createUrl('/manage/product?school='.$l->id); ?>" class=""><i class="fa fa-credit-card"></i>  产品列表</a>
						</li>
					</ul>
				</div>
			</td>
		</tr>
	<?php endforeach;?>
	</tbody>
</table>

<a href="<?php echo $this->createUrl('/manage/merchant/create'); ?>" class="btn btn-primary btn-sm" style="width: 100px;"><i class="fa fa-plus"></i> 添加商家</a>


<nav>
	<?php
	$this->widget('CLinkPager',array(
			'header'=>'',
			'firstPageLabel' => '首页',
			'lastPageLabel' => '末页',
			'prevPageLabel' => '上一页',
			'nextPageLabel' => '下一页',
			'pages' => $pager,
			'cssFile'=>false,
			'maxButtonCount'=>5,
			'selectedPageCssClass' => 'active',
			'hiddenPageCssClass' => 'disabled',
			'htmlOptions' => array(
				'class' => 'pagination'
			),
		)
	);
	?>
</nav>