<?php
class PageController extends AdminBaseController {


	public function actionIndex() {

		$list = Page::model()->findAll(array(
			'order' => 'id DESC'
		));



		$this->render('index', array(
			'list' => $list,
		));
	}


//	public function actionCreate() {
//
//		$model = new Page();
//
//
//		if($_POST) {
//			$model->url = $_POST['url'];
//			$model->sort_num = intval($_POST['sort_num']);
//			$model->img = $img;
//			$model->save();
//
//			$this->showMsg('操作成功', 'success', 'slider/index');
//		}
//
//
//		$this->render('form', array(
//			'model' => $model,
//		));
//	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Page::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {

			$model->content = $_POST['content'];
			$model->save();


			$this->showMsg('编辑成功', 'success', 'page/index');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}
//
//	public function actionDelete($id) {
//
//		$id = intval($id);
//		if(!$id)
//			$this->showMsg('数据不存在，或者已被删除', 'error');
//
//		$model = Page::model()->findByPk($id);
//
//		if(!$model)
//			$this->showMsg('数据不存在，或者已被删除', 'error');
//
//		$model->delete();
//
//		$this->showMsg('操作成功');
//	}

}