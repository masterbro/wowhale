<?php
class BalanceController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($_GET['start'] && $_GET['to']) {
			$start = strtotime($_GET['start']);
			$end = strtotime($_GET['to']) + 86399;
			$criteria->addCondition("create_time > {$start} and create_time < {$end}");
		}

		$partner_id = intval($_GET['partner_id']);
		if($partner_id)
			$criteria->addCondition('partner_id = '.$partner_id);


		if($_GET['st'] == 'approved') {
			$criteria->addCondition('status = 2');
		} else if($_GET['st'] == 'rejected'){
			$criteria->addCondition('status = 1');
		} else if($_GET['st'] == 'pending'){
			$criteria->addCondition('status = 0');
		}


		$count = BalanceApply::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = BalanceApply::model()->findAll($criteria);

		$users = CommonHelper::getGroupField($list, Manager::model(), 'user_id', 'username');
		$manager = CommonHelper::getGroupField($list, Manager::model(), 'manager_id', 'username');


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'partners' => Partner::all(true),
			'users' => $users,
			'manager' => $manager,
		));

	}


	public function actionView($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = BalanceApply::model()->findByPk($id);
		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$partner = Partner::model()->findByPk($model->partner_id);

		$this->render('view', array(
			'model' => $model,
			'partner' => $partner,
		));
	}


	public function actionApprove() {
		$model = $this->checkApply();

		$model->manager_id = Yii::app()->user->getId();
		$model->complete_time = time();
		$model->status = BalanceApply::APPROVED;
		$model->save();

		JsonHelper::show(array(
			'error' => true,
			'msg' => '操作成功'
		));
	}


	public function actionReject() {
		$model = $this->checkApply();
		$remark = trim($_POST['remark']);
		if(!$remark)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '拒绝理由不能为空',
			));

		$connection = Yii::app()->db;
		$transaction = $connection->beginTransaction();
		try {

			$sql = "UPDATE {{balance}} SET balance = balance + {$model->money} WHERE partner_id = {$model->partner_id}";
			$connection->createCommand($sql)->execute();

			$model->manager_id = Yii::app()->user->getId();
			$model->complete_time = time();
			$model->remark = $remark;
			$model->status = BalanceApply::REJECTED;
			$model->save();

			$transaction->commit();

		} catch (Exception $e) {
			$transaction->rollBack();
			JsonHelper::show(array(
				'error' => true,
				'msg' => '操作失败请重试',
			));
		}

		JsonHelper::show(array(
			'error' => false,
			'msg' => '操作成功'
		));
	}


	private function checkApply() {
		$id = intval($_POST['id']);
		if(!$id)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除'
			));

		$model = BalanceApply::model()->findByPk($id);
		if(!$model)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除'
			));

		if($model->status)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '结算申请已经被处理'
			));

		return $model;
	}

}