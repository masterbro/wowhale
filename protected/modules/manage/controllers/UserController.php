<?php
class UserController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($_GET['school'])
			$criteria->addCondition('from_partner = '.intval($_GET['school']));

		if($_GET['student_name'])
			$criteria->addSearchCondition('student_name', urldecode($_GET['student_name']));

		if($_GET['parent_name'])
			$criteria->addSearchCondition('parent_name', urldecode($_GET['student_name']));

		$criteria->addCondition('student_name != ""');

		$count = User::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = User::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'partners' => Partner::all(),
		));
	}

}