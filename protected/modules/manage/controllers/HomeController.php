<?php
class HomeController extends AdminBaseController {
	public $partner_id = 0;

	public function actionIndex() {


		$user = Manager::model()->findByPk(Yii::app()->user->id);

		$stat = Order::stat($user->partner_id);
		$page_stat = PageTracking::stat($user->partner_id);
        try{
            $app_stat_yes = AppTracking::stat($user->partner_id,date('Ymd',time()-3600*24),date('Ymd',time()-3600*24));
            $app_stat = AppTracking::stat($user->partner_id);
            $now_time =time();
            $app_stats = array();
            $start = Yii::app()->request->getParam('start');
            $end = Yii::app()->request->getParam('end');
            if(!$start || !$end){
                $day_before = $now_time-3600*24*7;
                $start = date('Y-m-d',$day_before);
                $end = date('Y-m-d',$now_time);
            }
            else{
                $day_before = strtotime($start);
                $now_time =strtotime($end);
            }

            for($day_before;$day_before<=$now_time;$day_before=$day_before+3600*24){
                $app_stats[date('Y-m-d',$day_before)] = AppTracking::stat($user->partner_id,date('Ymd',$day_before),date('Ymd',$day_before),false);
            }
            $titles = array();
            $peoples = array();
            $times = array();
            $sep = $user->partner_id?1:3;
            foreach($app_stats as $k=>$v){
                $titles[] = $k;
                $peoples[] = (int)$v['today_people']*$sep;
                $times[] = (int)$v['today_times']*$sep;
            }
        }
        catch(Exception $e){

        }


		if(Yii::app()->user->getState('type')) {
			$this->layout = 'partner';
			$this->partner_id = $user->partner_id;
		}

		$this->render('index', array(
			'user' => $user,
			'stat' => $stat,
			'page_stat' => $page_stat,
            'app_stat'=>$app_stat,
            'app_stat_yes'=>$app_stat_yes,
            'app_stats'=>$app_stats,
            'titles'=>$titles,
            'peoples'=>$peoples,
            'times'=>$times,
            'start'=>$start,
            'end'=>$end
		));
	}


	public function actionProfile() {
		if(Yii::app()->user->getState('type')) {
			$this->layout = 'partner';
		}
		$model = Manager::model()->findByPk(Yii::app()->user->id);

		if($_POST) {
			$model->realname = trim($_POST['realname']);
			if($_POST['password'] && $_POST['old_password']) {
				if($model->password != $model->hashPassword($_POST['old_password']))
					JsonHelper::show(array(
						'error' => true,
						'msg' => '原密码错误'
					));

				$model->password = $model->hashPassword($_POST['password']);
			}


			$model->save();

			JsonHelper::show(array(
				'error' => false,
				'msg' => '更新成功'
			));
		}

		$this->render('profile', array('model' => $model));
	}
}