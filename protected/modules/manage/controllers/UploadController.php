<?php
class UploadController extends ApplicationController {


	public function actionIndex() {
		try{
			$path = "tmp";
			if($_GET['type'] == 'school') {
				$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);
			} else {
				$img = UploadHelper::image('', 'files', array(), $path);
			}


			$error = '';
		} catch(UploadException $e) {
			$error = $e->getMessage();
		} catch(CException $e) {
			$error = $e->getMessage();
		}
		Yii::app()->detachEventHandler('onEndRequest',Yii::app()->getEventHandlers('onEndRequest')->itemAt(0));
		JsonHelper::show(compact('error','img'));
	}

    public function actionUploadImg(){
        $partner_id = Yii::app()->request->getParam('partner_id');
        $dir_name = Yii::app()->request->getParam('dir');

        $save_path = "kind/";
        if($partner_id) {
            $save_path .= $partner_id . "/";
        }


        //创建文件夹
        if ($dir_name !== '') {
            $save_path .= $dir_name . "/";
        }
        $ymd = date("Ymd");
        $save_path .= $ymd . "/";
        try{
            $url = UploadHelper::image('', 'imgFile', array(640), $save_path,true,'','resize');
            $url = HtmlHelper::assets('upload/'.$url);
            $error = 0;
        } catch(UploadException $e) {
            $error = 1;
            $message = $e->getMessage();
        } catch(CException $e) {
            $error = 1;
            $message = $e->getMessage();
        }
        Yii::app()->detachEventHandler('onEndRequest',Yii::app()->getEventHandlers('onEndRequest')->itemAt(0));
        JsonHelper::show(compact('error','url','message'));
    }
}