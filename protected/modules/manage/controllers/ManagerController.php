<?php
class ManagerController extends AdminBaseController {
	public function actionIndex() {
		$criteria = new CDbCriteria();
		$criteria->addCondition('status >= 0');

		if($_GET['type'] == 'system') {
			$criteria->addCondition('type = 0');
		} else if($_GET['type'] == 'school') {
			$criteria->addCondition('type = 1');

			$school = intval($_GET['school']);
			if($school) {
				$criteria->addCondition('partner_id = '.$school);
			}
		}

		$count = Manager::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'status DESC, id DESC';
		$list = Manager::model()->findAll($criteria);

		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'partners' => Partner::all(true),
		));
	}


	public function actionCreate() {

		$model = new Manager();
		$auth = Yii::app()->authManager;
		$roles = $auth->getRoles();


		if($_POST) {
			$username = trim($_POST['username']);
			if(!$username)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名不能为空',
				));

			if(Manager::model()->findByAttributes(array('username' => $username)))
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名已存在',
				));

			$model->username = $username;
			$model->password = $model->hashPassword($_POST['password']);
			$model->realname = $_POST['realname'];
			$model->tel = $_POST['tel'];


			$model->status = $_POST['status'];
			$model->type = $_POST['type'] == 1 ? 1 : 0;

			if($model->type) {
				$model->partner_id = intval($_POST['partner_id']);
			} else {
				$model->role_id = $_POST['role_id'];
				$model->role_name = $_POST['role_name'];
			}

			$model->save();

			if($model->type != 1) {
				$auth = Yii::app()->authManager;
				$auth->assign($model->role_id, $model->id);
			}


			JsonHelper::show(array(
				'error' => false,
				'msg' => '添加成功',
			));
		}


		$this->render('create', array(
			'model' => $model,
			'roles' => $roles,
			'partners' => Partner::all(),
		));
	}


	public function actionUpdate($id) {
		$id = intval($id);
		$model = Manager::model()->findByPk($id);

		if(!$model || $model->id == 1) $this->showMsg('数据不存在或者已被删除', 'error');

		$auth = Yii::app()->authManager;
		$roles = $auth->getRoles();


		if($_POST) {

			$username = trim($_POST['username']);
			if(!$username)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名不能为空',
				));

			if(Manager::model()->find('username = :username and id !='.$id, array(':username' => $username)))
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名已存在',
				));

			$model->username = $username;

			if($_POST['password'])
				$model->password = $model->hashPassword($_POST['password']);

			$model->realname = $_POST['realname'];
			$model->tel = $_POST['tel'];

			$model->type = $_POST['type'] == 1 ? 1 : 0;

			if($model->type) {
				$model->partner_id = intval($_POST['partner_id']);
				$model->role_id = null;
				$model->role_name = null;
			} else {
				//$model->role_id = $_POST['role_id'];
				if($model->role_id != $_POST['role_id']) {
					$auth = Yii::app()->authManager;

					$auth->assign($_POST['role_id'], $model->id);
					$auth->revoke($model->role_id, $model->id);
					$model->role_id = $_POST['role_id'];
				}
				$model->partner_id = null;
				$model->role_name = $_POST['role_name'];
			}



			$model->status = $_POST['status'];
			$model->save();


			JsonHelper::show(array(
				'error' => false,
				'msg' => '编辑成功',
			));
		}


		$this->render('create', array(
			'model' => $model,
			'roles' => $roles,
			'partners' => Partner::all(true),
		));
	}


	public function actionScope($id) {
		$id = intval($id);
		$model = Manager::model()->findByPk($id);
		$role_id = Yii::app()->params['role_id'];
		if(!$model || $model->type || !in_array($model->role_id, $role_id))
			$this->showMsg('数据不存在或者已被删除', 'error');

		$type = $model->role_id == $role_id['school'] ? 'school' : 'merchant';


		if($_POST) {
			ManagerScope::model()->deleteAll('manager_id = '.$id);
			$partner_scope = intval($_POST['partner_scope']) == 1 ? 1 : 0;
			if($partner_scope) {
				foreach($_POST['partners'] as $p) {
					$partner_id = intval($p);
					if(!$partner_id)
						continue;

					$scope = new ManagerScope;
					$scope->partner_id = $partner_id;
					$scope->manager_id = $id;
					$scope->save();
				}
			}

			$model->partner_scope = $partner_scope;
			$model->save();

			JsonHelper::show(array(
				'error' => false,
				'msg' => '编辑成功',
			));
		}


		if($type == 'school') {
			$partners = School::model()->findAll(array(
				'order' => 'id DESC',
			));
		} else {
			$partners = Merchant::model()->findAll(array(
				'order' => 'id DESC',
			));
		}

		$results = ManagerScope::model()->findAll('manager_id = '.$id);
		$scopes = array();
		foreach($results as $r)
			$scopes[] = $r->partner_id;



		$this->render('scope', array(
			'model' => $model,
			'scopes' => $scopes,
			'partners' => $partners,
		));
	}
}