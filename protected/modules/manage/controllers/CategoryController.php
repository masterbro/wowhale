<?php
class CategoryController extends AdminBaseController {


	public function actionIndex() {

		$list = Category::all();



		$this->render('index', array(
			'list' => $list,
		));
	}


	public function actionCreate() {

		$model = new Category();


		if($_POST) {

			try{
				$path = "category";
				$img = UploadHelper::image('', 'files', array(), $path);

				$error = '';
			} catch(UploadException $e) {
				$error = $e->getMessage();
			} catch(CException $e) {
				$error = '系统错误';
			}

			if($error)
				$this->showMsg($error, 'error');

			$model->name = $_POST['name'];
			$model->sort_num = intval($_POST['sort_num']);
			$model->image = $img;
			$model->save();

			$this->showMsg('操作成功', 'success', 'category/index');
		}


		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Category::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {
			try{
				$path = "category";
				$img = UploadHelper::image('', 'files', array(), $path);


				$error = '';
			} catch(UploadException $e) {
				$error = $e->getMessage();
			} catch(CException $e) {
				$error = '系统错误';
			}

			if(!$error)
				$model->image = $img;

			$model->name = $_POST['name'];
			$model->sort_num = intval($_POST['sort_num']);
			$model->save();


			$this->showMsg('编辑成功', 'success', 'category/index');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Category::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}

}