<?php
class MerchantController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($_GET['st'] == 'normal') {
			$criteria->addCondition('is_deleted = 0');
		} else if($_GET['st'] == 'deleted'){
			$criteria->addCondition('is_deleted = 1');
		}

		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));

		$count = Merchant::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'is_deleted, id DESC';
		$list = Merchant::model()->findAll($criteria);



		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
		));
	}


	public function actionCreate() {

		$model = new Merchant();


		if($_POST) {
			$name = trim($_POST['name']);
			if(!$name)
				$this->showMsg('名称不能为空', 'error');

			if(Partner::model()->findByAttributes(array('name' => $name)))
				$this->showMsg('商家 <b>'. $name. '</b> 已存在', 'error');


			$model->name = $name;
			$model->tel = $_POST['tel'];
			$model->type = 1;
			$model->contacts = $_POST['contacts'];
			$model->save();

			$this->showMsg('操作成功', 'success', 'merchant/index');
		}


		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Merchant::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {
			$model->name = $_POST['name'];
			$model->tel = $_POST['tel'];
			$model->contacts = $_POST['contacts'];
			$model->save();


			$this->showMsg('编辑成功', 'success', 'merchant/index');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Merchant::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->is_deleted = $model->is_deleted ? 0 : 1;
		$model->save();

		$this->showMsg('操作成功');
	}

	public function actionMobile($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Merchant::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');


		if($_POST) {
			$model->title = $_POST['title'];
//			$model->content = $_POST['content'];
			$model->contact_tel = $_POST['contact_tel'];
			$slider = array();
//			foreach($_POST['slider'] as $s) {
//				$slider[] = UploadHelper::moveTmp($s, 'Merchant/'.$model->id.'/slider/');
//			}

			$model->slider = json_encode($slider);

			$model->save();

			$this->showMsg('操作成功');
		}


		$this->render('mobile', array(
			'model' => $model,
		));
	}


	public function actionApply() {
		$criteria = new CDbCriteria();


		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));

		$criteria->addCondition("type = 1");

		$count = PartnerApply::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'status DESC, id DESC';
		$list = PartnerApply::model()->findAll($criteria);

		$this->render('apply', array(
			'list' => $list,
			'pager' => $pager,
		));
	}


	public function actionApplyStatus($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerApply::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($model->status == 1)
			$this->showMsg('数据已经通过审核，不能进行操作', 'error');


		Yii::import('application.extensions.sms.*');
		if($_GET['action'] == 'approve') {
			if(PartnerApply::model()->find("name = :name and id != {$id}", array(':name' => $model->name)))
				$this->showMsg('你已经申请过合作，请不要重复申请', 'error');

			if(Partner::model()->findByAttributes(array('name' => $model->name)))
				$this->showMsg('商家 <b>'. $model->name. '</b> 已存在', 'error');

			$partner = new Partner;
			$partner->name = $model->name;
			$partner->tel = $model->tel;
			$partner->type = 1;
			$partner->contacts = $model->contacts;
			$partner->save();


			$model->status = 1;
			$model->save();

			$manager = Manager::model()->findByPk($model->manager_id);
			if($manager) {
				$manager->partner_id = $partner->id;
				$manager->status = Manager::ST_ACTIVE;
				$manager->save();
			}

			SmsService::send($model->tel, SmsContent::MERCHANT_REQUEST_APPROVE, false);
		} else if($_GET['action'] == 'pending') {
//			$model->step = 1;
//			$model->status = 2;
//			$model->reason = $_POST['reason'];
//			$model->save();


		} else if($_GET['action'] == 'delete') {
			$manager = Manager::model()->findByPk($model->manager_id);
			if($manager)
				$manager->delete();

			$model->delete();
			$this->showMsg('删除成功');
		} else {
			$model->status = 2;
			$model->reason = urldecode($_GET['reason']);
			$model->save();

			$manager = Manager::model()->findByPk($model->manager_id);
			if($manager) {
				$manager->join_step = 3;
				$manager->status = Manager::ST_PENDING;
				$manager->save();
			}

			SmsService::send($model->tel, SmsContent::MERCHANT_REQUEST_REJECT, false);
		}






		$this->showMsg('操作成功');
	}
}