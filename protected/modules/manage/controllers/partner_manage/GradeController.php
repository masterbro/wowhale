<?php
class GradeController extends PartnerAdminBaseController {
    private $config = 'init';
	public function actionIndex() {

		$this->render('index', array(
			'lists' => Grade::model()->findAll('partner_id = '.$this->partner_id),
		));
	}


	public function actionCreate() {
		if($_POST) {
			if(!$_POST['name'])
				$this->showMsg('名称不能为空', 'error');

			$model = new Grade();


			$model->partner_id = $this->partner_id;
			$model->name = $_POST['name'];
			$model->save();
			$this->showMsg('添加成功');
		}
	}


	public function actionUpdate() {
		if($_POST) {
			$id = intval($_POST['id']);
			$model = Grade::model()->find("id = {$id} and partner_id = {$this->partner_id}");

			if(!$model) $this->showMsg('数据不存在或者已被删除', 'error');

			if(!$_POST['name'])
				$this->showMsg('名称不能为空', 'error');

			$model->name = $_POST['name'];
			$model->save();
            $config = $this->wxConfig();
            if($config){
                $app = new WxQyContacts($config);
                $depart_key = 'grade_'.$model->id;
                try{
                    if($config->departmentIdsObj->$depart_key){
                        $app->updateDepart($config->departmentIdsObj->$depart_key, $model->name);
                    }

                }
                catch(Exception $e){

                }

            }
			$this->showMsg('编辑成功');
		}
	}


	public function actionDelete($id) {
		$id = intval($id);
		$model = Grade::model()->find("id = {$id} and partner_id = {$this->partner_id}");

		if($model) {
			$model->delete();
		}


		$this->showMsg('删除成功');
	}

    public function wxConfig() {
        if($this->config != 'init') {
            return $this->config;
        }
        $ww_apps = CommonHelper::qyApp();
        $this->config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
        return $this->config;
    }
}