<?php
class AlbumController extends PartnerAdminBaseController {


	public function actionIndex() {

		$criteria = new CDbCriteria();

		$criteria->addCondition('partner_id = '.$this->partner_id);



		$count = Album::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'status DESC, id DESC';
		$list = Album::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
		));
	}


	public function actionCreate() {

		if(!$_POST['title'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '标题不能为空',
			));


		$model = new Album();
		$model->partner_id = $this->partner_id;
		$model->title = $_POST['title'];
		$model->save();

		JsonHelper::show(array(
			'error' => false,
			'msg' => '添加成功',
		));
	}

	public function actionUpdate() {

		$id = intval($_POST['id']);
		if(!$id)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		$model = Album::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		if(!$_POST['title'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '标题不能为空',
			));

		$model->title = $_POST['title'];
		$model->save();

		JsonHelper::show(array(
			'error' => false,
			'msg' => '添加成功',
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerSlider::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}

}