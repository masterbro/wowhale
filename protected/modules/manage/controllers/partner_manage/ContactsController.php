<?php
class ContactsController extends PartnerAdminBaseController {
	private $is_teacher = false;
	private $ww_suite_id = false;
    private $config = 'init';

	public function beforeAction($action) {
		if($_GET['type'] == 'teacher')
			$this->is_teacher = true;

		$ww_apps = CommonHelper::qyApp();
		$this->ww_suite_id = $ww_apps['notice']['ww_suite_id'];

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->addCondition('partner_id = '.$this->partner_id);

		if($this->is_teacher) {
			$criteria->addCondition('type = 2');
		} else {
			$criteria->addCondition('type = 1');
		}

		if($_GET['name'])
			$criteria->addSearchCondition('parent_name', urldecode($_GET['name']));


		if($_GET['grade'])
			$criteria->addCondition('grade_id = '.intval($_GET['grade']));

		$count = Contacts::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Contacts::model()->findAll($criteria);


		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);

		$grades = CHtml::listData($grades, 'id', 'name');
		$this->render($this->is_teacher ? 'teacher' : 'student', array(
			'list' => $list,
			'pager' => $pager,
			'grades' => $grades
		));
	}


	public function actionCreate() {

		$model = new Contacts();

		if($_POST) {
			$model->parent_name = $_POST['parent_name'];
			$model->parent_phone = $_POST['parent_phone'];
			$model->grade_id = intval($_POST['grade_id']);
			$model->partner_id = $this->partner_id;

			if($_POST['leader'] == 1) {
				$model->leader = 1;
			}

			$config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);
			$contacts = Contacts::model()->find(
				"partner_id = {$this->partner_id} AND parent_phone = :parent_phone",
				array(
					':parent_phone' => $_POST['parent_phone']
				)
			);

			if($contacts ) {
				if($contacts->type == Contacts::TY_TEACHER) {
					$this->showMsg('员工已经存在', 'error');
				} else {
					$model->type = Contacts::TY_TEACHER;
                    $model->id = $contacts->id;
                    $model->setIsNewRecord(false);
					$model->save();

					if($config) {
						$app = new WxQyContacts($config);
                        /*$departments = array( $config->teacher_department);
                        $students = $model->students;
                        if($students && count($students)>0){
                            $departments[] = $config->student_department;
                        }
                        if($model->leader){
                            $departments[] = $config->leader_department;
                        }*/
                        $departments = $this->getDepartment($model);

                        try{
                            $updated = $app->update($model, $departments);
                        }
                        catch(Exception $e){
                            $updated = $app->create($model, $departments);
                        }
                        if(!$updated){
                            try {
                                $updated = $app->create($model, $departments);
                            }
                            catch(Exception $e){

                            }
                        }
					}

					$this->showMsg('操作成功', 'success', 'partner_manage/contacts/index?type='.$_GET['type']);
				}
			}



			$model->type = Contacts::TY_TEACHER;
            /*$departments = array( $config->teacher_department);
            $students = $model->students;
            if($students && count($students)>0){
                $departments[] = $config->student_department;
            }
            if($model->leader){
                $departments[] = $config->leader_department;
            }
			if($config)
				$department = $departments;*/

			$model->save();

            $department = $this->getDepartment($model);
			$model->generateToken();

			if($config) {
				$app = new WxQyContacts($config);
				$app->create($model, $department);
                $app->invite($model);
			}



			$this->showMsg('操作成功', 'success', 'partner_manage/contacts/index?type='.$_GET['type']);
		}

		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
		$grades = CHtml::listData($grades, 'id', 'name');


		$this->render($this->is_teacher ? 'form_teacher' : 'form', array(
			'model' => $model,
			'grades' => $grades,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Contacts::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id);

		if($_POST) {
			$model->parent_name = $_POST['parent_name'];
			$model->parent_phone = $_POST['parent_phone'];
			$model->grade_id = intval($_POST['grade_id']);

			$exists = Contacts::model()->exists(
				"partner_id = {$this->partner_id} AND id != {$id} AND parent_phone = :parent_phone",
				array(
					':parent_phone' => $_POST['parent_phone']
				)
			);

			if($exists)
				$this->showMsg('员工已经存在', 'error');

			if($_POST['leader'] == 1) {
				$model->leader = 1;
			} else {
				$model->leader = null;
			}

            /*$departments = array( $config->teacher_department);
            $students = $model->students;
            if($students && count($students)>0){
                $departments[] = $config->student_department;
            }
            if($model->leader){
                $departments[] = $config->leader_department;
            }
            if($config)
                $department = $departments;*/
            $department = $this->getDepartment($model);

			$model->partner_id = $this->partner_id;
			$model->save();

			if($config) {
				$app = new WxQyContacts($config);
                try{
                    $updated = $app->update($model, $department);
                }
                catch(Exception $e){
                    $updated = $app->create($model, $department);
                }
                if(!$updated){
                    try {
                        $updated = $app->create($model, $department);
                    }
                    catch(Exception $e){

                    }
                }

                $app->invite($model);
			}


			$type = $model->type ? 'teacher' : 'student';
			$this->showMsg('操作成功', 'success', 'partner_manage/contacts/index?type='.$type);
		}



		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
		$grades = CHtml::listData($grades, 'id', 'name');


		$this->render( $model->type ? 'form_teacher' : 'form', array(
			'model' => $model,
			'grades' => $grades,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Contacts::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);


		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$is_parent = $model->students;
		if($is_parent) {
			$model->type = Contacts::TY_STUDENT;
			$model->save();
		}

		if( $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id)	) {
			$app = new WxQyContacts($config);
            if($is_parent){
                $department = $this->getDepartment($model);
                $app->update($model,$department);
            }
            else{
                $app->delete($model);
            }
		}

		if(!$is_parent)
			$model->delete();

		$this->showMsg('操作成功');
	}
    public function actionDelAll() {

        $ids = Yii::app()->request->getParam('ids');
        if(!$ids){
            JsonHelper::show(array('error'=>1,'msg'=>'请至少选择一个员工'));
        }
        $error_count = 0;
        $success_count = 0;
        foreach($ids as $k=>$id){
            $model = Contacts::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

            if(!$model){
                $error_count++;
            }
            //$this->showMsg('数据不存在，或者已被删除', 'error');

            $is_parent = $model->students;
            if($is_parent) {
                $model->type = Contacts::TY_STUDENT;
                $model->save();
            }

            if( $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id)	) {
                $app = new WxQyContacts($config);
                if($is_parent){
                    $app->update($model,$this->getDepartment($model)/*array($config->student_department)*/);
                }
                else{
                    $app->delete($model);
                }

            }

            if(!$is_parent)
                $model->delete();

            $success_count++;
        }
        if($success_count<1){
            $error = 1;
            $msg = '删除失败'.$error_count.'个';
        }
        else{
            $error = 0;
            $msg = '删除成功'.$success_count.'个';
            if($error_count>0){
                $msg .= '删除失败'.$error_count.'个';
            }
        }
        JsonHelper::show(compact('error','msg'));

    }


	public function actionImport() {
		if($_FILES){
			$config = array(
				'A'=>'student_name',
				'B'=>'student_birth',
				'C'=>'parent_name',
				'D'=>'parent_phone',
				'E'=>'grade',
			);
			$data = $this->import($config,$_FILES['file']['tmp_name']);

			$error = '';
			if(!$data) {
				$error = 'excel文件没有内容，请查证后上传';
			} else {
				$title = array_shift($data);

				if($title['student_name'] != '学生姓名' || $title['student_birth'] != '学生出生年月'
					|| $title['parent_name'] != '家长姓名' || $title['parent_phone'] != '家长电话'
					|| $title['grade'] != '班级')
				{
					$error = 'excel模板有误，请确定你使用的EXCEL为本网站下载链接给出的模板';
				}
			}

			if($error)
				$this->showMsg($error, 'error');

			$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
			$grades = CHtml::listData($grades, 'id', 'name');
			$grades = array_flip($grades);
			$info = array(
				'error' => 0,
				'success' => 0,
				'tips' => ''
			);


			$models = array();
			foreach($data as $d) {
				if(!$d['student_name'] || !$d['parent_name'] || !$d['parent_phone']) {
					$info['error']++;
					continue;
				}

				$grade_id = null;
				if($grades[$d['grade']])
					$grade_id = $grades[$d['grade']];

				$exists = Contacts::model()->exists(
					"student_name = :student_name and parent_name = :parent_name and parent_phone = :parent_phone",
					array(
						':student_name' => $d['student_name'],
						':parent_name' => $d['parent_name'],
						':parent_phone' => $d['parent_phone']
					)
				);

				if($exists) {
					$info['error']++;
					$info['tips'] .= '<br />'.$d['student_name'].'已经存在';
					continue;
				}

				$model = new Contacts();
				$model->partner_id = $this->partner_id;
				if($grade_id)
					$model->grade_id = $grade_id;
				$model->student_name = $d['student_name'];
				if(strtotime($d['student_birth']) !== false)
					$model->student_birth = date('Ym', strtotime($d['student_birth']));

				$model->parent_name = $d['parent_name'];
				$model->parent_phone = $d['parent_phone'];
				$model->save();

				$models[] = $model;
				$info['success']++;
			}

			$tips = '共导入数据：'.($info['success'] + $info['error']).'条 成功：'.$info['success'].'条, 失败：'.$info['error'].'条';
			if($info['tips']) $tips .= '错误原因：'.$info['tips'];


			if( $config = WechatConfig::getInstance($this->partner_id, $this->ww_suite_id)	) {
				try {
					$app = new WxQyContacts($config);
					$app->batchCreate($models, $config->student_department);
				} catch(WxAppException $e) {
				}
			}

			$this->showMsg($tips);
		}
	}


    private function getDepartment($contact){
        $config = $this->wxConfig();
        if(!$config){
            return array();
        }
        $student_department = $config->student_department;
        $teacher_department = $config->teacher_department;
        $leader_department = $config->leader_department;
        $departs = array();
        if($contact->leader){
            $departs[] = $leader_department;
        }
        if($contact->type==2){
            $departs[] = $teacher_department;
            if(count($contact->students)>0 ){
                //$departs[] = $student_department;
                $students = $contact->students;
                foreach($students as $k=>$v){
                    $departs[] = $config->getGradeDepartment($v->grade_id);
                }
            }

        }
        else{
            //$departs[] = $student_department;
            $students = $contact->students;
            foreach($students as $k=>$v){
                $departs[] = $config->getGradeDepartment($v->grade_id);
            }
        }
        return $departs;
    }

	//读取excel数据   $config配置    文件路径
	public function import($config,$filePath,$maxColumn= 100){
		Yii::import('application.extensions.PHPExcel');
		Yii::import('application.extensions.PHPExcel.*');
		$PHPExcel = new PHPExcel();
		/**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
		$PHPReader = new PHPExcel_Reader_Excel2007();
		if(!$PHPReader->canRead($filePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
			if(!$PHPReader->canRead($filePath)){
				return array();
			}
		}
		$objPHPExcel = $PHPReader->load($filePath) ;
		$currentSheet = $objPHPExcel->getSheet(0);
		/**取得最大的列号*/
		$allColumn = PHPExcel_Cell::columnIndexFromString($currentSheet->getHighestColumn()) - 1;
		$allColumn = $allColumn > $maxColumn?$maxColumn:$allColumn;
		/**取得一共有多少行*/
		$allRow = $currentSheet->getHighestRow();
		$count = 0;
		$data = array();
		for($currentRow = 1;$currentRow <= $allRow;$currentRow++){
			$_data = array();
			for($currentColumn= 'A';(ord($currentColumn) - 65)<= $allColumn; $currentColumn++){
				$cell = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow);
				$value = $cell->getValue();
				if($cell->getDataType()==PHPExcel_Cell_DataType::TYPE_NUMERIC){
					$cellstyleformat=$cell->getStyle( $cell->getCoordinate() )->getNumberFormat();

					$formatcode=$cellstyleformat->getFormatCode();
					if (preg_match('/^(\[\$[A-Z]*-[0-9A-F]*\])*[hmsdy]/i', $formatcode)) {
						$value=PHPExcel_Shared_Date::ExcelToPHP($value);
					}else{
						$value=PHPExcel_Style_NumberFormat::toFormattedString($value,$formatcode);
					}
				}
				else{
					$value = (string)$value;
				}
				if(isset($config[$currentColumn])){
					$_data[ $config[$currentColumn] ] = $value;
				}

			}
			$data[] = $_data;
		}
		return $data;
	}

    public function wxConfig() {
        if($this->config != 'init') {
            return $this->config;
        }
        $ww_apps = CommonHelper::qyApp();
        $this->config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
        return $this->config;
    }
	private function getConfig() {

	}
}