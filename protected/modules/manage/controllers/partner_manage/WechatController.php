<?php
class WechatController extends PartnerAdminBaseController {


	public function actionIndex() {

		$config = WechatConfig::model()->find("partner_id = {$this->partner_id} AND ww_suite_id = 3");
//		$config2 = WechatConfig::model()->find("partner_id = {$this->partner_id} AND ww_suite_id = 2");
//		$suite = WxSuite::model()->find(1);


		$partner_apps = array();
		if($config && $config->app_ids) {
			$partner_apps = (array) json_decode($config->app_ids);
		}
//
//		if($config2 && $config2->app_ids) {
//			$partner_apps += (array) json_decode($config2->app_ids);
//		}


		$this->render('index', array(
			'partner_apps' => $partner_apps,
			'app' => CommonHelper::qyApp($this->partner_id),
		));
	}
	public function actionMywechat() {

		$config = WechatConfig::model()->find("partner_id = {$this->partner_id} AND ww_suite_id = 3");
//		$config2 = WechatConfig::model()->find("partner_id = {$this->partner_id} AND ww_suite_id = 2");
//		$suite = WxSuite::model()->find(1);


		$partner_apps = array();
		if($config && $config->app_ids) {
			$partner_apps = (array) json_decode($config->app_ids);
		}

//		if($config2 && $config2->app_ids) {
//			$partner_apps += (array) json_decode($config2->app_ids);
//		}


		$this->render('mywechat', array(
			'partner_apps' => $partner_apps,
			'app' => CommonHelper::qyApp($this->partner_id),
		));
	}

	public function actionOauth() {

		$app = CommonHelper::qyApp($this->partner_id);

		$key = trim($_POST['key']);
		if($key && (array_key_exists($key, $app) || $key == 'all')) {

			if($key == 'all') {
				$suite = WxQySuite::getSuite(3);
				$apps = array(1, 2, 3, 4, 5);

			} else {
				$oauth_app = $app[$key];
				$suite = WxQySuite::getSuite($oauth_app['suite_id']);
				$apps = array($oauth_app['id']);
			}
			$pre_auth_code = CommonHelper::getPreAuthCode($suite);

			//设置此次授权app
			$app = new WxQySuite();
//			$app->throw_exception = true;
			$app->set_session_info($pre_auth_code, $suite->suite_access_token, $apps);

			$redirect_uri = $this->createAbsoluteUrl("/wechatQyApp/oauth?suite_id={$suite->id}&partner_id={$this->partner_id}");
			$oauth_url = "https://qy.weixin.qq.com/cgi-bin/loginpage?suite_id={$suite->suite_id}&pre_auth_code={$pre_auth_code}&redirect_uri={$redirect_uri}&state=";

			JsonHelper::show(array(
				'success' => true,
				'oauth_url' => $oauth_url
			));
		}

	}



}