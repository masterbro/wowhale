<?php
class PostController extends PartnerAdminBaseController {


	public function actionIndex() {

		$type = PartnerPost::$post_type[$_GET['type']];
		if(!$type)
			$this->showMsg('页面不存在', 'error');



		$criteria = new CDbCriteria();
		$criteria->addCondition("partner_id = {$this->partner_id} and type = :type");
		$criteria->params[':type'] = $_GET['type'];

		$count = PartnerPost::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'sort_num DESC, id DESC';
		$list = PartnerPost::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'type' => $type,
		));
	}


	public function actionCreate() {

		$type = PartnerPost::$post_type[$_GET['type']];
		if(!$type)
			$this->showMsg('页面不存在', 'error');

		$model = new PartnerPost();


		if($_POST) {

			$img = false;
			if($type['has_thumb']) {
				try{
					$path = "partner/{$this->partner_id}/post";
					$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);

					$error = '';
				} catch(UploadException $e) {
					$error = $e->getMessage();
				} catch(CException $e) {
					$error = '系统错误';
				}
			}

			$model->title = $_POST['title'];
			$model->type = $_GET['type'];
			$model->partner_id = $this->partner_id;
			$model->content = $_POST['content'];
			$model->sort_num = intval($_POST['sort_num']);

			if($img)
				$model->thumb = $img;

			$model->save();

			$this->showMsg('操作成功', 'success', '/manage/partner_manage/post/index?type='.$_GET['type']);
		}


		$this->render('form', array(
			'model' => $model,
			'type' => $type,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerPost::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$type = PartnerPost::$post_type[$model->type];

		if($_POST) {

			$img = false;
			if($type['has_thumb']) {
				try{
					$path = "partner/{$this->partner_id}/post";
					$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);

					$error = '';
				} catch(UploadException $e) {
					$error = $e->getMessage();
				} catch(CException $e) {
					$error = '系统错误';
				}
			}

			$model->title = $_POST['title'];
			$model->content = $_POST['content'];
			$model->sort_num = intval($_POST['sort_num']);

			if($img)
				$model->thumb = $img;

			$model->save();

			$this->showMsg('操作成功', 'success');
		}

		$this->render('form', array(
			'model' => $model,
			'type' => $type,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerPost::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}

}