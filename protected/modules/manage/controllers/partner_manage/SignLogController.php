<?php
class SignLogController extends PartnerAdminBaseController {
	public function actionIndex() {
        $criteria = new CDbCriteria();

        $criteria->addCondition('partner_id = '.$this->partner_id);
        $contact_id = (int) Yii::app()->request->getParam('contact_id');
        $name_extra = '';
        if($_GET['start']  ) {
            $start = strtotime($_GET['start']);
            $criteria->addCondition("time > {$start} ");
            $name_extra .= 'f'.$_GET['start'];
        }
        if(  $_GET['to']) {
            $name_extra .= 't'.$_GET['to'];
            $end = strtotime($_GET['to']) + 86399;
            $criteria->addCondition("time < {$end}");
        }
        $contacts = Contacts::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'type'=>2));
        $contacts_obj = array();
        foreach($contacts as $k=>$v){
            $contacts_obj[$v->id] = $v;
        }
        if($contact_id>0){
            $criteria->addCondition("contacts_id = {$contact_id}");
            $name_extra .= 'j'.$contacts_obj[$contact_id]->parent_name;
        }
        $criteria->order = 'id DESC';

        if($_GET['export']){
            $list = Order::model()->findAll($criteria);
            $data = array();
            $contacts = array();
            $grades = array();
            foreach($list as $l) {
                $name = $contacts_obj[$l->contacts_id]?$contacts_obj[$l->contacts_id]->parent_name:'--';
                $data[] = array($name,date('Y-m-d',$l->time),date('H:i:s',$l->time),($l->type==1?'上班':'下班'));
            }
            $title = array(
                '姓名', '日期', '时间','签到类型'
            );

            HtmlHelper::exportExcel($data, $title, "签到报表{$name_extra}.xls");
        }
        else{
            $count = SignLog::model()->count($criteria);
            $pager = new CPagination($count);
            $pager->pageSize = 20;
            $pager->applyLimit($criteria);
            $list = SignLog::model()->findAll($criteria);
            $this->render('index', compact('contacts_obj','list','pager','contact_id'));
        }

	}

	public function actionUpdate() {
		if($_POST) {
			$lat = Yii::app()->request->getParam('lat');
            $lng = Yii::app()->request->getParam('lng');
            $limit = Yii::app()->request->getParam('limit');
            if(!is_numeric($lat) || !is_numeric($lng)){
                $this->showMsg('经纬度录入错误');
            }
            if(!is_numeric($lat) || strpos('.',$lat)>-1){
                $this->showMsg('精度只支持整数');
            }
            $partner = $this->partner;
            $partner->sign_log_point = $lat.','.$lng;
            $partner->sign_log_limit = $limit;
            $partner->save();
            //var_dump($lat,$lng,$lat.','.$lng,$partner->save(),$partner->sign_log_point,$partner->sign_log_limit);exit;
			$this->showMsg('编辑成功');
		}
        $this->render('update',array(
            'partner'=>$this->partner
        ));
	}

}