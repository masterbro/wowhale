<?php
class PartnerController extends PartnerAdminBaseController {


	public function actionIndex() {

		$model = Partner::model()->find('id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');


		if($_POST) {
			$model->title = $_POST['title'];
			$model->content = $_POST['content'];
			$slider = array();
			if($_POST['slider']) {
				foreach($_POST['slider'] as $s) {
					$slider[] = UploadHelper::moveTmp($s, 'school/'.$model->id.'/slider/');
				}
			}

			$model->slider = json_encode($slider);
			$model->contact_tel = $_POST['contact_tel'];
			$model->save();

			$this->showMsg('操作成功');
		}
		$partner = $this->getPartner();


		$this->render($partner->type ? 'mobile_merchant' : 'mobile', array(
			'model' => $model,
		));
	}

}