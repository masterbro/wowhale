<?php
class BalanceController extends PartnerAdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->addCondition('partner_id = '.$this->partner_id);

		if($_GET['start'] && $_GET['to']) {
			$start = strtotime($_GET['start']);
			$end = strtotime($_GET['to']) + 86399;
			$criteria->addCondition("create_time > {$start} and create_time < {$end}");
		}

		if($_GET['st'] == 'approved') {
			$criteria->addCondition('status = 2');
		} else if($_GET['st'] == 'rejected'){
			$criteria->addCondition('status = 1');
		} else if($_GET['st'] == 'pending'){
			$criteria->addCondition('status = 0');
		}


		$count = BalanceApply::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = BalanceApply::model()->findAll($criteria);

		$users = CommonHelper::getGroupField($list, Manager::model(), 'user_id', 'username');
		$manager = CommonHelper::getGroupField($list, Manager::model(), 'manager_id', 'username');


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'users' => $users,
			'manager' => $manager,
		));

	}


	public function actionView($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = BalanceApply::model()->find("id = {$id} and partner_id = {$this->partner_id}");
		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$partner = Partner::model()->findByPk($model->partner_id);

		$this->render('view', array(
			'model' => $model,
			'partner' => $partner,
		));
	}


	public function actionCard() {


		$this->render('card', array(
			'list' => PartnerCard::model()->findAll("partner_id = {$this->partner_id}"),
		));
	}

	public function actionApply() {
		$model = Balance::model()->find("partner_id = {$this->partner_id}");
		if(!$model){
			$model = new Balance();
			$model->partner_id = $this->partner_id;
			$model->balance = 0;
			$model->create_time = time();
			$model->save();
		}


		$connection = Yii::app()->db;
		$transaction = $connection->beginTransaction();
		$last_day = strtotime(date('Y-m-d'));
		try {
			$condition = "school_id = {$this->partner_id} AND payment_status = 1 AND import_from = 0 AND create_time < {$last_day} AND into_balance = 0";
			$select = $connection->createCommand("SELECT SUM(`total`) as total_money FROM {{order}} WHERE ".$condition);
			$row = $select->queryRow();

			$increase = $row['total_money'] ? $row['total_money'] : 0;

			if($increase) {
				$select = Yii::app()->db->createCommand("SELECT min(id) min_id FROM {{order}} WHERE ".$condition);
				$row = $select->queryRow();
				$min_id = $row['min_id'];

				$select = Yii::app()->db->createCommand("SELECT max(id) max_id FROM {{order}} WHERE ".$condition);
				$row = $select->queryRow();
				$max_id = $row['max_id'];

				$log = new BalanceLog();
				$log->partner_id = $this->partner_id;
				$log->ids = $min_id.'|'.$max_id;
				$log->type = 'incoming_order';
				$log->money = $increase;
				$log->save();;
			}

			$sql = "UPDATE {{balance}} SET balance = balance + {$increase} WHERE partner_id = {$this->partner_id}";
			$connection->createCommand($sql)->execute();

			$sql = "UPDATE {{order}} SET into_balance = 1 WHERE {$condition}";
			$connection->createCommand($sql)->execute();

			$transaction->commit();

		} catch (Exception $e) {
			$transaction->rollBack();
			$this->showMsg('系统错误，请重试', 'error');
		}

		if($increase)
			$model = Balance::model()->find("partner_id = {$this->partner_id}");


		$this->render('apply', array(
			'model' => $model,
			'last_day' => $last_day,
			'partner' => $this->getPartner(),
			'cards' => PartnerCard::model()->findAll("partner_id = {$this->partner_id}"),
		));
	}

	public function actionRequest() {
		$money = intval($_POST['money']);
		$card_id = intval($_POST['card']);

		if(!$money || $money%100 || !$card_id)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '参数错误',
			));

		$card = PartnerCard::model()->find("id = {$card_id} AND partner_id = {$this->partner_id}");

		if(!$card)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '参数错误',
			));


		$connection = Yii::app()->db;
		$transaction = $connection->beginTransaction();
		$money = $money * 100;
		$partner = $this->getPartner();

		try {
			$select = $connection->createCommand("SELECT balance FROM {{balance}} WHERE partner_id = {$this->partner_id} FOR UPDATE");
			$row = $select->queryRow();

			if(!$row || $row['balance'] < $money)
				throw new Exception('余额不足', 1000);

			$sql = "UPDATE {{balance}} SET balance = balance - {$money} WHERE partner_id = {$this->partner_id}";
			$connection->createCommand($sql)->execute();

			$apply = new BalanceApply();
			$apply->partner_id = $this->partner_id;
			$apply->money = $money;
			if($partner->type) {
				$apply->fee = intval($money * BalanceApply::FEE_RATE);
			}
			$apply->status = 0;
			$apply->user_id = Yii::app()->user->getId();
			$apply->card_info = json_encode(array(
				'name' => $card->name,
				'bank_name' => $card->bank_name,
				'card_num' => $card->card_num,
				'bank_info' => $card->bank_info,
			));
			$apply->save();

			$transaction->commit();
		} catch (Exception $e) {
			$transaction->rollBack();
			JsonHelper::show(array(
				'error' => true,
				'msg' => $e->getCode() == 1000 ? $e->getMessage() : '系统错误，请重试',
			));
		}


		JsonHelper::show(array(
			'error' => false,
			'msg' => '申请成功',
		));

	}


	public function actionCardCreate() {
		$model = new PartnerCard();

		if($_POST) {
			$model->name = $_POST['name'];
			$model->bank_name = $_POST['bank_name'];
			$model->card_num = $_POST['card_num'];
			$model->bank_info = $_POST['bank_info'];
			$model->partner_id = $this->partner_id;
			$model->save();

			$this->showMsg('操作成功', 'success', '/manage/partner_manage/balance/card');
		}

		$this->render('cardForm', array(
			'model' => $model,
		));
	}

	public function actionCardUpdate($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerCard::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');


		$this->render('cardForm', array(
			'model' => $model
		));
	}

	public function actionCardDelete($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = PartnerCard::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if($model)
			$model->delete();


		$this->showMsg('操作成功');
	}
}