<?php
class NoticeController extends PartnerAdminBaseController {


	public function actionIndex() {

		$criteria = new CDbCriteria();
		$criteria->addCondition("partner_id = {$this->partner_id}");

		$count = Notice::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 20;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Notice::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'teachers' => Contacts::model()->findAll('partner_id = '.$this->partner_id.' AND type = 2'),
		));
	}


	public function actionCreate() {

		$model = new Notice();


		if($_POST) {

			$img = false;
			if($_POST['type'] != 1) {
				try{
					$path = "partner/{$this->partner_id}/notice";
					$img = UploadHelper::image('', 'files', array(), $path);

					$error = '';
				} catch(UploadException $e) {
					$error = $e->getMessage();
				} catch(CException $e) {
					$error = '系统错误';
				}
			}

			$model->title = $_POST['title'];
			$model->description = $_POST['description'];
			$model->content = $_POST['content'];

			if($img)
				$model->thumb = $img;

			$model->type = $_POST['type'] == 1 ? 1 : 0;
			$model->push_to = $_POST['push_to'] == 1 ? 1 : 0;
			$model->partner_id = $this->partner_id;
			$model->save();

			$this->showMsg('操作成功', 'success', '/manage/partner_manage/notice/');
		}


		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Notice::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);
		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {

			$img = false;
			if($_POST['type'] != 1) {
				try{
					$path = "partner/{$this->partner_id}/notice";
					$img = UploadHelper::image('', 'files', array(), $path);

					$error = '';
				} catch(UploadException $e) {
					$error = $e->getMessage();
				} catch(CException $e) {
					$error = '系统错误';
				}
			}

			$model->title = $_POST['title'];
			$model->description = $_POST['description'];
			$model->content = $_POST['content'];

			if($img)
				$model->thumb = $img;

			$model->type = $_POST['type'] == 1 ? 1 : 0;
			$model->push_to = $_POST['push_to'] == 1 ? 1 : 0;
			$model->partner_id = $this->partner_id;
			$model->save();

			$this->showMsg('操作成功', 'success', '/manage/partner_manage/notice/');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionPush() {
		$id = intval($_POST['id']);
		if(!$id)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		$model = Notice::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);
		if(!$model)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		if(!$_POST['push_to'] && $model->last_push &&  time() - $model->last_push < 600) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => '再次推送不能小于10分钟',
			));
		}

		$ww_apps = CommonHelper::qyApp();
		$config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
		if(!$config || !$config->app_ids)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '你还没有配置微信服务号',
			));

		$app = new WxQyMsg($config);
		$app->throw_exception = true;
		$app_ids = json_decode($config->app_ids);
		if(!$app_ids->notice)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '你还没有添加微公告应用',
			));

		if($_POST['push_to']) {
			$to = 'sh'.$_POST['push_to'];
		} else {
			if($model->push_to == 1) {
				//推送给老师
				$to = array();
				$contacts = Contacts::model()->findAll("partner_id = {$this->partner_id} AND type = 2");
				foreach($contacts as $c) {
					$to[] = 'sh'.$c->id;
				}
				if(!$to)
					JsonHelper::show(array(
						'error' => true,
						'msg' => '你还没有添加老师',
					));

				$to = implode('|', $to);
			} else {
				//所有人
				$to = '@all';
			}
		}


		$wx_app_id = $app_ids->notice;

		if($model->type == 1) {
			//文字消息
			try {
				$app->text($to, $model->description, $wx_app_id);
			} catch(WxAppException $e) {
				JsonHelper::show(array(
					'error' => true,
					'msg' => $e->getMessage(),
				));
			}

		} else {
			//图文消息

			try {
				$news = array(
					array(
						'title' => $model->title,
						'description' => $model->description,
						'url' => Yii::app()->params['mobile_base'].'/notice/view?id='.$model->id,
						'picurl' => HtmlHelper::image($model->thumb),
					)
				);

				$app->news($to, $news, $wx_app_id);
			} catch(WxAppException $e) {
				JsonHelper::show(array(
					'error' => true,
					'msg' => $e->getMessage(),
				));
			}
		}

		if(!$_POST['push_to']) {
			$model->last_push = time();
			$model->save();
		}

		JsonHelper::show(array(
			'error' => false,
			'msg' => '推送成功',
            'last_push'=>$model->last_push?date('Y-m-d H:i:s',$model->last_push):''
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Notice::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}

    public function actionShipu(){
        $start_date = Yii::app()->request->getParam('start');
        $end_date = Yii::app()->request->getParam('to');
        $export = Yii::app()->request->getParam('export');
        $criteria = new CDbCriteria();
        $criteria->addCondition("partner_id = {$this->partner_id}");
        if(!$start_date){
            $start_date = date('Y-m-d',time()-3600*24*7);
        }
        if(!$end_date){
            $end_date = date('Y-m-d');
        }
        $criteria->order = 'id asc';
        $criteria->addCondition("date >= '{$start_date}'");
        $criteria->addCondition("date <= '{$end_date}'");
        $data = Shipu::model()->findAll($criteria);
        $list = array();
        $data_keys = array();
        $week = array('日','一','二','三','四','五','六','日');
        foreach($data as $k=>$v){
            if(!$list[$v->date]){
                $list[$v->date] = array($v->date,'星期'.$week[date('w',strtotime($v->date))]);
            }
            if(!$data_keys[$v->type]){
                $data_keys[$v->type] = 1;
            }
            $list[$v->date][$v->type] = $v->description;
        }
        if($export){
            $titles = array_keys($data_keys);
            array_unshift($titles,'星 期');
            array_unshift($titles,'日期');
            HtmlHelper::exportExcel($list,$titles , "食谱_{$start_date}_{$end_date}.xls");
        }
        else{
            $this->render('shipu',compact('list','start_date','end_date','data_keys'));
        }

    }

    public  function actionPushShipu(){
        $start= Yii::app()->request->getParam('start');
        $to= Yii::app()->request->getParam('to');
        if(!$start ||!$to){
            JsonHelper::show(array(
                'error' => true,
                'msg' => '请选择推送食谱的起止时间',
            ));
        }
        $ww_apps = CommonHelper::qyApp();
        $config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
        if(!$config || !$config->app_ids)
            JsonHelper::show(array(
                'error' => true,
                'msg' => '你还没有配置微信服务号',
            ));

        $app = new WxQyMsg($config);
        $app->throw_exception = true;
        $app_ids = json_decode($config->app_ids);
        if(!$app_ids->notice)
            JsonHelper::show(array(
                'error' => true,
                'msg' => '你还没有添加微公告应用',
            ));
        $to_user = '@all';
        try {
            $pic_url =  Yii::app()->params['mobile_base'].HtmlHelper::assets('images/shipu/shipu_'.rand(0,2).'.jpg');
            $news = array(
                array(
                    'title' => '食谱更新',
                    'description' => $start.'到'.$to.'的食谱更新啦！~快来看看我们为小宝贝都准备了些什么食物吧',
                    'url' => Yii::app()->params['mobile_base'].'/notice/shipu?start='.$start.'&to='.$to.'&partner_id='.$this->partner_id.'&hide=1',
                    'picurl' => $pic_url,
                )
            );
            $wx_app_id = $app_ids->notice;
            $app->news($to_user, $news, $wx_app_id);
        } catch(WxAppException $e) {
            JsonHelper::show(array(
                'error' => true,
                'msg' => $e->getMessage(),
            ));
        }

    }
    public function actionShipuImport(){
        if($_FILES){
            $config = array(
                'A'=>'date',
                'B'=>'week',
                'C'=>'type1',
                'D'=>'type2',
                'E'=>'type3',
                'F'=>'type4',
                'G'=>'type5',
                'H'=>'type6',
                'I'=>'type7',
                'J'=>'type8',
                'K'=>'type9',
                'L'=>'type10',
            );
            set_time_limit(0);
            try{
                $data = $this->import($config,$_FILES['file']['tmp_name'],count($config));
            }
            catch(Exception $e){
                $this->showMsg($e->getMessage(), 'error');
            }
            //var_dump($data);exit;

            $error = '';
            if(!$data) {
                $error = 'excel文件没有内容，请查证后上传';
            } else {
                $title = array_shift($data);

                if($title['date'] != '日期' || $title['week'] != '星 期' || $title['type1'] == '' )
                {
                    $error = 'excel模板有误，请确保第一行第一列为日期第二列为星期并且第三列不为空';
                }
            }

            if($error)
                $this->showMsg($error, 'error');

            $info = array(
                'error' => 0,
                'success' => 0,
                'tips' => ''
            );

            $models = array();
            //var_dump($data);exit;
            foreach($data as $d) {
                $i=1;
                if(!$d['date']){
                    continue;
                }
                $day = date('Y-m-d',$d['date']);
                for($i;$i<=10;$i++){
                    $type = $d['type'.$i];
                    if(isset($type)){
                        $_title = $title['type'.$i];
                        $shipu = Shipu::model()->findByAttributes(array('partner_id'=>$this->partner_id,'date'=>$day,'type'=>$_title));
                        if(!$shipu){
                            $shipu = new Shipu();
                            $shipu->partner_id = $this->partner_id;
                            $shipu->date = $day;
                            $shipu->type = $_title;
                        }
                        if(!$type){
                            if($shipu->id){
                                $shipu->delete();
                            }
                        }
                        else{
                            $shipu->description = $type ;
                            $shipu->save();
                        }
                    }
                    else{
                        continue;
                    }
                }
            }
            //echo '<pre>';
            //var_dump($data);exit;
            $tips = '导入成功';
            //$tips = '共导入数据：'.($info['success'] + $info['error']).'条 成功：'.$info['success'].'条, 失败：'.$info['error'].'条';
            //if($info['tips']) $tips .= '错误原因：'.$info['tips'];

            $this->showMsg($tips);
        }
        $this->showMsg('请上传模板文件');
    }
    //读取excel数据   $config配置    文件路径
    public function import($config,$filePath,$maxColumn= 100){
        Yii::import('application.extensions.PHPExcel');
        Yii::import('application.extensions.PHPExcel.*');
        $PHPExcel = new PHPExcel();
        /**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
        $PHPReader = new PHPExcel_Reader_Excel2007();
        if(!$PHPReader->canRead($filePath)){
            $PHPReader = new PHPExcel_Reader_Excel5();
            if(!$PHPReader->canRead($filePath)){
                return array();
            }
        }
        $objPHPExcel = $PHPReader->load($filePath) ;
        $currentSheet = $objPHPExcel->getSheet(0);
        /**取得最大的列号*/
        $allColumn = PHPExcel_Cell::columnIndexFromString($currentSheet->getHighestColumn()) - 1;
        $allColumn = $allColumn > $maxColumn?$maxColumn:$allColumn;
        /**取得一共有多少行*/
        $allRow = $currentSheet->getHighestRow();
        //var_dump($allRow,$allColumn);exit;
        $count = 0;
        $data = array();
        for($currentRow = 1;$currentRow <= $allRow;$currentRow++){
            $_data = array();
            //echo '<br>';
            for($currentColumn= 'A';(ord($currentColumn) - 65)<= $allColumn; $currentColumn++){
                $cell = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow);
                $value = $cell->getValue();
                if($cell->getDataType()==PHPExcel_Cell_DataType::TYPE_NUMERIC){
                    $cellstyleformat=$cell->getStyle( $cell->getCoordinate() )->getNumberFormat();

                    $formatcode=$cellstyleformat->getFormatCode();
                    //var_dump($formatcode,$currentColumn);
                    if ( $formatcode == 't0') {
                        $value=PHPExcel_Shared_Date::ExcelToPHP($value);
                    }
                    else if (preg_match('/^(\[\$[A-Za-z]*-[0-9A-Fa-f]*\])*[hmsdy]/i', $formatcode)) {
                        $value=PHPExcel_Shared_Date::ExcelToPHP($value);
                    }else{
                        $value=PHPExcel_Style_NumberFormat::toFormattedString($value,$formatcode);
                    }
                }
                else{
                    $value = (string)$value;
                }
                if(isset($config[$currentColumn])){
                    $_data[ $config[$currentColumn] ] = trim($value);
                }

            }
            if(isset($_data['date']) && $_data['date']!=''){
                if(strpos($_data['date'],'.',1) || strpos($_data['date'],'-',1) || strpos($_data['date'],'/',1)){
                    $_data['date'] = str_replace(array('.','/'),'-',$_data['date']);
                    $_data['date']  = strtotime($_data['date']);
                }
            }
            $data[] = $_data;
        }
        return $data;
    }

}