<?php
class StudentController extends PartnerAdminBaseController {
	private $config = 'init';

	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->addCondition('partner_id = '.$this->partner_id);


		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));


		if($_GET['grade'])
			$criteria->addCondition('grade_id = '.intval($_GET['grade']));

		$count = Student::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = $_GET['per_page']?$_GET['per_page']:10;
		$pager->applyLimit($criteria);

		$criteria->order = 't.id DESC';
		$list = Student::model()->with('parents')->findAll($criteria);


		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);

		$grades = CHtml::listData($grades, 'id', 'name');
		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'grades' => $grades
		));
	}


	public function actionCreate() {

		$model = new Student();


		if($_POST) {
			$model->name = $_POST['name'];
			if(strtotime($_POST['birth']) !== false)
				$model->birth = date('Ym', strtotime($_POST['birth']));

			$model->partner_id = $this->partner_id;
			$model->id_num = $_POST['id_num'];
			$model->register_address = $_POST['register_address'];
			$model->address = $_POST['address'];
			$model->emergency_contact = $_POST['emergency_contact'];
			$model->emergency_phone = $_POST['emergency_phone'];
			$model->grade_id = intval($_POST['grade_id']);
			$model->save();

			if(is_array($_POST['parent_phone'])) {
				foreach($_POST['parent_phone'] as $k=>$p) {
					if(!CommonHelper::validMobile($p))
						continue;

					$contacts = Contacts::model()->find(
						"partner_id = {$this->partner_id} AND parent_phone = :parent_phone",
						array(
							':parent_phone' => $p
						)
					);
					$create = false;
					if(!$contacts) {
						$contacts = new Contacts();
						$contacts->partner_id = $this->partner_id;
						$contacts->parent_phone = $p;
						$contacts->parent_name = $_POST['parent_name'][$k];
						$contacts->type = Contacts::TY_STUDENT;
						$contacts->save();
						$contacts->generateToken();
						$create = true;
					}
					$sp = new StudentParent();
					$sp->student_id = $model->id;
					$sp->parent_id = $contacts->id;
					$sp->save();
                    $departs = $this->getDepartment($contacts);
					if($config = $this->wxConfig()) {
						if($create) {
							$app = new WxQyContacts($config);
							$app->create($contacts, $departs);
                            $app->invite($contacts);
						} else {
							$app = new WxQyContacts($config);
							$department = $departs;//$contacts->leader?array($config->leader_department,$config->teacher_department, $config->student_department):($contacts->type == Contacts::TY_TEACHER ? array($config->teacher_department, $config->student_department) : $config->student_department);
							//$app->update($contacts, $department);
                            try{
                                $updated = $app->update($contacts, $department);
                            }
                            catch(Exception $e){
                                $updated = $app->create($contacts, $department);
                            }
                            if(!$updated){
                                try {
                                    $updated = $app->create($contacts, $department);
                                }
                                catch(Exception $e){

                                }
                            }
						}
					}

				}
			}

			$this->showMsg('操作成功', 'success', 'partner_manage/student');
		}

		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
		$grades = CHtml::listData($grades, 'id', 'name');


		$this->render('form', array(
			'model' => $model,
			'grades' => $grades,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Student::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {
			$model->name = $_POST['name'];
			if(strtotime($_POST['birth']) !== false)
				$model->birth = date('Ym', strtotime($_POST['birth']));

			$model->id_num = $_POST['id_num'];
			$model->register_address = $_POST['register_address'];
			$model->address = $_POST['address'];
			$model->emergency_contact = $_POST['emergency_contact'];
			$model->emergency_phone = $_POST['emergency_phone'];
			$model->grade_id = intval($_POST['grade_id']);
			$model->save();

			if(is_array($_POST['parent_phone'])) {
				$config = $this->wxConfig();
				$old_parents = $model->parents;
				StudentParent::model()->deleteAll("student_id = {$model->id}");
				//删除修改前的联系人
				foreach($old_parents as $p) {
					if(!in_array($p->parent_phone, $_POST['parent_phone'])) {
						if($p->type != Contacts::TY_TEACHER && !$p->students) {
							if($config) {
								$app = new WxQyContacts($config);
								$app->delete($p);
							}
							$p->delete();
						}
					}
				}

				$delete_contacts = trim($_POST['delete_contacts'], ',');
				if($delete_contacts) {
					$delete_contacts = explode(',', $delete_contacts);
					foreach($delete_contacts as $d) {
						$d = intval($d);
						if(!StudentParent::model()->find("parent_id = {$d}")) {
							$deleted = Contacts::model()->findByPk($d);
							if($deleted && $deleted->type != Contacts::TY_TEACHER) {
								if($config) {
									$app = new WxQyContacts($config);
									$app->delete($deleted);
								}
								$deleted->delete();
							}
						}
					}
				}

				foreach($_POST['parent_phone'] as $k=>$p) {
					if(!CommonHelper::validMobile($p)){
                        continue;
                    }

					$contacts = Contacts::model()->find(
						"partner_id = {$this->partner_id} AND parent_phone = :parent_phone",
						array(
							':parent_phone' => $p
						)
					);
					if(!$contacts) {
						$contacts = new Contacts();
						$contacts->partner_id = $this->partner_id;
						$contacts->parent_phone = $p;
						$contacts->parent_name = $_POST['parent_name'][$k];
						$contacts->type = Contacts::TY_STUDENT;
						$contacts->save();
						$contacts->generateToken();
						$create = true;
					} else {
						$create = false;
						$contacts->parent_name = $_POST['parent_name'][$k];
						$contacts->save();
					}
					$sp = new StudentParent();
					$sp->student_id = $model->id;
					$sp->parent_id = $contacts->id;
					$sp->save();
                    $departs = $this->getDepartment($contacts);

					if($create) {
						if($config) {
							$app = new WxQyContacts($config);
							$app->create($contacts, $departs);
                            $app->invite($contacts);
						}
					} else {
						if($config) {
							$app = new WxQyContacts($config);
                            $department = $departs;//$this->getDepartment($contacts);//$contacts->leader?array($config->leader_department,$config->teacher_department, $config->student_department):($contacts->type == Contacts::TY_TEACHER ? array($config->teacher_department, $config->student_department) : $config->student_department);
                            //$app->update($contacts, $department);
                            try{
                                $updated = $app->update($contacts, $department);
                            }
                            catch(Exception $e){
                                $updated = $app->create($contacts, $department);
                            }
                            if(!$updated){
                                try {
                                    $updated = $app->create($contacts, $department);
                                }
                                catch(Exception $e){

                                }
                            }

						}
					}
				}
			}



			$this->showMsg('操作成功', 'success', 'partner_manage/student');
		}



		$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
		$grades = CHtml::listData($grades, 'id', 'name');


		$this->render( 'form', array(
			'model' => $model,
			'grades' => $grades,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Student::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);


		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$config = $this->wxConfig();
		foreach($model->parents as $p) {
			if($p && $p->type != Contacts::TY_TEACHER) {
                $my_students = StudentParent::model()->find("parent_id = {$p->id} AND student_id!={$model->id}");
                if(!$my_students){
                    if($config) {
                        $app = new WxQyContacts($config);
                        $app->delete($p);
                    }
                    $p->delete();
                }
                else{
                    if($config) {
                        $app = new WxQyContacts($config);
                        $app->update($p,$this->getDepartment($p));
                    }
                }

			}

		}

		$model->delete();
		$this->showMsg('操作成功');
	}
    public function actionDelAll() {

        $ids = Yii::app()->request->getParam('ids');
        if(!$ids){
            JsonHelper::show(array('error'=>1,'msg'=>'请至少选择一个学生'));
        }
        $error_count = 0;
        $success_count = 0;
        $del_contacts = array();
        $update_contacts = array();
        $departs = array();
        foreach($ids as $k=>$id){
            $model = Student::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);


            if(!$model){
                $error_count++;
            }


            $config = $this->wxConfig();
            foreach($model->parents as $p) {
                if($p && $p->type != Contacts::TY_TEACHER) {
                    /*if($config) {
                        $app = new WxQyContacts($config);
                        $app->delete($p);
                    }*/
                    $my_students = StudentParent::model()->find("parent_id = {$p->id} AND student_id!={$model->id}");
                    if(!$my_students){
                        $del_contacts[$p->id] = 'sh'.$p->id;
                        $p->delete();
                    }
                    else{
                        $update_contacts[$p->id] = $p;
                    }
                }
                else if($p && $p->type == Contacts::TY_TEACHER){
                    /*if($config) {
                        $app = new WxQyContacts($config);
                        $department = array($config->teacher_department);
                        if($p->leader){
                            $department[] = $config->leader_department;
                        }
                        $app->update($p,$department);
                    }*/
                    $update_contacts[$p->id] = $p;
                }

            }

            $model->delete();
            $success_count++;
        }
        if($success_count<1){
            $error = 1;
            $msg = '删除失败'.$error_count.'个';
        }
        else{
            $error = 0;
            $msg = '删除成功'.$success_count.'个';
            if($error_count>0){
                $msg .= '删除失败'.$error_count.'个';
            }
        }
        if($config) {
            $app = new WxQyContacts($config);
            foreach($update_contacts as $p){
                $departs[$p->id] = $this->getDepartment($p);
            }
            sort($del_contacts);
            if(!empty($del_contacts)){
                try{
                    $app->batchDelete($del_contacts);
                }
                catch(Exception $e){

                }
            }
            if(!empty($update_contacts)){
                try{
                    $app->batchUpdate($this->partner_id,$update_contacts,$departs);
                }
                catch(Exception $e){

                }
            }
        }

        JsonHelper::show(compact('error','msg'));

    }





    private function getDepartment($contact){
        $config = $this->wxConfig();
        if(!$config){
            return array();
        }
        $student_department = $config->student_department;
        $teacher_department = $config->teacher_department;
        $leader_department = $config->leader_department;
        $departs = array();
        if($contact->leader){
            $departs[] = $leader_department;
        }
        if($contact->type==2){
            $departs[] = $teacher_department;
            if(count($contact->students)>0 ){
                //$departs[] = $student_department;
                $students = $contact->students;
                foreach($students as $k=>$v){
                    $departs[] = $config->getGradeDepartment($v->grade_id);
                }
            }

        }
        else{
            //$departs[] = $student_department;
            $students = $contact->students;
            foreach($students as $k=>$v){
                $departs[] = $config->getGradeDepartment($v->grade_id);
            }
        }
        return $departs;
    }
	public function actionImport() {
		if($_FILES){
			$config = array(
				'A'=>'student_name',
				'B'=>'student_birth',
				'C'=>'grade',
				'D'=>'parent_name',
				'E'=>'parent_phone',
				'F'=>'parent_name2',
				'G'=>'parent_phone2',
				'H'=>'emergency_contact',
				'I'=>'emergency_phone',
				'J'=>'register_address',
				'K'=>'address',
				'L'=>'id_num',
			);
            set_time_limit(0);
            try{
                $data = $this->import($config,$_FILES['file']['tmp_name'],count($config));
            }
            catch(Exception $e){
                $this->showMsg($e->getMessage(), 'error');
            }
            //var_dump($data);exit;

			$error = '';
			if(!$data) {
				$error = 'excel文件没有内容，请查证后上传';
			} else {
				$title = array_shift($data);

				if($title['student_name'] != '学生姓名' || $title['student_birth'] != '学生出生年月'
					|| $title['parent_name'] != '家长姓名1' || $title['parent_phone'] != '家长电话1'
					|| $title['parent_name2'] != '家长姓名2' || $title['parent_phone2'] != '家长电话2'
					|| $title['grade'] != '班级')
				{
					$error = 'excel模板有误，请确定你使用的EXCEL为本网站下载链接给出的模板';
				}
			}

			if($error)
				$this->showMsg($error, 'error');

			$grades = Grade::model()->findAll('partner_id = '.$this->partner_id);
			$grades = CHtml::listData($grades, 'id', 'name');
			$grades = array_flip($grades);
			$info = array(
				'error' => 0,
				'success' => 0,
				'tips' => ''
			);

			$need_push_contacts = array();
            $departs = array();

			foreach($data as $d) {
				if(!$d['student_name'] /*|| !$d['parent_name']*/  ) {
					$info['error']++;
                    $info['tips'] .= '<br />'.$d['parent_phone'].'缺少家长电话';
					continue;
				}
                elseif(!$d['parent_phone']){
                    $info['error']++;
                    $info['tips'] .= '<br />'.$d['student_name'].'缺少学生姓名';
                    continue;
                }
                $d['grade'] = trim($d['grade']);
				$grade_id = null;
				if($grades[$d['grade']])
					$grade_id = $grades[$d['grade']];
                if(!$grade_id){
                    $grade = new Grade();
                    $grade->name = $d['grade'];
                    $grade->partner_id = $this->partner_id;
                    $grade->create_time = time();
                    if($grade->save()){
                        $grade_id = $grade->id;
                        $grades[$d['grade']] = $grade_id;
                    }
                }

				//TODO 是否需要限制同名 避免重复导入
				$exists = Student::model()->exists(
					"name = :student_name and grade_id = :grade_id",
					array(
						':student_name' => $d['student_name'],
						':grade_id' => $grade_id,
					)
				);

				if($exists) {
					$info['error']++;
					$info['tips'] .= '<br />'.$d['student_name'].'已经存在';
					continue;
				}

				$model = new Student();
				$model->name = $d['student_name'];
                if($d['student_birth']){
                    $d['student_birth'] = trim(date('Y-m-d',$d['student_birth']) );
                    if(strtotime($d['student_birth']) !== false){
                        $model->birth = date('Ym', strtotime($d['student_birth']));
                    }
                    else{
                        $model->birth = '';
                    }

                }
                else{
                    $model->birth = '';
                }

                //$info['tips'] .= $d['student_birth'];

				$model->partner_id = $this->partner_id;
				$model->id_num = $d['id_num'];
				$model->register_address = $d['register_address'];
				$model->address = $d['address'];
				$model->emergency_contact = $d['emergency_contact'];
				$model->emergency_phone = $d['emergency_phone'];
				$model->grade_id = $grade_id;
				$model->save();



                $contact = $this->createParent($d['parent_phone'], $d['parent_name'], $model->id);
                $departs[$contact->id] = $this->getDepartment($contact);
                $need_push_contacts[$contact->id] = $contact;
				if($d['parent_phone2'] && $d['parent_name2']) {
                    $contact = $need_push_contacts[] = $this->createParent($d['parent_phone2'], $d['parent_name2'], $model->id);
                    $departs[$contact->id] = $this->getDepartment($contact);
                    $need_push_contacts[$contact->id] = $contact;
				}
				$info['success']++;
			}
            if(!empty($need_push_contacts)){
                $config = $this->wxConfig();
                if($config){
                    $app = new WxQyContacts($config);
                    $app->throw_exception = true;
                    try{
                        $app->batchUpdate($this->partner_id,$need_push_contacts ,$departs);
                    }
                    catch(Exception $e){
                        $info['tips'] .= '同步通讯录出现问题，请重新同步';
                    }
                }

            }
			$tips = '共导入数据：'.($info['success'] + $info['error']).'条 成功：'.$info['success'].'条, 失败：'.$info['error'].'条';
			if($info['tips']) $tips .= '错误原因：'.$info['tips'];

			$this->showMsg($tips);
		}
        $this->showMsg('请上传模板文件');
	}
    public function actionRefreshContact(){
        $config = $this->wxConfig();
        if(!$config){
            $this->showMsg('该学校没有配置企业号哦');
        }
        $student_department = $config->student_department;
        $teacher_department = $config->teacher_department;
        $leader_department = $config->leader_department;

        $contacts = Contacts::model()->findAllByAttributes(array('partner_id'=>$this->partner_id));
        //$br = "\n";
        //$txt ='姓名,帐号,微信号,手机号,邮箱,所在部门,职位'.$br;
        $departments = array();
        foreach($contacts as $k=>$v){
            $departs = $this->getDepartment($v);
            /*if($v->leader){
                $departs[] = $leader_department;
            }
            if($v->type==2){
                $departs[] = $teacher_department;
                if(count($v->students)>0 ){
                    $departs[] = $student_department;
                }

            }
            else{
                $departs[] = $student_department;
            }*/
            $departments[$v->id] = $departs;
        }
        //$wx_config = (Object)Yii::app()->params['wx_payment_qy'];
        $wechat_config = WechatConfig::getSingleInstance($this->partner_id);
        //$wx_config->access_token  = $wechat_config->access_token;//'bNU-4Jhs812YZMC1ZjjRPls-iVxO2jJIIefWv61VUheq73Vfro6uWEWRMe5bGwkjrlhiDo0GO10_bRuk1m7YTg';//WxQyUser::getAccessToken($wx_config)->access_token;
        $app = new WxQyContacts($wechat_config);
        $app->throw_exception = true;
        try{
            $error = 'success';
            $msg = '同步任务已添加成功,会有几分钟的延迟.请几分钟后查看企业号后台是否同步完成';
            $app->batchUpdate($this->partner_id,$contacts ,$departments);
        }
        catch(Exception $e){
            $error = 'error';
            $msg = $e->getMessage();
        }

        $this->showMsg($msg,$error);

    }
    private function name($ww_contacts) {
        if($ww_contacts->type == Contacts::TY_STUDENT) {
            $students = '';
            foreach($ww_contacts->students as $s) {
                $students .= ','.$s->name;
            }

            return trim($students, ',');
        } else {
            return $ww_contacts->parent_name;
        }

        //return $ww_contacts->type == Contacts::TY_STUDENT ? $ww_contacts->student_name : $ww_contacts->parent_name;
    }
	private function createParent($parent_phone, $parent_name, $student_id) {
		$contacts = Contacts::model()->find(
			"partner_id = {$this->partner_id} AND parent_phone = :parent_phone",
			array(
				':parent_phone' => $parent_phone
			)
		);
		$create = false;
		if(!$contacts) {
			$contacts = new Contacts();
			$contacts->partner_id = $this->partner_id;
			$contacts->parent_phone = $parent_phone;
			$contacts->parent_name = $parent_name;
			$contacts->type = Contacts::TY_STUDENT;
			$contacts->save();
			$contacts->generateToken();
			$create = true;
		}
		$sp = new StudentParent();
		$sp->student_id = $student_id;
		$sp->parent_id = $contacts->id;
		$sp->save();
        return $contacts;
		/*if($config = $this->wxConfig()) {
			if($create) {
				$app = new WxQyContacts($config);
				$app->create($contacts, $config->student_department);
				$app->invite($contacts);
			} else {
				$app = new WxQyContacts($config);
				$department = $contacts->type == Contacts::TY_TEACHER ? array($config->teacher_department, $config->student_department) : $config->student_department;
				$app->update($contacts, $department);
			}
		}*/
	}


	//读取excel数据   $config配置    文件路径
	public function import($config,$filePath,$maxColumn= 100){
		Yii::import('application.extensions.PHPExcel');
		Yii::import('application.extensions.PHPExcel.*');
		$PHPExcel = new PHPExcel();
		/**默认用excel2007读取excel，若格式不对，则用之前的版本进行读取*/
		$PHPReader = new PHPExcel_Reader_Excel2007();
		if(!$PHPReader->canRead($filePath)){
			$PHPReader = new PHPExcel_Reader_Excel5();
			if(!$PHPReader->canRead($filePath)){
				return array();
			}
		}
		$objPHPExcel = $PHPReader->load($filePath) ;
		$currentSheet = $objPHPExcel->getSheet(0);
		/**取得最大的列号*/
		$allColumn = PHPExcel_Cell::columnIndexFromString($currentSheet->getHighestColumn()) - 1;
		$allColumn = $allColumn > $maxColumn?$maxColumn:$allColumn;
		/**取得一共有多少行*/
		$allRow = $currentSheet->getHighestRow();
        //var_dump($allRow,$allColumn);exit;
		$count = 0;
		$data = array();
		for($currentRow = 1;$currentRow <= $allRow;$currentRow++){
			$_data = array();
            //echo '<br>';
			for($currentColumn= 'A';(ord($currentColumn) - 65)<= $allColumn; $currentColumn++){
				$cell = $currentSheet->getCellByColumnAndRow(ord($currentColumn) - 65,$currentRow);
				$value = $cell->getValue();
				if($cell->getDataType()==PHPExcel_Cell_DataType::TYPE_NUMERIC){
					$cellstyleformat=$cell->getStyle( $cell->getCoordinate() )->getNumberFormat();

					$formatcode=$cellstyleformat->getFormatCode();
                    //var_dump($formatcode,$currentColumn);
                    if ( $formatcode == 't0') {
                        $value=PHPExcel_Shared_Date::ExcelToPHP($value);
                    }
					else if (preg_match('/^(\[\$[A-Za-z]*-[0-9A-Fa-f]*\])*[hmsdy]/i', $formatcode)) {
						$value=PHPExcel_Shared_Date::ExcelToPHP($value);
					}else{
						$value=PHPExcel_Style_NumberFormat::toFormattedString($value,$formatcode);
					}
				}
				else{
					$value = (string)$value;
				}
				if(isset($config[$currentColumn])){
					$_data[ $config[$currentColumn] ] = trim($value);
				}

			}
            if(isset($_data['student_birth']) && $_data['student_birth']!=''){
                if(strpos($_data['student_birth'],'.',1) || strpos($_data['student_birth'],'-',1) || strpos($_data['student_birth'],'/',1)){
                    $_data['student_birth'] = str_replace(array('.','/'),'-',$_data['student_birth']);
                    $_data['student_birth']  = strtotime($_data['student_birth']);
                }
                else if(strlen($_data['student_birth'])==8){
                    $_data['student_birth'] = substr($_data['student_birth'],0,4).'-'.substr($_data['student_birth'],4,2).'-'.substr($_data['student_birth'],6,2);
                    $_data['student_birth']  = strtotime($_data['student_birth']);}
                else if(strlen($_data['student_birth'])==6){
                    $_data['student_birth'] = substr($_data['student_birth'],0,4).'-'.substr($_data['student_birth'],4,2).'-01';
                    $_data['student_birth']  = strtotime($_data['student_birth']);
                }
            }
            if(isset($_data['student_birth']) && $_data['student_birth']===false){
                //throw new Exception('请检查文件中生日格式是否正确,如2014-08-05',404);
            }
			$data[] = $_data;

		}
        //var_dump($data);exit;
		return $data;
	}


	public function wxConfig() {
		if($this->config != 'init') {
			return $this->config;
		}
		$ww_apps = CommonHelper::qyApp();
		$this->config = WechatConfig::getInstance($this->partner_id, $ww_apps['notice']['ww_suite_id']);
		return $this->config;
	}
}