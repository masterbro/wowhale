<?php
class OrderController extends PartnerAdminBaseController {

    public function actionList(){
        $criteria = new CDbCriteria();
        $grade_id = (int) Yii::app()->request->getParam('grade_id');
        $product_id = (int) Yii::app()->request->getParam('product_id');
        if(!$this->partner){
            $this->showError('notice_404');
        }
        $partner_id = (int) $this->partner_id;

        $criteria->addCondition('partner_id = '.$partner_id);
        $condition_array = array('partner_id'=>$partner_id);

        $products = Product::model()->findAllByAttributes(array('partner_id'=>$partner_id));
        $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$partner_id));
        if($product_id)  {
            $product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));
        }
        elseif($products && $products[0]){
            $product = $products[0];
        }
        if($grade_id){
            $condition_array['grade_id'] = $grade_id;
            $criteria->addCondition('grade_id='.$grade_id);
        }
        $payed = Yii::app()->request->getParam('payed');
        if( $product && $payed!=0){
            //$criteria->join = " LEFT JOIN ".Order::model()->tableName()." t_order ON t_order.student_id=t.id LEFT JOIN ".OrderDetail::model()->tableName()." order_detail ON order_detail.order_id=t_order.id AND ".'order_detail.pid='.$product->id;
            $order_str = "SELECT t_order.*,order_detail.pid as pid from ".Order::model()->tableName()." t_order LEFT JOIN  ".OrderDetail::model()->tableName()." order_detail ON order_detail.order_id=t_order.id ".' where order_detail.pid='.$product->id;
            //$criteria->addCondition('order_detail.pid='.$product->id);
            $criteria->select = "t_order.id as order_id,t.*";
            $criteria->join = " LEFT JOIN ( {$order_str}) as t_order ON t_order.student_id=t.id ";
            if($payed==1){
                $criteria->addCondition('t_order.payment_status=1 AND t_order.pid is not null');
            }
            else if($payed==2){
                $criteria->addCondition('( (t_order.payment_status=0  AND t_order.pid is not null)  OR t_order.payment_status is null)');
            }
        }
        if($product){
            $product_id = $product->id;
        }

        $student_count = Student::model()->countByAttributes($condition_array);
        $pager = new CPagination($student_count);
        $pager->pageSize = 20;
        $pager->applyLimit($criteria);
        $students = Student::model()->findAll($criteria);
        $this->render('list',compact('payed','products','grades','product','student_count','students','grade_id','product_id','pager'));
    }
    public function actionUnOnlineOrder(){
        $product_id = (int) Yii::app()->request->getParam('product_id');
        $grade_id = (int) Yii::app()->request->getParam('grade_id');
        $student_id = (int) Yii::app()->request->getParam('student_id');
        $order_type = (int) Yii::app()->request->getParam('order_type');//0取消缴费 1缴费
        if(!$this->partner){
            $this->showError('notice_404');
        }
        $partner_id = (int) $this->partner_id;
        $product = Product::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$product_id));

        if(!$product){
            //$this->showMsg('缴费项目不存在');
            JsonHelper::show(array(
                'error' => true,
                'msg' => '缴费项目不存在',
            ));
        }
        $grade = Grade::model()->findByAttributes(array('partner_id'=>$partner_id,'id'=>$grade_id));
        if(!$grade){
            //$this->showMsg('缴费班级不存在');
            JsonHelper::show(array(
                'error' => true,
                'msg' => '缴费班级不存在',
            ));
        }
        $student = Student::model()->findByPk($student_id);
        if(!$student){
            //$this->showMsg('学生不存在');
            JsonHelper::show(array(
                'error' => true,
                'msg' => '学生不存在',
            ));
        }
        $student_order = $student->getIsOrdered($product_id);
        if($order_type==1){
            if($student_order && $student_order->payment_status>0){
                //$this->showMsg('此学生已缴费');
                JsonHelper::show(array(
                    'error' => true,
                    'msg' => '此学生已缴费',
                ));
            }
        }
        else{
            if(!$student_order || $student_order->payment_status==0){

                //$this->showMsg('此学生并没有缴费');
                JsonHelper::show(array(
                    'error' => true,
                    'msg' => '此学生并没有缴费',
                ));
            }
            else if($student_order && $student_order->payment_type==1){
                //$this->showMsg('在线缴费项目不能取消缴费');
                JsonHelper::show(array(
                    'error' => true,
                    'msg' => '在线缴费项目不能取消缴费',
                ));
            }
        }

        if(!$student_order){
            $info = array();
            $info['student_name'] = $student->name;
            $info['parent_name'] = $student->emergency_contact;
            $info['tel'] = $student->emergency_phone;
            $info['birth'] = $student->birth;
            $info['student_grade'] = $grade->name;
            $student_order = new Order();
            //$order->uid = $this->current_user->id;
            $student_order->school_id = $product->partner_id;
            $student_order->student_id = $student_id;
            if($student->parents && $student->parents[0]){
                $student_order->contacts_id = $student->parents[0]->id;
            }
            $student_order->student_id = $student_id;
            $student_order->total = $product->price;
            $student_order->begin_time = time();
            $student_order->payment_type = Order::UnOnline;
            $student_order->shipping_info = json_encode($info);
            //if($_POST['remark'])
            //	$student_order->remark = $_POST['remark'];

            $student_order->save();
            $order_detail = new OrderDetail();
            $order_detail->order_id = $student_order->id;
            $order_detail->pid = $product->id;
            $order_detail->title = $product->name;
            $order_detail->product_attr_ids = '';
            $order_detail->qty = 1;
            $order_detail->price = $product->price;
            $order_detail->save();
            //$student_order->generateDisplayId();
        }
        if($order_type==1){
            $student_order->payment_status=1;
        }
        else{
            $student_order->payment_status=0;
        }
        $student_order->save();

        //$this->showMsg('支付状态切换成功');
        JsonHelper::show(array(
            'error' => false,
            'msg' => '支付状态切换成功',
        ));
    }
	public function actionIndex() {
		$criteria = new CDbCriteria();

		$partner = $this->getPartner();

		$criteria->addCondition('school_id = '.$this->partner_id);

		if($_GET['start'] && $_GET['to']) {
			$start = strtotime($_GET['start']);
			$end = strtotime($_GET['to']) + 86399;
			$criteria->addCondition("create_time > {$start} and create_time < {$end}");
		}

		if($_GET['id']) {
			$criteria->addCondition('display_id = :display_id');
			$criteria->params[':display_id'] = $_GET['id'];
		}

		if($_GET['trans_id']) {
			$criteria->addCondition('payment_transaction_id = :payment_transaction_id');
			$criteria->params[':payment_transaction_id'] = $_GET['trans_id'];
		}

		if($_GET['export']) {
			$criteria->order = 'begin_time DESC';
			$list = Order::model()->findAll($criteria);
			$data = array();
            $contacts = array();
            $grades = array();
			foreach($list as $l) {
				$shipping_info = $l->shippingInfo();
				$order_details = '';

				foreach($l->order_details as $lk=>$lo) {
					if($lk) $order_details .= '<br />';
					$order_details .=  $lo->title. '￥'.CommonHelper::price($lo->price).'x'.$lo->qty.'';
				}
                $student = $l->student;
                if($student && $student->name){
                    $shipping_info['student_name'] = $student->name;
                }
                $show_old_data = true;
                if($l->contacts_id){
                    if(!$contacts[$l->contacts_id]){
                        $contacts[$l->contacts_id] = Contacts::model()->findByPk($l->contacts_id);
                    }
                    if($contacts[$l->contacts_id] && $contacts[$l->contacts_id]->parent_phone){
                        $shipping_info['tel'] = $contacts[$l->contacts_id]->parent_phone;
                        $show_old_data = false;
                    }
                }
                if($student && $show_old_data && $student->parents&& $student->parents[0]&& $student->parents[0]->parent_phone){
                    $shipping_info['tel'] = $student->parents[0]->parent_phone;
                    $show_old_data = false;
                }
                if($student){
                    if(!$grades[$student->grade_id]){
                        $grades[$student->grade_id] = Grade::model()->findByPk($student->grade_id);
                    }
                    if($grades[$student->grade_id] && $grades[$student->grade_id]->name){
                        $shipping_info['student_grade'] = $grades[$student->grade_id]->name;
                        $show_old_data = false;
                    }
                }
				$data[] = array(
                    $l->import_from ? '微小店导入' : ($l->payment_type==2?'线下支付':$l->display_id),
                    $l->payment_status?'已支付':'未支付',
					$order_details, '支付状态', '￥'.CommonHelper::price($l->total),
					$shipping_info['student_name'],
					$shipping_info['tel'],
					$partner->type ? CommonHelper::age($shipping_info['birth']) : $shipping_info['student_grade'],
					"&nbsp;".$l->payment_transaction_id,
					date('y/m/d H:i:s', $l->begin_time)
				);
			}

			$title = array(
				'订单号', '订单内容', '订单金额',
				'姓名', '电话', $partner->type ? '年龄' : '班级', '支付单号', '创建时间'
			);

			HtmlHelper::exportExcel($data, $title, "订单列表.xls");
		} else {
			$count = Order::model()->count($criteria);
			$pager = new CPagination($count);
			$pager->pageSize = 20;
			$pager->applyLimit($criteria);

			$criteria->order = 'begin_time DESC';
			$list = Order::model()->findAll($criteria);


			$this->render('index', array(
				'list' => $list,
				'pager' => $pager,
				'partner' => $partner,
			));
		}


	}

}