<?php
class AlbumsController extends PartnerAdminBaseController {


	public function actionIndex() {

		$criteria = new CDbCriteria();
        $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$this->partner_id));
        $grade_id = (int)Yii::app()->request->getParam('grade_id');
        //$start = (int)Yii::app()->request->getParam('start');
        //$to = (int)Yii::app()->request->getParam('to');

		$criteria->addCondition('partner_id = '.$this->partner_id);
        if($grade_id){
            $criteria->addCondition('grade_id = '.$grade_id);
        }
        if($_GET['start']  ) {
            $start = strtotime($_GET['start']);
            $criteria->addCondition("create_time > {$start}");
        }
        if($_GET['to']){

            $end = strtotime($_GET['to']) + 86399;
            $criteria->addCondition("create_time < {$end}");
        }

		$count = Albums::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = ' id DESC';
		$list = Albums::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
            'grade_id'=>$grade_id,
            'grades'=>$grades
		));
	}


	public function actionCreate() {

		if(!$_POST['title'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '标题不能为空',
			));


		$model = new Album();
		$model->partner_id = $this->partner_id;
		$model->title = $_POST['title'];
		$model->save();

		JsonHelper::show(array(
			'error' => false,
			'msg' => '添加成功',
		));
	}

	public function actionUpdate() {

		$id = intval($_POST['id']);
		if(!$id)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		$model = Albums::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '数据不存在，或者已被删除',
			));

		if(!$_POST['title'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '标题不能为空',
			));

		$model->title = $_POST['title'];
		$model->save();

		JsonHelper::show(array(
			'error' => false,
			'msg' => '编辑成功',
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Albums::model()->find("id = {$id} AND partner_id = {$this->partner_id}");

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}
    public function actionDelAll() {

        $ids = Yii::app()->request->getParam('ids');
        if(!$ids){
            JsonHelper::show(array('error'=>1,'msg'=>'数据不存在，或者已被删除'));
        }
            //$this->showMsg('数据不存在，或者已被删除', 'error');
        if(!is_array($ids)||empty($ids)){
            JsonHelper::show(array('error'=>1,'msg'=>'数据不存在，或者已被删除'));
        }
        $msg = '';
        $none_del_count = 0;
        foreach($ids as $k=>$v){
            $model = Albums::model()->find("id = {$v} AND partner_id = {$this->partner_id}");
            if(!$model){
                $none_del_count++;
                continue;
            }
            $model->delete();
        }
        if($none_del_count){
            $msg = '删除成功,但有'.$none_del_count.'条数据未找到';
        }
        else{
            $msg = '删除成功';
        }
        JsonHelper::show(array('error'=>0,'msg'=>$msg));
    }

}