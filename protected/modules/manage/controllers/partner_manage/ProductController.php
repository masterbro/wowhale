<?php
class ProductController extends PartnerAdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		$criteria->addCondition('partner_id = '.$this->partner_id);

		if($_GET['st'] == 'normal') {
			$criteria->addCondition('status = 1');
		} else if($_GET['st'] == 'disable'){
			$criteria->addCondition('status = 0');
		}

		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));

		$count = Product::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'status DESC, sort_num DESC, id DESC';
		$list = Product::model()->findAll($criteria);


		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
		));
	}


	public function actionCreate() {

		$model = new Product();
		$partner = $this->getPartner();

		if($_POST) {
			$name = trim($_POST['name']);
			if(!$name)
				$this->showMsg('产品名称不能为空', 'error');


			$model->name = $name;
			$model->partner_id = $this->partner_id;
			$model->price = intval($_POST['price'] * 100);
			$model->original_price = $_POST['original_price'] ? intval($_POST['original_price'] * 100) : NULL;
			if($_POST['cid'])
				$model->cid = intval($_POST['cid']);

			if($partner->type) {
				$error = false;
				try{
					$img = UploadHelper::image('', 'files', array(480, 320), 'products', true, false, 'resize');
				} catch(Exception $e) {
					$error = true;
				}

				if(!$error) $model->image = $img;
				$model->content = $_POST['content'];
			}

			$model->save();


			$attr_parent = $_POST['attr_parent'] ? $_POST['attr_parent'] : array();
			foreach($attr_parent as $k=>$v) {
				if(!$v) continue;

				$p_attr = new ProductAttr();
				$p_attr->fid = 0;
				$p_attr->product_id = $model->id;
				$p_attr->name = $v;
				$p_attr->save();

				$sub_attr_name = $_POST['attr_name'][$k];
				$sub_attr_change_price = $_POST['attr_change_price_val'][$k];
				$sub_attr_price = $_POST['attr_price'][$k];

				$sub_attr_name = $sub_attr_name ? $sub_attr_name : array();
				foreach($sub_attr_name as $sk=>$sv) {
					if(!$sv) continue;

					$s_attr = new ProductAttr();
					$s_attr->fid = $p_attr->id;
					$s_attr->product_id = $model->id;
					$s_attr->name = $sv;
					if($sub_attr_change_price[$sk] == 1) {
						$tmp_price = intval($sub_attr_price[$sk] * 100);
						$s_attr->price =  $model->price + $tmp_price <= 0 ? NULL : $tmp_price;
					} else {
						$s_attr->price =  NULL;
					}

					$s_attr->save();
				}
			}

			$this->showMsg('操作成功', 'success', 'partner_manage/product/index');
		}


		$this->render($partner->type ? 'form_merchant' : 'form', array(
			'model' => $model,
			'schools' => School::all(),
			'category' => Category::all()
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Product::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$partner = $this->getPartner();

		if($_POST) {
			$model->name = $_POST['name'];
			$model->price = intval($_POST['price'] * 100);
			$model->original_price = $_POST['original_price'] ? intval($_POST['original_price'] * 100) : NULL;
			if($_POST['cid'])
				$model->cid = intval($_POST['cid']);

			if($partner->type) {
				$error = false;
				try{
					$img = UploadHelper::image('', 'files', array(480, 320), 'products', true, false, 'resize');
				} catch(Exception $e) {
					$error = true;
				}

				if(!$error) $model->image = $img;
				$model->content = $_POST['content'];
			}
			$model->save();


			$attr_parent = $_POST['attr_parent'] ? $_POST['attr_parent'] : array();
			$remove_attr = trim($_POST['remove_attr'], ',');
			foreach($attr_parent as $k=>$v) {
				if(!trim($v)) continue;

				if($_POST['attr_id'][$k]) {
					$p_attr = ProductAttr::model()->find('id = '.intval($_POST['attr_id'][$k]).' AND product_id = '.$model->id);
					//if(!$p_attr) continue;
				} else {
					$p_attr = new ProductAttr();
				}

				$p_attr->fid = 0;
				$p_attr->product_id = $model->id;
				$p_attr->name = $v;
				$p_attr->save();

				$sub_attr_name = $_POST['attr_name'][$k];
				$sub_attr_change_price = $_POST['attr_change_price_val'][$k];
				$sub_attr_price = $_POST['attr_price'][$k];
				$sub_id = $_POST['attr_sub_id'][$k];


				if($remove_attr) {
					//var_dump(  $remove_attr );exit;
					$remove_attr = $remove_attr . '';//奇怪的问题 var dump的时候明明是字符串到下一行就会变成array 此处强制转换
					$remove_attr = explode(',', $remove_attr);
					$ids = array();
					foreach($remove_attr as $r) {
						$id = intval($r);
						if($id) $ids[] = $r;
					}
					$ids = implode(',', $ids);
					if($ids){
						$attr = ProductAttr::model()->findAll('product_id = '.$model->id.' AND id IN ('.$ids.')');
						foreach($attr as $a) {
							if($a) $a->delete();
						}
					}
				}
				if(!$sub_attr_name)$sub_attr_name = array();
				foreach($sub_attr_name as $sk=>$sv) {
					if(!trim($sv)) continue;

					if($sub_id[$sk]) {
						$s_attr = ProductAttr::model()->find('id = '.intval($sub_id[$sk]).' AND product_id = '.$model->id);
						//if(!$s_attr) continue;
					} else {
						$s_attr = new ProductAttr();
					}

					$s_attr->fid = $p_attr->id;
					$s_attr->product_id = $model->id;
					$s_attr->name = $sv;
					if($sub_attr_change_price[$sk] == 1) {
						$tmp_price = intval($sub_attr_price[$sk] * 100);
						$s_attr->price =  $model->price + $tmp_price <= 0 ? NULL : $tmp_price;
					} else {
						$s_attr->price =  NULL;
					}

					$s_attr->save();
				}
			}
			if(empty($attr_parent)){
				if($remove_attr) {
					$remove_attr = explode(',', $remove_attr);
					$ids = array();
					foreach($remove_attr as $r) {
						$id = intval($r);
						if($id) $ids[] = $r;
					}
					$ids = implode(',', $ids);
					if($ids){
						$attr = ProductAttr::model()->findAll('product_id = '.$model->id.' AND id IN ('.$ids.')');
						foreach($attr as $a) {
							if($a) $a->delete();
						}
					}
				}
			}


			$this->showMsg('编辑成功', 'success', 'partner_manage/product/index');
		}

		$this->render($partner->type ? 'form_merchant' : 'form', array(
			'model' => $model,
			'schools' => School::all(),
			'category' => Category::all()
		));
	}


	public function actionStatus($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Product::model()->find('id = '.$id.' AND partner_id = '.$this->partner_id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->status = $model->status ? 0 : 1;
		$model->save();

		$this->showMsg('操作成功');
	}
}