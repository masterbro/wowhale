<?php
class RoleController extends AdminBaseController {
	public function actionIndex() {
		$auth = Yii::app()->authManager;

		$this->render('index', array(
			'roles' => $auth->getRoles(),
			'operations' => $auth->getOperations()
		));
	}


	public function actionCreate() {
		if(!$_POST['operations'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '至少选择一个权限',
			));

		$description = trim($_POST['description']);
		if(!$description)
			JsonHelper::show(array(
				'error' => true,
				'msg' => '角色名称不能为空',
			));


		$auth = Yii::app()->authManager;
		$roles = $auth->getRoles();

		foreach($roles as $r) {
			if($r->description == $description)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '角色名称已经存在',
				));
		}

		$role=$auth->createRole(md5(uniqid('role_name')), $description);

		foreach($_POST['operations'] as $o)
			$role->addChild($o);


		JsonHelper::show(array(
			'error' => false,
			'msg' => '添加成功',
		));
	}

	public function actionUpdate() {
		$role_id = $_GET['id'];

		$auth = Yii::app()->authManager;

		$role = $auth->getAuthItem($role_id);

		if(!$role) $this->showMsg('数据不存在或者已被删除', 'error');


		if($_POST) {
			$description = trim($_POST['description']);
			if(!$description)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '角色名称不能为空',
				));

			if($role->description != $description) {
				Manager::model()->updateAll(array('role_name' => $description), 'role_id=:role_id',array(':role_id'=> $role->name));

				$role->description = $description;
				$auth->saveAuthItem($role);
			}

			$children = $auth->getItemChildren($role->name);
			$old = array();
			foreach($children as $c)
				$old[] = $c->name;

			$new = $_POST['operations'] ? $_POST['operations'] : array();

			$create = array_diff($new, $old);
			$delete = array_diff($old, $new);

			foreach($create as $co)
				$auth->addItemChild($role->name, $co);

			foreach($delete as $do)
				$auth->removeItemChild($role->name, $do);


			JsonHelper::show(array(
				'error' => false,
				'msg' => '编辑成功',
			));
		}


		$this->render('update', array(
			'roles' => $auth->getRoles(),
			'operations' => $auth->getOperations(),
			'model' => $role,
		));
	}


	public function actionDelete() {
		$role_id = $_GET['id'];

		$auth = Yii::app()->authManager;

		$role = $auth->getAuthItem($role_id);

		if($role) {
			$auth->removeAuthItem($role->name);
			Manager::model()->updateAll(array('role_id' => null, 'role_name' => '无'), 'role_id=:role_id',array(':role_id'=> $role->name));
		}

		$this->showMsg('删除成功');
	}


	public function actionInit() {

		$auth = Yii::app()->authManager;

		$operations = $auth->getOperations();
		$op = array();
		foreach($operations as $o)
			$op[] = $o->name;

		foreach(PermissionsHelper::rules() as $p) {
			foreach($p['rule'] as $rk=>$rv) {
				if(!in_array($rk, $op))
					$auth->createOperation(strtolower($rk), $rv);
			}
		}


		echo 'success';
	}
}