<?php
class SchoolController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($_GET['st'] == 'normal') {
			$criteria->addCondition('is_deleted = 0');
		} else if($_GET['st'] == 'deleted'){
			$criteria->addCondition('is_deleted = 1');
		}

		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));


		if($scope = $this->partner_scope('id'))
			$criteria->addCondition($scope);


		$count = School::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'is_deleted, id DESC';
		$list = School::model()->findAll($criteria);



		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
		));
	}


	public function actionCreate() {

		$model = new School();


		if($_POST) {
			$name = trim($_POST['name']);
			if(!$name)
				$this->showMsg('校名不能为空', 'error');

			if(Partner::model()->findByAttributes(array('name' => $name)))
				$this->showMsg('学校 <b>'. $name. '</b> 已存在', 'error');


			$model->name = $name;
			$model->tel = $_POST['tel'];

			$model->contacts = $_POST['contacts'];
			$model->save();

			$this->showMsg('操作成功', 'success', 'school/index');
		}


		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = School::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$wx_payment = WxPayment::getInstance($id);

		if($_POST) {
			$pay_to = in_array($_POST['pay_to'], Partner::$pay_to_key) ? $_POST['pay_to'] : 'system';

			$model->name = $_POST['name'];
			$model->tel = $_POST['tel'];
			$model->contacts = $_POST['contacts'];
			$model->pay_to = $pay_to;
			$model->save();

			if($pay_to != 'system' && $pay_to != 'system_fuwu') {
				$wx_payment->app_id = $_POST['app_id'];
				$wx_payment->app_secret = $_POST['app_secret'];
				$wx_payment->key = $_POST['key'];
				$wx_payment->mchid = $_POST['mchid'];
				$wx_payment->save();
			}


			$this->showMsg('编辑成功', 'success', 'school/index');
		}

		$this->render('form', array(
			'model' => $model,
			'wx_payment' => $wx_payment,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = School::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->is_deleted = $model->is_deleted ? 0 : 1;
		$model->save();

		$this->showMsg('操作成功');
	}

	public function actionMobile($id) {
		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = School::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');


		if($_POST) {
			$model->title = $_POST['title'];
			$model->content = $_POST['content'];
			$model->contact_tel = $_POST['contact_tel'];
			$slider = array();
			foreach($_POST['slider'] as $s) {
				$slider[] = UploadHelper::moveTmp($s, 'school/'.$model->id.'/slider/');
			}

			$model->slider = json_encode($slider);

			$model->save();

			$this->showMsg('操作成功');
		}


		$this->render('mobile', array(
			'model' => $model,
		));
	}


	public function actionApply() {
		$criteria = new CDbCriteria();


		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));

		$criteria->addCondition("type = 0");

		$count = PartnerApply::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'status DESC, id DESC';
		$list = PartnerApply::model()->findAll($criteria);

		$this->render('apply', array(
			'list' => $list,
			'pager' => $pager,
		));
	}
    public function actionResetApp($id){
        $id = intval($id);
        if(!$id)
            $this->showMsg('数据不存在，或者已被删除', 'error');
        $partner = Partner::model()->findByPk($id);
        //$config = WechatConfig::getInstance($id, 1);
        //$config2 = WechatConfig::getInstance($id, 2);
        $config3 = WechatConfig::getInstance($id, 3);
        //$config2 = WechatConfig::model()->find("partner_id = {$id} AND ww_suite_id = 2");
        $app = CommonHelper::qyApp($partner->id);
        $msgs = array();
        $app_config3 = new WxQyApp($config3);
        $app_config3->throw_exception = true;
        if(!$config3){
            $this->showMsg('学校还未绑定企业号', 'error');
        }
        if($config3){
            foreach($config3->appIdsObj as $key=>$v){
                $oauth_app = $app[$key];
                try{
                    $app_config3->menu($oauth_app['menu'], $v);
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'重置自定义菜单：'.$e->getMessage();
                }
                try{
                    $app_config3->appSetting(array(
                        'agentid' => $v,
                        'report_location_flag' => 0,
                        'isreportenter' =>1
                    ));
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'设置属性：'.$e->getMessage();
                }
            }
        }
        /*
        if(!$config && !$config2){
            $this->showMsg('学校还未绑定企业号', 'error');
        }
        if($config){
            foreach($config->appIdsObj as $key=>$v){
                $oauth_app = $app[$key];
                try{
                    $app_config->menu($oauth_app['menu'], $v);
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'重置自定义菜单：'.$e->getMessage();
                }
                try{
                    $app_config->appSetting(array(
                        'agentid' => $v,
                        'report_location_flag' => 0,
                        'isreportenter' =>1
                    ));
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'设置属性：'.$e->getMessage();
                }
            }
        }

        $app_config2 = new WxQyApp($config2);
        $app_config2->throw_exception = true;
        if($config2){
            foreach($config2->appIdsObj as $key=>$v){
                $oauth_app = $app[$key];
                try{
                    $app_config2->menu($oauth_app['menu'], $v);
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'重置自定义菜单：'.$e->getMessage();
                }
                try{
                    $app_config2->appSetting(array(
                        'agentid' => $v,
                        'report_location_flag' => 0,
                        'isreportenter' =>1
                    ));
                }
                catch(Exception $e){
                    $msgs[] = $oauth_app['name'].'设置属性：'.$e->getMessage();
                }
            }
        }
        */
        if(count($msgs)<1){
            $this->showMsg('更新成功');
        }
        else{
            $this->showMsg(implode(',',$msgs), 'error');

        }


    }
    public function actionApplyStatus($id) {
        $id = intval($id);
        if(!$id)
            $this->showMsg('数据不存在，或者已被删除', 'error');

        $model = PartnerApply::model()->findByPk($id);

        if(!$model)
            $this->showMsg('数据不存在，或者已被删除', 'error');

        if($model->status == 1)
            $this->showMsg('数据已经通过审核，不能进行操作', 'error');


        Yii::import('application.extensions.sms.*');
        if($_GET['action'] == 'approve') {
            if(PartnerApply::model()->find("name = :name and id != {$id}", array(':name' => $model->name)))
                $this->showMsg('你已经申请过合作，请不要重复申请', 'error');

            if(Partner::model()->findByAttributes(array('name' => $model->name)))
                $this->showMsg('商家 <b>'. $model->name. '</b> 已存在', 'error');

            $partner = new Partner;
            $partner->name = $model->name;
            $partner->tel = $model->tel;
            $partner->type = 0;
            $partner->contacts = $model->contacts;
            $partner->save();


            $model->status = 1;
            $model->save();

            $manager = Manager::model()->findByPk($model->manager_id);
            if($manager) {
                $manager->partner_id = $partner->id;
                $manager->status = Manager::ST_ACTIVE;
                //$password = rand(100000,999999);
                //$manager->password = $manager->hashPassword($password);
                $manager->save();
            }

            SmsService::send($model->tel, SmsContent::MERCHANT_REQUEST_APPROVE, false);
        } else if($_GET['action'] == 'pending') {
//			$model->step = 1;
//			$model->status = 2;
//			$model->reason = $_POST['reason'];
//			$model->save();


        } else if($_GET['action'] == 'delete') {
            $manager = Manager::model()->findByPk($model->manager_id);
            if($manager)
                $manager->delete();

            $model->delete();
            $this->showMsg('删除成功');
        } else {
            $model->status = 2;
            $model->reason = urldecode($_GET['reason']);
            $model->save();

            $manager = Manager::model()->findByPk($model->manager_id);
            if($manager) {
                $manager->join_step = 3;
                $manager->status = Manager::ST_PENDING;
                $manager->save();
            }

            SmsService::send($model->tel, SmsContent::MERCHANT_REQUEST_REJECT, false);
        }
        $this->showMsg('操作成功');
    }
}