<?php
class AccountController extends ApplicationController {
    public $layout = false;

    public function actionIndex() {
        $this->render('index');
    }

	public function actionLogin() {

		if($_POST['username'] && $_POST['password'] && $_POST['captcha']) {

			$captcha_code = Yii::app()->session['captcha_code'];
			if($captcha_code != strtolower($_POST['captcha'])) {
				echo json_encode(array(
					'error' => 1,
					'msg' => '验证码错误！',
				));
				Yii::app()->end();
			}

//			$user = Manager::model()->find(array(
//				'condition' => 'username = :username',
//				'params' => array(':username' => $_POST['username'])
//			));
			$identity = new UserIdentity($_POST['username'] , $_POST['password']);
			if($identity->authenticate()) {
				Yii::app()->user->login($identity);

				echo json_encode(array(
					'error' => 0,
					'msg' => '登录成功，欢迎回来！',
				));
			} else {
				echo json_encode(array(
					'error' => 1,
					'msg' => '登录失败，用户名或者密码错误！',
				));
			}
		}
	}

	public function actionCaptcha() {
		$captcha = new Captcha();
		$captcha->set_ttf('./assets/fonts/texb.ttf');
		$captcha->show();
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect('/');
	}
}