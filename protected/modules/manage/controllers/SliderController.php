<?php
class SliderController extends AdminBaseController {


	public function actionIndex() {

		$list = Slider::model()->findAll(array(
			'order' => 'sort_num DESC, id DESC'
		));



		$this->render('index', array(
			'list' => $list,
		));
	}


	public function actionCreate() {

		$model = new Slider();


		if($_POST) {

			try{
				$path = "slider";
				$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);

				$error = '';
			} catch(UploadException $e) {
				$error = $e->getMessage();
			} catch(CException $e) {
				$error = '系统错误';
			}

			if($error)
				$this->showMsg($error, 'error');

			$model->url = $_POST['url'];
			$model->sort_num = intval($_POST['sort_num']);
			$model->img = $img;
			$model->save();

			$this->showMsg('操作成功', 'success', 'slider/index');
		}


		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Slider::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		if($_POST) {
			try{
				$path = "slider";
				$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);

				$error = '';
			} catch(UploadException $e) {
				$error = $e->getMessage();
			} catch(CException $e) {
				$error = '系统错误';
			}

			if(!$error)
				$model->img = $img;

			$model->url = $_POST['url'];
			$model->sort_num = intval($_POST['sort_num']);
			$model->save();


			$this->showMsg('编辑成功', 'success', 'slider/index');
		}

		$this->render('form', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {

		$id = intval($id);
		if(!$id)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model = Slider::model()->findByPk($id);

		if(!$model)
			$this->showMsg('数据不存在，或者已被删除', 'error');

		$model->delete();

		$this->showMsg('操作成功');
	}

}