<?php
class TeacherController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

//		$criteria->addCondition('partner_id = '.$this->partner_id);

		$criteria->addCondition('type = 2');

		if($scope = $this->partner_scope('partner_id'))
			$criteria->addCondition($scope);


		if($_GET['name'])
			$criteria->addSearchCondition('parent_name', urldecode($_GET['name']));



		$count = Contacts::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = 10;
		$pager->applyLimit($criteria);

		$criteria->order = 'id DESC';
		$list = Contacts::model()->findAll($criteria);


		$grades = CommonHelper::getGroupField($list, Grade::model(), 'grade_id', 'name');
		$schools = CommonHelper::getGroupField($list, Partner::model(), 'partner_id', 'name');
		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'grades' => $grades,
			'schools' => $schools,
		));
	}
}