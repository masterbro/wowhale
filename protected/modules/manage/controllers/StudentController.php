<?php
class StudentController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($scope = $this->partner_scope('t.partner_id'))
			$criteria->addCondition($scope);

		if($_GET['name'])
			$criteria->addSearchCondition('name', urldecode($_GET['name']));


		$count = Student::model()->count($criteria);
		$pager = new CPagination($count);
		$pager->pageSize = $_GET['per_page']?$_GET['per_page']:10;
		$pager->applyLimit($criteria);

		$criteria->order = 't.id DESC';
		$list = Student::model()->with('parents')->findAll($criteria);


		$grades = CommonHelper::getGroupField($list, Grade::model(), 'grade_id', 'name');
		$schools = CommonHelper::getGroupField($list, Partner::model(), 'partner_id', 'name');
		$this->render('index', array(
			'list' => $list,
			'pager' => $pager,
			'grades' => $grades,
			'schools' => $schools,
		));
	}
}