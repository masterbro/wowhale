<?php
class OrderController extends AdminBaseController {


	public function actionIndex() {
		$criteria = new CDbCriteria();

		if($scope = $this->partner_scope('school_id'))
			$criteria->addCondition($scope);

		if($_GET['start'] && $_GET['to']) {
			$start = strtotime($_GET['start']);
			$end = strtotime($_GET['to']) + 86399;
			$criteria->addCondition("begin_time > {$start} and begin_time < {$end}");
		}

		$school = intval($_GET['school']);
		if($school)
			$criteria->addCondition('school_id = '.$school);

		if($_GET['id']) {
			$criteria->addCondition('display_id = :display_id');
			$criteria->params[':display_id'] = $_GET['id'];
		}

		if($_GET['trans_id']) {
			$criteria->addCondition('payment_transaction_id = :payment_transaction_id');
			$criteria->params[':payment_transaction_id'] = $_GET['trans_id'];
		}

		if($_GET['export']) {
			$criteria->order = 'begin_time DESC';
			$list = Order::model()->findAll($criteria);
			$schools = School::all(true);

			$data = array();
			$grades = array();
			$contacts = array();
			foreach($list as $l) {
				$shipping_info = $l->shippingInfo();
				$order_details = '';

				foreach($l->order_details as $lk=>$lo) {
					if($lk) $order_details .= '<br />';
					$order_details .=  $lo->title. '￥'.CommonHelper::price($lo->price).'x'.$lo->qty.'';
				}
				$student = $l->student;
				if($student && $student->name){
					$shipping_info['student_name'] = $student->name;
				}
				$show_old_data = true;
				if($l->contacts_id){
					if(!$contacts[$l->contacts_id]){
						$contacts[$l->contacts_id] = Contacts::model()->findByPk($l->contacts_id);
					}
					if($contacts[$l->contacts_id] && $contacts[$l->contacts_id]->parent_phone){
						$shipping_info['tel'] = $contacts[$l->contacts_id]->parent_phone;
						$show_old_data = false;
					}
				}
				if($student && $show_old_data && $student->parents&& $student->parents[0]&& $student->parents[0]->parent_phone){
					$shipping_info['tel'] = $student->parents[0]->parent_phone;
					$show_old_data = false;
				}
				if($student){
					if(!$grades[$student->grade_id]){
						$grades[$student->grade_id] = Grade::model()->findByPk($student->grade_id);
					}
					if($grades[$student->grade_id] && $grades[$student->grade_id]->name){
						$shipping_info['student_grade'] = $grades[$student->grade_id]->name;
						$show_old_data = false;
					}
				}
				$data[] = array(
                    $l->import_from ? '微小店导入' : ($l->payment_type==2?'线下支付':$l->display_id),
                    $l->payment_status?'已支付':'未支付',
					$order_details, '￥'.CommonHelper::price($l->total),
					$schools[$l->school_id], $shipping_info['student_name'],
					$shipping_info['tel'], $shipping_info['student_grade'], "&nbsp;".$l->payment_transaction_id,
					date('y/m/d H:i:s', $l->begin_time)
				);
			}

			$title = array(
				'订单号', '支付状态','订单内容', '订单金额', '学校',
				'姓名', '电话', '班级', '支付单号', '创建时间'
			);

			HtmlHelper::exportExcel($data, $title, "订单列表.xls");
		} else {
			$count = Order::model()->count($criteria);
			$pager = new CPagination($count);
			$pager->pageSize = 20;
			$pager->applyLimit($criteria);

			$criteria->order = 'begin_time DESC';
			$list = Order::model()->findAll($criteria);

			$this->render('index', array(
				'list' => $list,
				'pager' => $pager,
				'partners' => Partner::all(true),
			));

		}
	}


}