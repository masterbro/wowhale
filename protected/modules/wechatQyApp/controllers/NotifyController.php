<?php
class NotifyController extends WechatQyAppController {


	//接收微信服务器推送的suite_ticket
	public function actionIndex() {
		$id = intval($_GET['app']);
		if(!$id)
			die('error');

		$suite = WxSuite::model()->findByPk($id);
		if(!$suite)
			die('error');

		$wxcpt = new WXBizMsgCrypt($suite->token, $suite->encoding_aes_key, $suite->suite_id);
		$sReqMsgSig = $_GET['msg_signature'];
		$sReqTimeStamp = $_GET['timestamp'];
		$sReqNonce = $_GET['nonce'];

		$sReqData = file_get_contents("php://input");
		Yii::log("wechat notify push:".$sReqData, 'error');
		Yii::log("wechat notify push global:".$GLOBALS["HTTP_RAW_POST_DATA"], 'error');

		$sMsg = "";  // 解析之后的明文
		$errCode = $wxcpt->DecryptMsg($sReqMsgSig, $sReqTimeStamp, $sReqNonce, $sReqData, $sMsg);

		Yii::log("wechat notify encode:".$sMsg);

		if ($errCode == 0) {

			$data = simplexml_load_string($sMsg, 'SimpleXMLElement', LIBXML_NOCDATA);
			$suite_id = (string) $data->SuiteId;
			$info_type = (string) $data->InfoType;

			switch($info_type) {
				case 'suite_ticket':
					if($suite->suite_id == $suite_id) {
						$suite->suite_ticket = (string) $data->SuiteTicket;
						$suite->save();
					}
					break;

				case 'cancel_auth':
					$crop_id = (string) $data->AuthCorpId;
					$config = WechatConfig::model()->find('corpid = :corpid', array(
						':corpid' => $crop_id
					));
					if($config) {
						$config->status = 0;
						$config->save();
					}
					break;

				default:
					break;
			}

			die('success');
		} else {
			Yii::log("wechat notify push error :".$errCode);
			print("ERR: " . $errCode . "\n\n");
			exit();
		}

	}
}