<?php
class OauthController extends WechatQyAppController {
	public $layout='//layouts/application';

	public function actionIndex() {
		$user_id = Yii::app()->user->getId();
		if(!$user_id)
			$this->redirect('oauth/step1');

		if(!$_GET['auth_code'])
			throw new CHttpException(404);

		$user = Manager::model()->findByPk($user_id);
		$qy_apps = CommonHelper::qyApp($user->partner_id);

		$suite_id = intval($_GET['suite_id']);
		if(!$suite_id)
			throw new CHttpException(404);

		$suite = WxQySuite::getSuite($suite_id);
		if(!$suite)
			throw new CHttpException(404);


		$app = new WxQySuite();
		$app->throw_exception = true;
		$error = false;
		try {
			$result = $app->get_permanent_code($suite->suite_id, $suite->suite_access_token, $_GET['auth_code']);
		} catch (Exception $e) {
			$error = true;
		}


		if(!$error) {
			$config = WechatConfig::model()->find("partner_id = {$user->partner_id} AND ww_suite_id = {$suite->id}");
			if(!$config) {
				$config = new WechatConfig();
				$config->partner_id = $user->partner_id;
				$config->ww_suite_id = $suite->id;
			}
			$config->permanent_code = $result->permanent_code;
			$config->access_token = $result->access_token;
			$config->expired_at = $result->expires_in + time();
            if($config->corpid!=$result->auth_corp_info->corpid){
                $config->leader_department  = null;
                $config->teacher_department = null;
                $config->student_department = null;
            }
			$config->corpid = $result->auth_corp_info->corpid;
			$config->suite_id = $suite->suite_id;

			$app_ids = $config->app_ids ? (array) json_decode($config->app_ids) : array();

			foreach($result->auth_info->agent as $k=>$a) {
				foreach($qy_apps as $qk=>$qy) {
					if($a->appid == $qy['id']) {
						$app_ids[$qk] = $a->agentid;
					}
				}
			}
			$config->app_ids = json_encode($app_ids);

			$config->status = 1;
			$config->data = json_encode($result);
			$config->save();


			$domain = str_replace("http://", "", Yii::app()->params['mobile_base']);
			$app = new WxQyApp($config);
//			$app->throw_exception = true;
			foreach($app_ids as $k=>$v) {
				if(!$qy_apps[$k])
					continue;

				$app->menu($qy_apps[$k]['menu'], $v);
                $setting = array(
                    'agentid' => $v,
                    'report_location_flag' => 0,
                    'redirect_domain' => $domain,
                    'isreportenter' =>1
                );
                if($qy_apps[$k]['name']){
                    $setting['name']=$qy_apps[$k]['name'];
                }
				$app->appSetting($setting);
			}



			Yii::app()->user->setFlash('success', '授权成功');
			$this->redirect($this->createUrl('/manage/partner_manage/wechat'));
			Yii::app()->end();
		}

		$this->render('index', array(
			'error' => $error
		));
	}


    public function actionReStep1(){
        $this->redirect('/wechatQyApp/oauth/step1?reStep1=1#apply');
    }
	public function actionStep1() {
        $user = $this->getUser();
        if($user ){
            $apply = PartnerApply::model()->findByAttributes(array('manager_id' =>$user->id));
            $extra = $apply->extra?(Object)CJSON::decode($apply->extra):new stdClass();
            $reset = Yii::app()->request->getParam('reStep1');
            if($user->status==Manager::ST_PENDING && !$reset){
                $this->redirect('/wechatQyApp/oauth/step1Two#apply');
            }
            else if($apply && $apply->status!=2){
                $this->redirect('/wechatQyApp/oauth/step1Two#apply');
            }
            //var_dump($extra);

        }
		$this->render('step1', compact('user','apply','extra'));
	}

	public function actionStep1Two() {
        $user = $this->getUser();
        if(!$user){
            $this->redirect('/wechatQyApp/oauth/step1#apply');
        }
        $apply = PartnerApply::model()->findByAttributes(array('manager_id' =>$user->id));
        $extra =($apply&&isset($apply->extra) )?(Object)CJSON::decode($apply->extra):new stdClass();;
        //var_dump($extra);
		$this->render('step1Two', array(
            'apply'=>$apply,
            'extra'=>$extra,
            'user'=>$user
		));
	}

	public function actionStep2() {
		if(!Yii::app()->session['oauth_school'])
			$this->redirect('oauth/step1');

		$user = Manager::model()->findByPk(Yii::app()->session['oauth_school']);
		$partner = Partner::model()->findByPk($user->partner_id);


		$suite = WxQySuite::getSuite();
		$pre_auth_code = $this->getPreAuthCode($suite);


		$redirect_uri = $this->createAbsoluteUrl('/wechatQyApp/oauth');
		$oauth_url = "https://qy.weixin.qq.com/cgi-bin/loginpage?suite_id={$suite->suite_id}&pre_auth_code={$pre_auth_code}&redirect_uri={$redirect_uri}&state=";

		$config = WechatConfig::model()->find("partner_id = {$partner->id}");

		$this->render('step2', array(
			'user' => $user,
			'partner' => $partner,
			'oauth_url' => $oauth_url,
			'config' => $config,
		));
	}


	private function getPreAuthCode($suite) {
		return CommonHelper::getPreAuthCode($suite);
	}


	public function actionChange() {
		unset(Yii::app()->session['oauth_school']);


		$this->redirect('oauth/step1');
	}

	public function actionLogin() {
		if($_POST['username'] && $_POST['password']) {


			$user = Manager::model()->find(array(
				'condition' => 'username = :username',
				'params' => array(':username' => $_POST['username'])
			));
			if(!$user || $user->password != $user->hashPassword($_POST['password'])) {
				JsonHelper::show(array(
					'error' => 1,
					'msg' => '验证失败，用户名或者密码错误！',
				));
			}

			Yii::app()->session['oauth_school'] = $user->id;

			JsonHelper::show(array(
				'error' => 0,
				'msg' => '验证成功！',
			));
		}
	}

    public function actionOtherInfo(){
        //Yii::app()->session['join_school_user'] = 13;
        $user = $this->getUser();
        if(!$user){
            $error = 1;
            $msg = '请先申请帐号';
        }
        else{
            //var_dump($user->id);
            $apply = PartnerApply::model()->findByAttributes(array('manager_id' =>$user->id));
            if(!$apply){
                $error = 1;
                $msg = '请先申请帐号';
            }
            else if($user->status != Manager::ST_PENDING){
                $error = 1;
                $msg = '帐号已被处理';
            }
            else{
                /*if(!$_POST['shenfengzheng']){
                    $error = 1;
                    $msg = '请上传身份证扫描件';
                }
                else */if(!$_POST['zhuzhijigou']){
                    $error = 1;
                    $msg = '请上传组织机构扫描件';
                }
                else{
                    //$shenfengzheng = UploadHelper::moveTmp($_POST['shenfengzheng'], 'shenfengzheng');
                    $zhuzhijigou = UploadHelper::moveTmp($_POST['zhuzhijigou'], 'zhuzhijigou');
                    $data = $_POST;
                    //$data['shenfengzheng'] = $shenfengzheng;
                    $data['zhuzhijigou'] = $zhuzhijigou;
                    if(/*!$shenfengzheng ||*/ !$zhuzhijigou){
                        $error = 1;
                        $msg = '请上传组织机构扫描件';
                    }
                    else{
                        $extra_old = CJSON::decode($apply->extra);
                        $extra = array_merge($extra_old,$data);
                        $extra = CJSON::encode($extra);
                        $apply->extra = $extra ;
                        $apply->save();
                        $user->join_step = 2;
                        $user->save();
                        $error = 0;
                        Yii::import('application.extensions.sms.*');
                        SmsService::send($apply->tel, SmsContent::SCHOOL_AFTER_REGISTER, false);
                        $msg = '提交成功';
                    }

                }

            }

        }
        JsonHelper::show(compact('error','msg'));
    }
	public function actionApply() {
		$name = trim($_POST['school']);
		if($_POST['name'] && $_POST['mobile'] && $_POST['school']) {
            $user = $this->getUser();
            if($user){
                if(PartnerApply::model()->findByAttributes(array('name' =>$name),array('condition'=>'manager_id!='.$user->id)))
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '学校 '. $name. ' 已存在',
                    ));
                $partner = Partner::model()->findByAttributes(array('name' => $name));
                if($partner && $user->partner_id!=$partner->id)
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '学校 '. $name. ' 已存在',
                    ));
                $apply = PartnerApply::model()->findByAttributes(array('manager_id'=>$user->id));
            }
            else{
                if(PartnerApply::model()->findByAttributes(array('name' =>$name)))
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '学校 '. $name. ' 已存在',
                    ));

                if(Partner::model()->findByAttributes(array('name' => $name)))
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '学校 '. $name. ' 已存在',
                    ));

                $mobile = trim($_POST['mobile']);
                if(!CommonHelper::validMobile($mobile))
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '手机号错误',
                    ));

                if(!$_POST['captcha'])
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '验证码错误',
                    ));



                if(Manager::model()->findByAttributes(array('username' => $mobile)))
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => '手机号已经被注册',
                    ));



                Yii::import('application.extensions.sms.*');
                try {
                    $sms_captcha = new SmsCaptcha('school_request_' . $mobile);
                    $sms_captcha->valid($_POST['captcha']);
                } catch(Exception $e) {
                    JsonHelper::show(array(
                        'error' => true,
                        'msg' => $e->getMessage()
                    ));
                }
                $user = new Manager();
                $user->username = $mobile;
                $user->password = $user->hashPassword($_POST['password']);
                $user->tel = $mobile;
            }
            if(!$_POST['zhuzhijigou']){
                JsonHelper::show(array(
                    'error' => true,
                    'msg' => '请上传组织机构扫描件',
                ));
            }
            $zhuzhijigou = UploadHelper::moveTmp($_POST['zhuzhijigou'], 'zhuzhijigou');
            $_POST['zhuzhijigou'] = $zhuzhijigou ;
            if(!$zhuzhijigou){
                JsonHelper::show(array(
                    'error' => true,
                    'msg' => '请上传组织机构扫描件'
                ));
            }


            $user->join_step = 2;
			$user->status = Manager::ST_PENDING;
			$user->type = 1;//Manager::TYPE_SCHOOL; //这个type是系统管理圆/合作伙伴的区别
			$user->save();

            if(!$apply){
                $model = new PartnerApply();
            }
            else{
                $model = $apply;
            }
			$model->name = $name;
			$model->tel = $_POST['mobile'];
			$model->contacts = $_POST['name'];
            $model->email = $_POST['email'];
            $model->wechat_id = $_POST['weixin'];
			$model->manager_id = $user->id;
            $model->status = 0;
			$model->type = PartnerApply::TYPE_SCHOOL;

            $extra = array('IDCard'=>$_POST['IDCard'],'zhuzhijigou'=>$zhuzhijigou);
            $model->extra = CJSON::encode($extra);
			$model->save();
            Yii::app()->session['join_school_user'] = $user->id;
            Yii::import('application.extensions.sms.*');
            SmsService::send($model->tel, SmsContent::SCHOOL_AFTER_REGISTER, false);
            $msg = '提交成功';
			JsonHelper::show(array(
				'error' => false,
				'msg' => '申请成功，请等待审核',
			));
		}
        JsonHelper::show(array(
            'error' => true,
            'msg' => '非法操作',
        ));
	}


    private function getUser() {
        $join_uid = Yii::app()->session['join_school_user'];
        if($join_uid) {
            $user = Manager::model()->findByPk($join_uid);
        }

        return $user ? $user : false;
    }
	public function actionCaptcha() {
		$mobile = trim($_POST['mobile']);
		if(!CommonHelper::validMobile($mobile))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号错误',
			));

		if(Manager::model()->findByAttributes(array('username' => $mobile)))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号已经被注册',
			));

		Yii::import('application.extensions.sms.*');

		try {
			$sms_captcha = new SmsCaptcha('school_request_' . $mobile);
			$sms_captcha->send($mobile, SmsContent::MERCHANT_REQUEST, 1);
		}catch (SmsCaptchaException $e) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => $e->getMessage()
			));
		}

		JsonHelper::show(array(
			'error' => false,
			'msg' => '发送成功',
		));
	}
}