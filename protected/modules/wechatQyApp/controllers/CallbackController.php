<?php
class CallbackController extends WechatQyAppController {


	//接收微信服务器推送的suite_ticket
	public function actionIndex() {
        Yii::log("wehcat callback get:".CJSON::encode($_GET), 'error');
        $id = intval($_GET['app']);
        if(!$id)
            die('error');

        $suite = WxSuite::model()->findByPk($id);
        if(!$suite)
            die('error');
        $cropid = $_GET['cropid'];
        //$wxcpt = new WXBizMsgCrypt($suite->token, $suite->encoding_aes_key, $suite->suite_id);
        $wxcpt = new WXBizMsgCrypt($suite->token, $suite->encoding_aes_key, $cropid);
        $sReqMsgSig = $_GET['msg_signature'];
        $sReqTimeStamp = $_GET['timestamp'];
        $sReqNonce = $_GET['nonce'];

        $sReqData = file_get_contents("php://input");
        $sMsg = "";  // 解析之后的明文
        $errCode = $wxcpt->DecryptMsg($sReqMsgSig, $sReqTimeStamp, $sReqNonce, $sReqData, $sMsg);

        Yii::log("wechat callback encode:".$sMsg, 'error');

        if ($errCode == 0) {
            $data = simplexml_load_string($sMsg, 'SimpleXMLElement', LIBXML_NOCDATA);
            $msg_type = (string) $data->MsgType;
            switch($msg_type){
                case 'image':$this->responseImage($data,$cropid, $id);break;
                case 'event':$this->responseEvent($data,$cropid, $id);break;
                case '':break;
                case '':break;
                default:break;
            }
            //$suite_id = (string) $data->SuiteId;
            //$info_type = (string) $data->InfoType;
        }
        else {
            Yii::log("wechat callback push error :".$errCode, 'error');
            print("ERR: " . $errCode . "\n\n");
            exit();
        }
		Yii::log("wechat callback push:".$sReqData, 'error');
        Yii::log("wehcat callback post:".CJSON::encode($_POST), 'error');

		die('success');
	}
    private function responseEvent($data,$cropid, $ww_suite_id) {
        $from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $cropid = $cropid;
        $wechat_config = WechatConfig::model()->findByAttributes(
            array(
                'corpid'=>$cropid,
                'ww_suite_id'=>$ww_suite_id,
            ),array('order'=>'id desc'));
        Yii::log("wechat callback cropid:".$cropid.' '.$agent_id, 'error');
        $agent_type = '';
        switch($agent_id){
            case 0:$agent_type='qyxzs';break;
            case $wechat_config->appIdsObj->teacher:$agent_type='teacher';break;
            case $wechat_config->appIdsObj->album:$agent_type='album';break;
            case $wechat_config->appIdsObj->notice:$agent_type='notice';break;
            case $wechat_config->appIdsObj->contacts:$agent_type='contacts';break;
            case $wechat_config->appIdsObj->product:$agent_type='product';break;
            case $wechat_config->appIdsObj->order:$agent_type='order';break;
            case $wechat_config->appIdsObj->sign_log:$agent_type='sign_log';break;
            case $wechat_config->appIdsObj->student:
            case $wechat_config->appIdsObj->wap:$agent_type='student';break;
            case $wechat_config->appIdsObj->system:$agent_type='system';break;
            case $wechat_config->appIdsObj->student_teacher:break;
            default:$agent_type='other';break;
        }
        //return '';
        switch((string)$data->Event){
            case 'enter_agent':return  $this->reponseEnterAgent($data,$cropid,$wechat_config,$agent_type);break;
            case 'subscribe':
            case 'unsubscribe':return  $this->reponseSubscribe($data,$cropid,$wechat_config,$agent_type);break;
        }
        return '';
    }
    private function reponseSubscribe($data,$cropid,$wechat_config,$agent_type){
        if(!$agent_type){
            return '';
        }
        $from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $cropid = $cropid;
        $partner_id = $wechat_config->partner_id;
        $contact_id =  str_replace('sh','',$from_user_name);

        $agent_type_id = $agent_type.'_'.$agent_id;
        $in_data= array();
        $in_data['app_key'] = $agent_type;
        $in_data['app_id'] = $agent_id;
        $in_data['create_time'] = (string)$data->CreateTime;
        $in_data['subscribe'] = (string)$data->Event=='subscribe'?1:0;
        try{
            AppSubscribe:: create($partner_id, $contact_id, $in_data);
        }
        catch(Exception $e){

        }
        if($agent_type=='notice'){
            $contact = Contacts::model()->findByPk($contact_id);
            if($contact->leader){
                $grades = array();//Grade::model()->findAllByAttributes(array('partner_id'=>$contact->partner_id));
            }
            else if($contact->type==Contacts::TY_STUDENT){
                $students = $contact->students;
                $grades = array();
                foreach($students as $k=>$v){
                    if(!$grades[$v->grade_id]){
                        $grades[$v->grade_id] = Grade::model()->findByAttributes(array('partner_id'=>$contact->partner_id,'id'=>$v->grade_id));
                    }
                }
            }
            else if($contact->type==Contacts::TY_TEACHER){
                $grades = array();
                $grade_id = $contact->grade_id;
                $grade = Grade::model()->findByPk($grade_id);
                $grades[$grade_id] = $grade;
            }
            if(!empty($grades)){
                $wechat_config->getToken();
                $app = new WxQyMsg($wechat_config);
                foreach($grades as $k=>$v){
                    if(!$v->id){
                        continue;
                    }
                    $chat = $app->getChat('c'.$v->id);
                    Yii::log(CJSON::encode($chat),'error');
                    $needCreate = true;
                    if($chat){
                        $update_data = array('chatid'=>'c'.$v->id,'op_user'=>$from_user_name);
                        $user_list = $chat->chat_info->userlist;
                        $_the_owner = $chat->chat_info->owner;


                        $needCreate = false;
                        if(!$_the_owner){
                            $sql = "SELECT contact.id as id FROM {{app_subscribe}} t LEFT JOIN {{contacts}} contact ON contact.id=t.contacts_id where t.partner_id={$wechat_config->partner_id} AND t.app_id={$agent_id} AND t.subscribe=1 AND contact.grade_id={$v->id} AND (contact.`leader` !=1 OR  contact.`leader` IS NULL)" ;
                            $teachers = Yii::app()->db->createCommand($sql)->queryAll();
                            if(!$update_data['add_user_list']){
                                $update_data['add_user_list'] = array();
                            }
                            foreach($teachers as $kv=>$vv){
                                $update_data['add_user_list'][] = 'sh'.$vv['id'];
                                //$update_data['add_user_list'][] = 'sh'.$vv['id'];
                            }
                            $sql = "SELECT contact.id as id FROM {{student}} student LEFT JOIN {{student_parent}} parent ON parent.student_id = student.id LEFT JOIN {{contacts}} contact ON contact.id=parent.parent_id  LEFT JOIN {{app_subscribe}}  subscribe ON contact.id=subscribe.contacts_id where subscribe.partner_id={$wechat_config->partner_id} AND subscribe.app_id={$agent_id} AND subscribe.subscribe=1 AND student.grade_id={$v->id}" ;
                            $students = Yii::app()->db->createCommand($sql)->queryAll();

                            foreach($students as $kv=>$vv){
                                $update_data['add_user_list'][] = 'sh'.$vv['id'];
                            }
                            $_the_owner = $update_data['owner'] = $update_data['add_user_list'][0];
                        }
                        if((string)$data->Event=='subscribe'){
                            if(!$update_data['add_user_list']){
                                $update_data['add_user_list'] = array();
                            }
                            $update_data['add_user_list'][] = $from_user_name;
                            $update_data['add_user_list'] = array_diff($update_data['add_user_list'],$chat->chat_info->userlist);
                            sort($update_data['add_user_list']);
                            if(!$_the_owner && !$update_data['owner']){
                                $_the_owner = $update_data['owner'] = $from_user_name;
                            }
                            if($_the_owner==$chat->chat_info->owner){
                                $update_data['op_user']= $_the_owner;
                            }
                            else{
                                $update_data['op_user']= $chat->chat_info->userlist[0];
                            }


                        }
                        else{
                            $update_data['del_user_list'] = array($from_user_name);
                            if($_the_owner == $from_user_name){
                                unset($update_data['del_user_list']);
                            }
                            /*if(count($user_list)<2){
                                try{
                                    $c = $app->quitChat($v->id);
                                    Yii::log('quit '.CJSON::encode($c));
                                }
                                catch(Exception $e){
                                    Yii::log('quit error '.$e->getMessage());

                                }
                            }*/
                        }

                        try{
                            Yii::log('updata '.CJSON::encode($update_data),'error');
                            $app->updateChat($update_data);
                        }
                        catch(Exception $e){

                        }


                    }
                    if($needCreate && (string)$data->Event=='subscribe'){
                        //$teachers = Contacts::model()->findAllByAttributes(array('grade_id'=>$v->id,'type'=>Contacts::TY_TEACHER));
                        $added_uid = array();
                        $sql = "SELECT contact.id as id FROM {{app_subscribe}} t LEFT JOIN {{contacts}} contact ON contact.id=t.contacts_id where t.partner_id={$wechat_config->partner_id} AND t.app_id={$agent_id} AND t.subscribe=1 AND contact.grade_id={$v->id} AND (contact.`leader` !=1 OR  contact.`leader` IS NULL)" ;
                        $teachers = Yii::app()->db->createCommand($sql)->queryAll();

                        foreach($teachers as $kv=>$vv){
                            $added_uid[] = 'sh'.$vv['id'];
                        }
                        $owner = $added_uid[0];
                        $sql = "SELECT contact.id as id FROM {{student}} student LEFT JOIN {{student_parent}} parent ON parent.student_id = student.id LEFT JOIN {{contacts}} contact ON contact.id=parent.parent_id  LEFT JOIN {{app_subscribe}}  subscribe ON contact.id=subscribe.contacts_id where subscribe.partner_id={$wechat_config->partner_id} AND subscribe.app_id={$agent_id} AND subscribe.subscribe=1 AND student.grade_id={$v->id}" ;
                        $students = Yii::app()->db->createCommand($sql)->queryAll();

                        foreach($students as $kv=>$vv){
                            $added_uid[] = 'sh'.$vv['id'];
                        }
                        if(empty($added_uid)){
                            continue;
                        }
                        if(!$owner){
                            $owner = $from_user_name;
                        }
                        $added_uid[] = $from_user_name;
                        if(count($added_uid)<3){
                            $added_uid[] = $from_user_name;
                            $added_uid[] = $from_user_name;
                            $added_uid[] = $from_user_name;
                        }
                        $add_data = array();
                        $add_data['chatid']  = 'c'.$v->id;
                        $add_data['name']  = $v->name;
                        $add_data['owner']  = $owner;
                        $add_data['userlist'] =  $added_uid;
                        //Yii::log(CJSON::encode($teachers).CJSON::encode($students),'error');
                        try{
                            $app->chat($add_data);
                        }
                        catch(Exception $e){

                        }

                        /*{
                            "chatid": "1",
                           "name": "企业应用中心",
                           "owner": "zhangsan",
                           "userlist": ["zhangsan","lisi","wangwu"]
                        }*/
                    }
                }
            }

        }
        return '';


    }

    private function reponseEnterAgent($data,$cropid,$wechat_config,$agent_type){
        if(!$agent_type){
            return;
        }
        $from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $cropid = $cropid;
        $partner_id = $wechat_config->partner_id;
        $contact_id =  str_replace('sh','',$from_user_name);

        $agent_type_id = $agent_type.'_'.$agent_id;
        $in_data= array();
        $in_data['app_key'] = $agent_type;
        $in_data['app_id'] = $agent_id;
        $in_data['create_time'] = (string)$data->CreateTime;
        try{
            AppTracking:: create($partner_id, $contact_id, $in_data);
        }
        catch(Exception $e){

        }
        return;
        $contact = Contacts::model()->findByPk($contact_id);
        if(!$contact){
            return '';
        }
        if($contact->first_in_send_apps && $contact->first_in_send_apps->$agent_type_id){
            return '';
        }
        $wechat_config->getToken();
        $app = new WxQyMsg($wechat_config);
        $cache_key = 'media_firstimg_'.$partner_id.'_'.$cropid.'_'.$agent_id;
        $ww_apps = CommonHelper::qyApp();
        $media_id = Yii::app()->cache->get($cache_key);
        $app->throw_exception = true;
        if(!$media_id){
            try{
                $img_base = $ww_apps[$agent_type]['help_img'];//TMP_PATH.'/../../images/adi.jpg';
                if($img_base){
                    $medias = $app->upload('image',$img_base);
                    $media_id = $medias->media_id;
                    Yii::app()->cache->set($cache_key,$media_id,3600*24*2);
                }

            }
            catch(Exception $e){
                Yii::log('upload '.$e->getMessage().$img_base,'error');
            }

        }
        if($media_id){
            try{
                //$counts = $app->getMediaCounts($agent_id);
                $app->image($from_user_name,$media_id,$agent_id);
                $first_in_send_apps = $contact->first_in_send_apps;
                $first_in_send_apps->$agent_type_id = 1;
                $contact->first_in_send = CJSON::encode($first_in_send_apps);
                $contact->save();
            }
            catch(Exception $e){
                Yii::log('send '.$e->getMessage(),'error');
            }
        }
        else{
            Yii::log('media_id not found' ,'error');
        }


    }
    private function responseImage($data,$cropid, $ww_suite_id) {
        $from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $cropid = $cropid;
        $wechat_config = WechatConfig::model()->findByAttributes(
	        array(
		        'corpid'=>$cropid,
		        'ww_suite_id'=>$ww_suite_id,
	        ),array('order'=>'id desc'));
        Yii::log("wechat callback cropid:".$cropid.' '.$agent_id, 'error');
        switch($agent_id){
            case $wechat_config->appIdsObj->teacher:
            case $wechat_config->appIdsObj->album:$this->responseTeacherImage($data,$cropid,$wechat_config);break;
            case $wechat_config->appIdsObj->student_teacher:$this->responseStudentTeacherImage($data,$cropid,$wechat_config);break;
            default:break;
        }
        return '';
    }
    private  function responseTeacherImage($data,$cropid,$wechat_config){
        $message_id = (string)$data->MsgId;
        $cache_key = 'msg_'.$cropid.'_'.$message_id;
        if(Yii::app()->cache->get($cache_key)){
            return '';
        }
        Yii::app()->cache->set($cache_key,1,300);
        $from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $pic_url = (string)$data->PicUrl;

        $cropid = $cropid;
        $partner_id = $wechat_config->partner_id;
        $contact_id =  str_replace('sh','',$from_user_name);
        $contact = Contacts::model()->findByPk($contact_id);
        if(!$contact){
            return '';
        }
        if($contact->leader){
            $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$contact->partner_id));
        }
        else if($contact->type==1){
            $students = $contact->students;
            $grades = array();
            foreach($students as $k=>$v){
                if(!$grades[$v->grade_id]){
                    $grades[$v->grade_id] = Grade::model()->findByAttributes(array('partner_id'=>$contact->partner_id,'id'=>$v->grade_id));
                }
            }
        }
        else if($contact->type==2){
            $grades = array();
            $grade_id = $contact->grade_id;
            $grade = Grade::model()->findByPk($grade_id);
            $grades[$grade_id] = $grade;
        }
        else {
            return '';
        }
        /*if(!$contact || $contact->type==1){
            return '';
        }
        $grade_id = $contact->grade_id;
        if(!$grade_id){
            return '';
        }
        $grade = Grade::model()->findByPk($grade_id);
        if(!$grade){
            return;
        }*/
        $saved_grade = array();
        if(count($grades)>0){
            foreach($grades as $k=>$v){
                if(!$v){
                    continue;
                }
                $grade_name = $v->name;
                $time = time();
                $day = date('Ymd',$time);
                $albums = new Albums();
                $albums->partner_id = $partner_id;
                $albums->contacts_id = $contact_id;
                $albums->pic_url = $pic_url;
                $albums->grade_id = $v->id;
                $albums->day = $day;
                $albums->type = 1;
                $albums->save();
                $saved_grade[] = $v->name;
            }
        }
        if(count($saved_grade)>0){
            $wechat_config->getToken();
            $app = new WxQyMsg($wechat_config);
            $app->throw_exception = true;
            try{
                //$counts = $app->getMediaCounts($agent_id);
                //$app->text($from_user_name,'上传'.join(',',$saved_grade).'照片成功',$agent_id);
            }
            catch(Exception $e){
                //Yii::log($e->getMessage(),'error');
            }
            /*if(function_exists('ob_flush')){
                echo 'success';
                ignore_user_abort(true);
                ob_flush();
                flush();
            }*/
            try{
                file_get_contents(HtmlHelper::wx_image($albums->pic_url));
            }
            catch(Exception $e){
                Yii::log($e->getMessage(),'error');
            }
        }
    }

    private  function responseStudentTeacherImage($data,$cropid,$wechat_config){
        /*$from_user_name = (string)$data->FromUserName;
        $agent_id = (string)$data->AgentID;
        $pic_url = (string)$data->PicUrl;

        $cropid = $cropid;
        $partner_id = $wechat_config->partner_id;
        $contact_id =  str_replace('sh','',$from_user_name);
        $contact = Contacts::model()->findByPk($contact_id);
        if(!$contact || $contact->type==1){
            return '';
        }
        $grade_id = $contact->grade_id;
        if(!$grade_id){
            return '';
        }
        $time = time();
        $day = date('Ymd',$time);
        $albums = new Albums();
        $albums->partner_id = $partner_id;
        $albums->contact_id = $contact_id;
        $albums->pic_url = $pic_url;
        $albums->grade_id = $grade_id;
        $albums->day = $day;
        $albums->save();*/
    }
}