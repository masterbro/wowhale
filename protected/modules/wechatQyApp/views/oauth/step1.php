<style>

	.wrap {
		max-width: 400px;
		padding: 15px;
		margin: 0 auto;
	}
	.form-signin .form-signin-heading,
	.form-signin .checkbox {
		margin-bottom: 10px;
	}
	.form-signin .checkbox {
		font-weight: normal;
	}

	.form-signin input {
		border-radius: 0;
	}
	.form-signin label {
		padding-top: 5px;
	}
	.form-signin .no-pl {
		padding-left: 0 !important;
	}
	.form-signin .no-pr {
		padding-right: 0 !important;
		text-align: right;
	}
	.button-finish {
		box-shadow: inset 0 -4px 0 rgba(0, 0, 0, 0.2);
	}
	.row{
		margin-top: 20px;
		margin-left: 0;
		margin-right: 0;
		font-size: 18px;
	}
	.row .col-sm-3 a{
		font-size:14px;
	}

	#registered1{
		margin-top: 50px;
		margin-bottom: 50px;
		width: 100%;
	}
	#apply .row p.title{
		line-height: 34px;
		text-align: right;
	}
	#apply .row a{
		line-height: 34px;
	}
	#ww-zhuzhijigou-preview{
		text-align: right;
	}
	#explain{
		text-align: left;
		line-height: 34px;
	}
	#explain span{
		color: red;
	}
	@media (max-width: 768px){
		#registered1 h1{
			text-align: center;
		}
		#apply .row p.title{
			text-align: center;
		}
		#ww-zhuzhijigou-preview{
			text-align: center;
			margin-bottom: 10px;
		}
		#ww-add-zhuzhijigou-img{
			margin: 0 auto;
		}
	}
</style>

<script>
	//动态修改img的宽度
	function setImgWidth(){
		var ele = $("#registered1 .col-sm-8");
		var width = $(ele).width();
		$("#registered1 .col-sm-8 img.register_title").css("width",width+'px');
	}
	/*为页面加载完成后注册事件*/
	window.onload = function(){
		setImgWidth();
		setFormM();
	}
	/*监听浏览器窗口大小改变的事件*/
	window.onresize = function(){
		setImgWidth();
		setFormM();
	}
	//当为手机屏幕时，让form表单有个列偏移
	function setFormM(){
		var w = $(window).width();
		if(w < 768){
			$("#apply > .row > .col-xs-10").addClass("col-xs-offset-1");
			$("#yan-zheng-ma").addClass("col-xs-offset-1")
		}
	}
</script>

<div id="registered1" class="row">
	<div class="col-sm-8 col-sm-offset-2">
		<h1>免费注册<h1>
		<img class="hidden-xs register_title" src="<?php echo HtmlHelper::assets('images/registered-1.png') ?>">

		<div>
			<form class="form-signin" id="apply">
				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">学校名:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="学校" name="school" value="<?php echo $apply->name;?>"/>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">管理员姓名:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="管理员姓名" name="name" value="<?php echo $apply->contacts;?>"/>
					</div>
				</div>

				<!---->
				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">管理员身份证:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="管理员身份证号" name="IDCard" id="IDCard"  value="<?php echo $extra->IDCard;?>"/>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">邮箱:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="邮箱" name="email" id="email" value="<?php echo $apply->email;?>"/>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">微信号:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="微信号" name="weixin"  value="<?php echo $apply->wechat_id;?>"/>
					</div>
				</div>
				<!---->
                <?php if(!$user):?>
				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">手机号:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="text" class="form-control" placeholder="手机号" name="mobile"  value="<?php echo $apply->tel;?>"/>
					</div>
				</div>


				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">验证码:</p>
					</div>
					<div id="yan-zheng-ma" class="col-sm-3 col-xs-5">
						<input type="text" class="form-control" placeholder="验证码" name="captcha"  />
					</div>
					<div class="col-sm-3 col-xs-6">
						<a href="javascript:;" id="send-captcha">发送短信验证码</a>
					</div>

				</div>
				<div class="row">
					<div class="col-sm-5 hidden-xs">
						<p class="title">密码:</p>
					</div>
					<div class="col-sm-4 col-xs-10">
						<input type="password" class="form-control" placeholder="登录密码" name="password" />
					</div>
				</div>
                <?php else:?>
                <div class="row">
                    <div class="col-sm-5 hidden-xs">
                        <p class="title">手机号:</p>
                    </div>
                    <div class="col-sm-4 col-xs-10">
                        <input type="text" class="form-control" placeholder="手机号" name="mobile"  value="<?php echo $apply->tel;?>" readonly />
                    </div>
                </div>
                <?php endif;?>
                <div class="row">
                    <div class="col-sm-5">
                        <p class="title">组织机构代码证扫描件:</p>

                        <div id="ww-zhuzhijigou-preview">
                            <?php if($extra->zhuzhijigou):?>
                                <img src="<?php echo HtmlHelper::image($extra->zhuzhijigou);?>" style="width:150px !important"/>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-10">
                        <p id="explain">请上传<span>组织机构代码证</span>扫描件，需由申请主体负责人<span>签字</span>、<span>加盖公章</span>，文件格式必须为.jpg。</p>
                        <a href="javascript:;" class="btn ww-btn-blue btn-block" style="width: 100px;" data-loading-text="上传中..." id="ww-add-zhuzhijigou-img">上传图片</a>
                        <input type="hidden" name="zhuzhijigou" id="zhuzhijigou" value="<?php echo $extra->zhuzhijigou;?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-5">

                    </div>
                    <div class="col-sm-7 ">
                        <p><input type="checkbox" checked name="policy" id="policy">&nbsp;&nbsp;&nbsp;同意<a href="/page?key=page_policy" target="_blank">隐私条款</a></p>
                    </div>
                </div>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4 col-xs-10">
						<br />
						<button class="btn btn-lg btn-success btn-block button-finish" type="button" id="sub-apply">注册</button>
						<!--<a href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1Two#apply');?>" class="btn btn-lg btn-success btn-block">下一步</a>-->
						<br />
					</div>
				</div>
			</form>
		</div>
        <input type="file" name="files" id="fileupload_input_2" style="visibility: hidden; width: 100%" />
	</div>
</div>

<script>
    function isEmail(str){
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        return reg.test(str);
    }
    function isCardNo(card)
    {
        // 身份证号码为15位或者18位，15位时全为数字，18位前17位为数字，最后一位是校验位，可能为数字或字符X
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        if(reg.test(card) === false)
        {
            //alert("身份证输入不合法");
            return  false;
        }
        return true;
    }
	$(function() {
        var uploadBtn2 = $("#ww-add-zhuzhijigou-img");
        uploadBtn2.click(function(){
            $("#fileupload_input_2").trigger('click')
        });
        $("#fileupload_input_2").fileupload({
            url: BASE_URL + "/upload",//文件上传地址，当然也可以直接写在input的data-url属性内
            send: function() {
                uploadBtn2.button('loading');
            },
            done:function(e,data){
                console.info(e,data)
                var result = JSON.parse(data.result);
                if(result.error) {
                    alert(result.error);
                } else {
                    var html = '';
                    html += '<img src="'+ BASE_URL + '/assets/upload/' +  result.img  +'" style="width:150px;"/>';
                    $('#ww-zhuzhijigou-preview').html(html);
                    uploadBtn2.next('input').val(result.img);
                }
            },
            always: function (e, data) {
                uploadBtn2.button('reset');
            }
        });
		$('#submit').click(function(){
			var form = $('#login-form');

			var mobile = form.find('input[name="username"]').val();
			if(!mobile) {
				alert('请输入你的用户名');
				return false;
			}

			var password = form.find('input[name="password"]').val();
			if(!password) {
				alert('请输入你的密码');
				return false;
			}
			$.ajax({
				url:BASE_URL+'/wechatQyApp/oauth/login',
				data:form.serialize(),
				dataType:'json',
				type:'post',
				success: function(data) {
					alert(data.msg);
					if(!data.error) {
						window.location.href = BASE_URL+'/wechatQyApp/oauth/step2';
					}
				}
			})
		});


		$('#sub-apply').click(function(){

			var form = $('#apply');
			var name = form.find('input[name="name"]').val();
			if(!name) {
				alert('请输入你的姓名');
                form.find('input[name="name"]').focus();
				return false;
			}
			if(!$('#IDCard').val()){
				alert('请输入你的身份证号');
                $('#IDCard').focus();
				return false;
			}
            if(!isCardNo($('#IDCard').val())){
                alert('请输入正确的身份证号');
                $('#IDCard').focus();
                return false;
            }
			var phone = form.find('input[name="mobile"]').val();
			if(!phone) {
                form.find('input[name="mobile"]').focus();
				alert('请输入你的手机号');
				return false;
			}

			var school = form.find('input[name="school"]').val();
			if(!school) {
                form.find('input[name="school"]').focus();
				alert('请输入你的学校名称');
				return false;
			}

            if(!$('#zhuzhijigou').val()){
                alert('请上传组织机构扫描件');
                return;
            }
            if(!$('#policy:checked').get(0)){
                alert('请勾选同意隐藏条款');
                return;
            }
            var _email = $('#email').val();
            if(!_email){
                $('#email').focus();
                alert('请输入电子邮件');
                return;
            }
            if(!isEmail(_email)){
                $('#email').focus();
                alert('请输入正确的电子邮件');
                return;
            }
            var _button = $(this);
            if(_button.attr('data-loading')=='1'){
                return;
            }
            _button.attr('data-loading','1');
            _button.text('注册中..');
			$.ajax({
				url:BASE_URL+'/wechatQyApp/oauth/apply',
				data:form.serialize(),
				dataType:'json',
				type:'post',
				success: function(data) {
                    alert(data.msg)
					if(!data.error) {
						$('#apply')[0].reset();
                        document.location.href='/wechatQyApp/oauth/step1Two#apply'
					}
                    _button.text('注册').attr('data-loading',0);
				}
			})
		});

		$('#send-captcha').click(sendSms);

	});

	var bindSmsCountDownInterval = null, sending = false;

	var sendSms = function() {

		var form = $('#apply');

		var btn = $('#send-captcha');

		if(btn.attr('data-sending') == 'true')
			return false;

		var mobile = form.find('input[name="mobile"]').val();

		if(!mobile) {
			alert('请输入你的手机号');
			return false;
		}

		if(!/^1\d{10}$/.test(mobile)) {
			alert('请输入正确的手机号');
			return false;
		}

		sending = true;
		btn.html('发送中...').attr('data-sending', 'true');
		$.ajax({
			url:BASE_URL+'/wechatQyApp/oauth/captcha',
			type:'post',
			dataType:'json',
			data:{mobile:mobile},
			success: function(data) {
				if(data.error) {
					alert(data.msg);
					btn.html('发送验证码').attr('data-sending', 'false');
				} else {
					bindSmsCountDown();
				}
			},
			error: function(){
				sending = false;
				btn.html('发送验证码').attr('data-sending', 'false');
			}
		})
	}

	var bindSmsCountDown = function() {
		$('#send-captcha').html('<em>60</em> 秒后重新发送');

		bindSmsCountDownInterval = setInterval(function(){
			var btn = $('#send-captcha');
			var i = parseInt(btn.find('em').html());
			i--;
			if(i>0) {
				btn.html('<em>'+ i +'</em> 秒后重新发送');
			} else {
				sending = false;
				btn.html('发送验证码').attr('data-sending', 'false');
				clearInterval(bindSmsCountDownInterval);
			}
		}, 1000);
	};

</script>