<style>

    .wrap {
        max-width: 400px;
        padding: 15px;
        margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
        margin-bottom: 10px;
    }
    .form-signin .checkbox {
        font-weight: normal;
    }

    .form-signin input {
        border-radius: 0;
    }
    .form-signin label {
        padding-top: 5px;
    }
    .form-signin .no-pl {
        padding-left: 0 !important;
    }
    .form-signin .no-pr {
        padding-right: 0 !important;
        text-align: right;
    }
    button {
        box-shadow: inset 0 -4px 0 rgba(0, 0, 0, 0.2);
    }
    .row{
        margin-top: 20px;;
        font-size: 16px;
    }
    .row .col-sm-3 a{
        font-size:14px;
    }
    .row .col-sm-4 p span{
        color:orangered;
    }

    #registered1{
        margin-top: 50px;
        margin-bottom: 50px;
        width:100%;
    }
    @media (max-width: 768px){
        #top{
            text-align: center;
        }
    }
</style>

<script>
    //动态修改img的宽度
    function setImgWidth(){
        var ele = $("#registered1 .col-sm-8");
        var width = $(ele).width();
        $("#registered1 .col-sm-8 img.backgrounds").css("width",width+'px');
    }
    //修改表单的左部位左对齐并且设置行高好对齐
    function setFormRight() {
        var w = $(window).width();
        if (w > 768) {
            $("#apply .row .col-sm-3 p").css("text-align", "right");
            //var height = $("#apply .row").height();//因为删除了一部分，所以得修改行高
            var height = 40;
            //$("#apply .row p").css("line-height", height + 'px');
            //$("#apply .row a").css("line-height", height + 'px');
        }
    }
    /*为页面加载完成后注册事件*/
    window.onload = function(){
        setImgWidth();
        setFormRight();
    }
    /*监听浏览器窗口大小改变的事件*/
    window.onresize = function(){
        setImgWidth();
    }
</script>

<div id="registered1" class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div id="top"><h1>免费注册<h1></div>
                <img class="hidden-xs backgrounds" src="<?php echo HtmlHelper::assets('images/registered-2.png') ?>">

                <div>
                    <form class="form-signin" id="apply">

                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>组织机构代码证扫描件:</p>

                                <div id="ww-zhuzhijigou-preview">
                                    <?php if($extra->zhuzhijigou):?>
                                        <img src="<?php echo HtmlHelper::image($extra->zhuzhijigou);?>" style="width:150px"/>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="col-sm-4 ">
                                <?php if($user & $user->join_step==1):?>
                                <p>请上传组织机构代码证扫描件，需由申请主体负责人<span>签字</span>、<span>加盖公章</span>，文件格式必须为.jpg。</p>
                                <a href="javascript:;" class="btn ww-btn-blue btn-block" style="width: 100px;" data-loading-text="上传中..." id="ww-add-zhuzhijigou-img">上传图片</a>
                                <?php endif;?>
                                <input type="hidden" name="zhuzhijigou" id="zhuzhijigou" value="<?php echo $extra->zhuzhijigou;?>" />
                            </div>
                        </div>
                        <?php if($user && ($user->join_step!=1) ):?>
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>当前状态：</p>
                            </div>
                            <div class="col-sm-4">
                                <p><?php
                                    if($apply->status == 1) {
                                        echo '<span class="label label-success">审核成功</span>';
                                    } else if($apply->status == 2) {
                                        echo '<span class="label label-danger">已拒绝</span>';
                                    } else {
                                        echo '<span class="label label-default">等待审核</span>';
                                    }
                                    ?>
                                </p>
                            </div>

                        </div>
                        <?php if($apply->status == 2) :?>
                            <div class="row">
                                <div class="col-sm-3 col-sm-offset-2">
                                    <p>拒绝理由：</p>
                                </div>
                                <div class="col-sm-4 ">
                                    <p><?php echo $apply->reason; ?>
                                    </p>
                                </div>
                            </div>
                        <?php endif;?>
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>学校名称：</p>
                            </div>
                            <div class="col-sm-4 ">
                                <p><?php echo $apply->name; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>管理员：</p>
                            </div>
                            <div class="col-sm-4 ">
                                <p><?php echo $apply->contacts; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>手机号：</p>
                            </div>
                            <div class="col-sm-4 ">
                                <p><?php echo $apply->tel; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-sm-offset-2">
                                <p>邮箱：</p>
                            </div>
                            <div class="col-sm-4 ">
                                <p><?php echo $apply->email; ?></p>
                            </div>
                        </div>
                        <?php endif;?>
                    </form>
                </div>

                <?php if($user & $user->join_step==1):?>
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-2">
                        <p class="title" style="text-align: right;"><input type="checkbox" name="policy" id="policy"></p>
                    </div>
                    <div class="col-sm-4 ">
                        <p> 同意<a href="/page?key=page_policy" target="_blank">隐私条款</a></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <a class="btn btn-lg btn-success btn-block" id="doRegister">完成注册</a>
                    </div>
                </div>
                <?php endif;?>
                <?php if($apply & $apply->status==2):?>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <a class="btn btn-lg btn-success btn-block" href="/wechatQyApp/oauth/reStep1">重新提交材料</a>
                        </div>
                    </div>
                <?php endif;?>

    </div>

    <input type="file" name="files" id="fileupload_input_1" style="visibility: hidden;" />

    <input type="file" name="files" id="fileupload_input_2" style="visibility: hidden;" />
</div>
<script>
    $(function(){
        var uploadBtn2 = $("#ww-add-zhuzhijigou-img");
        uploadBtn2.click(function(){
            $("#fileupload_input_2").trigger('click')
        });
        $("#fileupload_input_2").fileupload({
            url: BASE_URL + "/upload",//文件上传地址，当然也可以直接写在input的data-url属性内
            send: function() {
                uploadBtn2.button('loading');
            },
            done:function(e,data){
                console.info(e,data)
                var result = JSON.parse(data.result);
                if(result.error) {
                    alert(result.error);
                } else {
                    var html = '';
                    html += '<img src="'+ BASE_URL + '/assets/upload/' +  result.img  +'" style="width:150px;"/>';
                    $('#ww-zhuzhijigou-preview').html(html);
                    uploadBtn2.next('input').val(result.img);
                }
            },
            always: function (e, data) {
                uploadBtn2.button('reset');
            }
        });
        var uploadBtn1 = $("#ww-add-shenfengzheng-img");
        uploadBtn1.click(function(){
            $("#fileupload_input_1").trigger('click')
        });
        $("#fileupload_input_1").fileupload({
            url: BASE_URL + "/upload",//文件上传地址，当然也可以直接写在input的data-url属性内
            send: function() {
                uploadBtn1.button('loading');
            },
            done:function(e,data){
                var result = JSON.parse(data.result);
                if(result.error) {
                    alert(result.error);
                } else {
                    var html = '';
                    html += '<img src="'+ BASE_URL + '/assets/upload/' +  result.img  +'" style="width:150px;"/>';
                    $('#ww-shenfengzheng-preview').html(html);
                    uploadBtn1.next('input').val(result.img);
                }
            },
            always: function (e, data) {
                uploadBtn1.button('reset');
            }
        });
        $('#doRegister').click(function(){
            <?php if($apply->status!=0):?>
            alert('申请已处理');
            return;
            <?php if($apply->status==1){unset(Yii::app()->session['join_school_user']);};?>
            <?php endif;?>
            /*if(!$('#shenfengzheng').val()){
                alert('请上传身份证扫描件');
                return;
            }*/
            if(!$('#zhuzhijigou').val()){
                alert('请上传组织机构扫描件');
                return;
            }
            if(!$('#policy:checked').get(0)){
                alert('请勾选同意隐藏条款');
                return;
            }
            /*
            if(!$('#IDCard').val()){
                alert('请填写身份证号');
                return;
            }
            */
            $('#doRegister').button('loading');
            var _data = $('form#apply').serialize();
            $.post('/wechatQyApp/oauth/otherInfo',_data,function(data){
                if((typeof(data)).toLowerCase()=='string'){
                    try{
                        data = JSON.parse(data);
                    }
                    catch(e){

                    }
                }
                if(!data.error){
                    alert('提交成功 ,请等待审核回复');
                }
                else{
                    alert(data.msg);
                }
                $('#doRegister').button('reset')
            })

        })
    })
</script>
