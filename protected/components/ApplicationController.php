<?php
class ApplicationController extends CController {
    public $layout='application';
	public $meta = array();


	public function beforeAction($action){

		return parent::beforeAction($action);
	}

	public function showMsg($msg, $type = 'success', $redirect = NULL) {
		Yii::app()->user->setFlash($type, $msg);
		if(!$redirect) {
			$redirect = $_SERVER['HTTP_REFERER'];
		} else {
			$redirect = $this->createUrl($redirect);
		}
		$this->redirect($redirect);
		Yii::app()->end();
	}
}