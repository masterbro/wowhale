<?php
class PartnerAdminBaseController extends CController {
	public $layout = "partner";
	public $pageTitle = "后台管理";
	public $current_admin = false;
	public $operation_name = false;
	public $partner_id;


	public function beforeAction($action){
		if($parent_before = parent::beforeAction($action)) {
			$user_id = Yii::app()->user->getId();
			if(!$user_id)
				$this->redirect('/manage/account/index');


			$this->partner_id = Yii::app()->user->getState('partner_id');
            if(!$this->partner_id)
                $this->redirect('/manage/account/index');
		}

		return parent::beforeAction($action);
	}

	public function showMsg($msg, $type = 'success', $redirect = NULL) {
		Yii::app()->user->setFlash($type, $msg);
		if(!$redirect) {
			$redirect = $_SERVER['HTTP_REFERER'];
		} else {
			$redirect = $this->createUrl($redirect);
		}
		$this->redirect($redirect);
		Yii::app()->end();
	}

	public function set_error($msg) {
		Yii::app()->user->setFlash('error', $msg);
	}


	public function getPartner() {
		$model = Partner::model()->findByPk($this->partner_id);
		return $model;
	}
}