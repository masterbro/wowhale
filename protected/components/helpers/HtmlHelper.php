<?php
class HtmlHelper {
    public static function get_head_img($app,$user_id){
        $cache = Yii::app()->cache;
        $cache_key  = 'wx_user_info_sh'.$user_id;
        if($data = $cache->get($cache_key)){
            return $data->avatar?$data->avatar:'/favicon.ico';
        }
        if(!$app){
            return "/favicon.ico";
        }
        $info = $app->getUserInfo('sh'.$user_id);
        if($info && $info->avatar){
            $cache->set($cache_key,$info,3600);
            return $info->avatar;
        }
        else{
            //$cache->set($cache_key,"/favicon.ico",60);
            return "/favicon.ico";
        }
    }
    public static function get_nick_name($app,$user_id){
        $cache = Yii::app()->cache;
        $cache_key  = 'wx_user_info_sh'.$user_id;
        if($data = $cache->get($cache_key)){
            return $data->name?$data->name:'';
        }
        if(!$app){
            return "";
        }
        $info = $app->getUserInfo('sh'.$user_id);
        if($info && $info->name){
            $cache->set($cache_key,$info,3600);
            return $info->name;
        }
        else{
            return "";
        }
    }
	public static function assets($uri) {
        if(Yii::app()->params['stc_url']){
            return 'http://'.Yii::app()->params['stc_url'].'/'.$uri;
        }
        else{
            return Yii::app()->baseUrl.'/assets/'.$uri;
        }

	}

	public static function image($uri) {
		return Yii::app()->controller->createAbsoluteUrl('/').'/assets/upload/'.$uri;
	}
    public static function wx_image($uri) {
        //var_dump(strpos($uri,'shp.qpic.cn'),$uri);
        if(Yii::app()->params['stc_url']){
            if(strpos($uri,'shp.qpic.cn')!==false){
                $url = str_replace('shp.qpic.cn',Yii::app()->params['stc_url'].'/upload/',$uri);
                $url = rtrim($url,'/');
                $url.='.jpg';
                $url = str_replace('https','http',$url);
                return $url;
            }
            else if(strpos($uri,'http')!==false){
                return $uri;
            }
            else{

            }
        }
        else if(strpos($uri,'http')!==false){
            return $uri;
        }
        //var_dump(strpos($uri,'http'));

        return Yii::app()->controller->createAbsoluteUrl('/').'/assets/upload/'.$uri;
    }

	public static function base_url($uri = '') {
		if($uri) $uri = '/'.ltrim($uri, '/');
		return Yii::app()->baseUrl.$uri;
	}


	public static function avatar($avatar) {
		return Yii::app()->baseUrl.'/assets/images/avatar.jpg';
	}


	public static function subText($text, $num = 40) {
		$text = strip_tags($text);
		$text = str_replace('&nbsp;', '', $text);
		//$text = str_replace(' ', '', $text);
		$text = str_replace("\r", '', $text);
		$text = str_replace("\n", '', $text);
		$text = str_replace("\t", '', $text);
		$return = self::subChinese($text, 0, $num, '');
		$return = htmlspecialchars($return);

		if($num < mb_strlen($text) && $return) $return .= "...";
		return $return;
	}

	public static function subChinese($str ,$start ,$width ,$trimmarker = '...') {
		$str = strip_tags($str);
		if(mb_strlen($str) < $width - $start) return $str;
		$output = preg_replace('/^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start.'}((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$width.'}).*/s', '\1', $str);
		return $output.$trimmarker;
	}

	/**
	 * 导出数据为excel表格
	 * $data    一个二维数组,结构如同从数据库查出来的数组
	 * $title   excel的第一行标题,一个数组,如果为空则没有标题
	 * $name 下载的文件名
	 */
	public static function exportExcel($data=array(),$title=array(),$name='report'){
		//导出xls 开始
		$html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">';
		$html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
		$html .= '<html>';
		$html .= '<head>';
		$html .= '<meta http-equiv="Content-type" content="text/html;charset=gbk" />';
		$html .= '<style id="Classeur1_16681_Styles"></style>';
		$html .= '</head>';
		$html .= '<body>';
		$html .= '<div id="Classeur1_16681" align=center x:publishsource="Excel">';
		$html .= '<table  x:str border=0 cellpadding=0 cellspacing=0 width=100% style="border-collapse: collapse">';
		if (!empty($title)){
			$html .= '<tr>';
			foreach ($title as $v) {
				$html .= '<th>'.$v.'</th>';
			}
			$html .= '</tr>';
		}
		if (!empty($data)){
			foreach($data as $val){
				$html .= "<tr>";
				foreach ($val as $cv) {
					$html .= "<td  class='xl2216681 nowrap'>{$cv}</td>";
				}
				$html .= "</tr>";
			}
		}

		if(strpos($_SERVER["HTTP_USER_AGENT"],"MSIE") || strpos($_SERVER["HTTP_USER_AGENT"],"Trident")){
			$name = urlencode($name);
			header("Pragma: public"); header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header('Content-Type: application/vnd.ms-excel; charset=gbk');
			header( 'Content-Transfer-Encoding: binary' );
		}
		else{
			header("Content-type: application/octet-stream;");
		}

		$html = iconv('utf-8', 'gbk', $html);
		header("Accept-Ranges: bytes");
		header("Accept-Length: ".strlen($html));
		header("Content-Disposition: attachment; filename=" . $name);
		echo $html;
        try{
            $log = Yii::app()->log;
            foreach($log->routes as $k=>$v){
                $_class = strtolower(get_class($v));
                if(stripos($_class, 'weblog')>-1 ){
                    $v->enabled = false;
                }
            }
        }catch(Exception $e){

        }
        Yii::app()->end();
	}
}