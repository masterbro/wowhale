<?php
class CurlHelper {
	public static function apiGet($uri, $get = array()) {
		$content = self::get($uri, array(
			'params' => $get ? json_encode($get) : '',
		));

		return self::parseResult($content);
	}

	public static function apiPost($uri, $post) {


		$data =  array(
			'params' => json_encode($post)
		);

		if($current_user = SessionHelper::currentUser())
			$data['sessionId'] = $current_user->sessionId;

		$content = self::post($uri, $data);

		return self::parseResult($content);
	}

	private static function parseResult($content) {
		if($content->status == ApiConfig::FAIL_PROCESS)
			throw new ApiException(ApiConfig::errorMsg($content->msg), $content->msg);

		return $content;
	}

	public static function get($uri, $get) {
		$ch	= curl_init();
		$url = Yii::app()->params['api_host'].$uri.'?'.http_build_query($get);
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$content = curl_exec($ch);
		$err_no = curl_errno($ch);
		curl_close($ch);

		if($err_no !== 0) {
			Yii::log("api request error , error num {$err_no}");
			throw new ApiException('系统错误', 1);
		}

		return json_decode($content);
	}

	public static function post($uri, $post) {
		$ch	= curl_init();
		$url = Yii::app()->params['api_host'].$uri;
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post );
		$content = curl_exec($ch);
		$err_no = curl_errno($ch);
		curl_close($ch);

		if($err_no !== 0) {
			Yii::log("api request error , error num {$err_no}");
			throw new ApiException('系统错误', $err_no);
		}

		return json_decode($content);
	}

	public static function postResponseHeader($uri, $post) {
		$ch	= curl_init();
		$url = Yii::app()->params['api_host'].$uri;
		$post = array('params' => json_encode($post));

		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, http_build_query($post) );
		curl_setopt($ch, CURLOPT_HEADER, TRUE);
		curl_setopt($ch, CURLOPT_NOBODY, FALSE);
		$content = curl_exec($ch);
		$err_no = curl_errno($ch);
		//curl_close($ch);

		if ($err_no === 0 && curl_getinfo($ch, CURLINFO_HTTP_CODE) == '200') {
			list($header, $body) = explode("\r\n\r\n", $content, 2);

			return array(
				'header' => $header,
				'body' => $body,
			);
		}

		return '';
	}
}