<?php
class SessionHelper {
	public static function isLogin(){
//		$token_name = self::cookieName();
		return self::getCookie('token');
	}

	private static function cookieName($name = 'token') {
		return 'wo_whale_'.$name;
	}

	public static function currentUser() {
		if(!($token = self::isLogin())) return false;

		if(!$user = Yii::app()->session['current_user']) {
			if( $user = User::model()->findByAttributes(array('token' => $token)) ) {
				$session = Yii::app()->session;
				$session['current_user'] = (object) $user->attributes;
			} else {
				self::destroy();
				return false;
			}
		}
        else{
            $cache_user = User::model()->cache(500)->findByAttributes(array('token' => $token));
            if(!$cache_user){
                self::destroy();
                return false;
            }
            else if((object) $cache_user->attributes!=$user){
                $user = (object) $cache_user->attributes;
            }
        }

		return $user;
	}

	public static function refreshSession($user) {
		$session = Yii::app()->session;
		$session['current_user'] = (object) $user->attributes;
	}

	public static function create($user, $remember = false) {
		$session = Yii::app()->session;
		$session['current_user'] = (object) $user->attributes;


		self::createCookie('token', $user->token, $remember);
	}


	public static function isContacts(){
		return self::getCookie('contacts');
	}
	public static function currentContacts($partner_id) {
		$cookie_name = 'contacts';
		if(!($token = self::getCookie($cookie_name))) return false;

		$return = true;

		$user = Yii::app()->session['current_contacts'];
		if(!$user) {
			if( $user = Contacts::model()->findByAttributes(array('token' => $token)) && $user->partner_id==$partner_id ) {
				$session = Yii::app()->session;
				$session['current_contacts'] = (object) $user->attributes;
			} else {
				$return = false;
			}
		} else if($user->partner_id != $partner_id) {
			$return = false;
		}
        else{
            $cache_user = Contacts::model()/*->cache(500)*/->findByAttributes(array('token' => $token));
            if(!$cache_user){
                //self::destroy();
                return false;
            }
            else if((object) $cache_user->attributes!=$user){
                $user = (object) $cache_user->attributes;
            }
        }

		if(!$return) {
			self::clearSession();
			$token_name = self::cookieName($cookie_name);
			$cookie = new CHttpCookie($token_name, FALSE, array('expire' => time()-1000));
			Yii::app()->request->cookies[$token_name] = $cookie;
			return false;
		}

		return $user;
	}

	public static function createContacts($contacts, $remember = false) {
		$session = Yii::app()->session;
		$session['current_contacts'] = (object) $contacts->attributes;


		self::createCookie('contacts', $contacts->token, $remember);
	}



	private static function createCookie($key, $value, $remember) {
		if($remember) {
			$expire_at = time() + 86400 * 180;
		} else {
			$expire_at = 0;
		}
		$token_name = self::cookieName($key);
		$cookie = new CHttpCookie($token_name, $value, array('expire' => $expire_at));
		Yii::app()->request->cookies[$token_name] = $cookie;
	}

	public static function getCookie($key) {
		$token_name = self::cookieName($key);
		return (Yii::app()->request->cookies->contains($token_name) ?
			Yii::app()->request->cookies[$token_name]->value : FALSE);
	}


	public static function destroy() {
		self::clearSession();
		$token_name = self::cookieName();
		$cookie = new CHttpCookie($token_name, FALSE, array('expire' => time()-1000));
		Yii::app()->request->cookies[$token_name] = $cookie;
	}

	private static function clearSession() {
		Yii::app()->session->clear();
		Yii::app()->session->destroy();
	}

}