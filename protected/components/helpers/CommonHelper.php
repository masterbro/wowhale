<?php
class CommonHelper {
	public static function validMobile($mobile) {
		return preg_match("#^1\d{10}$#", $mobile);
	}

	public static function isWechat() {
		return strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'micromessenger') !== FALSE;
	}

	public static function isMobileUa() {
		$ua = array(
			'Android', 'AvantGo', 'BlackBerry', 'DoCoMo', 'Fennec', 'iPod', 'iPhone', 'iPad',
			'J2ME', 'MIDP', 'NetFront', 'Nokia', 'Opera Mini', 'Opera Mobi', 'PalmOS', 'PalmSource',
			'portalmmm', 'Plucker', 'ReqwirelessWeb', 'SonyEricsson', 'Symbian', 'UP\\.Browser',
			'webOS', 'Windows CE', 'Windows Phone OS', 'Xiino'
		);

		$pattern = '/' . implode('|', $ua) . '/i';
		return (bool) preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
	}

	public static function isEmail($email) {
		///[a-z0-9-.]{1,30}@[a-z0-9-]{1,65}.(com|net|org|info|biz|([a-z]{2,3}.[a-z]{2}))/
		return preg_match( "/[a-z0-9-.]{1,30}@[a-z0-9-]{1,65}.(com|net|org|info|biz|([a-z]{2,3}.[a-z]{2}))/", $email);
	}


	public static function hashMobile($mobile) {
		return substr($mobile, 0, 3).'****'.substr($mobile, -4);
	}


	public static function unicodeEncode($str, $encoding = 'UTF-8', $prefix = "\\u", $postfix = '') {
		$str = iconv($encoding, 'UCS-2', $str);
		$arrstr = str_split($str, 2);
		$unistr = '';
		for($i = 0, $len = count($arrstr); $i < $len; $i++) {
			$dec = hexdec(bin2hex($arrstr[$i]));
			$unistr .= $prefix . $dec . $postfix;
		}
		return $unistr;
	}

	public static function getGroupField($list, $model, $list_field, $model_field) {
		if(!$list) return array();
		$return = array();

		$ids = CHtml::listData($list, $list_field, $list_field);

		foreach($ids as $k=>$v)
			if(!$v) unset($ids[$k]);

		if(count($ids)) {
			$result = $model->findAll('id IN ('. implode(',', $ids) .')');
			foreach($result as $r) {
				$return[$r->id] = $r->{$model_field};
			}
		}


		return $return;
	}

	public static function price($price) {
		return sprintf("%.2f",$price/100);
	}

	public static function age($date){
		$year_diff = '';
		$time = strtotime($date);
		if(FALSE === $time){
			return '';
		}

		$date = date('Y-m-d', $time);
		list($year,$month,$day) = explode("-",$date);
		$year_diff = date("Y") - $year;
		$month_diff = date("m") - $month;
		$day_diff = date("d") - $day;
		if ($day_diff < 0 || $month_diff < 0) $year_diff--;

		return $year_diff;
	}


	public static function mobileError($err = '') {
		$error_tips =  array(
			'404' => '页面不存在',
			'school_404' => '学校不存在',
			'notice_404' => '公告不存在或者已被删除',
			'pd_error' => '产品异常',
			'wx_config_404' => '请先配置学校企业号',
			'contacts_404' => '你的帐号还没有加入到学校通讯录，请联系校方',
			'coming' => '敬请期待',
            'not_correct_contact' => '不是该生教师或者家长',
            'user_404' => '用户信息获取出错,请重新打开或者联系管理员',
            'permison_404' => '无权进行此操作',
            'pay_not_use' => '该校还未开启在线缴费，请联系学校'
		);

		return $error_tips[$_GET['e']] ? $error_tips[$_GET['e']] : '未知错误，请重试 :(';
	}


	public static function getPreAuthCode($suite) {
		$cache_key = 'ww_wx_pre_auth_code_1_'.$suite->id;
		$cache = Yii::app()->cache->get($cache_key);
		if($cache === FALSE) {
			$app = new WxQySuite();
			$pre_auth_code = $app->get_pre_auth_code($suite->suite_id, $suite->suite_access_token);

			$cache = $pre_auth_code->pre_auth_code;
			Yii::app()->cache->set($cache_key, $cache, $pre_auth_code->expires_in - 100);
		}

		return $cache;
	}


//	public static function qyApp($parent_id = '') {
//		$base = Yii::app()->params['mobile_base'];
//
//		return array(
//			'contacts' => array(
//				'id' => 3,
//				'suite_id' => 1,
//                'help_img'=>TMP_PATH.'/../../help_img/contacts.png',
//				'ww_suite_id' => 1,
//				'name' => '通讯录',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '通讯录',
//							'url' => $base.'/contacts/index?partner_id='.$parent_id,
//						),
//					)
//				)
//			),
//
//			'notice' => array(
//				'id' => 4,
//				'suite_id' => 1,
//				'ww_suite_id' => 1,
//                'help_img'=>TMP_PATH.'/../../help_img/notice.png',
//				'name' => '微公告',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '历史公告',
//							'url' => $base.'/notice/index?partner_id='.$parent_id,
//						),
//                        array(
//                            'type' => 'view',
//                            'name' => '校园食谱',
//                            'url' => $base.'/notice/shipu?partner_id='.$parent_id,
//                        ),
//					)
//				)
//			),
//
//			'wap' => array(
//				'id' => 5,
//				'suite_id' => 1,
//				'ww_suite_id' => 1,
//                'help_img'=>TMP_PATH.'/../../help_img/wap.png',
//				'name' => '微网站',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '微网站',
//							'url' => $base.'/home/partner?id='.$parent_id,
//						),
//					)
//				)
//			),
//
//			'product' => array(
//				'id' => 3,
//				'suite_id' => 2,
//				'ww_suite_id' => 2,
//                'help_img'=>TMP_PATH.'/../../help_img/product.png',
//				'name' => '微缴费',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '微缴费',
//							'url' => $base.'/school/view?partner_id='.$parent_id,
//						),
//					)
//				)
//			),
//
//			'album' => array(
//				'id' => 1,
//				'suite_id' => 2,
//				'ww_suite_id' => 2,
//				'name' => '微相册',
//                'help_img'=>TMP_PATH.'/../../help_img/album.png',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '微相册',
//							'url' => $base.'/album/index?partner_id='.$parent_id,
//						),
//					)
//				)
//			),
//
//			'sign_log' => array(
//				'id' => 2,
//				'suite_id' => 2,
//				'ww_suite_id' => 2,
//				'name' => '微签到',
//                'help_img'=>TMP_PATH.'/../../help_img/sign_log.png',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '签到',
//							'url' => $base.'/signLog/index?partner_id='.$parent_id,
//						),
//						array(
//							'type' => 'view',
//							'name' => '我的记录',
//							'url' => $base.'/signLog/logs?partner_id='.$parent_id,
//						),
//					)
//				)
//			),
//
//			'order' => array(
//				'id' => 4,
//				'suite_id' => 2,
//				'ww_suite_id' => 2,
//				'name' => '微财务',
//                'help_img'=>TMP_PATH.'/../../help_img/order.png',
//				'menu' => array(
//					'button' => array(
//						array(
//							'type' => 'view',
//							'name' => '缴费报表',
//							'url' => $base.'/orderReport/?partner_id='.$parent_id,
//						),
//					)
//				)
//			),
//
//		);
//	}
    public static function number($num,$sep=3){
        if(!$num){
            return 0;
        }
        return $num*$sep;
    }

	public static function qyApp($parent_id = '') {
		$base = Yii::app()->params['mobile_base'];

		return array(
			'notice' => array(
				'id' => 1,
				'suite_id' => 3,
				'ww_suite_id' => 3,
				'help_img'=>TMP_PATH.'/../../help_img/notice.png',
				'name' => '校园通知',
				'menu' => array(
					'button' => array(
						array(
							'type' => 'view',
							'name' => '公告',
							'url' => $base.'/notice/index?partner_id='.$parent_id,
						),
						array(
							'type' => 'view',
							'name' => '食谱',
							'url' => $base.'/notice/shipu?partner_id='.$parent_id,
						),
                        array(
                            'type' => 'view',
                            'name' => '微网站',
                            'url' => $base.'/home/partner?id='.$parent_id,
                        ),
					)
				)
			),

			'album' => array(
				'id' => 2,
				'suite_id' => 3,
				'ww_suite_id' => 3,
				'name' => '校园相册',
				'help_img'=>TMP_PATH.'/../../help_img/album.png',
				'menu' => array(
					'button' => array(
						array(
							'type' => 'view',
							'name' => '相册',
							'url' => $base.'/album/index?partner_id='.$parent_id,
						),
					)
				)
			),

			'student' => array(
				'id' => 3,
				'suite_id' => 3,
				'ww_suite_id' => 3,
				'help_img'=>TMP_PATH.'/../../help_img/wap.png',
				'name' => '家长助手',
				'menu' => array(
					'button' => array(
						array(
							'type' => 'view',
							'name' => '交学费',
							'url' => $base.'/school/view?partner_id='.$parent_id,
						),
						array(
							//'type' => 'view',
							'name' => '宝宝请假',
                            'sub_button'=>array(
                                array(
                                    'type' => 'view',
                                    'name' => '请假',
							        'url' => $base.'/leave?partner_id='.$parent_id,
						        ),
                                array(
                                    'type' => 'view',
                                    'name' => '我的记录',
                                    'url' => $base.'/leave/logs?partner_id='.$parent_id,
                                ),
                            ),
							//'url' => $base.'/leave?partner_id='.$parent_id,
						),
                        array(
                            'type' => 'view',
                            'name' => '我的帐号',
                            'url' => $base.'/myInfo?partner_id='.$parent_id,
                        ),
					)
				)
			),

			'teacher' => array(
				'id' => 4,
				'suite_id' => 3,
				'ww_suite_id' => 3,
				'help_img'=>TMP_PATH.'/../../help_img/sign_log.png',
				'name' => '教师助手',
				'menu' => array(
					'button' => array(
						array(
							'type' => 'view',
							'name' => '签到',
                            'sub_button'=>array(
                                array(
                                    'type' => 'view',
                                    'name' => '签到',
                                    'url' => $base.'/signLog/index?partner_id='.$parent_id,
                                ),
                                array(
                                    'type' => 'view',
                                    'name' => '我的记录',
                                    'url' => $base.'/signLog/logs?partner_id='.$parent_id,
                                ),
                                array(
                                    'type' => 'view',
                                    'name' => '全校记录',
                                    'url' => $base.'/signLog/allLogs?partner_id='.$parent_id,
                                ),
                            ),
							//'url' => $base.'/signLog/index?partner_id='.$parent_id,
						),
                        array(
                            'type' => 'view',
                            'name' => '学生管理',
                            'sub_button'=>array(
                                array(
                                    'type' => 'view',
                                    'name' => '学生请假',
                                    'url' => $base.'/leave/logs?partner_id='.$parent_id,
                                ),
                                array(
                                    'type' => 'view',
                                    'name' => '学生信息',
                                    'url' => $base.'/myInfo?partner_id='.$parent_id,
                                ),
                            ),
                            //'url' => $base.'/signLog/index?partner_id='.$parent_id,
                        ),
						array(
							'type' => 'view',
							'name' => '缴费报表',
							'url' => $base.'/orderReport/?partner_id='.$parent_id,
						),
					)
				)
			),



			'system' => array(
				'id' => 5,
				'suite_id' => 3,
				'ww_suite_id' => 3,
				'name' => '娃娃营',
				'help_img'=>TMP_PATH.'/../../help_img/sign_log.png',
				'menu' => array(
					'button' => array(
						array(
							'type' => 'view',
							'name' => '帮助',
							'url' => $base.'/system/help',
						),
						array(
							'type' => 'view',
							'name' => '资讯',
							'url' => $base.'/system/news',
						),
					)
				)
			),

		);
	}


}