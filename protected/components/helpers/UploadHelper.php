<?php
class UploadHelper {

	public static function image($model, $field,  $size, $dir, $resize = FALSE, $rename = FALSE, $action = 'crop') {
		if(is_object($model)) {
			$uploadedFile = CUploadedFile::getInstance($model, $field);
		} else {
			$files = CUploadedFile::getInstancesByName($field);

			if(is_array($files)){
				if(empty($files)){
					$uploadedFile = CUploadedFile::getInstanceByName($field);
				}
				else{
					$uploadedFile =   array_shift($files)  ;
				}
			}
			else{
				$uploadedFile =   NULL;
			}

		}
		return self::save($uploadedFile, $size, $dir, $resize, $rename, $action);
	}

	public static function multiImage($field, $size, $dir, $resize = FALSE) {
		$files = CUploadedFile::getInstancesByName($field);
		$return = array();
		foreach($files as $f) {
			$tmp = array('image' => '', 'error' => '');
			try{
				$tmp['image'] = self::save($f, $size, $dir, $resize);
			} catch(UploadException $e) {
				if($e->getCode() != 201) $tmp['error'] = $e->getMessage();
			} catch(CException $e) {
				$tmp['error'] = $e->getMessage();
			}
			$return[] = $tmp;
		}

		return $return;
	}

	public static function save($uploadedFile, $size, $dir, $resize = FALSE, $rename = FALSE, $action) {
		if(empty($uploadedFile))
			throw new UploadException('没有文件被上传。');

		if($uploadedFile->getHasError())
			throw new UploadException(self::errorText($uploadedFile->getError()));

		if(!self::checkType($uploadedFile->getType()))
			throw new UploadException('不允许的文件类型，只能上传gif,png和jpg格式的图片。');

		if($rename) {
			$file_name = $rename.'.'.$uploadedFile->getExtensionName();
		} else {
			$rnd = md5(microtime().rand(0,9999));
			$file_name = "{$rnd}.{$uploadedFile->getExtensionName()}";
		}

		$dir = trim($dir, '/');
		$file_path = UPLOAD_PATH.'/'.$dir.'/'.$file_name;
		if(!is_dir(dirname($file_path))) mkdir(dirname($file_path), 0777, TRUE);
		if(!$uploadedFile->saveAs($file_path)){
			$error = $uploadedFile->getError();
			throw new UploadException(self::errorText($error));
		}

		if(function_exists('exif_read_data') && (strpos( 'j', strtolower($uploadedFile->getExtensionName()) ) !== FALSE) ) {
			$exif = @exif_read_data($file_path);
			if(!empty($exif['Orientation'])) {
				$image = Yii::app()->image->load($file_path);
				switch($exif['Orientation']) {
					case 8:
						$image->rotate(90);
						$image->save();
						break;
					case 3:
						$image->rotate(180);
						$image->save();
						break;
					case 6:
						$image->rotate(-90);
						$image->save();
						break;
				}
			}
		}


		if($resize)
			self::resize($file_path, $size, '', $action);


		return "{$dir}/{$file_name}";
	}

	public static function resize($image, $size, $new_name = '', $action) {
		$img_size =  getimagesize($image);
        if(!$size[0]&&$size[1]){
            $size[0]=($size[1]/$img_size[1])*$img_size[0];
        }
        else if(!$size[1]&&$size[0]){
            $size[1]=($size[0]/$img_size[0])*$img_size[1];
        }

		if($img_size[0] < $size[0]) {
			if($size[0] == $size[1]) {
				$size[0] = $img_size[0];
				$size[1] = $size[0];
			} else {
				$tmp = $img_size[0]/$size[0];
				$size[0] = $img_size[0];
				$size[1] = $size[1] * $tmp;
			}

		}

		$image = Yii::app()->image->load($image);
		if($action == 'resize') {
			$image->resize($size[0], $size[1], Image::NONE)->quality(75);
		} else {
			$image->crop($size[0], $size[1], 0, 0)->quality(75);
		}


		if($new_name && !is_dir(dirname($new_name))) mkdir(dirname($new_name), 0777, TRUE);
		$image->save($new_name);
	}

	private static function checkType($type) {
		return in_array($type, array(
			'image/jpeg', 'image/png','image/gif','image/jpg','image/pjpeg','image/x-png','image/bmp'
		));
	}

	private static function errorText($code) {
		switch($code) {
			case UPLOAD_ERR_INI_SIZE :
				return '超过了允许上传文件的大小限制';
				break;
			case UPLOAD_ERR_FORM_SIZE :
				return '超过了文件的大小MAX_FILE_SIZE选项指定的值';
				break;
			case UPLOAD_ERR_PARTIAL :
				return '文件只有部分被上传';
				break;
			case UPLOAD_ERR_NO_FILE :
				return '没有文件被上传';
				break;
			case UPLOAD_ERR_CANT_WRITE :
				return '目标文件夹不可写';
				break;
		}
	}

	public static function diverseArray($vector) {
		$result = array();
		foreach($vector as $key1 => $value1)
			foreach($value1 as $key2 => $value2)
				$result[$key2][$key1] = $value2;
		return $result;
	}

	public static function moveTmp($tmp, $to_dir, $return_false = false) {
		if(!preg_match("#^tmp/#", $tmp))
			return $return_false ? false : $tmp;

		$to_dir = trim($to_dir, '/');
		$to_dir = '/'.$to_dir.'/';

		$tmp_path = UPLOAD_PATH.'/'.$tmp;
		if(file_exists($tmp_path)) {
			$path_parts = pathinfo($tmp);
			$file_path = UPLOAD_PATH.$to_dir;

			if(!is_dir($file_path)) mkdir($file_path, 0777, true);

			@rename($tmp_path, $file_path.$path_parts['basename']);
			return $to_dir.$path_parts['basename'];
		}

		return false;
	}
}