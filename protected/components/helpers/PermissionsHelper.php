<?php
class PermissionsHelper {


	public static  function rules() {
		return array(
			array(
				'name' => '学校管理',
				'rule' => array(
					'school_index' => '列表',
					'school_create' => '添加',
					'school_update' => '编辑',
					'school_delete' => '删除',
					'school_mobile' => '手机页面管理',
					'school_resetApp' => '更新应用',
					'school_apply' => '学校申请',
					'school_applyStatus' => '学校申请审核',
				),
			),

			array(
				'name' => '学生列表',
				'rule' => array(
					'student_index' => '列表',
				),
			),

			array(
				'name' => '老师列表',
				'rule' => array(
					'teacher_index' => '列表',
				),
			),

			array(
				'name' => '公告管理',
				'rule' => array(
					'notice_index' => '查看',
				),
			),

			array(
				'name' => '商家管理',
				'rule' => array(
					'merchant_index' => '列表',
					'merchant_create' => '添加',
					'merchant_update' => '编辑',
					'merchant_delete' => '删除',
					'merchant_mobile' => '手机页面管理',
					'merchant_apply' => '商家申请',
					'merchant_applyStatus' => '商家申请审核',
				),
			),

			array(
				'name' => '订单管理',
				'rule' => array(
					'order_index' => '列表',
				),
			),

			array(
				'name' => '产品管理',
				'rule' => array(
					'product_index' => '列表',
					'product_create' => '添加',
					'product_update' => '编辑',
					'product_status' => '上下架',
				),
			),

			array(
				'name' => '产品分类管理',
				'rule' => array(
					'category_index' => '列表',
					'category_create' => '添加',
					'category_update' => '编辑',
					'category_delete' => '删除',
				),
			),

			array(
				'name' => '结算管理',
				'rule' => array(
					'balance_index' => '列表',
					'balance_view' => '详情',
					'balance_reject' => '拒绝',
					'balance_apply' => '通过审核并结算',
				),
			),

			array(
				'name' => '文章管理',
				'rule' => array(
					'page_index' => '列表',
					'page_update' => '编辑',
				),
			),

			array(
				'name' => '首页BANNER管理',
				'rule' => array(
					'slider_index' => '列表',
					'slider_create' => '添加',
					'slider_update' => '编辑',
					'slider_delete' => '删除',
				),
			),
		);
	}
}