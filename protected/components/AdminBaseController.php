<?php
class AdminBaseController extends CController {
    public $layout = "application";
    public $pageTitle = "后台管理";
	public $current_admin = false;
	public $operation_name = false;
	private $skip_access = array('home');
	protected $skip_access_action = array();


	public function beforeAction($action){
		if($parent_before = parent::beforeAction($action)) {
			$user_id = Yii::app()->user->getId();
			if(!$user_id)
				$this->redirect('/manage/account/index');

			if(!$this->operation_name)
				$this->operation_name = strtolower($this->id.'_'.$action->id);

			if($this->id != 'home' && Yii::app()->user->getState('type')) {
				$this->showMsg('权限不足', 'error');
			}


			if(
				$user_id > 1 //super admin
				&& !in_array($this->id, $this->skip_access) //skip controllers
				&& !in_array($action->id, $this->skip_access_action) //skip actions
				&& !Yii::app()->user->checkAccess($this->operation_name)
			) {
				if(Yii::app()->request->isAjaxRequest) {
					JsonHelper::show(array(
						'error' => true,
						'msg' => '权限不足',
					));
				} else {
					$this->showMsg('权限不足', 'error');
				}
			}

			$this->current_admin = Manager::model()->findByPk($user_id);
		}

		/*若是管理学校的后台界面则采用管理学校的布局*/
		$role_id = $this->current_admin->role_id;
		$role_id_school = Yii::app()->params['role_id']['school'];
		if($role_id == $role_id_school ){
			$this->layout = 'manageSchoolPartner';
		}

		return parent::beforeAction($action);
	}

    public function showMsg($msg, $type = 'success', $redirect = NULL) {
        Yii::app()->user->setFlash($type, $msg);
        if(!$redirect) {
            $redirect = $_SERVER['HTTP_REFERER'];
        } else {
            $redirect = $this->createUrl($redirect);
        }
        $this->redirect($redirect);
        Yii::app()->end();
    }

    public function set_error($msg) {
        Yii::app()->user->setFlash('error', $msg);
    }


	//管理员管理范围
	protected function partner_scope($alias = 't.partner_id') {
		if(!$this->current_admin->partner_scope)
			return '';

		return "$alias IN (select partner_id from {{manager_scope}} where manager_id = {$this->current_admin->id})";
	}
}