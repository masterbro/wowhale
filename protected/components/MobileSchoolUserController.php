<?php
class MobileSchoolUserController extends MobileApplicationController {
	public $layout = "application";
	public $pageTitle = "娃娃营";
	public $current_user = false;
	public $partner_id = 0;
	public $page_tracking = false;
	public $partner = false;


	public function beforeAction($action){

		if($parent_before = parent::beforeAction($action)) {
			$partner_id = Yii::app()->request->getParam('partner_id');
			if(!$partner_id)
				$this->showError('404');

			$this->partner = $this->getPartner($partner_id);
			if(!$this->partner)
				$this->showError('404');

			$this->current_user = SessionHelper::currentContacts($partner_id);

			//for dev
			if(!$this->current_user && Yii::app()->params['evn'] != 'production') {
				$user = Contacts::model()->find('partner_id = '.$partner_id .' And  id=33');
				SessionHelper::createContacts($user, true);
				$this->current_user = $user;
			}


			$need_oauth = false;
			if(!$this->current_user) {
				if(Yii::app()->request->isAjaxRequest) {
					JsonHelper::show(array(
						'error' => true,
						'msg' => '请先登录'
					));
				} else {
                    //先取学校自身的open_id获取登录状态
					//$need_oauth = true;
                    $rd = urlencode($this->createAbsoluteUrl(Yii::app()->getRequest()->getUrl()));
                    $redirect = urlencode( $this->createAbsoluteUrl('/wx/school?rd='.$rd) );

                    $config = WechatConfig::model()->find("partner_id = {$this->partner_id}");
                    if(!$config)
                        $this->showError('wx_config_404');

                    $this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$config->corpid.'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$this->partner_id.'#wechat_redirect');

                }
			} else {
				if($this->partner->pay_to == Partner::$pay_to_key[0] && !$this->current_user->system_openid) {
					$need_oauth = true;
				} else if($this->partner->pay_to == Partner::$pay_to_key[1] && !$this->current_user->partner_openid) {
					$need_oauth = true;
				}
                else if($this->partner->pay_to == Partner::$pay_to_key[2] && !$this->current_user->system_qy_openid) {
                    $need_oauth = true;
                }

			}

			if($need_oauth) {
				$rd = urlencode($this->createAbsoluteUrl(Yii::app()->getRequest()->getUrl()));
				$open_id = Yii::app()->session['contacts_openid'];
				if($open_id) {
					//contacts_openid 当前联系人的openid未与联系人绑定则跳转到登录页进行绑定
					$this->redirect('/session/school?partner_id='.$this->partner_id.'&rd='.$rd);
				} else {

					//收款方是系统的要取系统公众号的open id来识别用户
					if($this->partner->pay_to == Partner::$pay_to_key[0]) {
						$redirect = urlencode( $this->createAbsoluteUrl('/wx/schoolSystem?rd='.$rd.'&c_id='.$this->current_user->id) );

						$wx_config = Yii::app()->params['wx_payment'];
						$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$this->partner_id.'#wechat_redirect');
					} else if($this->partner->pay_to == Partner::$pay_to_key[1]) {
						//收款方是学校自己的则取学校企业号的open id来识别用户
						$redirect = urlencode( $this->createAbsoluteUrl('/wx/school?rd='.$rd) );

						$config = WechatConfig::model()->find("partner_id = {$this->partner_id}");
						if(!$config)
							$this->showError('wx_config_404');

						$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$config->corpid.'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$this->partner_id.'#wechat_redirect');
					}
                    else{
                        $redirect = urlencode( $this->createAbsoluteUrl('/wx/schoolSystemQy?rd='.$rd.'&c_id='.$this->current_user->id) );

                        $wx_config = Yii::app()->params['wx_payment_qy'];
                        $this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state='.$this->partner_id.'#wechat_redirect');

                    }
				}

			}

		}

		return $parent_before;
	}

}