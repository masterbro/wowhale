<?php
class MobileApplicationController extends CController {
	public $layout = "application";
	public $pageTitle = "娃娃营";
	public $current_user = false;
	public $page_tracking = false;
	public $partner_id = 0;

	public function beforeAction($action){

		return parent::beforeAction($action);
	}

	public function getPartner($id) {
		$id = intval($id);
		if(!$id)
			$this->showError('404');

		$model = Partner::model()->findByPk($id);

		if(!$model || $model->is_deleted)
			$this->showError('404');

		$this->pageTitle = $model->name;
		$this->partner_id = $model->id;

		return $model;
	}


	public function showMsg($msg, $type = 'success', $redirect = NULL) {
		Yii::app()->user->setFlash($type, $msg);
		if(!$redirect) {
			$redirect = $_SERVER['HTTP_REFERER'];
		} else {
			$redirect = $this->createUrl($redirect);
		}
        if(!$redirect){
            $redirect = '/';
        }
		$this->redirect($redirect);
		Yii::app()->end();
	}


	public function showError($key) {
		if(Yii::app()->request->isAjaxRequest) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => CommonHelper::mobileError($key)
			));
		} else {
			$this->redirect('/error?e='.$key);
		}

	}
    public function afterRender($view, &$output){
        if(Yii::app()->request->getParam('ys')){
            $scripts = array();
            $ereg='/<script[^>]*>([\s\S])*<\/script>/isU';
            preg_match_all($ereg,$output,$scripts);
            $minified ='';
            Yii::import('application.extensions.*');
            $s = array();
            $sec = '';
            foreach($scripts[0] as $script){
                $ereg1='/src=[\'\"]([^\'\"]*)[\'\"]/is';
                $src = '';
                $code='';
                preg_match_all($ereg1,$script,$src);
                if(isset($src[1][0])&&$src[1][0]!=''){
                    $sec .= $code = $src[1][0];
                }
                else{
                    $sec .= $script;
                }
                $s[] = array('script'=>$script,'src'=>$code);
            }
            $sec_md5 = md5($sec);
            //if(!file_exists(TMP_PATH.'/../'.$sec_md5.'js')){
                foreach($s as $v){
                    $script = $v['script'];
                    $code = $v['src'];
                    if($code){
                        if(strpos($code,'/')===0||strpos($code,'./')===0||strpos($code,'../')===0){
                            $code = $_SERVER['DOCUMENT_ROOT'].$code;
                            //var_dump($code);
                        }
                        $code = @file_get_contents($code);
                        //var_dump($code);
                    }
                    else{
                        $code = preg_replace('/<script[^>]*>(.*)<\/script>/is','$1',$script);
                    }
                    if($code){
                        $minified .= JSMinPlus::minify($code).';';
                        $output = str_replace($script,'',$output);
                    }
                //}
                @file_put_contents(TMP_PATH.'/../'.$sec_md5.'.js',$minified);
            }
            $output.='<script src="/assets/upload/'.$sec_md5.'.js">'.$minified.'</script>';
        }

    }

}