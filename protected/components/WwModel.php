<?php
class WwModel extends CActiveRecord {
	const ST_NORMAL = 0;
	const ST_DELETED = 1;

	public function behaviors(){
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior'
			),
		);
	}


	public function getErrorInfo() {
		return count($this->getErrors()) ? array_shift(array_shift($this->getErrors())) : '';
	}

	public function recently($limit=5){
		$this->getDbCriteria()->mergeWith(array(
			'order'=>'create_time DESC',
			'limit'=>$limit,
		));

		return $this;
	}

	public function hot($limit=5){
		$this->getDbCriteria()->mergeWith(array(
			'order'=>'hits DESC',
			'limit'=>$limit,
		));

		return $this;
	}
}