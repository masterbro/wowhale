<?php
class MobileBaseController extends CController {
	public $layout = "application";
	public $pageTitle = "娃娃营";
	public $current_user = false;
	public $partner_id = 0;
	public $page_tracking = true;


	public function beforeAction($action){

		if($parent_before = parent::beforeAction($action)) {

			//for dev
			if(!SessionHelper::isLogin() && Yii::app()->params['evn'] != 'production') {
				$user = User::model()->findByPk(1);
				SessionHelper::create($user, true);
			}


			if(!SessionHelper::isLogin()) {
				if(Yii::app()->request->isAjaxRequest) {
					JsonHelper::show(array(
						'error' => true,
						'msg' => '请先登录'
					));
				} else {

					$wx_config = Yii::app()->params['wx_payment'];
					$redirect = urlencode( $this->createAbsoluteUrl('/wx/callback?rd='.urlencode($this->createAbsoluteUrl(Yii::app()->getRequest()->getUrl()))) );
					$this->redirect('https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$wx_config['app_id'].'&redirect_uri='.$redirect.'&response_type=code&scope=snsapi_base&state=STATE#wechat_redirect');
				}
			}



			$this->current_user = SessionHelper::currentUser();
		}

		return parent::beforeAction($action);
	}

	public function showError($key) {
		$this->redirect('/error?e='.$key);
	}


	public function getPartner($id) {
		$id = intval($id);
		if(!$id)
			$this->showError('404');

		$model = Partner::model()->findByPk($id);

		if(!$model || $model->is_deleted)
			$this->showError('404');

		$this->pageTitle = $model->name;
		$this->partner_id = $model->id;

		return $model;
	}


	public function showMsg($msg, $type = 'success', $redirect = NULL) {
		Yii::app()->user->setFlash($type, $msg);
		if(!$redirect) {
			$redirect = $_SERVER['HTTP_REFERER'];
		} else {
			$redirect = $this->createUrl($redirect);
		}
		$this->redirect($redirect);
		Yii::app()->end();
	}

}