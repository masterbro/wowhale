<?php
class UserIdentity extends CUserIdentity {
	private $_id;
	public function authenticate()
	{
		$record = Manager::model()->findByAttributes(array('username'=>$this->username));
		if($record===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($record->password!==$record->hashPassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else if($record->status!=1)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			if($record->type == 1) {
				$school = Partner::model()->findByPk($record->partner_id);
				if(!$school || $school->is_deleted) {
					$this->errorCode=self::ERROR_PASSWORD_INVALID;

					return !$this->errorCode;
				}

			}

			$this->_id=$record->id;
			$this->setState('last_login', $record->last_login);
			$this->setState('username', $record->username);
			$this->setState('type', $record->type);
            if($record->realname){
                $this->setState('realname', $record->realname);
            }
			if($record->type == 1) {
				$this->setState('school', $school->name);
				$this->setState('partner_id', $school->id);
			}


			$record->last_login = time();
			$record->save();

			$this->errorCode=self::ERROR_NONE;
		}
		return !$this->errorCode;
	}

	public function getId()
	{
		return $this->_id;
	}
}