<?php
class SmsContent {
	const MERCHANT_REQUEST = "你好，欢迎申请入驻娃娃营，你的验证码是 %s 【有效期30分钟】";

	const MERCHANT_REQUEST_APPROVE = "恭喜，你的入驻申请已经审核通过，请登录http://www.wwcamp.com 查看。";
	const MERCHANT_REQUEST_REJECT = "对不起，你的入驻申请未通过审核，请登录http://www.wwcamp.com 补齐资料。";


	const SCHOOL_CONTACTS_LOGIN = "你好，你的登录验证码是 %s 【有效期30分钟】";
    const SCHOOL_AFTER_REGISTER ="亲爱的园长，我们收到了您的申请，非常感谢您的关注！

我们已启动贵园微信企业号申请手续，并会与您电话联系，安排技术人员指导贵园使用。

再次感谢！";
}