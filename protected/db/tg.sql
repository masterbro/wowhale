
CREATE TABLE IF NOT EXISTS `ww_tg_school` (
  `school_name` varchar(128) NOT NULL,
  `school_link` varchar(64) NOT NULL,
  `school_id` bigint(11) NOT NULL,
  `price` int(11) NOT NULL,
  `price_unit` varchar(64) NOT NULL,
  `tel` varchar(64) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `comment_count` int(11) NOT NULL,
  `description` text NOT NULL,
  `city` varchar(128) NOT NULL,
  `address` varchar(256) DEFAULT NULL,
  `xingzhi` varchar(64) DEFAULT NULL COMMENT '性质',
  `gongyi` varchar(64) DEFAULT NULL COMMENT '公益',
  `bane` varchar(128) DEFAULT NULL COMMENT '班额',
  `yinshi` varchar(128) DEFAULT NULL COMMENT '饮食',
  `shenghuofei` varchar(128) DEFAULT NULL COMMENT '生活费',
  `dengji` varchar(64) DEFAULT NULL COMMENT '等级',
  `school_tags` text,
  `imgs` text,
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ww_tg_school_comment`
--

CREATE TABLE IF NOT EXISTS `ww_tg_school_comment` (
  `OauthAvatarMid` varchar(128) DEFAULT NULL,
  `CommentId` int(11) NOT NULL,
  `SchoolId` bigint(11) DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  `AuthId` int(11) DEFAULT NULL,
  `Body` text,
  `CommentType` int(11) DEFAULT NULL,
  `Score1` int(4) DEFAULT NULL,
  `Score2` int(4) DEFAULT NULL,
  `Score3` int(4) DEFAULT NULL,
  `Score4` int(4) DEFAULT NULL,
  `Score5` int(4) DEFAULT NULL,
  `PhotoAmount` int(11) DEFAULT NULL,
  `Tuition` int(11) DEFAULT NULL,
  `BoardWages` int(11) DEFAULT NULL,
  `CommentFrom` int(11) DEFAULT NULL,
  `CreateDateTime` datetime DEFAULT NULL,
  `IsEnable` tinyint(2) DEFAULT NULL,
  `ReplyAmount` int(11) DEFAULT NULL,
  `PraiseAmount` int(11) DEFAULT NULL,
  `OauthAvatar` varchar(128) DEFAULT NULL,
  `OauthNick` varchar(128) DEFAULT NULL,
  `OpenId` varchar(128) DEFAULT NULL,
  `NickName` varchar(128) DEFAULT NULL,
  `AccountType` int(11) DEFAULT NULL,
  `Avatar` varchar(128) DEFAULT NULL,
  `AvatarSmall` varchar(128) DEFAULT NULL,
  `AvatarMid` varchar(128) DEFAULT NULL,
  `AvatarLarge` varchar(128) DEFAULT NULL,
  `Photos` text NOT NULL,
  PRIMARY KEY (`CommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ww_tg_school_comment_replys`
--

CREATE TABLE IF NOT EXISTS `ww_tg_school_comment_replys` (
  `OauthAvatarMid` int(11) DEFAULT NULL,
  `ReplyId` int(11) NOT NULL DEFAULT '0',
  `SchoolId` bigint(20) DEFAULT NULL,
  `CommentId` int(11) DEFAULT NULL,
  `AuthorId` int(11) DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  `ToAccountId` int(11) DEFAULT NULL,
  `ToReplyId` int(11) DEFAULT NULL,
  `Body` text,
  `CommentFrom` int(11) DEFAULT NULL,
  `PraiseAmount` int(11) DEFAULT NULL,
  `CreateDateTime` datetime DEFAULT NULL,
  `OauthAvatar` varchar(128) DEFAULT NULL,
  `OauthNick` varchar(128) DEFAULT NULL,
  `OpenId` varchar(128) DEFAULT NULL,
  `NickName` varchar(128) DEFAULT NULL,
  `AccountType` int(11) DEFAULT NULL,
  `Avatar` varchar(128) DEFAULT NULL,
  `AvatarSmall` varchar(128) DEFAULT NULL,
  `AvatarMid` varchar(128) DEFAULT NULL,
  `AvatarLarge` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`ReplyId`),
  KEY `SchoolId` (`SchoolId`,`CommentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;