ALTER TABLE `ww_order` ADD `import_from` TINYINT NOT NULL DEFAULT '0' AFTER `shipping_info`;


#2015.07.06
ALTER TABLE `ww_school` ADD `type` TINYINT(1) DEFAULT '0' AFTER `title`;

ALTER TABLE `ww_product` CHANGE `school_id` `partner_id` INT(11) NOT NULL;

ALTER TABLE `ww_manager` CHANGE `school_id` `partner_id` INT(11) NULL DEFAULT NULL;


ALTER TABLE `ww_order` ADD `into_balance` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0为未入帐 1为已经入帐' AFTER `begin_time`;


#20150708
ALTER TABLE `ww_product` ADD `image` VARCHAR(255) DEFAULT NULL AFTER `status`;
ALTER TABLE `ww_product` ADD `content` TEXT NULL DEFAULT NULL AFTER `image`;
ALTER TABLE `ww_user` ADD `student_grade` VARCHAR(255) NULL DEFAULT NULL AFTER `student_name`;



#20150719
ALTER TABLE `ww_balance_apply` ADD `fee` INT NOT NULL DEFAULT '0' AFTER `money`;
ALTER TABLE `ww_user` ADD `from_partner` INT NOT NULL DEFAULT '0' AFTER `token`;

ALTER TABLE `ww_user` ADD `mobile` VARCHAR(20) NULL DEFAULT NULL AFTER `id`, ADD UNIQUE (`mobile`) ;


#20150721
ALTER TABLE `ww_product` ADD `cid` INT NULL DEFAULT NULL AFTER `partner_id`;


#20150722
ALTER TABLE `ww_order_tmp` ADD `remark` VARCHAR(255) NULL DEFAULT NULL AFTER `shipping_info`;
ALTER TABLE `ww_order` ADD `remark` VARCHAR(255) NULL DEFAULT NULL AFTER `shipping_info`;


#20150726
ALTER TABLE `ww_manager` ADD `join_step` TINYINT(1) NULL DEFAULT NULL;
ALTER TABLE `ww_partner_apply` ADD `manager_id` INT NULL DEFAULT NULL AFTER `partner_id`;
ALTER TABLE `ww_partner_apply` ADD `reason` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;


#20150730
ALTER TABLE `ww_partner_apply` ADD `description` VARCHAR(255) NULL DEFAULT NULL AFTER `license`, ADD `website` VARCHAR(255) NULL DEFAULT NULL AFTER `description`, ADD `wechat_id` VARCHAR(255) NULL DEFAULT NULL AFTER `website`;



ALTER TABLE `ww_partner` ADD `pay_to` VARCHAR(10) NOT NULL DEFAULT 'system' AFTER `type`;

ALTER TABLE `ww_order_tmp` ADD `student_id` INT NULL DEFAULT NULL AFTER `remark`, ADD `pay_to` TINYINT(1) NOT NULL DEFAULT '1' AFTER `student_id`;
ALTER TABLE `ww_order` ADD `student_id` INT NULL DEFAULT NULL AFTER `remark`, ADD `pay_to` TINYINT(1) NOT NULL DEFAULT '1' AFTER `student_id`;
ALTER TABLE `ww_order` ADD `contacts_id` INT NULL DEFAULT NULL AFTER `student_id`;
ALTER TABLE `ww_order_tmp` ADD `contacts_id` INT NULL DEFAULT NULL AFTER `student_id`;

ALTER TABLE `ww_order_tmp` CHANGE `uid` `uid` INT(11) NULL DEFAULT NULL;
ALTER TABLE `ww_order` CHANGE `uid` `uid` INT(11) NULL DEFAULT NULL;



ALTER TABLE `ww_contacts` ADD `system_openid` VARCHAR(255) NULL DEFAULT NULL AFTER `token`, ADD `partner_openid` VARCHAR(255) NULL DEFAULT NULL AFTER `system_openid`;



#20150817
ALTER TABLE `ww_wx_suite` ADD `suite_access_token` VARCHAR(255) NULL DEFAULT NULL AFTER `suite_ticket`, ADD `suite_access_token_expired` INT NULL DEFAULT NULL AFTER `suite_access_token`;
ALTER TABLE `ww_wechat_config` ADD `permanent_code` VARCHAR(255) NULL DEFAULT NULL AFTER `corpsecret`;
ALTER TABLE `ww_wechat_config` ADD `suite_id` VARCHAR(255) NULL DEFAULT NULL AFTER `expired_at`, ADD `auth_corpid` VARCHAR(255) NULL DEFAULT NULL AFTER `suite_id`;
ALTER TABLE `ww_wechat_config` ADD COLUMN `status` int(1) DEFAULT 1 AFTER `update_time`;
ALTER TABLE `ww_wechat_config` ADD `data` TEXT NULL DEFAULT NULL AFTER `status`;

ALTER TABLE `ww_wx_suite` ADD `appid` TEXT NULL DEFAULT NULL AFTER `suite_access_token_expired`;
UPDATE `ww_wx_suite` SET `appid` = '{"student_teacher":1,"teacher":2}' WHERE `ww_wx_suite`.`id` = 1;


#20150818
ALTER TABLE  `ww_wechat_config` ADD  `js_ticket` VARCHAR( 255 ) NOT NULL AFTER  `expired_at` ,
ADD  `js_ticket_expired_at` INT( 11 ) NOT NULL AFTER  `js_ticket` ;

CREATE TABLE IF NOT EXISTS `ww_sign_log` (
  `id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL COMMENT '学校id',
  `contacts_id` int(11) NOT NULL COMMENT '联系人id',
  `time` int(11) NOT NULL,
  `type` tinyint(2) NOT NULL COMMENT '1上班  2下班',
  `note` varchar(255) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#20150818
ALTER TABLE `ww_wechat_config` ADD `chat_id` BIGINT NOT NULL DEFAULT '1' AFTER `status`;

#20150819
ALTER TABLE  `ww_partner` ADD  `sign_log_point` VARCHAR( 128 ) NULL DEFAULT NULL COMMENT  '签到经纬度';
ALTER TABLE  `ww_partner` ADD  `sign_log_limit` INT( 11 ) NULL DEFAULT  '500' COMMENT  '签到精度';

#20150824

CREATE TABLE IF NOT EXISTS `ww_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT ,
  `partner_id` int(11) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL COMMENT '//类型 图片/视频/声音等',
  `pic_url` varchar(256) DEFAULT NULL,
  `title` varchar(256) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`,`grade_id`,`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ww_partner_apply` ADD `type` TINYINT(1) NULL DEFAULT '1' AFTER `manager_id`;
ALTER TABLE `ww_partner_apply` CHANGE `email` `email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

#20150826
ALTER TABLE  `ww_contacts` ADD  `system_qy_openid` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `system_openid` ;
ALTER TABLE  `ww_partner` CHANGE  `pay_to`  `pay_to` VARCHAR( 12 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'system';


#20150829
ALTER TABLE `ww_wechat_config` ADD COLUMN `ww_suite_id` int DEFAULT NULL AFTER `data`;


ALTER TABLE  `ww_wechat_config` ADD  `leader_department` INT( 11 ) NULL DEFAULT NULL AFTER `teacher_department` ;
ALTER TABLE  `ww_contacts` ADD  `first_in_send` TEXT NULL DEFAULT NULL AFTER  `partner_openid` ;

#20150903
CREATE TABLE IF NOT EXISTS `ww_school_shipu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `type` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8,
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

#20150906
ALTER TABLE  `ww_partner_apply` ADD  `extra` TEXT NULL ;

#20150908
CREATE TABLE IF NOT EXISTS `ww_leaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `contacts_id` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `confirm_at` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `start_at` int(11) DEFAULT NULL,
  `end_at` int(11) DEFAULT NULL,
  `reason` text,
  `confirm_txt` text,
  `teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

#20150909
CREATE TABLE IF NOT EXISTS `ww_app_tracking` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `app_key` varchar(64) NOT NULL,
  `app_id` varchar(20) NOT NULL,
  `year` int(4) NOT NULL,
  `month` int(6) NOT NULL,
  `day` int(8) NOT NULL,
  `contacts_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

#20150910
ALTER TABLE  `ww_partner` ADD  `hits` INT( 11 ) NULL DEFAULT  '0';

#20150914
CREATE TABLE IF NOT EXISTS `ww_app_subscribe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) NOT NULL,
  `app_key` varchar(64) NOT NULL,
  `app_id` varchar(20) NOT NULL,
  `subscribe` int(4) NOT NULL,
  `contacts_id` varchar(32) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`partner_id`,`app_id`,`contacts_id`) COMMENT '唯一用户'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

#20150915
ALTER TABLE `ww_manager` ADD `partner_scope` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '合作伙伴管理范围 0代表所有 1代表部分' ;


-- --------------------------------------------------------

--
-- 表的结构 `ww_manager_scope`
--

CREATE TABLE IF NOT EXISTS `ww_manager_scope` (
  `manager_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ww_manager_scope`
--
ALTER TABLE `ww_manager_scope`
 ADD KEY `manager_id` (`manager_id`,`partner_id`);

#20150921
ALTER TABLE  `ww_wechat_config` ADD  `departments` TEXT NULL DEFAULT NULL AFTER  `app_ids` ;
