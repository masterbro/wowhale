<?php
class Partner extends WwModel {

	public static $carousel_size = array(960, 960);

	public static $pay_to_key = array( 'system_fuwu','partner','system','none');


    public function tableName() {
        return '{{partner}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function relations() {
		return array(
			'balance' => array(self::HAS_ONE, 'Balance', 'partner_id'),
		);
	}

	protected function instantiate($attributes){
		switch($attributes['type']){
			case 0:
				$class = 'School';
				break;
			case 1:
				$class = 'Merchant';
				break;
			default:
				$class = get_class($this);
		}
		$model = new $class(null);
		return $model;
	}


	public function sliderArr() {
		if($this->slider) {
			return json_decode($this->slider);
		} else {
			return array();
		}
	}

	public static function all($show_deleted = false) {
		$results = Partner::model()->findAll(array(
			'condition' => $show_deleted ? '' : 'is_deleted = 0',
			'order' => 'id DESC',
		));
		$data = array();
		foreach($results as $r)
			$data[$r->id] = $r->name;

		return $data;
	}


	public function mobileUrl() {
		$show_wx_pay = $this->type ? '' : '&showwxpaytitle=1';
        $mobile_uri = $this->type ? '/merchant/view' : '/home/partner';
		return Yii::app()->params['mobile_base'].$mobile_uri.'?id='.$this->id.$show_wx_pay;
	}
    public function upHits(){
        try{
            $this->updateCounters(array('hits'=>1),'id='.$this->id);
            return $this->hits+1;
        }
        catch(Exception  $e){
            return 1;
        }

    }
}