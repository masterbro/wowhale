<?php
class Setting extends CActiveRecord {

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

    public function primaryKey() {
        return 'name';
    }

    public function tableName()
    {
        return '{{setting}}';
    }

    public static function getAll($refresh = false) {
        $data = array();
        $settings = Setting::model()->findAll();
        foreach($settings as $setting) {
            $data[$setting->name] = $setting->content;
        }

        return $data;
    }
}