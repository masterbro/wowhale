<?php
class OrderTmp extends WwModel {


	public function relations() {
		return array(
			'order_details' => array(self::HAS_MANY, 'OrderDetailTmp', 'order_id'),
			'user' => array(self::BELONGS_TO, 'User', 'uid'),
		);
	}

    public function tableName() {
        return '{{order_tmp}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function generateDisplayId() {
		if($this->display_id)
			return false;

		$display_id = 'WS'.date("Ymd");

		$display_id .= $this->school_id;
		$display_id .= $this->id;

		$this->display_id = $display_id;
		$this->save();
	}


	public function moveToOrder() {
		if(!$this->id) return false;
		$order = new Order();
		foreach($this->attributes as $k=>$v) {
			if(in_array($k, array('id', 'create_time', 'update_time')))
				continue;

			$order->$k = $v;
		}

		$order->tmp_oid = $this->id;
		$order->begin_time = $this->create_time;
		$order->save();
        if($order->user){
            $order->user->consume_total = $order->user->consume_total + $order->total;
            $order->user->last_consume_time = time();
            $order->user->save();
        }


		foreach($this->order_details as $d) {
			$order_details = new OrderDetail();
			$order_details->order_id = $order->id;
			$order_details->tmp_odid = $d->id;

			foreach($d->attributes as $dk=>$dv) {
				if(in_array($dk, array('id', 'order_id', 'create_time', 'update_time')))
					continue;
				$order_details->$dk = $dv;
			}
			$order_details->save();
		}

		return $order;
	}
}