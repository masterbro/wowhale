<?php
class ShippingInfo extends WwModel {

    public function tableName() {
        return '{{shipping_info}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}