<?php
class Page extends CActiveRecord {

	public static $page_title = array(
		'page_term' => '服务条款',
		'page_policy' => '隐私政策',
		'page_help' => '在线帮助',
		'page_about' => '关于我们',
		'page_contact' => '联系我们',
		'page_sponsors' => '合作伙伴',
        'page_joinus' =>'加入我们',
        'page_contact_help' => '通讯录聊天帮助',
	);


    public function tableName() {
        return '{{page}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}