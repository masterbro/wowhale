<?php
class WxPayment extends WwModel {


    public function tableName() {
        return '{{wx_payment}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


	public static function getInstance($partner_id) {
		$model = self::model()->find('partner_id = '.intval($partner_id));
		if(!$model) {
			$model = new WxPayment();
			$model->partner_id = $partner_id;
		}
		return $model;
	}
}