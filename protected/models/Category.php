<?php
class Category extends WwModel {


    public function tableName() {
        return '{{category}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


	public static function all() {
		return Category::model()->findAll(array(
			'order' => 'sort_num DESC, id'
		));
	}
}