<?php
class SignLog extends CActiveRecord {

    public function tableName() {
        return '{{sign_log}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}