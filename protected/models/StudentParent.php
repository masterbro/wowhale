<?php
class StudentParent extends CActiveRecord {
    public function tableName() {
        return '{{student_parent}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}