<?php
class Student extends WwModel {
    public function tableName() {
        return '{{student}}';
    }
    public $order_id = false;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public  function relations() {
		return array(
			'parents'=>array(self::MANY_MANY, 'Contacts', '{{student_parent}}(student_id, parent_id)'),
		);
	}

    public function getIsOrdered($product_id,$order_id=false){
        $criteria = new CDbCriteria();
        $criteria->order = 't.id DESC';
        $criteria->addCondition("t.school_id='{$this->partner_id}'");
        $criteria->addCondition("order_details.pid='{$product_id}'");
        $criteria->addCondition("t.student_id='{$this->id}'");
        if($order_id){
            $criteria->addCondition("t.id='{$order_id}'");
        }
        $order = Order::model()->with('order_details')->find($criteria);
        return $order;
    }
    public function getIsLeave($day=''){
        if(!$day){
            $day = date('Y-m-d');
        }
        //if(!$time_start){
            $time_start = strtotime($day);
        //}
        //if(!$time_end){
            $time_end = $time_start+3600*24;
        //}
        $criteria = new CDbCriteria();
        $criteria->addCondition("student_id='{$this->id}' AND status !=0");
        $criteria->order='status desc,id desc';
        if($time_start==$time_end){
            $criteria->addCondition("start_at<={$time_start} AND end_at>={$time_end}");
        }
        else{
            $criteria->addCondition("(start_at<={$time_start} AND end_at>={$time_start}) OR (start_at<={$time_end} AND end_at>={$time_end})");
        }
        return Leave::model()->find($criteria);
    }
}