<?php
class PartnerPost extends WwModel {

	public static $post_type = array(
		'member' => array(
			'name' => '员工风采',
			'has_thumb' => true,
			'thumb_size' => array(200, 200),
		),
		'news' => array(
			'name' => '新闻公告',
			'has_thumb' => false,
			'thumb_size' => array(),
		),
		'env' => array(
			'name' => '校园环境',
			'has_thumb' => true,
			'thumb_size' => array(200, 200),
		),
	);

    public function tableName() {
        return '{{partner_post}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}