<?php
class OrderDetail extends WwModel {
    public function tableName() {
        return '{{order_detail}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function relations() {
		return array(
			'order' => array(self::BELONGS_TO, 'Order', 'order_id')
		);
	}
}