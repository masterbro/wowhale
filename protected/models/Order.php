<?php
class Order extends WwModel {
	const PAY_WX = 1;
	const UnOnline = 2;

	const PAY_TO_SYSTEM = 1;
	const PAY_TO_PARTNER = 2;
    const PAY_TO_SYSTEM_QY = 3;

	public static $payment_type_txt = array(
		'', '微信支付',
	);

	public function relations() {
		return array(
			'order_details' => array(self::HAS_MANY, 'OrderDetail', 'order_id'),
			'student' => array(self::BELONGS_TO, 'Student', 'student_id'),
			'user' => array(self::BELONGS_TO, 'User', 'uid'),
		);
	}

    public function tableName() {
        return '{{order}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


	public function shippingInfo() {
		return $this->shipping_info ? json_decode($this->shipping_info) : new stdClass();
	}

	public function paymentInfo() {
		return $this->payment_info ? (array) json_decode($this->payment_info) : array();
	}


	public static function stat($school_id = 0) {
		$school_id = intval($school_id);
		$key = md5('wow_order_stat_'.$school_id);
		$stat = Yii::app()->cache->get($key);

		if($stat === FALSE) {
			$stat = array(
				'total' => 0,
				'qty' => 0,
				'today_total' => 0,
				'today_qty' => 0,
			);

			$where = '';
			if($school_id) {
				$where = ' WHERE school_id = '.$school_id;
			}

			$sql = "SELECT SUM(`total`) as order_total, COUNT(`id`) as qty FROM {{order}} ".$where;
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['total'] = $result[0]['order_total'];
			$stat['qty'] = $result[0]['qty'];

			$where = $where ? $where.' AND ' : 'WHERE ';
			$start = strtotime(date('Y-m-d'));
			$where .= 'create_time >= '.$start;

			$sql = "SELECT SUM(`total`) as order_total, COUNT(`id`) as qty FROM {{order}} ".$where;
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['today_total'] = $result[0]['order_total'];
			$stat['today_qty'] = $result[0]['qty'];

			Yii::app()->cache->set($key, $stat, 300);
		}

		return $stat;
	}
}