<?php
class PageTracking extends CActiveRecord {


    public function tableName() {
        return '{{page_tracking}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


	public static function create($partner_id, $uid, $data){
		$partner_id = intval($partner_id);

		$sql = "INSERT DELAYED INTO `{{page_tracking}}` (`id`, `partner_id`, `controller`, `action`, `uri`, `referer`, `ip_address`, `useragent`, `year`, `month`, `day`, `create_time`,`uid`) VALUES (NULL, :partner_id, :controller, :action, :uri, :referer, :ip_address, :useragent,:year, :month, :day, :create_time,:uid);";

		//var_dump($path);exit;
		Yii::app()->db->createCommand($sql)->query(
			array(
				':partner_id' => $partner_id,
				':controller' => $data['controller'],
				':action'=> $data['action'],
				':uri'=> $data['uri'],
				':referer'=> $data['referer'],
				':ip_address'=> Yii::app()->request->userHostAddress,
				':useragent'=>$_SERVER['HTTP_USER_AGENT']?$_SERVER['HTTP_USER_AGENT']:'',
				':year'=>date('Y'),
				':month'=>date('Ym'),
				':day'=>date('Ymd'),
				':create_time'=>time(),
				':uid'=>$uid,
			)
		);
	}


	public static function stat($partner_id = 0) {
		$partner_id = intval($partner_id);
		$key = md5('wow_page_tracking_stat_'.$partner_id);
		$stat = Yii::app()->cache->get($key);

		if($stat === FALSE) {
			$stat = array(
				'people' => 0,
				'times' => 0,
				'today_people' => 0,
				'today_times' => 0,
			);

			$where = '';
			if($partner_id) {
				$where = ' WHERE partner_id = '.$partner_id;
			}

			$table = "{{page_tracking}}";

			$sql = "SELECT COUNT(`id`) as qty FROM {$table} ".$where;
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['times'] = $result[0]['qty'];

			$sql = "select count(1) qty from (SELECT id FROM {$table} ".$where." Group by uid)   as aa";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['people'] = $result[0]['qty'];

			$where = $where ? $where.' AND ' : 'WHERE ';
			$start = strtotime(date('Y-m-d'));
			$where .= 'create_time >= '.$start;

			$sql = "SELECT COUNT(`id`) as qty FROM {$table} ".$where;
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['today_times'] = $result[0]['qty'];

			$sql = "select count(1) qty from (SELECT id FROM {$table} ".$where." Group by uid)  as aa ";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['today_people'] = $result[0]['qty'];

			Yii::app()->cache->set($key, $stat, 300);
		}

		return $stat;
	}
}