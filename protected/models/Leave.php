<?php
class Leave extends WwModel {
    public static $status= array('拒绝请假','等待批复','同意请假');
    public function tableName() {
        return '{{leaves}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}