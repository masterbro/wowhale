<?php
class School extends Partner {


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function defaultScope(){
		return array(
			'condition' => 't.type = 0',
		);
	}

	public static function all($show_deleted = false) {
		$results = School::model()->findAll(array(
			'condition' => $show_deleted ? '' : 'is_deleted = 0',
			'order' => 'id DESC',
		));
		$data = array();
		foreach($results as $r)
			$data[$r->id] = $r->name;

		return $data;
	}
}