<?php
class UserOpenId extends WwModel {

    public function tableName() {
        return '{{user_open_id}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}