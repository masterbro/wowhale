<?php
class Product extends WwModel {

	private $_schoolOrderCount = false;
    private $_schoolOrderMoney = false;
	private $_schoolGradeOrderCount = false;
	private $_schoolGradeOrderMoney = false;
    public function tableName() {
        return '{{product}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function relations()
	{
		return array(
			'product_attr' => array(self::HAS_MANY, 'ProductAttr', 'product_id', 'order' => 'product_attr.id')
		);
	}

	public function getSchoolOrderCount(){
		if($this->_schoolOrderCount===false){
			$criteria = new CDbCriteria();
			$criteria->select = 'count(distinct(order.student_id)) as qty';
			$criteria->addCondition("order.school_id='{$this->partner_id}'");
			$criteria->addCondition("t.pid='{$this->id}'");
			$criteria->addCondition('order.payment_status=1');
			$order_count = OrderDetail::model()->with('order')->find($criteria);
			$criteria_count =  clone $criteria;
			$criteria_count->addCondition('order.payment_type=1');
			$criteria_unonline =  clone $criteria;
			$criteria_unonline->addCondition('order.payment_type=2');
			$online_count = OrderDetail::model()->with('order')->find($criteria_count);
			$unonline_count = OrderDetail::model()->with('order')->find($criteria_unonline);
			$order_count = $order_count?$order_count->qty:0;
			$online_count = $online_count?$online_count->qty:0;
			$unonline_count = $unonline_count?$unonline_count->qty:0;
			$this->_schoolOrderCount = compact('order_count','online_count','unonline_count');
		}
		return $this->_schoolOrderCount;
	}
    public function getSchoolOrderMoney(){
        if($this->_schoolOrderMoney===false){
            $criteria = new CDbCriteria();
            $criteria->select = 'SUM(order.total) as qty';
            $criteria->addCondition("order.school_id='{$this->partner_id}'");
            $criteria->addCondition("t.pid='{$this->id}'");
            $criteria->addCondition('order.payment_status=1');
            $order_count = OrderDetail::model()->with('order')->find($criteria);
            $criteria_count =  clone $criteria;
            $criteria_count->addCondition('order.payment_type=1');
            $criteria_unonline =  clone $criteria;
            $criteria_unonline->addCondition('order.payment_type=2');
            $online_count = OrderDetail::model()->with('order')->find($criteria_count);
            $unonline_count = OrderDetail::model()->with('order')->find($criteria_unonline);
            $order_count = $order_count?$order_count->qty:0;
            $online_count = $online_count?$online_count->qty:0;
            $unonline_count = $unonline_count?$unonline_count->qty:0;
            $this->_schoolOrderMoney = compact('order_count','online_count','unonline_count');
        }
        return $this->_schoolOrderMoney;
    }
	public function getSchoolGradeOrderCount(){
		if($this->_schoolGradeOrderCount===false){
			$criteria = new CDbCriteria();
			$criteria->select = 'count(distinct(order.student_id)) as qty,student.grade_id as tmp_odid';
			$criteria->addCondition("order.school_id='{$this->partner_id}'");
			$criteria->addCondition("t.pid='{$this->id}'");
			$criteria->addCondition('order.payment_status=1');
			$criteria->join = 'LEFT JOIN '.Order::model()->tableName().' as `order` ON `order`.id=t.order_id  LEFT JOIN '.Student::model()->tableName().' as `student` ON `student`.id=`order`.student_id ';
			$criteria->group = 'student.grade_id';
			$criteria_count =  clone $criteria;
			$criteria_count->addCondition('order.payment_type=1');
			$criteria_unonline =  clone $criteria;
			$criteria_unonline->addCondition('order.payment_type=2');
			$order_count_model = OrderDetail::model()->findAll($criteria);
			$online_count_model = OrderDetail::model()->findAll($criteria_count);
			$unonline_count_model = OrderDetail::model()->findAll($criteria_unonline);
			$order_count = $online_count = $unonline_count = array();
			if(!$order_count_model){
				$order_count_model = array();
			}
			foreach ($order_count_model as $key => $value) {
				$order_count[$value->tmp_odid] = $value->qty;
			}
			if(!$online_count_model){
				$online_count_model = array();
			}
			foreach ($online_count_model as $key => $value) {
				$online_count[$value->tmp_odid] = $value->qty;
			}
			if(!$unonline_count_model){
				$unonline_count_model = array();
			}
			foreach ($unonline_count_model as $key => $value) {
				$unonline_count[$value->tmp_odid] = $value->qty;
			}
			/*$order_count = $order_count?$order_count->qty:0;
			$online_count = $online_count?$online_count->qty:0;
			$unonline_count = $unonline_count?$unonline_count->qty:0;*/
			$this->_schoolGradeOrderCount = compact('order_count','online_count','unonline_count');
		}
		return $this->_schoolGradeOrderCount;
	}
	public function getSchoolGradeOrderMoney(){
		if($this->_schoolGradeOrderMoney===false){
			$criteria = new CDbCriteria();
			$criteria->select = 'SUM(order.total) as qty,student.grade_id as tmp_odid';
			$criteria->addCondition("order.school_id='{$this->partner_id}'");
			$criteria->addCondition("t.pid='{$this->id}'");
			$criteria->addCondition('order.payment_status=1');
			$criteria->join = 'LEFT JOIN '.Order::model()->tableName().' as `order` ON `order`.id=t.order_id  LEFT JOIN '.Student::model()->tableName().' as `student` ON `student`.id=`order`.student_id ';
			$criteria->group = 'student.grade_id';
			$criteria_count =  clone $criteria;
			$criteria_count->addCondition('order.payment_type=1');
			$criteria_unonline =  clone $criteria;
			$criteria_unonline->addCondition('order.payment_type=2');
			$order_count_model = OrderDetail::model()->findAll($criteria);
			$online_count_model = OrderDetail::model()->findAll($criteria_count);
			$unonline_count_model = OrderDetail::model()->findAll($criteria_unonline);
			$order_count = $online_count = $unonline_count = array();
			if(!$order_count_model){
				$order_count_model = array();
			}
			foreach ($order_count_model as $key => $value) {
				$order_count[$value->tmp_odid] = $value->qty;
			}
			if(!$online_count_model){
				$online_count_model = array();
			}
			foreach ($online_count_model as $key => $value) {
				$online_count[$value->tmp_odid] = $value->qty;
			}
			if(!$unonline_count_model){
				$unonline_count_model = array();
			}
			foreach ($unonline_count_model as $key => $value) {
				$unonline_count[$value->tmp_odid] = $value->qty;
			}
			/*$order_count = $order_count?$order_count->qty:0;
			$online_count = $online_count?$online_count->qty:0;
			$unonline_count = $unonline_count?$unonline_count->qty:0;*/
			$this->_schoolGradeOrderMoney = compact('order_count','online_count','unonline_count');
		}
		return $this->_schoolGradeOrderMoney;
	}
	public function attrData() {
		if(!$this->product_attr) return array();

		$tmp = array();
		foreach($this->product_attr as $a) {
			if($a->fid == 0) {
				$tmp[$a->id]['parent'] = $a;
			} else {
				if(!is_array($tmp[$a->fid]['sub'])) $tmp[$a->fid]['sub'] = array();
				$tmp[$a->fid]['sub'][] = $a;
			}
		}

		foreach($tmp as $k=>$v){
			if(!isset($v['sub'])) {
				$tmp[$k]['sub'] = array();
			}
		}

		return array_merge($tmp);
	}
}