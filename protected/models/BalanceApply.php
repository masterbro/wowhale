<?php
class BalanceApply extends WwModel {
	const APPROVED = 2;
	const REJECTED = 1;
	const FEE_RATE = 0.006;

	public static $status_txt = array('待审核', '拒绝', '成功');

	public function tableName() {
		return '{{balance_apply}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}