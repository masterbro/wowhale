<?php
class WechatConfig extends WwModel {


    public function tableName() {
        return '{{wechat_config}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public static function getSingleInstance($partner_id) {
		$model = WechatConfig::model()->find("partner_id = {$partner_id} and ww_suite_id = 3");
		if(!$model)
			return false;


		if(!$model->access_token ||$model->expired_at < time()) {
			try{
				$result = WxQyApp::getThirdPartAccessToken($model, $model->ww_suite_id);
				if($result&&$result->access_token) {
					$model->access_token = $result->access_token;
					$model->expired_at = time() + $result->expires_in;
				}
				$model->save();
			}
			catch(Exception $e){
				Yii::log($partner_id.'get access_token error:'.$e->getMessage(),'error');
			}

		}
		if(!$model->js_ticket || $model->js_ticket_expired_at < time()){
			try{
				$result = WxQyApp::getThirdPartjsTicket($model);
				if($result&&$result->ticket){
					$model->js_ticket = $result->ticket;
					$model->js_ticket_expired_at = time() + $result->expires_in;
					$model->save();
				}
			}
			catch(Exception $e){
				Yii::log($partner_id.'get js_ticket error:'.$e->getMessage(),'error');
			}
		}

		return $model ? $model : false;
	}

	public static function getInstance($partner_id, $ww_suite_id) {
		$model = WechatConfig::model()->find("partner_id = {$partner_id} and ww_suite_id = {$ww_suite_id}");
		if(!$model)
			return false;

		if(!$model->access_token ||$model->expired_at < time()) {
            try{
                $result = WxQyApp::getThirdPartAccessToken($model, $ww_suite_id);
                if($result&&$result->access_token) {
                    $model->access_token = $result->access_token;
                    $model->expired_at = time() + $result->expires_in;
                }
                $model->save();
            }
            catch(Exception $e){
                Yii::log($partner_id.'get access_token error:'.$e->getMessage(),'error');
            }

		}
        if(!$model->js_ticket || $model->js_ticket_expired_at < time()){
            try{
                $result = WxQyApp::getThirdPartjsTicket($model);
                if($result&&$result->ticket){
                    $model->js_ticket = $result->ticket;
                    $model->js_ticket_expired_at = time() + $result->expires_in;
                    $model->save();
                }
            }
            catch(Exception $e){
                Yii::log($partner_id.'get js_ticket error:'.$e->getMessage(),'error');
            }
        }
        $need_create_department = array();
        if(!$model->student_department){
            $need_create_department['student_department'] = 1;
        }
        if(!$model->teacher_department){
            $need_create_department['teacher_department'] = 1;
        }
        if(!$model->leader_department){
            $need_create_department['leader_department'] = 1;
        }
        if(!empty($need_create_department)){
            $wx_app = new WxQyContacts($model);
            $departments = $wx_app->createDepartment($need_create_department);
            //var_dump($need_create_department,$departments);exit;
            if($departments){
                if($departments['student_department'] && $departments['student_department']>0){
                    $model->student_department = $departments['student_department'];
                }
                if($departments['teacher_department'] && $departments['teacher_department']>0){
                    $model->teacher_department = $departments['teacher_department'];
                }
                if($departments['leader_department'] && $departments['leader_department']>0){
                    $model->leader_department = $departments['leader_department'];
                }
                $model->save();
            }
        }
		return $model ? $model : false;
	}
    public function getToken(){
        if(!$this->access_token ||$this->expired_at < time()) {
            try {
                $result = WxQyApp::getThirdPartAccessToken($this, $this->ww_suite_id);
                if ($result && $result->access_token) {
                    $this->access_token = $result->access_token;
                    $this->expired_at = time() + $result->expires_in;
                }
                $this->save();
            }
            catch(Exception $e){
                Yii::log($this->partner_id.'p get access_token error:'.$e->getMessage(),'error');
            }
        }
        if($this->access_token && (!$this->js_ticket || $this->js_ticket_expired_at < time()) ){
            $result = WxQyApp::getThirdPartjsTicket($this);
            try {
                if ($result && $result->ticket) {
                    $this->js_ticket = $result->ticket;
                    $this->js_ticket_expired_at = time() + $result->expires_in;
                    $this->save();
                }
            }
            catch(Exception $e){
                Yii::log($this->partner_id.'p get js_ticket error:'.$e->getMessage(),'error');
            }
        }
        return $this;
    }
	public function getAppIdsObj() {
		return $this->app_ids ? json_decode($this->app_ids) : new stdClass();
	}
    public function getDepartmentIdsObj() {
        return $this->departments ? json_decode($this->departments) : new stdClass();
    }

    public function getGradeDepartment($grade_id,$grade_name=''){
        $depart_key = 'grade_'.$grade_id;
        if(!$this->departmentIdsObj->$depart_key){
            if(!$grade_name){
                $grade = Grade::model()->findByPk($grade_id);
                $grade_name = $grade->name;
            }
            $wx_app = new WxQyContacts($this);
            $depart_id = $wx_app->createDepart($this->student_department,$grade_name);
            $depart = $this->departmentIdsObj;
            $depart->$depart_key = $depart_id;
            $this->departments = CJSON::encode($depart);
            $this->save();
        }
        return $this->departmentIdsObj->$depart_key;
    }
}