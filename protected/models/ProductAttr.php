<?php
class ProductAttr extends WwModel {
	public function tableName() {
		return '{{product_attr}}';
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function relations()
	{
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

}