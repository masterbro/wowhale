<?php
class Contacts extends WwModel {
	const TY_STUDENT = 1;
	const TY_TEACHER = 2;

    public function tableName() {
        return '{{contacts}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public  function relations() {
		return array(
			'students'=> array(self::MANY_MANY, 'Student', '{{student_parent}}(parent_id, student_id)'),
		);
	}


	public function generateToken() {
		if(!$this->id || $this->token)
			return false;

		$this->token = md5($this->id .'_time_' . microtime(). '_partner_' . $this->partner_id . uniqid('contact_token'));
		$this->save();
	}
    public function getFirst_in_send_apps() {
        return $this->first_in_send ? json_decode($this->first_in_send) : new stdClass();
    }
}