<?php
class Albums extends WwModel {


    public function tableName() {
        return '{{albums}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getSameDayAlbums(){
        return self::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'grade_id'=>$this->grade_id,'day'=>$this->day,'type'=>1),array('order'=>'id desc'));
    }
    public function getSameDaySchoolAlbums(){
        return self::model()->findAllByAttributes(array('partner_id'=>$this->partner_id,'day'=>$this->day,'type'=>1),array('order'=>'id desc'));
    }



}