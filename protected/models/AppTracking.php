<?php
class AppTracking extends CActiveRecord {


    public function tableName() {
        return '{{app_tracking}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


	public static function create($partner_id, $contacts_id, $data){
		$partner_id = intval($partner_id);

		$sql = "INSERT DELAYED INTO `{{app_tracking}}` (`id`, `partner_id`, `app_key`, `app_id`, `year`, `month`, `day`, `create_time`,`contacts_id`) VALUES (NULL, :partner_id, :app_key, :app_id,:year, :month, :day, :create_time,:contacts_id);";
        $time =  $data['create_time'];
		//var_dump($path);exit;
		Yii::app()->db->createCommand($sql)->query(
			array(
				':partner_id' => $partner_id,
				':app_key' => $data['app_key'],
				':app_id'=> $data['app_id'],
				':year'=>date('Y',$time),
				':month'=>date('Ym',$time),
				':day'=>date('Ymd',$time),
				':create_time'=>$time,
				':contacts_id'=>$contacts_id,
			)
		);
	}


	public static function stat($partner_id = 0,$dayStart=0,$dayEnd=0,$need_apps = true) {
        //var_dump($dayStart,$dayEnd);
		$partner_id = intval($partner_id);
		$key = md5('wow_app_tracking_stat_new1_'.$partner_id.'_'.$dayStart.'_'.$dayEnd.'_'.$need_apps);
		$stat = Yii::app()->cache->get($key);
        //$cache_time = 3600;
		if($stat === FALSE) {
			$stat = array(
				'people' => 0,
                'subscribed' => 0,
				'times' => 0,
				'today_people' => 0,
				'today_times' => 0,
                'people_app' => 0,
                'times_app' => 0,
                'today_people_app' => 0,
                'today_times_app' => 0,
                'subscribed_app' => 0,
			);

			$where = '';
			if($partner_id) {
				$where = ' WHERE partner_id = '.$partner_id;
			}
            $table_contact = '{{contacts}}';
            $table_subscribe = '{{app_subscribe}}';
            if($need_apps){
                $sql = "SELECT COUNT(`id`) as qty FROM {$table_contact} ".$where;
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                $stat['contacts'] = $result[0]['qty'];
            }


            $table = "{{app_tracking}}";
			$sql = "SELECT COUNT(`id`) as qty FROM {$table} ".$where;
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['times'] = $result[0]['qty'];

			$sql = "select count(1) qty from (SELECT id FROM {$table} ".$where." Group by contacts_id)   as aa";
			$result = Yii::app()->db->createCommand($sql)->queryAll();
			$stat['people'] = $result[0]['qty'];



            if($need_apps) {
                $where_sub = $where;
                $where_sub  .= $where?' AND ':'  ';
                $where_sub .= 'subscribe = 1';
                $sql = "select count(1) qty from (SELECT id FROM {$table_subscribe} ".$where." Group by contacts_id)   as aa";
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                $stat['subscribed'] = $result[0]['qty'];


                $sql = "SELECT COUNT(`id`) as qty,app_key FROM {$table_subscribe} " . $where . ' GROUP  BY app_key';
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                //$stat['times_app'] = $result;
                $stat['subscribed_app'] = array();
                if ($result) {
                    foreach ($result as $v) {
                        $stat['subscribed_app'][$v['app_key']] = $v['qty'];
                    }
                }

                $sql = "SELECT COUNT(`id`) as qty,app_key FROM {$table} " . $where . ' GROUP  BY app_key';
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                //$stat['times_app'] = $result;
                $stat['times_app'] = array();
                if ($result) {
                    foreach ($result as $v) {
                        $stat['times_app'][$v['app_key']] = $v['qty'];
                    }
                }

                $sql = "select count(1) qty,app_key from (SELECT id,app_key FROM {$table} " . $where . " Group by app_key,contacts_id)   as aa  GROUP  BY app_key";
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                //$stat['people_app'] = $result;
                $stat['people_app'] = array();
                if ($result) {
                    foreach ($result as $v) {
                        $stat['people_app'][$v['app_key']] = $v['qty'];
                    }
                }
            }
			$where = $where ? $where.' AND ' : 'WHERE ';
            if($dayStart &&!$dayEnd){
                $where .= 'day = '.$dayStart;
            }
            else if($dayStart &&$dayEnd && $dayStart!=$dayEnd){
                $where .= 'day>= '.$dayStart .' AND day<='.$dayEnd;
            }
            else if($dayStart &&$dayEnd && $dayStart==$dayEnd){
                $where .= 'day= '.$dayStart ;
            }
            else{
                $start = date('Ymd',time());
                $where .= 'day = '.$start;
            }



            $sql = "SELECT COUNT(`id`) as qty FROM {$table} ".$where;
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $stat['today_times'] = $result[0]['qty'];

            $sql = "select count(1) qty from (SELECT id FROM {$table} ".$where." Group by contacts_id)  as aa ";
            $result = Yii::app()->db->createCommand($sql)->queryAll();
            $stat['today_people'] = $result[0]['qty'];
            if($need_apps) {
                $sql = "SELECT COUNT(`id`) as qty,app_key FROM {$table} " . $where . ' GROUP  BY app_key';;
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                $stat['today_times_app'] = array();
                if ($result) {
                    foreach ($result as $v) {
                        $stat['today_times_app'][$v['app_key']] = $v['qty'];
                    }
                }

                $sql = "select count(1) qty,app_key from (SELECT id,app_key FROM {$table} " . $where . " Group by app_key,contacts_id)  as aa " . ' GROUP  BY app_key';;
                $result = Yii::app()->db->createCommand($sql)->queryAll();
                //$stat['today_people_app'] = $result;
                $stat['today_people_app'] = array();
                if ($result) {
                    foreach ($result as $v) {
                        $stat['today_people_app'][$v['app_key']] = $v['qty'];
                    }
                }
            }
            if(!$dayStart || !$dayEnd){
                Yii::app()->cache->set($key, $stat, 300);
            }
            else{
                Yii::app()->cache->set($key, $stat, strtotime(date('Y-m-d',time()+3600*24)) - time() );
            }

		}

		return $stat;
	}
}