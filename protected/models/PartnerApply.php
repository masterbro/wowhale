<?php
class PartnerApply extends WwModel {
	const TYPE_SCHOOL = 0;

    public function tableName() {
        return '{{partner_apply}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}