<?php
class Manager extends WwModel {

	const TYPE_PARTNER = 1;
	const TYPE_SCHOOL = 0;
	const ST_PENDING = -1;
	const ST_ACTIVE = 1;

    public $password_confirmation;

    public function tableName() {
        return '{{manager}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('username', 'required'),
            //array('password_confirmation', 'compare', 'compareAttribute' => 'password'),
            array('username', 'unique', 'message' => "用户名已存在"),
            array('realname, tel', 'default'),
        );
    }

//    public function afterValidate() {
//        if(!empty($this->password_confirmation)) $this->password = $this->hashPassword($this->password_confirmation);
//        return parent::afterValidate();
//    }

    public function hashPassword($pwd) {
        return md5('hrm_*#&_manage_@^!*_hash'.$pwd);
    }
}