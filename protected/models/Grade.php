<?php
class Grade extends WwModel {

    public $_album = false;
    public function tableName() {
        return '{{school_grade}}';
    }
    public static $default_thumbs = array('阿童木.jpg','车尼龟.jpg','哆啦A梦.jpg','海绵宝宝.jpg','菊草叶.jpg','橘子.jpg','柯南.jpg',
            '蜡笔小新.jpg','老虎.jpg','龙猫.jpg','龙猫2.jpg','龙猫3.jpg','芒果.jpg','皮卡鸣人.jpg','苹果.jpg','狮子.jpg',
            '悟空.jpg','香蕉.jpg','小火龙.jpg','小精灵-鼠.jpg','小精灵01.jpg','熊熊.jpg','一休.jpg','猪猪侠.jpg','QB.jpg'
        );
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getAblumCount(){
        $count = Albums::model()->countByAttributes(array('grade_id'=>$this->id,'partner_id'=>$this->partner_id,'type'=>1));
        return $count;
    }
    public function getAlbum(){
        if($this->_album===false){
            $this->_album = Albums::model()->findByAttributes(array('grade_id'=>$this->id,'partner_id'=>$this->partner_id,'type'=>1),array('order'=>'id desc'));
        }
        return $this->_album;

    }
    public function getLastAlbumTime(){
        return $this->album?$this->album->create_time:false;
    }
    public function getLastAlbumPic(){
        return ($this->album&&$this->album->pic_url)?$this->album->pic_url:"http://m.wowhale.com/assets/upload/slider/2e0e1dd7caffed49238429ed58d5e7cf.jpg";
    }

    public function getStudentCount(){
        $student_count = Student::model()->cache(500)->countByAttributes(array('partner_id'=>$this->partner_id,'grade_id'=>$this->id));
        return $student_count;
    }

    public function getLeaveCount($time=''){
        if(!$time){
            $time = time();
        }
        $criteria = new CDbCriteria();
        $criteria->select='t.id';
        $criteria->addCondition('t.partner_id='.$this->partner_id.' AND t.status=2');
        $criteria->addCondition('student.grade_id='.$this->id);
        $criteria->addCondition('start_at<='.$time.' AND end_at>='.$time);
        $criteria->join = 'LEFT JOIN {{student}} student ON student.id=t.student_id';
        $criteria->group = 'student_id';
        return Leave::model()->count($criteria);
    }
}