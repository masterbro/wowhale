<?php
class User extends WwModel {
	const SAFE_SALT = 'wo_whale_ahod#$%08726SDFCx';

    public function tableName() {
        return '{{user}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function relations()
	{
		return array(
			'shipping_info' => array(self::HAS_MANY, 'ShippingInfo', 'user_id', 'order' => 'shipping_info.id')
		);
	}

	public function generateToken() {
		if(!$this->id)
			return false;

		$this->token = md5($this->id .'_time_' . microtime() . uniqid('user_token'));
		$this->save();
	}
}