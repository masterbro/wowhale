<?php
class Notice extends WwModel {
	public static $type_txt = array('图文消息', '文字消息');
	public static $push_txt = array('学校公告', '员工公告');

    public function tableName() {
        return '{{notice}}';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function upHits(){
        try{
            $this->updateCounters(array('hits'=>1),'id='.$this->id);
            return $this->hits+1;
        }
        catch(Exception  $e){
            return 1;
        }
    }

}