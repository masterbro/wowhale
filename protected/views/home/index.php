<style type="text/css">
    body{height: auto;}
    .row{margin: 0;padding: 0;}
    ul{list-style-type: none;padding: 0;}
    #first{margin: 0;}
    #first p{color:#fff;margin: 10px 0;}
    #first a{background-color:rgba(255,255,255,0.4);color: #fff;display: inline-block;font-size: 16px;border: 1px solid rgba(255,255,255,0.8);padding: 3px 20px;border-radius: 5px;}
    #first a:hover,#first a:focus{background-color:rgba(255,255,255,0.2);border: 1px solid rgba(255,255,255,0.4);}
    #towards-left{position: absolute;top:200px;z-index: 1000;left: 15px;}
    #towards-right{position: absolute;top:200px;z-index: 1000;right: 15px;}
    .big-title{font-size: 30px;}
    .small-title{font-size: 24px;}
   /* .banner ul{list-style-type: none;padding: 0;}*/
    .banner ul li{padding-top: 60px;text-align: center;width:25%;height:500px;background-image: url(<?php echo HtmlHelper::assets('images/home/second/banner-content.png') ?>);background-size: 35%;}
    .banner ol{text-align: center;padding: 0;}
    .banner .dots {position: absolute;left: 0;right: 0;bottom: 20px;}
    .banner .dots li {
        display: inline-block;
        width: 10px;
        height: 10px;
        margin: 0 4px;

        text-indent: -999em;

        border: 2px solid #fff;
        border-radius: 6px;

        cursor: pointer;
        opacity: .4;

        -webkit-transition: background .5s, opacity .5s;
        -moz-transition: background .5s, opacity .5s;
        transition: background .5s, opacity .5s;
    }
    .banner .dots li.active {background: #fff;opacity: 1;}

    .banner2-p-parent{float: left;width: 45%;padding-top: 50px;}
    .banner2-p-parent p{text-align: left;}
    .banner2-p-parent div{text-align: left;margin-top: 20px;}

    .banner4-btn{position: relative;top:60px;height: 10px;}


    #cloud-parent{position:absolute;top:360px;}
    #cloud-parent img{width:100%;height: 200px;}


    .second-title{width: 45%;font-size: 36px;}
    .second-title span{color:#e33f4c;}
    .second-left{float: left;width: 30%;text-align: left;font-size: 16px;color: #999;}
    .second-right{float: right;width: 70%;text-align:left;font-size: 16px;color: #999;}
    #second h1{color:#3f94d3;}
    #second ul{overflow: hidden;padding-left:30px;padding-top: 10px;}

    #third{height:620px;background-image: url(<?php echo HtmlHelper::assets('images/home/second/product-show-bg.png') ?>);}
    .third-title{color: #fff;border-bottom: solid 1px #eee;overflow: hidden;height: 70px;padding: 0 25%;}
    .third-title h1{float: left;line-height: 70px;margin: 0;}
    .third-title p{float: right;line-height: 70px;margin: 0;font-size: 18px;padding-top: 5px;}
    #show{position: relative;height: 510px;}
    #show-img-main{
        height: 500px;
        background: no-repeat;
        background-image: url(<?php echo HtmlHelper::assets('images/home/second/show/home.jpg')?>);
        background-size: 192px 342px;
        background-position:35px 79px;

        -moz-transition:background-image 0.5s linear 0s;
        -webkit-transition:background-image 0.5s linear 0s;
        -o-transition:background-image 0.5s linear 0s;
        transition: background-image 0.5s linear 0s;
    }
    #show div{position: absolute;}
    #show .left-a{left:0;top:200px;cursor: pointer;}
    #show .left-a:hover,#show .left-a:focus,
    #show .right-a:hover, #show .right-a:focus{opacity:0.4;filter:alpha(opacity=40);}
    #show .right-a{right:0;top:200px;cursor: pointer;}
    #show-content{color:#fff;font-size: 18px;}


    #fourth{height:600px;padding-top: 30px;}
    .index-top{padding-top: 50px;}
    .index-top p{color:#666;}
    .group-img{margin-top: 60px;}
    .group-p-1{margin-top: 20px;font-size: 14px;}
    .group-p-2{font-size: 12px;color:#999;}
    #gao-ke-kao{margin-top: 45px;}

    #fifth{height:600px;color: #fff;background-color: #6096d6;padding-top: 30px;}
    .fifth-title hr{width: 500px;margin: 5px auto;}
    #fifth p{font-size: 28px;}
    #fifth span{color: #f9f02e;font-size: 40px;}
    .img-parent{overflow: hidden;}
    .img-parent img{height: 400px;}

    #sixth{color:#fff;background-color:#8c7772;height: 100px;font-size: 28px;padding-top: 10px;}
    .num{font-size: 36px;}
    .num-content{font-size: 16px;}

    #seventh{padding-top: 10px;height: 700px;}
    #seventh .hr{margin: 20px auto;border-bottom: solid 1px #ccc;width: 500px;}
    #seventh p{color: #999;font-size: 16px;margin: 0 auto;}
    #seventh .btn{display: inline-block;color: #ed5417;font-size: 30px;border: 1px solid #ed5417;padding: 3px 30px;border-radius: 30px;margin: 0 20px;}
    .btn-parent{margin: 20px auto;}
    .btn-parent p{display: inline;}
    #btn-parent a:hover,#btn-parent a:focus{color: #fff;background-color:#ed5417; }

    @media (max-width: 768px) {
        hr{margin-top: 0;}
        #first{margin: 0;}
        .banner ul li{background-size: 150%;}
        .banner ul li{height: 450px;}
        .big-title{font-size: 24px;}
        .small-title{font-size: 18px;}
        .banner2-p-parent{clear:both;width: 100%;padding-top: 50px;}
        .banner2-p-parent p{text-align: center;}
        .banner2-p-parent div{text-align: center;margin-top: 20px;}


        #second h1{font-size: 18px;}
        .second-title{font-size: 24px;}
        #second ul{padding-left: 10px;}
        .second-left,.second-right{font-size: 13px;}

        #third{height: 520px;}
        .third-title{padding: 0;}
        .third-title h1{font-size: 28px;}
        #show{height: 410px;}
        #show-content{font-size: 14px;}
        #show-img-main{height: 400px;background-size: 154px 273px;background-position:28px 63px;}


        #fourth{padding-bottom: 20px;height: auto;}
        .index-top{padding-top: 10px;}
        .index-top h1{font-size: 28px;}
        #gao-ke-kao{margin-top: 10px;}
        .group-img{margin-top: 20px;}

        #fifth{padding-top: 10px;height: 400px;}
        #fifth h1{font-size: 24px;}
        #fifth hr{width: auto;}
        #fifth p{font-size: 16px;}
        #fifth span{font-size: 30px;}
        .img-parent{margin-top: 50px;}
        .img-parent img{height: auto;width: 100%;}

        #seventh{height: 550px;}
        #seventh .hr{width: auto;}
        #seventh h1{font-size: 16px;}
        #seventh p{font-size: 14px;}
        #seventh .btn{font-size: 24px;border-radius: 24px;margin: 0;}
        .btn-parent p{display: block;}
    }
    @media (min-width: 768px) {
        .group-p-2{margin: 0 auto;width: 50%;}
    }
</style>

<div id="first" class="row">
    <div class="col-xs-12" style="padding: 0;">
        <div class="banner" style="width: 100%">
            <div class="hidden-xs" id="towards-left"><a style="border-radius:0;border: 0;cursor: pointer;" class="unslider-arrow prev"><img src="<?php echo HtmlHelper::assets('images/home/second/towards-left.png') ?>"></a></div>
            <div class="hidden-xs" id="towards-right"><a style="border-radius:0;border: 0;cursor: pointer;" class="unslider-arrow next"><img src="<?php echo HtmlHelper::assets('images/home/second/towards-right.png') ?>"></a></div>
            <ul>
                 <li style="background-color:#00a0e9;">
                     <img style="width: 200px;" src="<?php echo HtmlHelper::assets('images/home/second/banner1-img.png') ?>">
                     <p class="small-title">娃娃营云服务&nbsp;·&nbsp;微信幼儿园管理系统</p>
                     <p class="big-title">学前教育的科技枢纽</p>
                     <div style="position: relative;z-index: 10;">
                         <a href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>">免费开通</a>&nbsp;&nbsp;&nbsp;&nbsp;
                         <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal">立即登录</a>
                     </div>
                 </li>
                 <li style="background-color:#20ce98;">
                     <div class="row" style="margin: 0;">
                         <div class="col-sm-3 hidden-xs"></div>
                         <div class="col-sm-6 col-xs-12">
                             <div class="banner2-p-parent">
                                 <p class="big-title">幼儿园管理</p>
                                 <p class="big-title">一个微信通通搞定</p>
                                 <p style="color:rgba(255,255,255,0.6);">微网站、公告、校园相册、宝宝请假、缴学费、学费报表、食谱、老师打卡，管理学生信息，不用下载APP！一个微信全搞定！！</p>
                                 <div style="position: relative;z-index: 10;">
                                     <a href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>">免费开通</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                     <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal">立即登录</a>
                                 </div>
                             </div>
                             <img style="float: right;width: 50%" class="hidden-xs" src="<?php echo HtmlHelper::assets('images/home/second/banner2-img.png') ?>">
                         </div>
                     </div>
                 </li>
                 <li style="background-color:#75bb6d;">
                     <div class="row" style="margin: 0;">
                         <div class="col-sm-2 hidden-xs"></div>

                         <div class="col-sm-8 col-xs-12">
                             <div style="width: 100%;margin-bottom: 10px;">
                                 <p class="big-title">让老师把生活和工作分开</p>
                                 <p style="color:rgba(255,255,255,0.6);">老师自己的生活自己做主，不用加家长好友，也能和家长联系，再也不用担心自己隐私被家长看见了。</p>
                             </div>
                             <div class="banner4-btn" style="position: relative;z-index: 10;">
                                 <div style="margin-bottom: 20px;">
                                     <a href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>">免费开通</a>
                                 </div>
                                 <div>
                                     <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal">立即登录</a>
                                 </div>
                             </div>
                             <img style="width:100%;" class="hidden-xs" src="<?php echo HtmlHelper::assets('images/home/second/banner4-img.png') ?>">
                         </div>
                     </div>
                 </li>
             </ul>
        </div>
    </div>
</div>

<div id="cloud-parent">
    <img class="hidden-xs" src="<?php echo HtmlHelper::assets('images/home/second/cloud.png') ?>">
</div>

<div id="second" class="row">
    <div class="col-xs-12 text-center">
        <h1>娃娃营云服务·微信幼儿园管理系统</h1>
        <div class="row">
            <div class="col-sm-4 hidden-xs"></div>
            <div class="col-sm-4">
                <img style="width: 100%;margin: 10px auto;" src="<?php echo HtmlHelper::assets('images/home/second/phone.png') ?>">
                <div style="border-bottom: solid 1px #ccc;overflow: hidden;">
                    <p class="second-title" style="float: left;"><span>五</span>个应用</p>
                    <p class="second-title" style="float: right;"><span>十</span>大功能</p>
                </div>
                <div>
                   <ul>
                       <li><p class="second-left">校园公告</p><p class="second-right">公告、食谱。微网站</p></li>
                       <li><p class="second-left">老师助手</p><p class="second-right">签到、学生管理、缴费报表</p></li>
                       <li><p class="second-left">家长助手</p><p class="second-right">缴学费、宝宝请假、信息管理</p></li>
                       <li><p class="second-left">校园相册</p><p class="second-right">老师简单传、家长随时看</p></li>
                       <li><p class="second-left">娃娃营</p><p class="second-right">帮助、资讯</p></li>
                   </ul>
                </div>
            </div>
            <div class="col-sm-4 hidden-xs"></div>
        </div>
    </div>
</div>

<div id="third" class="row text-center">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <div class="third-title"><h1>应用展示</h1><p id="show-title">娃娃营幼儿园管理</p></div>
        <div id="show">
            <img id="show-img-main" src="<?php echo HtmlHelper::assets('images/home/second/iPhone.png') ?>">
            <div class="hidden-xs left-a"><a style="border-radius:0;border: 0;"><img src="<?php echo HtmlHelper::assets('images/home/second/towards-left.png') ?>"></a></div>
            <div class="hidden-xs right-a"><a style="border-radius:0;border: 0;"><img src="<?php echo HtmlHelper::assets('images/home/second/towards-right.png') ?>"></a></div>
        </div>
        <p id="show-content"></p>
    </div>
    <div class="col-sm-3"></div>
</div>

<div id="fourth">
    <div class="row text-center">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="index-top">
                <h1>可靠的安全保障</h1><br>
                <p>我们应用建立在微信的安全基础之上，腾讯及微信已建立的业界一流的安全保障体系、高可靠的系统实现机制，以及企业号完善的安全特性，
                    <br>可以为幼儿园的信息安全提供了全方位的安全保障。</p>
            </div>

            <div class="row text-center group-img">
                <div class="col-sm-4">
                    <img src="<?php echo HtmlHelper::assets('images/home/img-10.png') ?>">
                    <p class="group-p-1">一流的安全保障体系</p>
                    <p class="group-p-2">专业安全平台7*24小时监护闭环高效的反垃圾、反病毒处理机制领先的防黑客攻击技术与组织保障</p>
                </div>
                <div class="col-sm-4">
                    <img src="<?php echo HtmlHelper::assets('images/home/img-11.png') ?>">
                    <p id="gao-ke-kao" class="group-p-1">高可靠的系统实现机制</p>
                    <p class="group-p-2">消息通讯全程加密传输企业关键信息安全存储端到端高可靠性服务处理微信安装包及网页防撰改技术</p>
                </div>
                <div class="col-sm-4">
                    <img src="<?php echo HtmlHelper::assets('images/home/img-12.png') ?>">
                    <p class="group-p-1">完善的应用安全特性</p>
                    <p class="group-p-2">完善用户生命周期管理授权用户才可关注与使用分级分权限管理企业资源保密消息，有效确保消息的阅读范围</p>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="fifth" class="row text-center">
    <div class="col-sm-12 fifth-title">
        <h1>有限的关注，无限的可能</h1>
        <hr>
        <p>让您的学校在<span>900000000</span>人群中传播</p>
    </div>
    <div class="col-sm-12 img-parent">
        <img src="<?php echo HtmlHelper::assets('images/home/second/spread.png') ?>">
    </div>
</div>

<div id="sixth" class="row text-center">
    <div style="margin-top: 20px;" class="col-sm-3 col-xs-6"><p>已入驻</p></div>
    <div class="col-sm-3 col-xs-6"><p class="num">76</p><p class="num-content">幼儿园</p></div>
    <div class="col-sm-3 hidden-xs"><p class="num">2818</p><p class="num-content">老师</p></div>
    <div class="col-sm-3 hidden-xs"><p class="num">13411</p><p class="num-content">家长</p></div>
</div>

<div id="seventh" class="row text-center">
    <div class="col-sm-8 col-sm-offset-2">
        <h1>免费使用娃娃营微信幼儿园管理系统</h1>
        <div class="hr"></div>
        <p>娃娃营提供了多种的功能帮助幼儿园便利快捷地实现基本的沟通雨协同管理。九大功能能帮助幼儿园快速高校地利用微信来管理幼儿园。</p>
        <p>永久免费的微信幼儿园管理系统，赶快来注册吧！！</p>
        <div id="btn-parent" class="btn-parent">
            <a class="btn" href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>">免费开通</a>
            <p style="font-size: 36px;color: #5f9ce2;">or</p>
            <a class="btn" data-toggle="modal" data-target="#myModal">立即登录</a>
        </div>
    </div>
    <div class="col-sm-12 img-parent">
        <img src="<?php echo HtmlHelper::assets('images/home/second/city.png') ?>">
    </div>
</div>


<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/unslider.min.js"></script>
<script>
    //轮播图用的js代码
    $(function() {
        $('.banner').unslider({
            speed: 1000,               //  The speed to animate each slide (in milliseconds)
            delay: 3000,              //  The delay between slide animations (in milliseconds)
            keys: true,               //  Enable keyboard (left, right) arrow shortcuts
            dots: true,               //  Display dot navigation
            fluid: true              //  Support responsive design. May break non-responsive designs
        });
    });

    var unslider = $('.banner').unslider();
    $('.unslider-arrow').click(function() {
        var fn = this.className.split(' ')[1];

        //  Either do unslider.data('unslider').next() or .prev() depending on the className
        unslider.data('unslider')[fn]();
    });
</script>

<script>
    /*应该展示的图片切换功能*/
    var count = 1;
    var titles = new Array("娃娃营幼儿园管理","公告","食谱","微网站","相册","相册","宝宝请假","宝宝请假","宝宝请假",
                            "缴学费","缴学费","老师签到");
    var contents = new Array("","实时发公告，家长不错过。","推送校园食谱，助力宝宝健康成长。",
                             "幼儿园的微网站，可以分享到朋友圈哦。","宝宝的成长记录，老师简单传，家长随时看 !",
                             "宝宝的成长记录，老师简单传，家长随时看 !","家长简单请假，老师随时审核。",
                             "家长简单请假，老师随时审核。","家长简单请假，老师随时审核。",
                             "选择缴费项目，网络支付，立即到账。","选择缴费项目，网络支付，立即到账。","上下班用微信打卡，gps精确定位。");
    var bg = "<?php echo HtmlHelper::assets('images/home/second/show/home.jpg')?>";

    function change(){
        $("#show-title").html(titles[count]);
        $("#show-content").html(contents[count]);
        switch (count){
            case 0:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/home.jpg')?>";
                break;
            case 1:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/notice.jpg')?>";
                break;
            case 2:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/shipu.jpg')?>";
                break;
            case 3:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/web.jpg')?>";
                break;
            case 4:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/albums.jpg')?>";
                break;
            case 5:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/album.jpg')?>";
                break;
            case 6:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/leave-1.jpg')?>";
                break;
            case 7:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/leave-2.jpg')?>";
                break;
            case 8:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/leave-3.png')?>";
                break;
            case 9:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/pay-1.jpg')?>";
                break;
            case 10:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/pay-2.jpg')?>";
                break;
            case 11:
                bg = "<?php echo HtmlHelper::assets('images/home/second/show/sign.jpg')?>";
                break;
        }
        $("#show-img-main").css("background-image","url("+bg+")");
    }
    function checkCount(){
        if(count >= 12){
            count = 0;
        }else if(count <= -1){
            count = 11
        }
    }

    $("#show-img-main").click(function(){
        change();
        count++;
        checkCount();
    });
    $(".left-a").click(function(){
        change();
        count--;
        checkCount();
    });
    $(".right-a").click(function(){
        change();
        count++;
        checkCount();
    });
</script>


<script src="http://cdn.bootcss.com/stellar.js/0.6.2/jquery.stellar.min.js"></script>