<script>
    $("#nva-right li").removeClass("active");
    $("#nva-right li:eq(0)").addClass("active");
</script>

<style type="text/css">
    .row{
        width: 100%;
        margin: 0 auto;
    }
    .index-top p{
        color:#666;
    }
    .group-img{
        margin-top: 60px;
    }
    .group-p-1{
        margin-top: 20px;
        font-size: 14px;
    }
    .group-p-2{
        font-size: 12px;
        color:#999;
    }
    #gao-ke-kao{
        margin-top: 45px;
    }
    #you-er-yuan p{
        margin-top: 10px;;
        font-size: 14px;
    }
    @media (min-width: 768px) {
        .group-p-2{
            margin: 0 auto;
            width: 50%;
        }
    }

    @media (max-width: 768px) {
        .group-img{
            margin-top: 20px;
        }
        #gao-ke-kao{
            margin-top: 10px;
        }
        h1{
            font-size: 22px;
        }
    }
</style>

<div id="home-main">

        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="index-top">
                    <h1>丰富的功能体验</h1><br>
                    <p>娃娃营提供了多种的功能帮助幼儿园便捷地实现基本的沟通与协同管理。九大功能帮助幼儿园快速高效地利用微信来管理幼儿园。
                    <br>微信每个人都会使用，永久免费的微信幼儿园管理系统，等您来体验！！</p>
                </div>

                <div class="row text-center group-img">

                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-1.png') ?>">
                        <p class="group-p-1">园所介绍</p>
                        <p class="group-p-2">微网站让传播无处不在！！家长只需打开微信就能访问幼儿园的微网站。</p>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-2.png') ?>"><br>
                        <p class="group-p-1">园所通知</p>
                        <p class="group-p-2">老师在后台发送公告消息，家长在手机微信上接收，更方便学校工作和宣传。</p>
                    </div>

                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-3.png') ?>">
                        <p class="group-p-1">宝宝相册</p>
                        <p class="group-p-2">把相册装进微信，老师简单传，家长随时看 !</p>
                    </div>
                </div>

                <div class="row text-center group-img">
                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-4.png') ?>">
                        <p class="group-p-1">食谱</p>
                        <p class="group-p-2">每天发布一次食谱，家长更关注宝宝的健康成长。</p>
                    </div>

                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-5.png') ?>">
                        <p class="group-p-1">缴学费</p>
                        <p class="group-p-2">家长用微信缴学费，立即到账，方便管理。</p>
                    </div>

                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-6.png') ?>">
                        <p class="group-p-1">学费报表</p>
                        <p class="group-p-2">网络缴费，报表呈现！！</p>
                    </div>


                </div>

                <div class="row text-center group-img">
                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-7.png') ?>">
                        <p class="group-p-1">老师打卡</p>
                        <p class="group-p-2">老师用微信打卡，月末自动生成报表。</p>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-8.png') ?>">
                        <p class="group-p-1">宝宝请假</p>
                        <p class="group-p-2">家长用微信请假，老师方便审核，园长统一管理。</p>
                    </div>
                    <div class="col-xs-4 col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-9.png') ?>">
                        <p class="group-p-1">资讯</p>
                        <p class="group-p-2">新闻资讯，生活常识，更多知识，一网打尽。</p>
                    </div>

                </div>

            </div>
        </div>

        <hr>

        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="index-top">
                    <h1>可靠的安全保障</h1><br>
                    <p>我们应用建立在微信的安全基础之上，腾讯及微信已建立的业界一流的安全保障体系、高可靠的系统实现机制，以及企业号完善的安全特性，
                        <br>可以为幼儿园的信息安全提供了全方位的安全保障。</p>
                </div>

                <div class="row text-center group-img">
                    <div class="col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-10.png') ?>">
                        <p class="group-p-1">一流的安全保障体系</p>
                        <p class="group-p-2">专业安全平台7*24小时监护闭环高效的反垃圾、反病毒处理机制领先的防黑客攻击技术与组织保障</p>
                    </div>
                    <div class="col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-11.png') ?>">
                        <p id="gao-ke-kao" class="group-p-1">高可靠的系统实现机制</p>
                        <p class="group-p-2">消息通讯全程加密传输企业关键信息安全存储端到端高可靠性服务处理微信安装包及网页防撰改技术</p>
                    </div>
                    <div class="col-sm-4">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-12.png') ?>">
                        <p class="group-p-1">完善的应用安全特性</p>
                        <p class="group-p-2">完善用户生命周期管理授权用户才可关注与使用分级分权限管理企业资源保密消息，有效确保消息的阅读范围</p>
                    </div>
                </div>

            </div>
        </div>

        <hr>

        <div class="row text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="index-top">
                    <h1>他们都在用</h1><br>
                </div>

                <div id="you-er-yuan" class="row text-center">
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-lyl.png') ?>">
                        <p>蓝月亮幼儿园</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-aybb.png') ?>">
                        <p>爱婴贝贝幼儿园</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-15.png') ?>">
                        <p>峨音幼儿园</p>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo HtmlHelper::assets('images/home/img-16.png') ?>">
                        <p>托菲诺幼儿园</p>
                    </div>
                </div>
            </div>
        </div>


</div>
