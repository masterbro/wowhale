<!--<div class="visible-xs full-width">-->
<!--		<img src="--><?php //echo HtmlHelper::assets('images/banner_yellow.png') ?><!--" class="full-width" />-->
<!---->
<!--</div>-->
<div class="full-width home-banner" id="home-banner">

	<div class="container full-height">
		
        <div class="row">
        <div class="col-lg-5 col-md-6 col-md-push-6">
            <div class="home-login-wrap">
                <div class="home-login">
                    <div class="home-login-inner" id="home-login">
                        <h3>已入驻机构登录</h3>
                        <div class="form-group">
                            <input type="text" class="form-control hr-mi required" name="mobile" placeholder="手机号" value="" />
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control hr-mi required" name="password" placeholder="密码" value="" />
                        </div>
                        <div class="checkbox">
                            <a href="javascript:;" class="pull-right">忘记密码?</a>
                            <label>
                                <input type="checkbox" value="1" name="remember" checked="checked"> 自动登录
                            </label>
                        </div>
                        <div class="form-group" style="margin-bottom: 0">
    
                            <button class="btn btn-success btn-block ww-btn ww-login-sub" data-loading-text="登录中..." id="ww-login-sub">登&nbsp;&nbsp;&nbsp;&nbsp;录</button>
                        </div>
                    </div>
                </div>
            </div>
		</div>
        <div class="col-lg-7 col-md-6 col-md-pull-6 text-center">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
			<a href="<?php echo $this->createUrl('/joinUs') ?>" class="ww-join-btn-2">立即免费入驻</a>
        </div>
        </div>
	</div>
</div>
<!--<script>
	var setBanner = function() {
		var h = (document.documentElement.clientWidth / 1280) * 399;

		document.getElementById('home-banner').style.height = h + 'px';
	};
	setBanner();
</script>-->

<div class="full-width home-step2">
	<div class="container">
		<img src="<?php echo HtmlHelper::assets('images/step.png'); ?>" />
	</div>
</div>



<div class="full-width home-strength">
	<div class="container">
	<div class="row">
		<div class="col-md-4">
			<h3 class="title">我们帮助您</h3>
		</div>

		<div class="col-md-8">
			<div class="media">
				<div class="media-left">
					<a href="#">
						<img class="media-object" src="<?php echo HtmlHelper::assets('images/strength_1.png') ?>" alt="...">
					</a>
				</div>
				<div class="media-body">
					<h4 class="media-heading">品牌更出众</h4>
					<p>免费为机构建立专属H5主页，全方位展现机构风貌，7*24小时交易支持。让更多的儿童和家长了解您、认可您。</p>
				</div>
			</div>
			<div class="media">
				<div class="media-left">
					<a href="#">
						<img class="media-object" src="<?php echo HtmlHelper::assets('images/strength_2.png') ?>" alt="...">
					</a>
				</div>
				<div class="media-body">
					<h4 class="media-heading">报名更简单</h4>
					<p>通过H5页面，家长可以随时随地的报名参与您提供的服务</p>
				</div>
			</div>
			<div class="media last">
				<div class="media-left">
					<a href="#">
						<img class="media-object" src="<?php echo HtmlHelper::assets('images/strength_3.png') ?>" alt="...">
					</a>
				</div>
				<div class="media-body">
					<h4 class="media-heading">管理更高效</h4>
					<p>为机构提供更高效的后台管理系统，您可以随时随地管理您订单以及资金</p>
				</div>
			</div>
		</div>
		</div>

	</div>
</div>

<div class="full-width home-friend">
<div class="container ">
	<h2 class="title">TA们已入驻</h2>

	<div class="logos">
		<a href="http://www.tonghuaart.com/ " target="_blank">
			<img src="<?php echo HtmlHelper::assets('images/xjer.jpg') ?>" width="200" />
		</a>
		<a href="http://www.adi-sports.com/" target="_blank">
			<img src="<?php echo HtmlHelper::assets('images/adi.jpg') ?>" width="200" height="50" />
		</a>
	</div>
	<div class="clearfix text-center btn-wrap">
		<a href="<?php echo $this->createUrl('/joinUs') ?>" class="ww-join-btn">立即免费入驻</a>
	</div>

</div>
</div>