<?php $join_url = $this->createUrl('/wechatQyApp/oauth/step1'); ?>

<script>
	$("#nva-right li").removeClass("active");
	$("#nva-right li:eq(1)").addClass("active");
</script>

<div class="full-width school-home-grey school-home-icons">
	<div class="container">
		<div class="row">
			<div class="col-md-2 col-xs-4 item">
				<a href="#school-0"><img src="<?php echo HtmlHelper::assets('images/school/home-icon1.png') ?>" /></a>
            	<h5>通讯录</h5>
			</div>
			<div class="col-md-2 col-xs-4 item">
				<a href="#school-1"><img src="<?php echo HtmlHelper::assets('images/school/home-icon2.png') ?>" /></a>
				<h5>园所介绍</h5>
			</div>
			<div class="col-md-2 col-xs-4 item">
				<a href="#school-2"><img src="<?php echo HtmlHelper::assets('images/school/home-icon3.png') ?>" /></a>
				<h5>园所通知</h5>
			</div>
			<div class="col-md-2 col-xs-4 item">
				<a href="#school-3"><img src="<?php echo HtmlHelper::assets('images/school/home-icon4.png') ?>" /></a>
				<h5>缴学费</h5>
			</div>
            <div class="col-md-2 col-xs-4 item">
				<a href="#school-4"><img src="<?php echo HtmlHelper::assets('images/school/home-icon5.png') ?>" /></a>
				<h5>宝宝相册</h5>
			</div>
            <div class="col-md-2 col-xs-4 item">
				<a href="#school-5"><img src="<?php echo HtmlHelper::assets('images/school/home-icon6.png') ?>" /></a>
				<h5>老师打卡</h5>
			</div>        
		</div>
	</div>
</div>


<a name="school-2"></a>
<div class="full-width school-home-fff school-home-block">
	<div class="container">
		<div class="row">
			
           <div class="col-md-6 col-md-push-6 description ">
				<h2>一个<span class="orange smoove1" data-rotate-y="180deg" data-move-z="-100px" data-move-x="500px">微信</span>通通搞定</h2>
				<p class="smoove1" data-rotate-x="180deg" data-rotate-y="45deg" data-move-z="-500px">学校公告，家园沟通，只需要一个微信。</p>
			<!--<p class="smoove1" data-rotate-x="180deg" data-rotate-y="45deg" data-move-z="-500px">老师能容易的通知家长，家长也更方便与老师交流。</p>-->
                <p><a href="<?php echo $join_url ?>" class="school-join smoove1">免费开通</a></p>
			</div>
            
			<div class="col-md-6 col-md-pull-6 text-center">
            	<!--data-*是添加进入动画的效果的属性-->
				<img data-rotate-y="180deg" data-move-z="-200px" data-move-x="-300px" class="smoove1" src="<?php echo HtmlHelper::assets('images/school/home-img2.png') ?>" width="220px;" />
			</div>
            
		</div>
	</div>
</div>


<a name="school-1"></a>
<div class="full-width school-home-grey school-home-block">
	<div class="container">
		<div class="row">
             	
			<div class="col-md-4 col-md-offset-2 description">
            	<!--data-*是添加进入动画的效果的属性-->
				<h2><span class="orange smoove1" data-rotate-x="90deg" data-move-z="-500px" data-move-y="200px">微网站</span>让传播无处不在</h2>
				<p class="smoove1" data-rotate-x="90deg" data-move-z="-500px" data-move-y="200px">打开微信就能访问幼儿园的微网站。</p>
			</div>

			<div class="col-md-6 text-center">
				<img class="smoove1" data-rotate-x="90deg" data-move-z="-500px" data-move-y="200px" src="<?php echo HtmlHelper::assets('images/school/home-img1.png') ?>" width="220px;" />
			</div>
            
		</div>      
	</div>
</div>


<a name="school-0"></a>
<div class="full-width school-home-fff school-home-block">
	<div class="container">
		<div class="row">
            
             <div class="col-md-6 col-md-push-6 description">
				<h2><span class="orange smoove1">通讯录</span>不加好友也能建圈子</h2>
                <p><a href="<?php echo $join_url ?>" class="school-join smoove1">免费开通</a></p>
			</div>
            
			<div class="col-md-6 col-md-pull-6 text-center">
            	<!--data-*是添加进入动画的效果的属性-->
				<img class="smoove1" src="<?php echo HtmlHelper::assets('images/school/home-img0.png') ?>" width="220px;" />
			</div>
            
		</div>
	</div>
</div>

<a name="school-3"></a>
<div class="full-width school-home-grey school-home-block">
	<div class="container">
		<div class="row">
        	
			<div class="col-md-4 col-md-offset-2 description payment">
            <!--data-*是添加进入动画的效果的属性-->
				<h2>网络缴费，呈现<span class="orange smoove2" data-rotate-y="270deg" data-move-x="-150%">报表</span>呈现</h2>
				<p class="smoove2" data-rotate-y="270deg" data-move-x="-150%">线上线下支付，报表轻松合并。</p>
				<p class="smoove2" data-rotate-y="270deg" data-move-x="-150%">预算、台账，流水账，分分钟搞定！</p>
			</div>

			<div class="col-md-6 text-center">
				<img class="smoove2" data-rotate-y="270deg" data-move-x="150%" src="<?php echo HtmlHelper::assets('images/school/home-img5.png') ?>" width="220px;" />
			</div>
            
		</div>
	</div>
</div>

<a name="school-4"></a>
<div class="full-width school-home-fff school-home-block">
	<div class="container">
		<div class="row">
        
        	<div class="col-md-6 col-md-push-6 description">
				<h2>一个<span class="orange smoove2" data-move-y="200px" data-move-x="-200px">微相册</span>记录校园生活</h2>
				<p class="smoove2" data-move-y="200px" data-move-x="200px">把相册装进微信，老师简单传，家长随时看</p>
				<p><a href="<?php echo $join_url ?>" class="school-join smoove1">免费开通</a></p>
			</div>
        	
			<div class="col-md-6 col-md-pull-6 text-center">
            <!--data-*是添加进入动画的效果的属性-->
				<!--<img src="<?php echo HtmlHelper::assets('images/school/home-img4.png') ?>" width="180px;" />-->
                <img class="smoove2" data-move-x="-500px" data-rotate="90deg" src="<?php echo HtmlHelper::assets('images/school/home-img4-1.png') ?>" width="110px;" />
                <img class="smoove2" data-move-x="500px" data-rotate="-90deg" src="<?php echo HtmlHelper::assets('images/school/home-img4-2.png') ?>" width="110px;" />
			</div>          
            
		</div>
	</div>
</div>

<a name="school-5"></a>
<div class="full-width school-home-grey school-home-block">
	<div class="container">
		<div class="row">
        	
			<div class="col-md-4 col-md-offset-2 description">
            <!--data-*是添加进入动画的效果的属性-->
				<h2><span class="orange smoove2" data-move-y="200px" data-move-x="-100px">微助手</span>让校园管理更方便</h2>
				<p class="smoove2" data-move-y="200px" data-move-x="100px">日常签到，财务审批，专家共享，人才招聘，下载公共资料。</p>
			</div>

			<div class="col-md-6 text-center">
				<!--<img src="<?php echo HtmlHelper::assets('images/school/home-img3.png') ?>" width="180px;"  />-->
                <img class="smoove2" data-move-y="200px" data-move-x="-200px" src="<?php echo HtmlHelper::assets('images/school/home-img3-1.png') ?>" width="110px;"  />
                <img class="smoove2" data-move-y="200px" data-move-x="200px" src="<?php echo HtmlHelper::assets('images/school/home-img3-2.png') ?>" width="110px;"  />
			</div>
            
		</div>
	</div>
</div>


<div class="full-width school-home-fff school-home-block">
	<div class="container text-center">
		<img src="<?php echo HtmlHelper::assets('images/school/home-txt-orange.png') ?>" class="school-home-txt-img" />
		<br />
		<br />
		<p><a href="<?php echo $join_url ?>" class="school-join">免费开通</a></p>

	</div>
</div>


<!--滑动动画效果-->
<script src="http://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.smoove.js"></script>
<script>
	var mq = $(window).width();
	if(mq > 768) {
		// 浏览器宽度大于七百
		$('.smoove1').smoove({offset:'30%'});
		$('.smoove2').smoove({offset:'40%'});
	} else {
		//浏览器宽度小于七百
		$('.smoove1').smoove({offset:'16%'});
		$('.smoove2').smoove({offset:'16%'});
	}
</script>