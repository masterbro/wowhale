<style type="text/css">
    body{
        position: relative;
    }
    #main{
        margin-top:17px ;
    }
    #a{
        margin-bottom: 50px;
    }
    #b,#c,#d,#e{
        margin-bottom: 150px;
    }
    .row{
        margin: 0;
        margin-top: 20px;
    }
    .img-parent{
        text-align: center;
    }
    .img-parent img{
        width: 80%;
    }
    #pordAttr li{
        padding-left: 50px;
    }
    .col-sm-7 p{
        margin-top: 20px;
        line-height: 2em;
    }
    .img-parent{
        margin-top: 20px;
    }
    @media (max-width: 768px) {
        #a,#b,#c,#d,#e{
            margin-bottom: 50px;
        }
        .col-sm-7 p{
            margin-top: 10px;
            line-height: 2em;
        }
        .img-parent{
            margin-top: 10px;
        }
    }
</style>
<script>
    //bootstrap里的监听滚动条的方法
    $("body").scrollspy({ target: "#navbar-ex" });
    $('[data-spy="scroll"]').each(function () {
        var $spy = $(this).scrollspy('refresh')
    })
</script>
<div id="main">
    <div class="row">
        <div class="col-sm-3 hidden-xs">
            <div id="navbar-ex">
                <ul id="pordAttr" class="nav nav-pills nav-stacked" role="tablist">
                    <li role="presentation" class="active"><a href="#a">1.注册娃娃营</a></li>
                    <li role="presentation"><a href="#b">2.应用添加安装：</a></li>
                    <li role="presentation"><a href="#c">3.授权微信企业号登录</a></li>
                    <li role="presentation"><a href="#d">4.完成</a></li>
                    <li role="presentation"><a href="#e">5.导入通讯录</a></li>
                </ul>
            </div>
        </div>

        <div class="col-sm-7">
            <div>
                <div id="a"><h1>帮助</h1></div>
                <div>
                    <p><h4>1 注册娃娃营--&gt;2 应用添加安装--&gt;3 授权微信企业号登录--&gt;4 完成--&gt;5 导入通讯录</h4></p>
                    <p><h4>1.注册娃娃营：</h4></p>
                    <p>点击首页“免费开通”，填写相关信息，完成后点击“下一步”，上传好签字、盖章的相关证件，等待审核成功后，会发送短信到注册手机提醒注册成功的消息。然后返回首页登录，安装好相关应用就可以正常使用了。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-1.png') ?>">
                    </div>
                </div>

                <div id="b"></div>
                <div>
                    <p><h4>2.应用添加安装：</h4></p>
                    <p>点击“应用中心”，或者“我的应用”选择需要的应用。可单独安装，也可通过应用最下方“全部安装”进行操作。安装后进入微信授权企业号界面。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-3.png') ?>">
                    </div>
                </div>

                <div id="c"></div>
                <div>
                    <p><h4>3.授权微信企业号登录：</h4></p>
                    <p>用手机进行扫描二维码（请注意：必须使用企业号系统管理员的微信进行扫描）。扫描后进入输入企业号密码界面，输入确定即可。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-4.png') ?>">
                    </div>
                    <p>确定后会弹出“授权测试套件托管你的应用”界面 </p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-5.png') ?>">
                    </div>
                    <p>“选择套件应用”，点击“添加”可以选择多个应用。再“选择授权应用”选择“默认创建应用”。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-6.png') ?>">
                    </div>
                    <p>选择创建默认应用。授权应用和套件应用是一对一的。选择创建默认应用既可。<br>
                        添加完成后，点击下方的设置通讯录管理权限 按钮，添加安装应用的管理员权限。添加完成后点击授权托管即可。选择完成后需要设置通讯录管理权限。点击“授权托管”后就完成了应用的安装。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-7.png') ?>">
                    </div>
                </div>

                <div id="d"></div>
                <div>
                    <p><h4>4.完成：</h4></p>
                    <p>自动跳转到后台管理界面。此时所有安装操作全部完成。可正常对应用进行管理操作。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-8.png') ?>">
                    </div>
                    <p>（注：第一次使用如有应用程序在手机端没有显示，需要进入到微信企业号，添加可见范围。）添加可见范围：使用企业号管理员登录到微信企业号。例如：“<a href="https://qy.weixin.qq.com/" target="_blank">https://qy.weixin.qq.com/</a>”选择应用。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-9.png') ?>">
                    </div>
                    <p>点击可见范围后面的修改。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-10.png') ?>">
                    </div>
                    <p>出现选择范围。可选择部门也可选择成员。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-11.png') ?>">
                    </div>
                    <p>选择后点击确认即可。</p>
                </div>

                <div id="e"></div>
                <div>
                    <p><h4>5.导入通讯录</h4></p>
                    <p>A 点击左侧导航“通讯录”，点击“班级”，再“添加班级”，这样导入学生或者员工可以选择所在的班级或者负责的班级。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-12.png') ?>">
                    </div>
                    <p>b 选择“员工”添加员工，填写好信息后点击“保存”，点击“通讯录”返回通讯录界面。</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-13.png') ?>"><br>
                        <img src="<?php echo HtmlHelper::assets('images/help/img-14.png') ?>">
                    </div>
                    <p>C 导入学生</p>
                    <div class="img-parent">
                        <img src="<?php echo HtmlHelper::assets('images/help/img-15.png') ?>">
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>


<!--到指定位置固定侧边栏strat-->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script>
    $(function() {
        var elm = $('#pordAttr');
        var startWidth = $(elm).width();
        var startPos = $(elm).offset().top - 80;//获取元素在页面中距离屏幕顶部的距离
        $.event.add(window, "scroll", function() {
            var p = $(window).scrollTop();
            $(elm).css('width',startWidth+'px');
            $(elm).css('position',((p) > startPos) ? 'fixed' : 'static');
           $(elm).css('top',((p) > startPos) ? '70px' : '');
        });
    });
</script>
<!--到指定位置固定侧边栏end-->