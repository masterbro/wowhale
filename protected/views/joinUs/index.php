<div class="join-step full-width">

	<div class="container">
		<div class="row">
			<div class="col-md-4 <?php if($step == 1) echo 'active' ?>">
				<h3><span>1</span> 免费注册</h3>
				<div class="arrow hidden-xs"></div>
			</div>
			<div class="col-md-4 <?php if($step == 2) echo 'active' ?>">
				<h3><span>2</span>  在线签约</h3>
				<div class="arrow hidden-xs"></div>
			</div>
			<div class="col-md-4 <?php if($step == 3) echo 'active' ?>">
				<h3><span>3</span>  提交资料</h3>
				<div class="arrow hidden-xs"></div>
			</div>
		</div>
	</div>


</div>

<div class="container join-container">

	<div class="join-step-content <?php if($step == 2 || $step == 3) echo 'hidden' ?>" id="join-step-content-1">
		<div class="row join-form-wrap">
			<div class="col-md-3">
				<div class="text">
					<h3>创建机构账号</h3>
					<p>已有机构账号，请 <a href="<?php echo $this->createUrl('/home') ?>">登录</a></p>
				</div>

			</div>
			<div class="col-md-9">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label"><em class="text-danger">*</em>手机号：</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="mobile" />
						</div>
						<div class="col-sm-4"></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><em class="text-danger">*</em>验证码：</label>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-7">
									<input type="text" class="form-control" name="captcha" />
								</div>
								<div class="col-sm-5">
									<button class="btn ww-btn-blue btn-block" type="button" id="send-captcha">发送验证码</button>
								</div>
							</div>
						</div>
						<div class="col-sm-4"><p class="help-block"><a href="#" style="font-size: 12px;">没有收到验证码？</a></p></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><em class="text-danger">*</em>密码：</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="password" />
						</div>
						<div class="col-sm-4 help"><p class="help-block">密码长度为6到30位，数字、字母</p></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label"><em class="text-danger">*</em>确认密码：</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="password_confirm" />
						</div>
						<div class="col-sm-4"></div>
					</div>

					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-1">
						<button type="button" class="btn ww-btn-yellow btn-block submit" id="step1-submit">提交注册</button>
						</div>
					</div>
					<div class="form-group">
						<p class="help-block">点击"提交注册"，即表示您已阅读并同意 <a href="javascript:;" class="term-link" id="show-term">网站条款</a></p>
					</div>
				</div>

			</div>

		</div>
	</div>

	<div class="join-step-content <?php if($step != 2) echo 'hidden' ?>" id="join-step-content-2">
		<div class="join-step-protocol">
			<h2 class="title">《机构用户入驻协议》</h2>

			<div class="rich-text" style="overflow-y: scroll;">
                <b>欢迎机构用户入驻娃娃营！娃娃营网站由成都品众游科技有限公司负责运营，致力于打造人人乐用的课外活动服务平台，本站的宗旨是为孩子提供更为健康的童年。</b>
                <p>请机构用户在入驻娃娃营之前仔细阅读本协议。机构用户接收本协议点击“同意”按钮完成入驻签约，这表示机构用户与娃娃营已达成入驻协议并接受所有的入驻条款。</p>
                <p>一、定义</p>
                <p>机构用户是指合法成立并存续的具有法人主体资格的企业、非企业单位，或者个体教师工作室以团体名义入驻娃娃营的非法人单位。
                机构用户，以下亦称机构。</p>
                <p>娃娃营网站，以下简称娃娃营。</p>
                <p>二、合作模式及入驻流程</p>
                <p>1、机构用户通过注册并经审核入驻娃娃营后，将在娃娃营上拥有专属的网店，网店可以由机构下挂至自主注册的微信公众号，网店可展示其logo、基本信息、介绍、地址路线、课程、教师照片、教学视频等内容；机构认真维护微信公众号以及网店信息，通过给学生提供优秀师资、优质课程和良好的服务，创造更多的线上交易额。娃娃营通过网络推广积极提升机构的业界知名度、为机构扩大生源。娃娃营为机构网店提供技术服务。</p>
                <p>2、机构入驻应依照下列流程（详见娃娃营首页“机构平台入驻流程“）：</p>
                <p>注册帐号 在线签约 提交资料 进入审核 进入机构平台</p>
                <p>三、双方权利义务及法律责任</p>
                <p>1、机构应对自身提交及上传娃娃营的资料信息的真实性、合法性、有效性承担法律责任。</p>
                <p>2、机构应确保注册教师的教学水平达到同行业中等及中等以上的教学水准。</p>
                <p>3、机构应敦促注册教师及时维护其个人主页的活跃度，并确保更新内容的真实性、合法性、准确性及有效性。基于注册教师上传信息的虚假性或其他非法行为给娃娃营造成损害的，机构与注册教师应承担连带赔偿责任。</p>
                <p>4、学员与机构教师约课成功后，上课过程中发生的教师及学员的人身、财产损害事件，由相关责任方承担法律责任。</p>
                <p>7、机构及注册教师应妥善保管自己的帐号及密码，娃娃营也应采取适当的技术措施，共同维护机构及注册教师帐号的安全性。</p>
                <p>8、娃娃营受理学员对机构注册教师投诉事件的，机构应积极协助并妥善处理。</p>
                <p>9、娃娃营有权保留并合理使用机构及其教师注册及上传的所有信息。</p>
                <p>10、娃娃营谨慎使用用户的所有信息，非依法律规定及用户许可，不得向任何第三方透露用户信息。娃娃营对相关信息采用专业加密存储与传输方式，保障用户个人信息的安全。</p>
                <p>11、机构上传娃娃营的任何内容如涉嫌侵犯第三方合法权益的，娃娃营有权采取删除、屏蔽或断开链接等技术措施，机构须独立承担因侵权所产生法律责任。</p>
                <p>四、入驻费用</p>
                <p>娃娃营对入驻机构暂不收取任何费用。</p>
                <p>五、知识产权</p>
                <p>1、机构在娃娃营发布书稿、课件等信息或作品的，用户应独立享有相关著作权。如受到第三方的投诉或举报，机构应独立承担相关法律责任。</p>
                <p>2、娃娃营在服务中提供的内容（包括但不限于网页、文字、图片、音频、视频、图表等）的知识产权归娃娃营所有，非经娃娃营书面许可，机构不得任意使用或创造相关衍生作品。</p>
                <p>3、机构未经娃娃营书面许可不得擅自使用、不得以任何方式或理由对娃娃营文字及图形标识的任何部分进行使用、复制、修改、传播或与其他产品捆绑使用，不得以任何可能引起消费者混淆的方式或任何诋毁或诽谤娃娃营的方式用于任何商品或服务上。</p>
                <p>4、软件使用：娃娃营在此授予机构免费的、不可转让的、非独占的全球性个人许可，允许机构使用由娃娃营提供的、包含在服务中的软件。但是，用户不得复制、修改、发布、出售或出租娃娃营的服务软件或所含软件的任何部分，也不得进行反向工程或试图提取该软件的源代码。</p>
                <p>六、免责条款</p>
                <p>基于包括但不限于下列娃娃营不可控制的原因或并非娃娃营的过错所造成的损失，机构应自行承担或向有关责任方追偿：</p>
                <p>1、不可抗力事件导致的服务中断；</p>
                <p>2、由于受到计算机病毒、木马或其他恶意程序、黑客攻击的破坏等不可抗拒因素可能引起的信息丢失、泄漏等风险；</p>
                <p>3、用户的电脑软件、系统、硬件和通信线路出现故障或自身操作不当；</p>
                <p>4、由于网络信号不稳定等原因所引起的登录失败、资料同步不完整、页面打开速度慢等；</p>
                <p>5、机构发布的内容被他人转发、复制等传播可能带来的风险和责任；</p>
                <p>6、其他娃娃营无法控制的原因；</p>
                <p>7、如因系统维护或升级而需要暂停网络服务，娃娃营将事先在网站发布通知。</p>
                <p>七、法律适用与争议解决</p>
                <p>1、本协议的履行与解释均适用中华人民共和国法律。</p>
                <p>2、娃娃营与机构之间的任何争议应友好协商解决，协商不成的，任一方有权将争议提交至当地人民法院诉讼解决。</p>
                <p>八、本协议条款的修改权与可分性</p>
                <p>1、为更好地提供服务并符合相关监管政策，娃娃营有权及时修改本协议条款。请机构定期查阅本协议条款。娃娃营会在网页上公布这些条款的修改通知。</p>
                <p>2、本协议条款中任何一条被视为无效或因任何理由不可执行，不影响任何其余条款的有效性和可执行性。</p>
                <p>九、通知</p>
                <p>娃娃营将通过在网站上发布通知或其他方式与机构进行联系。机构同意用网站发布通知方式接收所有协议、通知、披露和其他信息。</p>
                <p>十、协议生效</p>
                <p>本协议自机构点击同意之日起生效。</p>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<button type="button" class="btn ww-btn-yellow btn-block submit" id="step2-submit" data-loading-text="提交数据中...">同意，下一步</button>
				</div>
			</div>
		</div>

	</div>


	<div class="join-step-content <?php if($step != 3) echo 'hidden' ?>" id="join-step-content-3">
		<div class="join-step-license">
			<div class="row">
				<div class="col-md-10 col-md-offset-2">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label"><em class="text-danger">*</em>商户名称：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="name" />
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><em class="text-danger">*</em>邮箱：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="email" />
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><em class="text-danger">*</em>联系人姓名：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="contacts" />
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><em class="text-danger">*</em>营业执照：</label>
							<div class="col-sm-8">
								<div class="row">
									<div class="col-sm-3 img">
										<img src="<?php echo HtmlHelper::assets('images/license.png') ?>" />
										<div id="ww-preview">
										</div>
									</div>
									<div class="col-sm-6 help readme img">
										<h5>参考示例</h5>
										<ul>
											<li>请上传清晰彩色扫描件或者数码照。</li>
											<li>照片内容真实有效，不得做任何修改。</li>
											<li>支持jpg、jpeg、png格式照片，大小不超过2M。</li>
										</ul>
										<a href="javascript:;" class="btn ww-btn-blue btn-block" style="width: 100px;" data-loading-text="上传中..." id="ww-add-img">上传图片</a>
										<input type="hidden" name="license" value="" />
									</div>
								</div>

							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><em class="text-danger">*</em>业务描述：</label>
							<div class="col-sm-4">
								<textarea class="form-control" name="description" rows="5"></textarea>
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">网址：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="website" />
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">公众号：</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="wechat_id" />
							</div>
							<div class="col-sm-4"><p class="help-block"></p></div>
						</div>

						<div class="form-group">
							<div class="col-sm-4 col-md-offset-2">
								<button class="btn btn-block ww-btn-yellow submit"  type="button" id="step3-submit" data-loading-text="提交资料中...">提交资料</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>

<input type="file" name="files" id="fileupload_input" style="visibility: hidden;" />

<!-- Modal -->
<div class="modal fade" id="modal-term" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">网站条款</h4>
			</div>
			<div class="modal-body text-center">
				<p>
					网站条款
				</p>

			</div>
		</div>
	</div>
</div>
