
	<div class="container" style="padding: 80px 0px;">

		<h3>入驻申请状态</h3>
		<p>当前状态：
			<?php
			if($apply->status == 1) {
				echo '<span class="label label-success">审核成功</span>';
			} else if($apply->status == 2) {
				echo '<span class="label label-danger">已拒绝</span>';
			} else {
				echo '<span class="label label-default">等待审核</span>';
			}
			?>
		</p>
		<?php if($apply->status == 2) :?>
		<p>拒绝理由：<?php echo $apply->reason; ?></p>
		<?php endif;?>
		<p>商户名称：<?php echo $apply->name; ?></p>
		<p>联系人：<?php echo $apply->contacts; ?></p>
		<p>联系电话：<?php echo $apply->tel; ?></p>
		<p>邮箱：<?php echo $apply->email; ?></p>
		<p>营业执照：<img src="<?php echo HtmlHelper::image($apply->license); ?>" width="300px" /></p>
		<p>业务描述：<?php echo $apply->description; ?></p>
		<p>网址：<?php echo $apply->website; ?></p>
		<p>公众号：<?php echo $apply->wechat_id; ?></p>
	</div>