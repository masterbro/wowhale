<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<meta charset="utf-8">
	<meta name="renderer" content="webkit">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<title>娃娃营</title>
	<meta name="keywords" content="娃娃营 幼儿园云服务的缔造者" />
	<meta name="description" content="娃娃营 幼儿园云服务的缔造者" />

	<!-- Bootstrap -->
	<?php $assets_version = Yii::app()->params['assets_version'];?>
	<link rel='stylesheet' id='open-sans-css'  href='//fonts.useso.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&subset=latin%2Clatin-ext&ver=4.2.3' type='text/css' media='all' />

	<link href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	   
	<script>
		BASE_URL = '<?php echo Yii::app()->baseUrl  ?>';
	</script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.1.9.1.min.js"></script>   
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="any" mask="" href="/favicon.ico">

	<link href="<?php echo Yii::app()->baseUrl ?>/assets/css/web.css?v=<?php echo $assets_version ?>" rel="stylesheet">

	<style type="text/css">
		#home-banner-content p{
			margin-top: 100px;
		}
		.row{
			width: 100%;
		}
	</style>
</head>
<?php $app = Yii::app(); ?>
<body class="<?php echo 'body-'.$app->controller->id ?>">
<a id="login"></a>
<?php if($app->controller->id != 'joinUs' && $app->controller->action->id != 'merchant'):?>
	<div class="navbar-school-home full-width">

		<nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
			<div class="container">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?php echo $this->createUrl('/home') ?>"><img id="school-logn" src="<?php echo HtmlHelper::assets('images/home/logo.png') ?>" /></a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav"></ul>

					<ul id="nva-right" class="nav navbar-nav navbar-right">
						<li class="<?php if($this->id == 'home') echo 'active'?>"><a href="<?php echo $this->createUrl('/home') ?>">首页</a></li>
						<li><a href="<?php echo $this->createUrl('/page?key=page_joinus') ?>">加入我们</a></li>
						<li class="<?php if($this->id == 'help') echo 'active'?>"><a href="<?php echo $this->createUrl('/help') ?>">帮助</a></li>
						<li class="<?php if($this->id == 'page' && $_GET['key'] == 'page_about') echo 'active'?>"><a href="<?php echo $this->createUrl('/page?key=page_about') ?>">关于我们</a></li>


                        <?php $user_name = Yii::app()->user->getState('username'); ?>
                        <?php if($user_name):?>
                        <li><a href="<?php echo $this->createUrl('/manage/') ?>">管理 </a></li>
                        <?php else:?>
							<li class="hidden-xs"><a style="margin: 0;padding: 0;padding-top: 10px;margin-left:10px;" href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>"><button class="btn btn-success">注册</button></a></li>
							<li class="hidden-xs"><a style="margin: 0;padding: 0;padding-top: 10px;margin-left:10px; " data-toggle="modal" data-target="#myModal"><button class="btn btn-primary">登录</button></a></li>
							<li class="visible-xs"><a data-toggle="modal" data-target="#myModal">登录 </a></li>
							<li class="visible-xs"><a href="<?php echo $this->createUrl('/wechatQyApp/oauth/step1') ?>">注册</a></li>
                        <?php endif;?>
					</ul>
				</div>
			</div>
		</nav>
        <!--添加的modal start-->
        <div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" 
					aria-labelledby="myModalLabel" aria-hidden="true" >
           <div class="modal-dialog modal-sm">
              <div class="modal-content">            
                 <!--<form>-->
                 <div class="modal-body">                  
                     <div class="home-login-inner school-home-login-inner" id="home-login">
                        <center><h3>已入驻学校登录</h3></center>
					    <div style="margin-top:50px;"></div>
                        <div class="form-group">
                            <input type="text" class="form-control hr-mi required" name="mobile" placeholder="手机号" value="">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control hr-mi required" name="password" placeholder="密码" value="">
                        </div>
                        <div class="checkbox">
                            <a href="javascript:;" class="pull-right">忘记密码?</a>
                            <label>
                                <input type="checkbox" value="1" name="remember" checked="checked"> 自动登录
                            </label>
                        </div>
                        <div class="form-group" style="margin-bottom: 0">
    
                            <button class="btn btn-success btn-block ww-btn ww-login-sub" data-loading-text="登录中..." id="ww-login-sub">登&nbsp;&nbsp;&nbsp;&nbsp;录</button>
                        </div>
                      </div>                                                 
                 </div>
                 <!--</form> -->
              </div><!-- /.modal-content -->
		   </div><!-- /.modal -->
            
		</div>
		<!--添加的modal end-->
	</div>


<?php endif;?>

<?php if($app->controller->id != 'joinUs' && $app->controller->action->id != 'merchant'):?>
<div style="height: 52px;"></div>
<?php endif;?>

<?php echo $content; ?>

<hr>
<div class="row hidden-xs">
	<div class="col-sm-8 col-sm-offset-2">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<img src="<?php echo HtmlHelper::assets('images/home/logo2.png') ?>">
			</div>
			<div class="col-sm-2" style="font-size: 14px;">
				<div style="text-align: left;" >
					<h4>关于我们</h4>
					<a href="<?php echo $this->createUrl('/page?key=page_policy') ?>">隐私声明</a><br>
					<a href="<?php echo $this->createUrl('/page?key=page_joinus') ?>">加入我们</a><br>
					<a href="<?php echo $this->createUrl('/home/merchant') ?>">商家入口</a>
				</div>
			</div>
			<div class="col-sm-4" style="font-size: 14px;">
				<h4>联系我们</h4>
				<p>电话：158 8220 6050<br>
					邮箱：service@wwcamp.com<br>
					地址：成都市高新区国际节能大厦A-315<br>
					渠道合作：158 8220 6050</p>
			</div>
		</div>
	</div>
</div>

<div class="web-foot text-center">
	<p>Copyright &copy; <?php echo date('Y'); ?> WoWhale  蜀ICP备15013283号</p>
</div>

</body>

<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/web.js"></script>
<?php if($app->controller->id == 'joinUs' ||$app->controller->id=='oauth' ):?>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.ui.widget.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.fileupload.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/join_us.js"></script>
<?php endif;?>

<?php echo $this->renderPartial('//layouts/_baidu'); ?>
</html>