<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>娃娃营</title>
    <meta name="keywords" content="WoWhale 儿童课外活动第一品牌" />
    <meta name="description" content="WoWhale 儿童课外活动第一品牌" />

    <!-- Bootstrap -->
    <?php $assets_version = Yii::app()->params['assets_version'];?>
    <link rel='stylesheet' id='open-sans-css'  href='//fonts.useso.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&subset=latin%2Clatin-ext&ver=4.2.3' type='text/css' media='all' />

    <link href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->baseUrl ?>/assets/css/web.css?v=<?php echo $assets_version ?>" rel="stylesheet">

    <script>
        BASE_URL = '<?php echo Yii::app()->baseUrl  ?>';
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.1.9.1.min.js"></script>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="any" mask="" href="/favicon.ico">

</head>
<?php $app = Yii::app(); ?>
<body class="<?php echo 'body-'.$app->controller->id ?>">


    <?php echo $content; ?>



    <div class="full-width site-copyright">
        <div class="container web-foot">
            <p>Copyright &copy; <?php echo date('Y'); ?> WoWhale  蜀ICP备15013283号</p>
        </div>
    </div>

</body>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/web.js"></script>
<?php if($app->controller->id == 'joinUs' ||$app->controller->id=='oauth'):?>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.fileupload.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/join_us.js"></script>
<?php endif;?>
<?php echo $this->renderPartial('//layouts/_baidu'); ?>

</html>