<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>娃娃营</title>
    <meta name="keywords" content="WoWhale 儿童课外活动第一品牌" />
    <meta name="description" content="WoWhale 儿童课外活动第一品牌" />

    <!-- Bootstrap -->
    <?php $assets_version = Yii::app()->params['assets_version'];?>
    <link rel='stylesheet' id='open-sans-css'  href='//fonts.useso.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&subset=latin%2Clatin-ext&ver=4.2.3' type='text/css' media='all' />

    <link href="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <script>
        BASE_URL = '<?php echo Yii::app()->baseUrl  ?>';
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.1.9.1.min.js"></script>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" sizes="any" mask="" href="/favicon.ico">

    <link href="<?php echo Yii::app()->baseUrl ?>/assets/css/web.css?v=<?php echo $assets_version ?>" rel="stylesheet">

    <style type="text/css">
        .row{
            margin-left:0;
            margin-right:0;
        }
        #foot-info{
            font-size: 14px;
        }
        #navbar{
            margin-right: 60px;
        }
        @media (max-width: 768px) {
            #navbar{
                margin-right:0;
            }
            .navbar-toggle{
                margin-right:40px;
            }
            .container{
                padding-right: 0;
            }
        }
    </style>

</head>
<?php $app = Yii::app(); ?>
<body class="<?php echo 'body-'.$app->controller->id ?>">
<?php if($app->controller->id != 'joinUs' && $app->controller->action->id != 'merchant'):?>
    <div class="navbar-school-home full-width">
        <nav class="navbar navbar-inverse navbar-static-top navbar-fixed-top">
            <div class="container">


                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $this->createUrl('/home') ?>"><img src="<?php echo HtmlHelper::assets('images/home/logo.png') ?>" /></a>
                </div>


                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">

                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="<?php if($this->id == 'home') echo 'active'?>"><a href="<?php echo $this->createUrl('/home') ?>">首页</a></li>
                        <li><a href="<?php echo $this->createUrl('/home/product') ?>">产品介绍</a></li>
                        <li><a href="<?php echo $this->createUrl('/page?key=page_joinus') ?>">加入我们</a></li>
                        <li><a href="<?php echo $this->createUrl('/help') ?>">帮助</a></li>
                        <li class="<?php if($this->id == 'page' && $_GET['key'] == 'page_about') echo 'active'?>"><a href="<?php echo $this->createUrl('/page?key=page_about') ?>">关于我们</a></li>

                        <?php $user_name = Yii::app()->user->getState('username'); ?>
                        <?php if($user_name):?>
                            <li><a href="<?php echo $this->createUrl('/manage/') ?>">管理 </a></li>
                        <?php else:?>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <?php endif;?>
    <?php echo $content; ?>

    <hr>
    <div class="row hidden-xs">
        <div class="col-sm-8 col-sm-offset-2">
            <div id="foot-info" class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <img src="<?php echo HtmlHelper::assets('images/home/logo2.png') ?>">
                </div>
                <div class="col-sm-2">
                    <div style="text-align: left;">
                        <h4>关于我们</h4>
                        <a href="<?php echo $this->createUrl('/page?key=page_policy') ?>">隐私声明</a><br>
                        <a href="<?php echo $this->createUrl('/page?key=page_joinus') ?>">加入我们</a><br>
                        <a href="<?php echo $this->createUrl('/home/merchant') ?>">商家入口</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h4>联系我们</h4>
                    <p>电话：158 8220 6050<br>
                        邮箱：service@wwcamp.com<br>
                        地址：成都市高新区国际节能大厦A-315<br>
                        渠道合作：158 8220 6050</p>
                </div>
            </div>
        </div>
    </div>

    <div class="web-foot text-center">
        <p>Copyright &copy; <?php echo date('Y'); ?> WoWhale  蜀ICP备15013283号</p>
    </div>

</body>


<script src="<?php echo Yii::app()->baseUrl ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl ?>/assets/js/web.js"></script>
<?php if($app->controller->id == 'joinUs' ||$app->controller->id=='oauth' ):?>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/jquery_upload/jquery.fileupload.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->baseUrl ?>/assets/js/join_us.js"></script>
<?php endif;?>
<?php echo $this->renderPartial('//layouts/_baidu'); ?>

</html>