<?php
class TestController extends ApplicationController{
    public function actionTest(){
        Yii::import('application.extensions.sms.*');
        $phones = array(
            /*'13882280611',*/
            '13350894955',
            '18980096039',
            '18681347923',
            '18280285627',
            '15008483414',
            '18628979774',
            '13436034071',
            '15882055150',
            '18380232293',
            '18140009855',
            '18930941610',
            '15882004542',
            '13880285045',
            '15882206050',
            '13408473709'
        );
        /*foreach($phones as $k=>$v){
            SmsService::send($v, SmsContent::SCHOOL_AFTER_REGISTER, true);
        }*/
        /*foreach($phones as $k=>$v){
            try{
                    SmsService::send($v, "尊敬的老师您好：
    非常感谢您对腾讯企业号产品以及娃娃营校园云服务的支持，请访问www.wwcamp.com 点击【免费开通】 提交学校的基础材料，我们将在1-3个工作日内协助贵校开通腾讯企业号服务。 开通后我们的技术人员会协助贵校理解并使用娃娃营校园云服务。", true);

                //SmsService::send($tel, SmsContent::SCHOOL_AFTER_REGISTER, true);


            }
            catch(Exception $e){
                var_dump($e->getMessage());
            }
        }*/
        /*if($_GET['type']==1){
            var_dump(SmsService::send('13408473709', SmsContent::SCHOOL_CONTACTS_LOGIN, true));
        }
        else if($_GET['type']==2){
            var_dump(SmsService::send('13408473709', SmsContent::MERCHANT_REQUEST, true));
        }
        else{
            var_dump(SmsService::send('18981933175', SmsContent::SCHOOL_AFTER_REGISTER, true));
        }*/
    }
    public function actionGetImg(){
        set_time_limit(-1);
        $ablums = Albums::model()->findAll();
        foreach($ablums as $k=>$v){
            $url = HtmlHelper::wx_image($v->pic_url);
            file_get_contents($url);
        }
    }

    public function  actionTestChat(){
        $schools = WechatConfig::model()->findAllByAttributes(array('ww_suite_id'=>3));
        foreach($schools as $school){
            $grades = Grade::model()->findAllByAttributes(array('partner_id'=>$school->partner_id));
            $school->getToken();
            $app = new WxQyMsg($school);
            $partner = Partner::model()->findByPk($school->partner_id);
            echo '<h3>'.$partner->name.'</h3>';
            foreach($grades as $grade){
                $grade_name = $grade->name;
                $chat = $app->getChat('c'.$grade->id);
                $owner = ($chat->owner)?$chat->owner:'';
                $userlist = ($chat->chat_info)?$chat->chat_info->userlist:array();
                $agent_id = $school->appIdsObj->notice;
                $added_uid = array();
                $sql = "SELECT contact.id as id FROM {{app_subscribe}} t LEFT JOIN {{contacts}} contact ON contact.id=t.contacts_id where t.partner_id={$school->partner_id} AND t.app_id={$agent_id} AND t.subscribe=1 AND contact.grade_id={$grade->id} AND (contact.`leader` !=1 OR  contact.`leader` IS NULL)" ;
                $teachers = Yii::app()->db->createCommand($sql)->queryAll();

                foreach($teachers as $kv=>$vv){
                    $added_uid[] = 'sh'.$vv['id'];
                }
                $owner = $added_uid[0];
                $sql = "SELECT contact.id as id FROM {{student}} student LEFT JOIN {{student_parent}} parent ON parent.student_id = student.id LEFT JOIN {{contacts}} contact ON contact.id=parent.parent_id  LEFT JOIN {{app_subscribe}}  subscribe ON contact.id=subscribe.contacts_id where subscribe.partner_id={$school->partner_id} AND subscribe.app_id={$agent_id} AND subscribe.subscribe=1 AND student.grade_id={$grade->id}" ;
                $students = Yii::app()->db->createCommand($sql)->queryAll();

                foreach($students as $kv=>$vv){
                    $added_uid[] = 'sh'.$vv['id'];
                }

                sort($userlist);
                sort($added_uid);
                $diffs = array_diff($added_uid,$userlist);
                sort($diffs);

                echo '<h3>'.$grade_name.'</h3>';
                echo '<h3>管理员'.$owner.'</h3>';
                echo '<div><div>群组人员</div><div>'.implode(',',$userlist).'</div></div>';
                echo '<div><div>应有人员</div><div>'.implode(',',$added_uid).'</div></div>';
                if(!empty($diffs)){
                    echo '<div><div>应有未加</div><div>'.implode(',',$diffs).'</div></div>';
                }
            }

        }
    }
}