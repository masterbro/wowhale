<?php
class QrCodeController extends CController {


	public function actionIndex() {
		Yii::import('ext.qrcode.QRCode');
		$data = $_GET['data'] ? urldecode($_GET['data']) : FALSE;
		if(!$data) die();
		$size = $_GET['size']?(int) $_GET['size']:null ;
		$code=new QRCode($data);
		if($size){
			$code->module_size=$size;
		}
		header('Content-type:image/png');
		$code->create();
		exit;
	}
}