<?php
class UploadController extends ApplicationController {


	public function actionIndex() {
		try{
			$path = "tmp";
			if($_GET['type'] == 'school') {
				$img = UploadHelper::image('', 'files', Partner::$carousel_size, $path, true);
			} else {
				$img = UploadHelper::image('', 'files', array(), $path);
			}


			$error = '';
		} catch(UploadException $e) {
			$error = $e->getMessage();
		} catch(CException $e) {
			$error = $e->getMessage();
		}
		Yii::app()->detachEventHandler('onEndRequest',Yii::app()->getEventHandlers('onEndRequest')->itemAt(0));
		JsonHelper::show(compact('error','img'));
	}
}