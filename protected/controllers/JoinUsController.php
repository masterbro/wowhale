<?php
class JoinUsController extends ApplicationController {

    public function actionIndex(){
	    $user = $this->getUser();
//	    get_browser()

	    $step = $user ? $user->join_step : 1;

	    if($step > 3)
		    $this->redirect('/joinUs/status');

	    $this->render('index', array(
		    'step' => $step,
	    ));
    }


	public function actionStep1() {
		$mobile = trim($_POST['mobile']);
		if(!CommonHelper::validMobile($mobile))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号错误',
			));

		if(!$_POST['captcha'])
			JsonHelper::show(array(
				'error' => true,
				'msg' => '验证码错误',
			));
//
//		if($_POST['password'] != $_POST['password_confirm'])
//			JsonHelper::show(array(
//				'error' => true,
//				'msg' => '两次密码输入不一至',
//			));


		if(Manager::model()->findByAttributes(array('username' => $mobile)))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号已经被注册',
			));



		Yii::import('application.extensions.sms.*');
		try {
			$sms_captcha = new SmsCaptcha('merchant_request_' . $mobile);
			$sms_captcha->valid($_POST['captcha']);
		} catch(Exception $e) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => $e->getMessage()
			));
		}

		$model = new Manager();
		$model->username = $mobile;
		$model->password = $model->hashPassword($_POST['password']);
		$model->tel = $mobile;
		$model->status = Manager::ST_PENDING;
		$model->type = Manager::TYPE_PARTNER;
		$model->join_step = 2;
		$model->save();

		Yii::app()->session['join_user'] = $model->id;


		JsonHelper::show(array(
			'error' => false,
			'msg' => '注册成功',
		));
	}

	public function actionStep2() {

		if($_POST['agree']) {
			$user = $this->getUser();
			if(!$user)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名不存在',
				));

			$apply = PartnerApply::model()->find("manager_id = {$user->id}");
			if($apply)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '请不要重复申请',
				));

			$user->join_step = 3;
			$user->save();


			JsonHelper::show(array(
				'error' => false,
				'msg' => 'success',
			));
		}
	}

	public function actionStep3() {
		$name = trim($_POST['name']);
		if($name && $_POST['email'] && $_POST['contacts'] && $_POST['license']) {

			$user = $this->getUser();
			if(!$user)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '用户名不存在',
				));

			$apply = PartnerApply::model()->find("manager_id = {$user->id}");
			if($apply)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '请不要重复申请',
				));

			if(PartnerApply::model()->findByAttributes(array('name' =>$name)))
				JsonHelper::show(array(
					'error' => true,
					'msg' => '商家 '. $name. ' 已存在',
				));

			if(Partner::model()->findByAttributes(array('name' => $name)))
				JsonHelper::show(array(
					'error' => true,
					'msg' => '商家 '. $name. ' 已存在',
				));

			$license = UploadHelper::moveTmp($_POST['license'], 'license');
			if(!$license)
				JsonHelper::show(array(
					'error' => true,
					'msg' => '营业执照不能为空',
				));


			$model = new PartnerApply();
			$model->name = $_POST['name'];
			$model->email = $_POST['email'];
			$model->tel = $user->username;
			$model->license = $license;
			$model->manager_id = $user->id;
			$model->contacts = $_POST['contacts'];
			$model->description = $_POST['description'];

			if($_POST['website'])
				$model->website = $_POST['website'];

			if($_POST['wechat_id'])
				$model->wechat_id = $_POST['wechat_id'];

			$model->save();

			$user->join_step = 4;
			$user->save();

			JsonHelper::show(array(
				'error' => false,
				'msg' => '申请成功，请等待审核',
			));
		}
	}


	public function actionStatus() {

		$user = $this->getUser();
		if(!$user)
			throw new CHttpException(404);

		$apply = PartnerApply::model()->find("manager_id = {$user->id}");
		if(!$apply)
			throw new CHttpException(404);

		$this->render('status', array(
			'apply' => $apply,
		));
	}



	public function actionCaptcha() {
		$mobile = trim($_POST['mobile']);
		if(!CommonHelper::validMobile($mobile))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号错误',
			));

		if(Manager::model()->findByAttributes(array('username' => $mobile)))
			JsonHelper::show(array(
				'error' => true,
				'msg' => '手机号已经被注册',
			));

		Yii::import('application.extensions.sms.*');

		try {
			$sms_captcha = new SmsCaptcha('merchant_request_' . $mobile);
			$sms_captcha->send($mobile, SmsContent::MERCHANT_REQUEST, 1);
		}catch (SmsCaptchaException $e) {
			JsonHelper::show(array(
				'error' => true,
				'msg' => $e->getMessage()
			));
		}

		JsonHelper::show(array(
			'error' => false,
			'msg' => '发送成功',
		));
	}

	private function getUser() {
		$join_uid = Yii::app()->session['join_user'];
		if($join_uid) {
			$user = Manager::model()->findByPk($join_uid);
		}

		return $user ? $user : false;
	}
}