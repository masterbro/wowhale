<?php
class SessionController extends ApplicationController {

	public function actionLogin() {
			if($_POST['username'] && $_POST['password']) {


			$user = Manager::model()->find(array(
				'condition' => 'username = :username',
				'params' => array(':username' => $_POST['username'])
			));
			if(!$user || $user->password != $user->hashPassword($_POST['password'])) {
				JsonHelper::show(array(
					'error' => 1,
					'msg' => '登录失败，用户名或者密码错误！',
				));
			}

			if($user->type != Manager::TYPE_PARTNER)
				JsonHelper::show(array(
					'error' => 1,
					'msg' => '登录失败，用户名或者密码错误1！',
				));

			if($user->status == Manager::ST_PENDING) {
				$login_success = true;
                //var_dump($user->type);
                $apply = PartnerApply::model()->findByAttributes(array('manager_id' =>$user->id));
                if($apply&&$apply->type==Manager::TYPE_SCHOOL){
                    Yii::app()->session['join_school_user'] = $user->id;
                    $redirect = $this->createUrl('/wechatQyApp/oauth/step1Two#apply');
                }
                else{
                    Yii::app()->session['join_user'] = $user->id;
                    $redirect = $this->createUrl('/joinUs/status');
                }


			} else {
				$identity = new UserIdentity($_POST['username'] , $_POST['password']);
				$login_success = $identity->authenticate();
				if($login_success) {
					Yii::app()->user->login($identity);
					$redirect = $this->createUrl('/manage');
				}
			}

			if($login_success) {
				JsonHelper::show(array(
					'error' => 0,
					'msg' => '登录成功，欢迎回来！',
					'redirect' => $redirect
				));
			} else {
				JsonHelper::show(array(
					'error' => 1,
					'msg' => '登录失败，用户名或者密码错误！',
				));
			}
		}
	}


	public function actionResetPwd() {

		$this->render('resetPwd');
	}
}