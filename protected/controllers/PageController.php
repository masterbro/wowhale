<?php
class PageController extends ApplicationController {


    public function actionIndex(){

	    $key = $_GET['key'];

	    $page = Page::model()->find('`key` = :key', array(':key' => $key));
	    if(!$page)
		    throw new CHttpException(404);

        /*if($key=='page_contact_help'){
            $this->layout = 'application_without_top';
        }
        else{
            $this->layout = 'application_registered_top';
        }*/
	    $this->render('index', array(
		    'page' => $page,
		    'key' => $key,
	    ));
    }
}